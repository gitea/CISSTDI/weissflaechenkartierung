# Weißflächenkartierung (WFK)

**This plugin is currently only available in German. The plugin was tested on Windows 10 and macOS 12 with the following QGIS version: QGIS version 3.28.7-Firenze.**

If you have any questions, please contact us via [e-mail](mailto:forschung@ciss.de) or visit our [homepage](https://www.ciss.de).


## Abstract

**English:**

This plugin helps project planner to find potential areas for a wind plant. It provides a simple configurable approach to find potential areas by reviewing all limiting factors on a map. After the selection of a project area all of the following steps are carried out automatically: Loading of the relevant layers, buffering according to adjustable distance rules, and "punching out"/subtracting the buffered areas from the project area. The result is a map that shows the remaining potential areas immediately. For easy ordering of owner information of the remaining area, the plugin provides a simple export function to save the parcel numbers.
In addition, the plugin offers an easy integration of your exclusive special knowledge. Thus, on the one hand side you can easily monitor your knowledge on a map, and on the other hand side you can blacklist properties where you already know that the owner is against your project.
Furthermore, the tool provides an initial assessment of the remaining sites based on their distance to relevant infrastructure such as transformer stations.

Take a look at our [webinar](https://www.ciss.de/datenveredelung/werkzeuge/#wfk) (only in german) and our [presentation](https://pretalx.com/fossgis2022/talk/PHEDNN/) at FOSSGIS22.

**German:**

Das QGIS-Plug-in "WFK" von CISS TDI ist nach Installation über die reguläre QGIS-Werkzeugleiste aufrufbar und kann per einfachem Klick gestartet werden. Nach Auswahl eines Projektgebietes für welches die Weißflächenkartierung durchgeführt werden soll, werden alle folgenden Arbeitsschritte nach einmalig festgelegtem Ablaufplan automatisiert abgearbeitet: Laden der relevanten Layer, Pufferbildung entsprechend eingestellter Abstandsregeln, „Ausstanzen“/Subtrahieren der gepufferten Ausschluss-Flächen vom Projektgebiet. Das Ergebnis ist eine Kartendarstellung, welche auf einen Blick mögliche Weißflächen aufzeigt, die wiederum die Grundlage für weitere Planungen und Analysen bilden. Zur einfachen Bestellung von Eigentümerinformationen der relevanten Weißflächen bietet das Plugin eine einfache Export-Funktion von Flurstücksnummern und Umringen. Zudem bietet das Plugin eine einfach Integration Ihres exklusiven Spezialwissens, das Sie zu bereits betrachteten Grundstücken gesammelt haben. So können Sie beispielsweise bestimmte Grundstücke von der Weißflächenkartierung ausschließen und halten durch die Visualisierung und Direktanzeige von QGIS auch über dieses Spezialwissen stets den Überblick. Eine erste Bewertung der gefundenen Weißflächen anhand eines bestimmten Kriteriums (z.B. Abstand zum nächsten Umspannwerk, ....) ist ebenfalls möglich, um so eine weitere Entscheidungsebene zu erzeugen.

Mehr Informationen in dem zugehörigen [Webinar](https://www.ciss.de/datenveredelung/werkzeuge/#wfk) und unserer [Präsentation](https://pretalx.com/fossgis2022/talk/PHEDNN/) auf der FOSSGIS22.

## Installation

**English:**

The plugin can be downloaded from the [Code-Repository](https://git.osgeo.org/gitea/CISSTDI/weissflaechenkartierung) or installed directly via the [QGIS plugin administration](https://docs.qgis.org/3.22/en/docs/user_manual/plugins/plugins.html).

**German:**

Das Plguin kann über das [Code-Repository](https://git.osgeo.org/gitea/CISSTDI/weissflaechenkartierung) heruntergeladen oder über die [Pluginverwaltung](https://docs.qgis.org/3.22/de/docs/user_manual/plugins/plugins.html) von QGIS direkt installiert werden. Ein [Demoprojekt](https://git.osgeo.org/gitea/CISSTDI/weissflaechenkartierung/archive/master.zip) ist ebenfalls im Code-Repository zu finden.

### Installation über das Coderepository

```shell
git clone https://git.osgeo.org/gitea/CISSTDI/weissflaechenkartierung.git
make dist
```

Im Anschluss muss das Plugin aus `dist/weissflaechenkartierung-xxx.zip` über [QGIS installiert](https://docs.qgis.org/3.22/en/docs/user_manual/plugins/plugins.html#the-install-from-zip-tab) werden.

## Oberfläche

Nach Installation und Auswahl des [Demoprojekts](https://git.osgeo.org/gitea/CISSTDI/weissflaechenkartierung/archive/master.zip):

![WFK-Interface](app/resource/images/readme/wfk_interface.PNG)

## Konfiguration

**German:**

Die Konfiguration der verschiedenen Auschluss Kriterien erfolgt über die Oberfläche des Plugins. Die genaue Vorgehensweise kann dem User-Manual entnommen werden.
Weitere Informationen erhalten Sie in unserem Webinar oder auf Anfrage.

**English:**

The various exclusion criteria are configured via the plugin interface. The exact procedure can be found in the user manual (only in german).
Further information is available in our webinar or on request.

## Lizenz

[GPL](https://www.gnu.de/documents/gpl-2.0.de.html)
