import os.path
from typing import List
from decimal import *

from PyQt5.QtCore import Qt, pyqtSlot
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QHBoxLayout, QSizePolicy, QSpacerItem

from ...common import str_utils
from ...common.config import config
from ...common.logger import logger
from ...common.signal_bus import signalBus

from ...common.wfk.wfk_config import WfkConfig
from ...common.wfk.wfk_factories import CategorizationFactory
from ...components.buttons.tooltip_button import TooltipPushButton, AnalysisInfoButton
from ...components.dialog_box import MessageDialog
from ...components.widgets.check_box import TextCheckBox
from ...components.widgets.interfaces import BaseProcessInterface
from ...components.widgets.line_edit import LineEdit
from ...components.widgets.plot_widget import AnalysisBarPlotWidget, AnalysisPieWidget
from ...components.widgets.spin_box import DoubleSpinBox, SpinBox
from ...components.table_view.table_view import AnalysisTableView
from ...components.widgets.tooltip import QGisTaskStateToolTip
from ...view.creation_interface.module_dialog import ModuleDialog

try:
    from ...common.wfk.tasks.task_result import WfkTaskResult, AnalysisTaskResult
    from ...common.wfk.tasks.task_info import TaskInfo, AnalysisTaskInfo
    from ...common.wfk.tasks.analysis_task import AnalysisTask
    from ...common.qgis import QGisProjectWrapper, QGisApiWrapper
    from qgis.core import QgsProject, QgsApplication, QgsVectorLayer
except (ImportError, NameError):
    pass


class AnalyseInterface(BaseProcessInterface):
    """
    analyse interface
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        # layout
        self._layout = QVBoxLayout()

        # data fields
        self._current_config: WfkConfig = WfkConfig()

        # module wizard
        self._module_wizard = None
        self._cate_module = None
        self._qgis_base_layer = None

        # widgets
        self.l_parameters_power = QLabel(self.tr("Leistungsdichte"), self._scroll_widget)
        self.l_parameters_full_load = QLabel(self.tr("Volllaststunden"), self._scroll_widget)

        self.btn_create_module = TooltipPushButton("", self)

        self.check_box_activate_categorisation = TextCheckBox(parent=self._scroll_widget)

        self.btn_info = AnalysisInfoButton("", self)
        self.btn_info_cate = TooltipPushButton("", self)
        self.btn_info_parameter = TooltipPushButton("", self)
        self.btn_diagramms = TooltipPushButton("", self)
        self.btn_refresh_analysis = TooltipPushButton("", self)
        self.l_current_config = QLabel(self.tr("Aktuelles Konfiguration:"), self._scroll_widget)
        self.dsb_power_density = DoubleSpinBox(self._scroll_widget)
        self.dsb_annual_income = DoubleSpinBox(self._scroll_widget)
        self.table_view = AnalysisTableView(self._scroll_widget)
        self.bar_plot_widget = AnalysisBarPlotWidget(self._scroll_widget)
        self.cb_show_bar_len = TextCheckBox(self._scroll_widget)
        self.pie_plot_widget = AnalysisPieWidget(self._scroll_widget)
        self.pie_plot_result = AnalysisPieWidget(self._scroll_widget)
        self.l_bar_plot = QLabel("Darstellung der bedingt geeigneten Potentialflächen", self._scroll_widget)
        self.l_table_result = QLabel("Ergebnis: Ermittelte Potentiale", self._scroll_widget)
        self.l_cate_module = QLabel("Bezugslayer für die Distanzberechnung auswählen", self._scroll_widget)
        self.l_parameter = QLabel(self.tr("Parameter"), self._scroll_widget)
        self.sb_cat_classes = SpinBox(self._scroll_widget)
        self.le_field_name = LineEdit(self._scroll_widget)

        # init the ui
        self._init_widget()

    def _connect_signals(self) -> None:
        """
        :return: None
        """
        super()._connect_signals()
        self.btn_refresh_analysis.clicked.connect(self._reload_analysis)
        self.cb_show_bar_len.check_box.stateChanged.connect(self._on_show_bar_len_toggled)
        self.btn_create_module.clicked.connect(self._on_create_module_clicked)

        # wfk interface emits that
        signalBus.wfkConfigChangedSig.connect(self._on_wfk_config_changed)
        signalBus.newLayerSelected.connect(self._on_new_base_layer_selected)
        signalBus.wfkResultSig.connect(self._on_wfk_process_finished)

        # module creation
        signalBus.newModuleSig.connect(self._on_new_module_created)

        # analysis interface
        signalBus.stopAnalysisSig.connect(self._on_stop_tasks)
        signalBus.analysisFinishedSig.connect(self._on_process_finished)

        signalBus.updateWfkConfigSig.connect(self._on_update_wfk_config)

    def _init_widget(self) -> None:
        """
        init widgets

        :return: None
        """
        super()._init_widget()
        self.setViewportMargins(0, 0, 0, 0)

        self.label_title.setText(self.tr("Weißflächenanalyse"))
        self.label_title.adjustSize()

        self.check_box_activate_categorisation.setFixedHeight(40)
        self.check_box_activate_categorisation.setText(self.tr("Kategorisieren"))
        self.check_box_activate_categorisation.setToolTip(self.tr("Hiermit wird gesteuert ob bei der\nAnalyse "
                                                                  "eine Kategorisierung erstellt werden soll"))

        self.btn_create_module.setFixedSize(200, 30)
        self.btn_create_module.setText(self.tr("Kategorisierung wählen"))
        self.btn_create_module.setToolTip(self.tr("Bezugslayer für die\nDistanzberechnung auswählen"))

        self.btn_info.setCursor(Qt.WhatsThisCursor)
        self.btn_info.setFixedSize(20, 20)
        self.btn_info.setText("i")
        self.btn_info.setToolTip(
            str_utils.tooltip(p_capital=self.tr("Weißflächenanalyse"),
                              p_content=self.tr("Im Rahmen der Weißflächenanalyse erfolgt eine<br>gründliche"
                                                " Erfassung und Bewertung der berechneten<br>Weißflächen, die"
                                                " auf den vorab festgelegten Ausschluss-,<br>Abwägungs- und Gunstkriterien"
                                                " basieren. Die Ergebnisse<br>dieser Analyse werden"
                                                " sorgfältig dokumentiert und in<br>Form von übersichtlichen"
                                                " Tabellen und Diagrammen<br>dargestellt."))
        )

        self.btn_info_cate.setCursor(Qt.WhatsThisCursor)
        self.btn_info_cate.setFixedSize(20, 20)
        self.btn_info_cate.setText("i")
        self.btn_info_cate.setToolTip(
            str_utils.tooltip(p_capital=self.tr("Kategorisierung"),
                p_content=
                self.tr("In diesem Bereich des Tools haben Anwender die Möglichkeit, Entfernungen der <br>potentiellen "
                    "Flächen zu bestimmten Referenzpunkten berechnen zu lassen, wie<br>zum Beispiel die Entfernung "
                    "zu Netzeinspeisepunkten. Durch eine automatisierte<br>Analyse erzeugt das Tool einen neuen Layer "
                    "in der QGIS-Legende. Dieser Layer<br>zeigt die ermittelten Potentialflächen anhand ihrer "
                    "Entfernungen zu den Zentren<br>der eingegangenen Layer an und stellt sie farblich "
                    "entsprechend ihrer Entfernung<br>dar. Die farbliche Darstellung der Potentialflächen ermöglicht "
                    "es den Nutzern, die<br>verschiedenen Entfernungsgrade auf einen Blick zu erfassen und zu "
                    "interpretieren.<br>Dies erleichtert Planern die Berücksichtigung räumlicher Nähe zu "
                    "spezifischen<br>Zentren oder Einrichtungen und unterstützt sie bei strategischen Entscheidungen<br>"
                    "bezüglich der Auswahl der Potentialflächen."))
        )

        self.btn_info_parameter.setCursor(Qt.WhatsThisCursor)
        self.btn_info_parameter.setFixedSize(20, 20)
        self.btn_info_parameter.setText("i")
        self.btn_info_parameter.setToolTip(
            str_utils.tooltip(p_capital=self.tr("Parameter"),
                              p_content=self.tr("Hier können Sie die Parameter für 'Leistungsdichte' und<br>"
                                                "'Volllaststunden' als Annahme angeben. Basierend auf<br>"
                                                "diesen Annahmen werden die Ergebnisse für<br>"
                                                "'installierbare Leistung' und 'jährlicher Ertrag' auf den<br>"
                                                "ermittelten Potentialflächen berechnet. Durch Klicken<br>"
                                                "auf die Schaltfläche 'Aktualisieren' werden die<br>"
                                                "aktualisierten Ergebnisse in der Tabelle angezeigt. "))
        )

        self.btn_diagramms.setCursor(Qt.WhatsThisCursor)
        self.btn_diagramms.setFixedSize(20, 20)
        self.btn_diagramms.setText("i")
        self.btn_diagramms.setToolTip(
            str_utils.tooltip(p_capital=self.tr("Parameter"),
                              p_content=self.tr("Die Ausprägung der zur definierten Abwägungskriterien werden "
                                                "in diesem Bereich<br>mittels Balken- und Kreisdiagrammen dargestellt."
                                                "Im Balkendiagramm werden<br> ausschließlich die einzelnen Flächengröße "
                                                "der jeweils definierten Abwägungskriterien<br>dargestellt."
                                                " Dies bedeutet, dass jeder Balken im Diagramm die relative "
                                                "Größe der<br>Fläche repräsentiert, die durch ein bestimmtes Kriterium "
                                                "beeinflusst wird. Anhand der<br>verschiedenen Balkenlängen können "
                                                "Anwender ablesen, welches Kriterium den<br>größten oder den "
                                                "geringsten Einfluss auf die Gesamtfläche der „bedingt "
                                                "geeigneten<br>Potentialflächen“ hat. Im Kreisdiagramm "
                                                "wird die gleiche Information wie im<br>Balkendiagramm dargestellt, "
                                                "jedoch in Prozent des Verhältnisses zur Gesamtfläche der<br>"
                                                " „bedingt geeigneten Potentialflächen“, die durch die "
                                                "Abwägungskriterien definiert<br>sind. Jeder Sektor des Kreisdiagramms"
                                                " repräsentiert den prozentualen Anteil der<br>Fläche, die "
                                                "durch ein bestimmtes Abwägungskriterium beeinflusst wird, "
                                                "im Verhältnis<br>zur Gesamtfläche der bedingt geeigneten Flächen. "
                                                "Durch die Analyse können Planer<br>und Entscheidungsträger "
                                                "schnell erkennen, welche Kriterien prioritär behandelt<br>"
                                                "werden können und welche weniger Einfluss auf die "
                                                "Gesamtfläche haben."))
        )

        self.btn_refresh_analysis.setFixedSize(125, 30)
        self.btn_refresh_analysis.setText(self.tr("Aktualisieren"))
        self.btn_refresh_analysis.setToolTip(self.tr("Mit aktuellen Werten aktualisieren"))

        self.l_current_config.adjustSize()
        self.l_current_config.setFixedHeight(35)
        self.l_bar_plot.adjustSize()
        self.l_bar_plot.setFixedHeight(self.l_bar_plot.height())
        self.l_table_result.adjustSize()
        self.l_table_result.setFixedHeight(self.l_table_result.height())
        self.l_parameters_power.adjustSize()
        self.l_parameters_power.setFixedHeight(self.l_parameters_power.height())
        self.l_parameters_full_load.adjustSize()
        self.l_parameters_full_load.setFixedHeight(self.l_parameters_full_load.height())
        self.dsb_power_density.setMaximum(50000)
        self.dsb_power_density.setFixedSize(158, 35)
        self.dsb_power_density.setSingleStep(0.25)
        self.dsb_power_density.setSuffix("  MW/km²")
        self.dsb_annual_income.setMaximum(50000)
        self.dsb_annual_income.setFixedSize(158, 35)
        self.dsb_annual_income.setSingleStep(0.25)
        self.dsb_annual_income.setSuffix("  h")

        self.le_field_name.setFixedWidth(130)
        self.le_field_name.setTitle(self.tr("Feldname"))

        self.sb_cat_classes.setFixedSize(130, 35)
        self.sb_cat_classes.setSuffix("  Klassen")

        self.sep = QWidget()
        self.sep.setFixedHeight(2)
        self.sep.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.sep.setStyleSheet("background-color: rgba(0, 0, 0, 30)")

        self.sep2 = QWidget()
        self.sep2.setFixedHeight(2)
        self.sep2.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.sep2.setStyleSheet("background-color: rgba(0, 0, 0, 30)")

        self.sep4 = QWidget()
        self.sep4.setFixedHeight(2)
        self.sep4.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        self.sep4.setStyleSheet("background-color: rgba(0, 0, 0, 30)")

        self.cb_show_bar_len.setText(self.tr("Balkenwerte anzeigen"))

        self.cb_show_bar_len.hide()
        self.bar_plot_widget.hide()
        self.table_view.hide()
        self.pie_plot_widget.hide()
        self.pie_plot_result.hide()

        self._init_layout()

    def _init_layout(self) -> None:
        """
        :return: None
        """
        super()._init_layout()
        l11 = QHBoxLayout()
        l11.setContentsMargins(0, 0, 0, 0)
        l11.setSpacing(6)
        l11.addWidget(self.btn_info)
        l11.addWidget(self.label_title)

        l13 = QVBoxLayout()
        l13.setContentsMargins(0, 0, 0, 0)
        l131 = QHBoxLayout()
        l131.setSpacing(6)
        l131.setContentsMargins(0, 0, 0, 0)
        l131.addWidget(self.btn_diagramms)
        l131.addWidget(self.l_bar_plot)
        l13.addLayout(l131)
        l13.addWidget(self.bar_plot_widget)

        l141 = QHBoxLayout()
        l141.setContentsMargins(0, 0, 0, 0)
        l141.addWidget(self.pie_plot_widget)
        l141.addItem(QSpacerItem(1, 1, QSizePolicy.Expanding, QSizePolicy.Fixed))

        l15 = QHBoxLayout()
        l15.setContentsMargins(0, 0, 0, 0)
        l15.setSpacing(6)
        l15.addWidget(self.btn_info_cate)
        l15.addWidget(self.l_cate_module)

        l17 = QVBoxLayout()
        l17.setContentsMargins(25, 5, 0, 0)
        l17.addWidget(self.l_parameters_power)
        l17.addWidget(self.dsb_power_density)
        l17.addWidget(self.l_parameters_full_load)
        l17.addWidget(self.dsb_annual_income)

        l172 = QHBoxLayout()
        l172.setContentsMargins(0, 0, 0, 0)
        l172.setSpacing(6)
        l172.addWidget(self.btn_info_parameter)
        l172.addWidget(self.l_parameter)

        l171 = QVBoxLayout()
        l171.addLayout(l172)
        l171.addLayout(l17)
        l171.addStretch(1)

        l16 = QHBoxLayout()
        l16.setContentsMargins(0, 0, 0, 0)
        l16.setSpacing(6)
        l16.addLayout(l171)
        l16.addSpacing(30)
        l16.addWidget(self.pie_plot_result)
        l16.addStretch(1)

        l18 = QVBoxLayout()
        l18.setContentsMargins(25, 10, 0, 0)
        l18.addWidget(self.btn_create_module)
        l18.addWidget(self.le_field_name)
        l18.addWidget(self.sb_cat_classes)
        l18.addWidget(self.check_box_activate_categorisation)

        self._layout.setContentsMargins(40, 0, 40, 40)
        self._layout.setSpacing(10)
        self._layout.addLayout(l11)
        self._layout.addSpacing(10)
        self._layout.addWidget(self.l_current_config)
        self._layout.addSpacing(10)
        self._layout.addLayout(l16)
        self._layout.addSpacing(20)
        # self._layout.addWidget(self.btn_refresh_analysis, 0, Qt.AlignLeft)
        self._layout.addSpacing(15)
        self._layout.addWidget(self.sep4)
        self._layout.addWidget(self.l_table_result)
        self._layout.addWidget(self.table_view)
        self._layout.addWidget(self.sep)
        self._layout.addLayout(l13)
        self._layout.addWidget(self.cb_show_bar_len)
        self._layout.addLayout(l141)
        self._layout.addWidget(self.sep2)
        self._layout.addSpacing(10)
        self._layout.addLayout(l15)
        self._layout.addLayout(l18)
        self._layout.addStretch(1)

        self._scroll_widget.adjustSize()

    def _set_qss(self) -> None:
        """
        :return: None
        """
        self.l_bar_plot.setObjectName("subTitleLabel")
        self.l_table_result.setObjectName("subTitleLabel")
        self.l_cate_module.setObjectName("subTitleLabel")
        self.l_parameter.setObjectName("subTitleLabel")
        self.l_parameters_full_load.setObjectName("underSubTitleLabel")
        self.l_parameters_power.setObjectName("underSubTitleLabel")
        self.btn_info.setObjectName("infoBtn")
        self.btn_info_cate.setObjectName("infoBtn")
        self.btn_info_parameter.setObjectName("infoBtn")
        self.btn_diagramms.setObjectName("infoBtn")
        self.btn_refresh_analysis.setObjectName("refreshAnalysisButton")
        self.table_view.setObjectName("resultTableView")
        super()._set_qss()

    def _adjust_layout(self):
        """adjust the layout"""
        super()._adjust_layout()
        if self._module_wizard is not None:
            self._module_wizard.adjust_widget_geometry()

        self.btn_refresh_analysis.move(self.width() - self.btn_refresh_analysis.width() - 30,
                                       self.height() - self.btn_refresh_analysis.height() - 30)

    # #######################################################
    # categorization module

    def _on_new_module_created(self, p_new_data: dict) -> None:
        """
        a new module got created
        :return: None
        """
        if len(p_new_data) != 3:
            return
        if p_new_data[2] is False:
            self._cate_module = p_new_data[1]
            if self._cate_module is not None:
                type_name: str = type(self._cate_module.categorization).__name__
                self.btn_create_module.setText(self._cate_module.name)
                self.btn_create_module.setToolTip(CategorizationFactory.registry[type_name]['desc'])
                self._save_categorisation_module()
            else:
                logger.warning("Can not save created categorization module, it's none!")

    def _on_create_module_clicked(self) -> None:
        """
        create a categorisation module
        :return: None
        """
        if self._cate_module is None:
            self._module_wizard = ModuleDialog(self.window(), p_default_parameter={
                "default_crs": config.get("default_crs", "")
            }, p_process_new_module=False, p_analysis_mode=True)
        else:
            self._module_wizard = ModuleDialog(self.window(), p_module=self._cate_module,
                                               p_default_parameter={
                                                   "default_crs": config.get("default_crs", "")
                                               }, p_process_new_module=False, p_analysis_mode=True)
        self._module_wizard.exec()

    # #######################################################
    # business logic

    def _on_process_result(self, result: AnalysisTaskResult) -> None:
        """
        emitted if the analysis task processed with results
        :param result: AnalysisTaskResult
        :return: None
        """
        if not super()._on_process_result(result):
            return

        # there is text to show
        if len(result.txt) > 0:
            m = MessageDialog(title=self.tr("Information"),
                              content=self.tr(result.txt),
                              parent=self.window())
            self._process_toasts.append(m)
            m.exec()

        # ##############################
        # add layers to qgis legend
        # adding cat layer
        layers_to_protocol = []
        if result.categorised_layer is None:
            if self.check_box_activate_categorisation.isChecked():
                logger.warning("The categorisation result layer is None!")
        else:
            layers_to_protocol.append(result.categorised_layer)

        # add good and ok layer to legend
        if result.consideration_good_layer is None:
            logger.warning("There are no good suited areas identified!")
        else:
            layers_to_protocol.append(result.consideration_good_layer)
        if result.consideration_ok_layer is None:
            logger.warning("There are no well suited areas identified!")
        else:
            layers_to_protocol.append(result.consideration_ok_layer)

        logger.debug("Add groups to qgis legend...")
        if len(layers_to_protocol) and not QGisProjectWrapper.add_layers_to_group("Analyse", layers_to_protocol):
            logger.debug(f"Can not add group Analysis with layers to qgis!")
            signalBus.showErrorToastTip.emit(self.tr(f"Analyse Gruppe konnte nicht hinzugefügt werden!"))
        else:
            logger.debug("Groups added")
        # ##############################

        if not len(result.bar_chart_result) or not len(result.calculations):
            logger.warning("There are no calculation results!")
            return

        # add calculations to row
        logger.debug("Fill table view...")
        self.table_view.model().resetInternalData()
        # manipulate header
        vertical_header: List[str] = []
        for i, x in enumerate(result.calculations[3:]):
            if i % 2 != 0:
                vertical_header.append(f"Gunstkriterium:\n{x[0]}\ngut geeignet")
            else:
                vertical_header.append(f"Gunstkriterium:\n{x[0]}\nbedingt geeignet")

        std_header = [x for x in self.table_view.std_vertical_header]
        std_header.extend(vertical_header)
        self.table_view.model().set_vertical_header(std_header)

        # add items to table
        for row in result.calculations:
            self.table_view.append_row(row[1:])

        self.table_view.setFixedHeight(self.table_view.get_min_vertical_content_height())
        self.table_view.show()
        logger.debug("Table view filled")
        self._scroll_widget.adjustSize()

        # relative to input layer
        names = [k["name"] for k in result.pie_chart_sum_relative_result]
        colors = [k["color"] for k in result.pie_chart_sum_relative_result]
        y: List[float] = [k["size"] for k in result.pie_chart_sum_relative_result]

        self.pie_plot_result.setFixedSize(600, 240)
        self.pie_plot_result.show()
        self.pie_plot_result.plot2(names, y, colors)

        # bar chart
        names = [k["name"] for k in result.bar_chart_result]
        colors = [k["color"] for k in result.bar_chart_result]
        y: List[float] = [k["size"] for k in result.bar_chart_result]
        p: List[float] = [k["percentage"] for k in result.bar_chart_result]

        self.bar_plot_widget.setFixedHeight(int(len(y) * 60))
        self.bar_plot_widget.show()
        self.bar_plot_widget.plot(names, y, colors)
        self.bar_plot_widget.setFixedWidth(1200)

        self.pie_plot_widget.setFixedSize(600, 240)
        self.pie_plot_widget.show()
        self.pie_plot_widget.plot2(names, p, colors)
        self._scroll_widget.adjustSize()
        self._adjust_layout()

    def _reload_analysis(self) -> None:
        """
        reload analysis with given parameters
        :return: None
        """
        if self.is_busy():
            signalBus.showInfoToastTip.emit(self.tr("Läuft bereits!"))
            return

        if not self._current_config.is_valid():
            signalBus.showErrorToastTip.emit(self.tr("Konfiguration nicht vollständig!"))
            return

        # check if the qgis interface is not none
        if config.qgis_interface is None:
            signalBus.showErrorToastTip.emit(self.tr("Das QGis interface ist nicht gegeben!"))
            return

        # if we have a wfk layer path, load it as temp layer
        if os.path.exists(self._current_config.wfk_layer_path):
            self._current_config.processed_wfk_layer = QGisApiWrapper.load_shape(
                p_url=self._current_config.wfk_layer_path,
                p_layer_name="loaded_local_layer",
                p_crs=config.get("default_crs", "")
            )

        if self._qgis_base_layer is None:
            signalBus.showErrorToastTip.emit(self.tr("Zielgebiet muss ausgewählt werden!"))
            return

        if self._current_config.processed_wfk_layer is None:
            signalBus.showErrorToastTip.emit(self.tr("Wfk-Layer wurde nicht erstellt!"))
            return

        if self._current_config.processed_wfk_layer.featureCount() == 0:
            logger.warning(self.tr("The given layer has no geometries!"))
            signalBus.showErrorToastTip.emit(self.tr("Wfk-Layer ohne Geometrien!"))
            return

        if len(self._current_config.modules) == 0:
            signalBus.showErrorToastTip.emit(self.tr("Es wurden keine Module erstellt!"))
            return

        # check if the user wants to run categorisation
        cate_modul = None
        if self.check_box_activate_categorisation.isChecked():
            cate_modul = self._cate_module

            # cate module is None but user wants to categorize, inform him
            if cate_modul is None:
                m = MessageDialog(title=self.tr("Information"),
                                  content=self.tr("Es wurde noch kein Modul für die Kategorisierung erstellt!"),
                                  parent=self.window())
                m.yesButton.setText(self.tr("ignorieren"))
                self._process_toasts.append(m)
                m.exec()
                if m.no_clicked:
                    return

        self.table_view.hide()
        self.pie_plot_result.hide()
        self.pie_plot_widget.hide()
        self.bar_plot_widget.hide()
        self._scroll_widget.adjustSize()
        self._adjust_layout()

        self._current_config.full_load_hours = self.dsb_annual_income.value()
        self._current_config.power_density = self.dsb_power_density.value()
        self._current_config.qgis_layer = self._qgis_base_layer
        self._current_config.class_count = self.sb_cat_classes.value()
        self._current_config.field_name = self.le_field_name.text()

        # save values to config
        self._save_categorisation_module()



        task = AnalysisTask(
            p_task_info=AnalysisTaskInfo(self._current_config, "AnalyseTask", cate_modul, self.sb_cat_classes.value(),
                                         self.le_field_name.text())
        )
        self._process_tasks.append(task)

        # connect signals
        task.progressChanged.connect(self._on_progress_changed)
        task.result.connect(self._on_process_result)
        signalBus.analysisFinishedSig.connect(self._on_process_finished)
        signalBus.stopAnalysisSig.connect(self._on_stop_tasks)

        # create loading widget
        loader = QGisTaskStateToolTip(title=self.tr("Analyse-Prozess"),
                                          content=self.tr("Prozess zu 0% ausgeführt"), parent=self.window())
        loader.show()
        self._process_toasts.append(loader)
        QgsApplication.taskManager().addTask(task)

    # #######################################################
    # wfk signal connections

    @pyqtSlot(WfkTaskResult)
    def _on_wfk_process_finished(self, p_r: WfkTaskResult) -> None:
        """
        If the wfk process finished we want to know what modules ran
        Copy the processed modules and the processed wfk layer
        :param p_r: task result
        :return: None
        """
        self._current_config.modules = p_r.wfk_config.modules
        self._current_config.processed_wfk_layer = p_r.wfk_config.processed_wfk_layer
        self._current_config.wfk_layer_path = ""

    @pyqtSlot(int)
    def _on_new_base_layer_selected(self, p_idx: int) -> None:
        """
        new layer selected update current config
        :param p_idx: index
        :return: None
        """
        self._qgis_base_layer = QGisProjectWrapper.get_layer_by_id(
            p_id=p_idx, p_interface=config.qgis_interface
        )

    @pyqtSlot(dict, object)
    def _on_update_wfk_config(self, p_data: dict, p_layer: object) -> None:
        """
        user saved config
        :param p_data: new config
        :param p_layer: layer
        :return: None
        """
        wfk_config = WfkConfig()
        wfk_config.from_json(p_data=p_data)
        wfk_config.qgis_layer = self._current_config.qgis_layer
        if p_layer is not None:
            if isinstance(p_layer, str):
                wfk_config.wfk_layer_path = p_layer
            elif isinstance(p_layer, QgsVectorLayer):
                wfk_config.processed_wfk_layer = p_layer

        self.l_current_config.setText(f"{self.tr('Aktuelle Konfiguration:')} <b>{wfk_config.name}</b>")
        if wfk_config.name == self._current_config.name:
            if self._current_config.analysis_module is not None:
                self._cate_module = self._current_config.analysis_module
            # special case
            if self._current_config.processed_wfk_layer is not None and \
                    (wfk_config.processed_wfk_layer is None or not wfk_config.wfk_layer_path):
                wfk_config.processed_wfk_layer = self._current_config.processed_wfk_layer

            self._current_config: WfkConfig = wfk_config
            self._reload_categorisation_module()

    @pyqtSlot(dict, object)
    def _on_wfk_config_changed(self, p_config: dict, p_layer: object) -> None:
        """
        emitted if the user changed the config inside wfk-interface
        :param p_config: dict
        :param p_layer: QVectorLayer
        """
        if self.is_busy():
            return
        wfk_config = WfkConfig()
        wfk_config.from_json(p_data=p_config)
        if p_layer is not None:
            if isinstance(p_layer, str):
                wfk_config.wfk_layer_path = p_layer
            elif isinstance(p_layer, QgsVectorLayer):
                wfk_config.processed_wfk_layer = p_layer
        self._current_config: WfkConfig = wfk_config
        self.l_current_config.setText(f"{self.tr('Aktuelle Konfiguration:')} <b>{wfk_config.name}</b>")
        self._reload_categorisation_module()

    # #######################################################
    # help functions

    def _reload_categorisation_module(self) -> None:
        """
        :return: None
        """
        self.dsb_annual_income.setValue(self._current_config.full_load_hours)
        self.dsb_power_density.setValue(self._current_config.power_density)
        self.sb_cat_classes.setValue(self._current_config.class_count)
        self.le_field_name.setText(self._current_config.field_name)

        if self._current_config.analysis_module != self._cate_module:
            self._cate_module = self._current_config.analysis_module

            if self._cate_module is not None and self._cate_module.categorization is not None:
                type_name: str = type(self._cate_module.categorization).__name__
                self.btn_create_module.setText(self._cate_module.name)
                self.btn_create_module.setToolTip(CategorizationFactory.registry[type_name]['desc'])
            else:
                self.btn_create_module.setText("Kategorisierung wählen")

    def _save_categorisation_module(self) -> None:
        """
        :return: None
        """
        self._current_config.full_load_hours = self.dsb_annual_income.value()
        self._current_config.power_density = self.dsb_power_density.value()
        self._current_config.class_count = self.sb_cat_classes.value()
        self._current_config.field_name = self.le_field_name.text()
        self._current_config.analysis_module = self._cate_module

        # save to config
        signalBus.updateWfkConfigSig.emit(self._current_config.to_json(), None)

    @pyqtSlot()
    def _on_show_bar_len_toggled(self) -> None:
        """
        user toggled check box
        :return: None
        """
        cur_data = self.bar_plot_widget.data
        if self.cb_show_bar_len.check_box.isChecked() and len(cur_data):
            self.cb_show_bar_len.setText(self.tr("Balkenwerte verstecken"))
            self.bar_plot_widget.add_annotations = True
            self.bar_plot_widget.plot(cur_data[0], cur_data[1], cur_data[2])
        else:
            self.cb_show_bar_len.setText(self.tr("Balkenwerte anzeigen"))
            self.bar_plot_widget.add_annotations = False
            self.bar_plot_widget.plot(cur_data[0], cur_data[1], cur_data[2])
