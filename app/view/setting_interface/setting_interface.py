import os.path

from PyQt5.QtCore import pyqtSignal, pyqtSlot, QTimer
from PyQt5.QtWidgets import QWidget, QLabel, QApplication, QFileDialog, QVBoxLayout, QSizePolicy, \
    QHBoxLayout

from ...common import str_utils, os_utils
from ...common.config import config
from ...common.logger import logger
from ...common.signal_bus import signalBus
from ...components.buttons.tooltip_button import TooltipPushButton
from ...components.widgets.check_box import TextCheckBox
from ...components.widgets.combo_box import ComboBox
from ...components.widgets.interfaces import BaseProcessInterface
from ...components.widgets.label import ClickableIconLabel
from ...components.widgets.line_edit import LineEdit

try:
    from ...common.qgis import QGisProjectWrapper, QGisApiWrapper
    from qgis.core import QgsVectorFileWriter, QgsProject
except ImportError:
    pass


class SettingInterface(BaseProcessInterface):
    """Setting interface"""
    minimizeToTrayChanged = pyqtSignal(bool)

    def __init__(self, parent=None):
        super().__init__(parent)
        # layout
        self._layout = QVBoxLayout()

        # wfk settings
        self.label_wfk_title = QLabel(self.tr("Einstellungen"), self._scroll_widget)
        # protocol layers
        self.cb_protocol_layer = TextCheckBox(self._scroll_widget)
        self.cb_run_wfk_with_errors = TextCheckBox(self._scroll_widget)

        # wfk one run, run analysis after wfk
        self.cb_wfk_one_run = TextCheckBox(self._scroll_widget)

        self.cb_get_crs_from_project = TextCheckBox(self._scroll_widget)

        # additional functionality
        self.label_additional_title = QLabel(self.tr("Andere"), self._scroll_widget)

        self.cb_qgis_layer = ComboBox(self._scroll_widget)
        self.btn_save_sel_area = TooltipPushButton("", self._scroll_widget)

        self.le_default_crs = LineEdit(self._scroll_widget)
        self.btn_take_over_crs = TooltipPushButton("", self._scroll_widget)

        self.cb_search_config_in_project_dir = TextCheckBox(self._scroll_widget)
        self.cb_search_config_in_plugin_dir = TextCheckBox(self._scroll_widget)
        self.cb_open_cur_config_dir = TextCheckBox(self._scroll_widget)
        self.cb_open_last_config = TextCheckBox(self._scroll_widget)

        # minimize to tray with close
        self.cb_minimize_to_try = TextCheckBox(self._scroll_widget)

        # help section
        self.label_help_title = QLabel(self.tr("Hilfe"), self._scroll_widget)

        self.ciss_website = ClickableIconLabel(parent=self._scroll_widget)

        # init the ui
        self._init_widget()

    def _init_widget(self) -> None:
        """
        init widgets

        :return: None
        """
        super()._init_widget()
        self.label_title.hide()
        # protocol layer
        protocol_layer: bool = config.set_default("protocol_layer", True)
        self.cb_protocol_layer.setChecked(protocol_layer)
        self.cb_protocol_layer.setText(self.tr("Prozess Layer protokollieren"))

        run_tasks_with_errors: bool = config.set_default("run_tasks_with_errors", False)
        self.cb_run_wfk_with_errors.setChecked(run_tasks_with_errors)
        self.cb_run_wfk_with_errors.setText(self.tr("Prozess Fehler ignorieren"))

        # one run
        wfk_one_run: bool = config.set_default("wfk_one_run", False)
        self.cb_wfk_one_run.setChecked(wfk_one_run)
        self.cb_wfk_one_run.setFixedSize(300, self.cb_wfk_one_run.height())
        self.cb_wfk_one_run.setText(self.tr("Im Anschluss an die Weißflächenkartierung die Analyse starten"))
        self.cb_wfk_one_run.setToolTip(self.tr("Aktuell nicht verfügbar!"))
        self.cb_wfk_one_run.setDisabled(True)

        # std crs from project crs
        crs_from_project: bool = config.set_default("get_crs_from_project", False)
        self.cb_get_crs_from_project.setChecked(crs_from_project)
        self.cb_get_crs_from_project.setFixedSize(300, self.cb_get_crs_from_project.height())
        self.cb_get_crs_from_project.setText(self.tr("Projektkoordinatensystem als Standard verwenden"))

        default_crs: str = config.set_default("default_crs", "")
        # check if we have to load the default project crs
        if self.cb_get_crs_from_project.isChecked():
            try:
                if len(QGisProjectWrapper.get_project_crs(config.qgis_interface)) > 0:
                    default_crs = QGisProjectWrapper.get_project_crs(config.qgis_interface)
            except Exception as e:
                pass
        self.le_default_crs.setTitle(self.tr("Koordinatensystem"))
        self.le_default_crs.setToolTip(self.tr("Das hier verwendete Koordinatensystem wird als<br>"
                                               "Standard während sämtlicher Layerinteraktionen verwendet"))
        self.le_default_crs.setText(default_crs)
        self.le_default_crs.setFixedWidth(200)
        self.btn_take_over_crs.setFixedSize(100, 35)
        self.btn_take_over_crs.setText(self.tr("Übernehmen"))
        self.btn_take_over_crs.setToolTip(
            str_utils.tooltip(p_capital=self.tr("CRS-Übernahme"),
                              p_content=self.tr("Eingegebenes Koordinatensystem<br>in die Konfiguration überhnehmen")))

        # other
        self.btn_save_sel_area.setText("Export")
        self.btn_save_sel_area.setFixedSize(100, 35)
        self.btn_save_sel_area.setToolTip(
            str_utils.tooltip(p_capital=self.tr("Export"),
                              p_content=self.tr("Export selektierter Flächen und Flurstückskennzeichen")))
        self.cb_qgis_layer.setToolTip(
            str_utils.tooltip(p_capital=self.tr("Layer-Auswahl"),
                              p_content=self.tr("Übersicht der zur Verfügung stehenden Layer!")))
        self.cb_qgis_layer.setTitle("Layer-Auswahl")
        self.cb_qgis_layer.setFixedWidth(200)

        search_config_in_project_dir: bool = config.set_default("search_config_in_project_dir", False)
        self.cb_search_config_in_project_dir.setChecked(search_config_in_project_dir)
        self.cb_search_config_in_project_dir.setFixedSize(300, 30)
        self.cb_search_config_in_project_dir.setText(self.tr("Konfiguration im Projektverzeichnis suchen"))

        search_config_in_plugin_dir: bool = config.set_default("search_config_in_plugin_dir", True)
        self.cb_search_config_in_plugin_dir.setChecked(search_config_in_plugin_dir)
        self.cb_search_config_in_plugin_dir.setFixedSize(300, 30)
        self.cb_search_config_in_plugin_dir.setText(self.tr("Konfiguration im Pluginverzeichnis suchen"))

        open_cur_config_dir: bool = config.set_default("open_cur_config_dir", True)
        self.cb_open_cur_config_dir.setChecked(True)
        self.cb_open_cur_config_dir.setFixedSize(300, 30)
        self.cb_open_cur_config_dir.setText(self.tr("Verzeichnis der letzten Konfiguration öffnen"))

        open_last_config: bool = config.set_default("open_last_config", True)
        self.cb_open_last_config.setChecked(open_last_config)
        self.cb_open_last_config.setFixedSize(300, 30)
        self.cb_open_last_config.setText(self.tr("Letzte Konfiguration öffnen"))

        # minimize to tray after close clicked
        enable_minimize_to_tray: bool = config.set_default("minimize_to_tray", False)
        self.cb_minimize_to_try.setChecked(enable_minimize_to_tray)
        self.cb_minimize_to_try.setFixedSize(300, 30)
        self.cb_minimize_to_try.setText(self.tr("Beim Click auf beenden das Fenster nur minimieren"))

        # help section
        self.ciss_website.setFixedSize(200, 31)
        self.ciss_website.setIcon(p_path=":/images/main_window/ciss.png")
        self.ciss_website.setText(f"<a href=\"https://www.ciss.de/impressum/\">{self.tr('Impressum')}</a>")
        self.ciss_website.label.setOpenExternalLinks(True)
        self.ciss_website.label.setToolTip(self.tr("Bei Fragen oder Anmerkungen"))

        # disable widgets for next release
        self.cb_wfk_one_run.hide()
        self.cb_open_cur_config_dir.hide()
        self.cb_minimize_to_try.hide()
        self.cb_search_config_in_plugin_dir.hide()
        self.cb_search_config_in_project_dir.hide()


        self._init_layout()

    def _init_layout(self) -> None:
        """
        init the layout

        :return: None
        """
        super()._init_layout()
        self._layout.setContentsMargins(0, 0, 75, 0)
        self._layout.addWidget(self.label_wfk_title)
        self._layout.addSpacing(10)
        self._layout.addWidget(self.cb_protocol_layer)
        self._layout.addWidget(self.cb_wfk_one_run)
        self._layout.addWidget(self.cb_run_wfk_with_errors)
        self._layout.addWidget(self.cb_get_crs_from_project)
        self._layout.addWidget(self.cb_open_last_config)
        self._layout.addSpacing(5)
        sep = QWidget()
        sep.setFixedHeight(2)
        sep.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sep.setStyleSheet("background-color: rgba(0, 0, 0, 30)")
        self._layout.addWidget(sep)
        self._layout.addSpacing(5)
        self._layout.addWidget(self.label_additional_title)
        l1 = QHBoxLayout()
        l1.setContentsMargins(0, 0, 0, 0)
        l1.setSpacing(6)
        l11 = QVBoxLayout()
        l11.setContentsMargins(0, 0, 0, 6)
        l11.addStretch(1)
        l11.addWidget(self.btn_save_sel_area)
        l1.addWidget(self.cb_qgis_layer)
        l1.addLayout(l11)
        l1.addStretch(3)
        l2 = QHBoxLayout()
        l2.setContentsMargins(0, 0, 0, 0)
        l2.setSpacing(6)
        l21 = QVBoxLayout()
        l21.setContentsMargins(0, 0, 0, 1)
        l21.addStretch(1)
        l21.addWidget(self.btn_take_over_crs)
        l2.addWidget(self.le_default_crs)
        l2.addLayout(l21)
        l2.addStretch(3)

        self._layout.addLayout(l1)
        self._layout.addSpacing(5)
        self._layout.addLayout(l2)
        self._layout.addSpacing(5)
        self._layout.addWidget(self.cb_search_config_in_project_dir)
        self._layout.addWidget(self.cb_search_config_in_plugin_dir)
        # self._layout.addWidget(self.cb_open_last_config)
        self._layout.addWidget(self.cb_open_cur_config_dir)
        self._layout.addWidget(self.cb_minimize_to_try)
        self._layout.addSpacing(5)
        sep1 = QWidget()
        sep1.setFixedHeight(2)
        sep1.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sep1.setStyleSheet("background-color: rgba(0, 0, 0, 30)")
        self._layout.addWidget(sep1)
        self._layout.addSpacing(5)
        self._layout.addWidget(self.label_help_title)
        self._layout.addSpacing(10)
        self._layout.addWidget(self.ciss_website)
        self._scroll_widget.adjustSize()

    def _set_qss(self) -> None:
        """set the style sheet"""
        # self.btn_save_sel_area.setObjectName("saveSelAreaButton")
        self.label_help_title.setObjectName("subTitleLabel")
        self.label_wfk_title.setObjectName("titleLabel")
        self.label_additional_title.setObjectName("subTitleLabel")

        super()._set_qss()

    def _connect_signals(self) -> None:
        """
        connects all signals from the settings

        :return: None
        """
        super()._connect_signals()
        signalBus.layerSectionRefreshed.connect(self._on_layer_refreshed)

        # wfk signals
        self.cb_protocol_layer.check_box.stateChanged.connect(self._on_protocol_layer_clicked)
        self.cb_wfk_one_run.check_box.stateChanged.connect(self._on_wfk_one_run_clicked)
        self.cb_run_wfk_with_errors.check_box.stateChanged.connect(self._on_run_with_errors_clicked)
        self.cb_get_crs_from_project.check_box.stateChanged.connect(self._on_take_project_crs_clicked)

        # other
        self.btn_take_over_crs.clicked.connect(self._on_take_over_crs_clicked)
        self.btn_save_sel_area.clicked.connect(self._on_export_selected_layer_clicked)
        self.cb_search_config_in_project_dir.check_box.stateChanged.connect(self._on_config_project_clicked)
        self.cb_search_config_in_plugin_dir.check_box.stateChanged.connect(self._on_config_plugin_clicked)
        self.cb_open_cur_config_dir.check_box.stateChanged.connect(self._on_open_cur_config_dir_clicked)
        self.cb_open_last_config.check_box.stateChanged.connect(self._on_open_last_config_clicked)
        self.cb_minimize_to_try.check_box.stateChanged.connect(self._on_minimize_to_tray_clicked)

    # ###############################################################################################################
    # signals

    @pyqtSlot()
    def _on_run_with_errors_clicked(self) -> None:
        """if the user clicked"""
        config["run_tasks_with_errors"] = self.cb_run_wfk_with_errors.isChecked()
        config.save(config.path)

    @pyqtSlot()
    def _on_protocol_layer_clicked(self) -> None:
        """if the user clicked"""
        config["protocol_layer"] = self.cb_protocol_layer.isChecked()
        config.save(config.path)

    @pyqtSlot()
    def _on_wfk_one_run_clicked(self) -> None:
        """if the user clicked"""
        config["wfk_one_run"] = self.cb_wfk_one_run.isChecked()
        config.save(config.path)

    @pyqtSlot()
    def _on_take_project_crs_clicked(self) -> None:
        """if the user clicked"""
        config["get_crs_from_project"] = self.cb_get_crs_from_project.isChecked()
        self.cb_get_crs_from_project.update()
        QApplication.processEvents()
        config.save(config.path)

    @pyqtSlot()
    def _on_take_over_crs_clicked(self) -> None:
        """
        user pressed take over crs
        :return: None
        """
        config["default_crs"] = self.le_default_crs.text()

        # if the user pressed take the project / qgis crs
        # and know input custom crs, if that is not the same
        # deselect the cb_get_crs_from_project checkbox, else select it
        try:
            if self.le_default_crs.text().lower() != QGisProjectWrapper.get_project_crs(config.qgis_interface).lower():
                self.cb_get_crs_from_project.setChecked(False)
            else:
                self.cb_get_crs_from_project.setChecked(True)
        except NameError as e:
            pass
        QApplication.processEvents()
        if config.save(config.path):
            signalBus.showInfoToastTip.emit(self.tr("Koordinatensytem übernommen!"))
        else:
            signalBus.showInfoToastTip.emit(self.tr("Fehler bei der Übernahme!"))

    # #######################
    # other

    @pyqtSlot(list)
    def _on_layer_refreshed(self, p_new_layer: list) -> None:
        """emitted if the layer list inside wfk_interface got refreshed"""
        if len(p_new_layer) == 0:
            return

        text = self.cb_qgis_layer.currentText()
        self.cb_qgis_layer.clear()
        for layer in p_new_layer:
            self.cb_qgis_layer.addItem(layer.name(), layer)
        self.cb_qgis_layer.setCurrentText(text)

    @pyqtSlot()
    def _on_export_selected_layer_clicked(self) -> None:
        """the user wants to export the selection on the selected layer"""
        current_layer = self.cb_qgis_layer.currentData()
        if current_layer is None:
            signalBus.showInfoToastTip.emit("Es wurde noch kein Layer ausgewählt!")
            return
        # get new shape file name
        file_path: str = QFileDialog.getSaveFileName(self,
                                                     self.tr("Neue Shape-datei erstellen..."),
                                                     filter=self.tr("ESRI Shapefile (*.shp)"))[0]

        if len(file_path) == 0:
            return

        # clone the selected working layer and save it temporary
        if current_layer.selectedFeatureCount() == 0:
            # clone the complete layer which we want to process and save it temporary
            cloned_layer = QGisApiWrapper.clone_all_features(p_input_layer=current_layer)
        else:
            # clone the selected features
            cloned_layer = QGisApiWrapper.clone_selected_features(p_input_layer=current_layer)

        if cloned_layer is None:
            signalBus.showErrorToastTip.emit("Der ausgewählte Layer konnte nicht geklont werden!")
            return

        try:
            save_options = QgsVectorFileWriter.SaveVectorOptions()
            save_options.driverName = "ESRI Shapefile"
            save_options.fileEncoding = "UTF-8"
            transform_context = QgsProject.instance().transformContext()

            writer_error, msg = QgsVectorFileWriter.writeAsVectorFormatV2(layer=cloned_layer, fileName=file_path,
                                                                          transformContext=transform_context,
                                                                          options=save_options)
            if len(msg) > 0 and writer_error != 0:
                logger.warning(f"error code: {writer_error} msg: {msg}")
        except Exception as e:
            logger.error(f"Can not export {current_layer.name()} -> {str(e)}")
            signalBus.showErrorToastTip.emit(f"Der Layer: {current_layer.name()} konnte nicht exportiert werden!")
            return

        # open exported file inside explorer
        if not os.path.exists(file_path):
            signalBus.showErrorToastTip.emit(f"Der Layer: {current_layer.name()} konnte nicht exportiert werden!")
            return

        logger.info(f"Layer {current_layer.name()} exported successful")
        signalBus.showCompleteToastTip.emit(f"Der Layer: {current_layer.name()} wurde exportiert!")
        QTimer.singleShot(0, lambda: os_utils.show_in_folder(file_path))

    @pyqtSlot()
    def _on_config_project_clicked(self) -> None:
        """if the user clicked"""
        config["search_config_in_project_dir"] = self.cb_search_config_in_project_dir.isChecked()
        config.save(config.path)

    @pyqtSlot()
    def _on_config_plugin_clicked(self) -> None:
        """if the user clicked"""
        config["search_config_in_plugin_dir"] = self.cb_search_config_in_plugin_dir.isChecked()
        config.save(config.path)

    @pyqtSlot()
    def _on_open_cur_config_dir_clicked(self) -> None:
        """if the user clicked"""
        config["open_cur_config_dir"] = self.cb_open_cur_config_dir.isChecked()
        config.save(config.path)

    @pyqtSlot()
    def _on_open_last_config_clicked(self) -> None:
        """if the user clicked"""
        config["open_last_config"] = self.cb_open_last_config.isChecked()
        config.save(config.path)

    @pyqtSlot()
    def _on_minimize_to_tray_clicked(self) -> None:
        """ if the user clicked """
        config["minimize_to_tray"] = self.cb_minimize_to_try.isChecked()
        config.save(config.path)
