from PyQt5.QtCore import Qt
from PyQt5.QtGui import QResizeEvent

from ...components.buttons.svg_button import SvgPaintButton
from ...components.toolbar.edit_menu_bar import EditMenuBarSvg


class ModuleEditBar(EditMenuBarSvg):
    def __init__(self, p_seperator_color: str = "#FFFFFF", p_background_color: str = "#6481e5",
                 iconSize: tuple = (40, 40), parent=None):
        super().__init__(p_seperator_color, p_background_color, iconSize, parent)

        self.btn_move_up = SvgPaintButton(p_svg_path=":/images/button/arrow_up_white.svg", parent=self)
        self.btn_move_down = SvgPaintButton(p_svg_path=":/images/button/arrow_down_white.svg", parent=self)

        self.btn_move_up.resize(*self._icon_size)
        self.btn_move_down.resize(*self._icon_size)

        self._buttons.extend([
            self.btn_move_up,
            self.btn_move_down
        ])
        self.btn_move_up.setCursor(Qt.PointingHandCursor)
        self.btn_move_down.setCursor(Qt.PointingHandCursor)


        self.btn_edit.setToolTip(self.tr("Ausgewähltes Modul bearbeiten"))
        self.btn_add.setToolTip(self.tr("Neues Modul erstellen"))
        self.btn_del.setToolTip(self.tr("Ausgewähltes Modul löschen"))
        self.btn_move_up.setToolTip(self.tr("Ausgewähltes Modul nach oben verschieben"))
        self.btn_move_down.setToolTip(self.tr("Ausgewähltes Modul nach unten verschieben"))

    def resizeEvent(self, e: QResizeEvent) -> None:
        """resize three buttons"""
        super(ModuleEditBar, self).resizeEvent(e)

        self.btn_move_down.move(self.btn_del.geometry().bottomRight().x() + self._padding, self._margin[1])
        self.btn_move_up.move(self.btn_move_down.geometry().bottomRight().x() + self._padding, self._margin[1])

        self.setFixedSize(self.btn_move_up.geometry().bottomRight().x() + self._margin[2],
                          self.btn_move_up.geometry().bottomRight().y() + self._margin[3])
