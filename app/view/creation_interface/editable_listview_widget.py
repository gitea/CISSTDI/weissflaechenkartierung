from PyQt5.QtCore import Qt, QModelIndex
from PyQt5.QtGui import QStandardItemModel, QStandardItem
from PyQt5.QtWidgets import QWidget, QPushButton

from ...components.widgets.list_view import ListView
from ...view.creation_interface.editable_table_widget import RowEditorDialog


class EditableListWidget(QWidget):
    def __init__(self, parent=None, mask_parent=None, size: tuple = (200, 200)):
        super().__init__(parent)
        self.mask_parent = mask_parent.parent().parent()
        self.editor_dialog = RowEditorDialog(self.mask_parent)
        self.setFixedSize(*size)

        self.btn_add = QPushButton(self)
        self.btn_del = QPushButton(self)
        self.btn_edit = QPushButton(self)

        self.list_view = ListView(self)
        self._init_widgets()
        self._connect_signals()

    def _set_qss(self) -> None:
        """set qss of the control"""
        self.btn_add.setObjectName("addButton")
        self.btn_edit.setObjectName("editButton")
        self.btn_del.setObjectName("deleteButton")

    def _init_widgets(self):
        """init the widgets"""
        self._set_qss()
        self.btn_edit.resize(25, 25)
        self.btn_add.resize(25, 25)
        self.btn_del.resize(25, 25)

        self.btn_edit.setToolTip(self.tr("Bearbeiten"))
        self.btn_add.setToolTip(self.tr("Neu"))
        self.btn_del.setToolTip(self.tr("Löschen"))

        self.list_view.setModel(QStandardItemModel())

        self._adjust_layout()

    def _connect_signals(self):
        """ connect signals of widgets """
        self.editor_dialog.newRow.connect(self.on_create_new_row)
        self.editor_dialog.rowEdited.connect(self.on_row_edited)

        self.btn_add.clicked.connect(self.on_add_row_clicked)
        self.btn_edit.clicked.connect(self.on_list_view_edit_row_clicked)
        self.btn_del.clicked.connect(self.on_list_view_delete_row)

        self.list_view.doubleClicked.connect(self.on_list_view_double_clicked)
        self.list_view.editRow.connect(self.on_list_view_edit_row_clicked)

        self.list_view.deleteRow.connect(self.on_list_view_delete_row)

    def _adjust_layout(self) -> None:
        """ init the layout """
        self.btn_add.move(0, 0)
        self.btn_edit.move(self.btn_add.geometry().bottomRight().x() + 6, 0)
        self.btn_del.move(self.btn_edit.geometry().bottomRight().x() + 6, 0)

        self.list_view.resize(self.width() - 1, self.height() - 56)

        self.list_view.move(1, self.btn_add.geometry().bottomLeft().y() + 6)

        self.resize(self.width(),
                    self.list_view.height() + 62)

        self.adjustSize()

        # self.editor_dialog.move(0, 0)
        self.editor_dialog.move(self.mask_parent.x(), self.mask_parent.y())
        self.editor_dialog.resize(self.mask_parent.size())
        self.editor_dialog.windowMask.resize(self.mask_parent.size())

    def resizeEvent(self, e):
        self._adjust_layout()

    #############################################################

    def on_row_edited(self, p_data: tuple):
        """ if a row got edited """
        if len(p_data) != 2:
            return
        new_data: str = p_data[1]
        if len(new_data) > 0:
            model: QStandardItemModel = self.list_view.model()
            model.setItem(QModelIndex(p_data[0]).row(), 0, QStandardItem(new_data[0]))

    def on_list_view_delete_row(self):
        """ delete the selected row """
        self.list_view.model().removeRow(self.list_view.currentIndex().row())

    def on_list_view_edit_row_clicked(self) -> None:
        """ edit selected row """
        if self.list_view.model().rowCount() > 0:
            self.on_list_view_double_clicked(None)

    def on_list_view_double_clicked(self, index) -> None:
        """ user double-clicked list item """
        self._adjust_layout()
        index = self.list_view.currentIndex()
        value: str = self.list_view.model().data(index, Qt.DisplayRole)
        self.editor_dialog.editRow.emit([index, [value, ""]])
        self.editor_dialog.exec()

    def on_add_row_clicked(self):
        self._adjust_layout()
        self.editor_dialog.exec()

    def on_create_new_row(self, p_data: list):
        if len(p_data) != 2:
            return
        self.list_view.model().appendRow(QStandardItem(p_data[0]))


class EditCatDisListView(EditableListWidget):
    def __init__(self, parent=None, mask_parent=None):
        super().__init__(parent, mask_parent)
        self.editor_dialog.dsp_spin.hide()
        self.editor_dialog.le_1.setPlaceholderText("GFK...")
        self.editor_dialog.btn_ok.setText(self.tr("Ok"))
        self.editor_dialog.btn_cancel.setText(self.tr("Abbrechen"))
        self.editor_dialog.adjustSize()
        self.editor_dialog.le_1.setFocus()


class EditCatPosListView(EditCatDisListView):
    def __init__(self, parent=None, mask_parent=None):
        super().__init__(parent, mask_parent)


class EditAddCatListView(EditCatPosListView):
    def __init__(self, parent=None, mask_parent=None):
        super().__init__(parent, mask_parent)
