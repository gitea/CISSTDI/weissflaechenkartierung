from PyQt5 import QtCore
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QWidget, QPushButton, QHBoxLayout, QVBoxLayout, QSizePolicy, QApplication

from ...components.dialog_box.mask_dialog_base import MaskDialogBase
from ...components.widgets.buttons import Button
from ...components.widgets.line_edit import LineEdit
from ...components.widgets.spin_box import DoubleSpinBox
from ...components.table_view.table_view import TableView


class RowEditorDialog(MaskDialogBase):
    newRow = pyqtSignal(list)
    editRow = pyqtSignal(list)
    rowEdited = pyqtSignal(list)

    def __init__(self, parent=None):
        super().__init__(parent)
        self.hide()

        self.vBoxLayout = QVBoxLayout(self.widget)

        self.edit: bool = False
        self.row_to_edit: QModelIndex = None

        self.le_1 = LineEdit(self.widget)
        self.dsp_spin = DoubleSpinBox(self.widget)
        self.btn_ok = QPushButton(self.widget)
        self.btn_cancel = Button(self.widget)

        self.__init_widgets()

    def __init_widgets(self):
        """ init the widgets """
        self.windowMask.installEventFilter(self)
        self.windowMask.setMouseTracking(True)

        self.btn_ok.setFixedSize(100, 30)
        self.btn_ok.setAutoDefault(True)  # emit the clicked signal when Enter is pressed on a focused QPushButton:

        self.btn_cancel.setFixedSize(100, 30)

        self.le_1.setTitle("Objekttyp")
        self.dsp_spin.setFixedSize(250, 36)
        self.dsp_spin.setToolTip("Puffer")

        self.__connect_signals()
        self.__init_layout()

        self._set_qss()

    def __connect_signals(self):
        """ connect signals """
        self.editRow.connect(self.on_edit_row_clicked)

        self.btn_ok.clicked.connect(self.on_ok_clicked)
        self.btn_cancel.clicked.connect(lambda: self.hide())
        self.le_1.textChanged.connect(self.on_text_changed)
        # when pressed enter emit btn clicked
        self.le_1.returnPressed.connect(self.btn_ok.click)

    def __init_layout(self):
        """ init the layout """
        self.vBoxLayout.setContentsMargins(25, 5, 25, 25)
        self.vBoxLayout.setSizeConstraint(QHBoxLayout.SetFixedSize)
        self.vBoxLayout.setAlignment(Qt.AlignTop)
        self.vBoxLayout.setSpacing(6)

        self.le_1.setSizePolicy(QSizePolicy(QSizePolicy.Expanding,
                                            QSizePolicy.Preferred))

        self.dsp_spin.setSizePolicy(QSizePolicy(QSizePolicy.Expanding,
                                                QSizePolicy.Preferred))

        l3 = QHBoxLayout()
        l3.setContentsMargins(0, 0, 0, 0)
        l3.setSpacing(6)
        l3.setAlignment(Qt.AlignRight)
        l3.addWidget(self.btn_cancel)
        l3.addWidget(self.btn_ok)

        self.vBoxLayout.addWidget(self.le_1, 0)
        self.vBoxLayout.addWidget(self.dsp_spin, 0)
        self.vBoxLayout.addSpacing(6)
        self.vBoxLayout.addLayout(l3, 0)

        self.adjustSize()

    def _set_qss(self):
        """ set the qss """
        self.le_1.setObjectName("contentEdit")
        # self.btn_ok.setObjectName("primaryButton")
        # self.btn_cancel.setObjectName("secondaryButton")

    def eventFilter(self, _object, event) -> bool:
        if _object == self.windowMask:
            if event.type() == QEvent.MouseButtonPress:
                self.newRow.emit([])
                self.hide()
        return super(RowEditorDialog, self).eventFilter(_object, event)

    @QtCore.pyqtSlot()
    def on_ok_clicked(self):
        if len(self.le_1.text()) == 0:
            self.le_1.setProperty("hasError", "true")
        elif self.dsp_spin.isVisible() and self.dsp_spin.value() < 0.0:
            self.dsp_spin.setProperty("hasError", "true")
        else:
            value = self.le_1.text()
            if ',' in value or '?' in value or '#' in value:
                self.le_1.setProperty("hasError", "true")
            else:
                self.le_1.setProperty("hasError", False)
                self.dsp_spin.setProperty("hasError", False)

                # data
                data = [self.le_1.text().rstrip().lstrip(), self.dsp_spin.value()]

                # check if we edited a row
                if self.row_to_edit is not None:
                    self.rowEdited.emit([self.row_to_edit, data])
                    self.row_to_edit = None
                else:
                    self.newRow.emit(data)
                self.hide()
        self.setStyle(QApplication.style())

    def exec(self) -> int:
        self.le_1.setProperty("hasError", False)
        self.dsp_spin.setProperty("error", False)
        if not self.edit:
            self.le_1.clear()
            self.dsp_spin.setValue(0.0)
        self.le_1.setFocus()
        self.edit = False
        return super(RowEditorDialog, self).exec()

    def on_text_changed(self, p_change: str):
        """ text changed"""
        if len(p_change) == 0:
            self.le_1.setProperty("hasError", False)
        else:
            self.le_1.setToolTip(p_change)

    def on_edit_row_clicked(self, p_data: list):
        """ set the data out of the list """
        if len(p_data) != 2 or p_data[1][0] is None:
            return
        self.row_to_edit = p_data[0]
        self.dsp_spin.setValue(0 if len(p_data[1][1]) == 0 else float(p_data[1][1]))
        self.le_1.setText("" if len(p_data[1][0]) == 0 else p_data[1][0])
        self.edit = True


class EditableTableWidget(QWidget):
    def __init__(self, parent=None, mask_parent=None, size: tuple = (200, 300)):
        super().__init__(parent)
        self.mask_parent = mask_parent.parent().parent()
        self.setFixedSize(*size)
        self.btn_add = QPushButton(self)
        self.btn_del = QPushButton(self)
        self.table_view = TableView(self)
        self.editor_dialog = RowEditorDialog(self.mask_parent)
        self._init_widgets()
        self._connect_signals()

    def _init_widgets(self):
        """ init the widgets """
        self._set_qss()
        self.btn_add.resize(25, 25)
        self.btn_del.resize(25, 25)

        self._adjust_layout()

    def _connect_signals(self):
        """ connect signals of widgets """
        self.editor_dialog.newRow.connect(self.on_create_new_row)
        self.btn_add.clicked.connect(self.on_add_row_clicked)

        self.btn_del.clicked.connect(self.delete_row)
        self.table_view.deleteRow.connect(self.delete_row)

    def _adjust_layout(self) -> None:
        """ init the layout """
        self.btn_add.move(0, 0)
        self.btn_del.move(self.btn_add.x() + self.btn_add.width() + 6, 0)
        self.table_view.resize(self.width() - 1, self.height() - (self.btn_del.height() + 6 + self.btn_add.height()))
        self.table_view.move(1, self.btn_del.y() + self.btn_del.height() + 6)
        self.resize(self.width(),
                    self.table_view.height() + 6 + self.btn_del.height() + 6 + self.btn_add.height())
        self.adjustSize()

        self.editor_dialog.adjust_widget_geometry()

    def _set_qss(self):
        """ set the qss """
        self.btn_add.setObjectName("addButton")
        self.btn_del.setObjectName("deleteButton")

    def resizeEvent(self, e):
        self._adjust_layout()

    def delete_row(self):
        model = self.table_view.model()
        if model.rowCount() > 0:  # check if there are rows inside the model
            indices = self.table_view.selectionModel().selectedRows()
            for index in sorted(indices):
                model.removeRow(index.row())  # delete the row

    def on_add_row_clicked(self):
        self.editor_dialog.exec()

    def on_create_new_row(self, p_data: list):
        if len(p_data) != 2:
            return
        if not self.editor_dialog.edit:
            self.table_view.append_row(p_row=p_data)
        self.adjustSize()


class EditBufferTableView(EditableTableWidget):
    def __init__(self, parent=None, mask_parent=None):
        super().__init__(parent, mask_parent)
        # self.editor_dialog.le_1.setTitle("AX...")
        self.editor_dialog.btn_ok.setText(self.tr("Ok"))
        self.editor_dialog.btn_cancel.setText(self.tr("Abbrechen"))
        self.table_view.model().set_header(["Type", "Puffer"])
        self.btn_add.setToolTip(self.tr("Hinzufügen"))
        self.btn_del.setToolTip(self.tr("Löschen"))
        self.editor_dialog.adjustSize()
        self.editor_dialog.le_1.setFocus()
