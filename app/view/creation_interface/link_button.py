from PyQt5.QtCore import Qt, QRect
from PyQt5.QtGui import QColor, QPainter, QPixmap
from PyQt5.QtWidgets import QPushButton

from ...components.buttons.tooltip_button import TooltipPushButton


class LinkButton(TooltipPushButton):
    def __init__(self, p_text: str = "", p_images=None, p_margin: list = None, p_spacing: int = 6,
                 parent=None):
        super(LinkButton, self).__init__("", parent)

        if p_images is None:
            p_images = {}
        # images to draw
        self._images: dict = p_images

        # left, top, right, bottom
        if p_margin is None:
            self._margin: list = [5, 5, 5, 5]
        else:
            self._margin = p_margin
        self._spacing: int = p_spacing

        # current icon to draw
        self._image = QPixmap(self._images["normal"])
        # self._image = self._image.scaled(25, 25)

        # text to draw
        self._txt: str = p_text

        # colors to draw
        self._border_color = QColor("#6481e5")
        self._back_ground_color = QColor("#6481e5")
        self._text_color = QColor("#FFFFFF")

        self._init_widget()

    def adjustSize(self) -> None:
        """adjust the minimum size"""
        width: int = self._margin[0] + self._margin[2] + self._spacing + self._image.width() \
                        + self.fontMetrics().width(self._txt)
        self.setMinimumWidth(width)
        self.setMinimumHeight(self._image.height())

        self.update()

    def _init_widget(self) -> None:
        """init the sub controls"""
        self.setAttribute(Qt.WA_TranslucentBackground)
        # set the minimum width and height
        self.adjustSize()
        self.setCursor(Qt.PointingHandCursor)

    def enterEvent(self, e):
        super().enterEvent(e)
        self._image = QPixmap(self._images["hover"])
        self._border_color = QColor("#4b61ac")
        self._back_ground_color = QColor("#4b61ac")
        self._text_color = QColor("#FFFFFF")
        self.update()

    def leaveEvent(self, e):
        super().leaveEvent(e)
        self._image = QPixmap(self._images["normal"])
        self._border_color = QColor("#6481e5")
        self._back_ground_color = QColor("#6481e5")
        self._text_color = QColor("#FFFFFF")
        self.update()

    def mousePressEvent(self, e):
        self._image = QPixmap(self._images["pressed"])
        self._border_color = QColor("#4B61AB")
        self._back_ground_color = QColor("#FFFFFF")
        self._text_color = QColor("#4B61AB")
        self.update()
        super().mousePressEvent(e)

    def mouseReleaseEvent(self, e):
        self._image = QPixmap(self._images["normal"])
        self._border_color = QColor("#6481e5")
        self._back_ground_color = QColor("#6481e5")
        self._text_color = QColor("#FFFFFF")
        self.update()
        super().mouseReleaseEvent(e)

    def setText(self, text: str) -> None:
        """set the text"""
        self._txt = text
        self.adjustSize()

    def paintEvent(self, e):
        """ paint button """
        super().paintEvent(e)
        painter = QPainter(self)
        painter.setRenderHints(
            QPainter.Antialiasing
            | QPainter.TextAntialiasing
            | QPainter.SmoothPixmapTransform
        )

        # draw border
        if self._border_color is not None:
            painter.setPen(self._border_color)
        painter.setBrush(self._back_ground_color)
        painter.drawRect(QRect(0, 0, self.width(), self.height()))

        # draw pixmap
        painter.setPen(Qt.NoPen)
        painter.drawPixmap(0, 0, self._image.width(), self._image.height(), self._image)

        # draw text
        if not len(self._txt):
            return

        painter.setPen(self._text_color)
        painter.setFont(self.font())
        text = painter.fontMetrics().elidedText(self._txt, Qt.ElideRight, 320)
        image_x: int = self._margin[0] + self._image.width() + self._spacing

        # use cap height instead of height:
        # It specifically is the height of capital letters that are flat - such as H or I
        image_y: int = self.height() // 2 + painter.fontMetrics().capHeight() // 2
        painter.drawText(image_x, image_y, text)
