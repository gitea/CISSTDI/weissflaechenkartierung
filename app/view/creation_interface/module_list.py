from PyQt5.QtCore import Qt, pyqtSignal, QItemSelectionModel, pyqtSlot, QModelIndex
from PyQt5.QtGui import QBrush, QColor, QContextMenuEvent, QStandardItem
from PyQt5.QtWidgets import QAction, QApplication, QMenu

from ...common.wfk.wfk_module import WfkModule
from ...components.widgets.list_view import ListView, USER_ROLE_ROW_DATA


class ModuleListViewMenu(QMenu):
    def __init__(self, parent):
        super().__init__("", parent)
        self.setObjectName("listViewMenu")

        self.deleteSel = QAction(self)
        self.deleteSel.setText(self.tr("Löschen"))
        self.deleteSel.setShortcut("Delete")

        self.editSel = QAction(self)
        self.editSel.setText(self.tr("Bearbeiten"))
        self.editSel.setShortcut("F2")

        self.moveUp = QAction(self)
        self.moveUp.setText(self.tr("Nach oben"))

        self.moveDown = QAction(self)
        self.moveDown.setText(self.tr("Nach unten"))

        self.activate = QAction(self)
        self.activate.setText(self.tr("Aktivieren"))

        self.deactivate = QAction(self)
        self.deactivate.setText(self.tr("Deaktivieren"))

        self.action_list = [self.deleteSel, self.editSel, self.addSeparator(), self.activate,
                            self.deactivate, self.addSeparator(), self.moveUp, self.moveDown]

        self.addActions(self.action_list)

    def exec_(self, pos):
        """create menu and animation"""
        self.setStyle(QApplication.style())
        super().exec_(pos)


class ModuleListView(ListView):
    moveUp = pyqtSignal()
    moveDown = pyqtSignal()
    activate = pyqtSignal()
    deactivate = pyqtSignal()
    updateIndex = pyqtSignal(dict)
    addNewModule = pyqtSignal(WfkModule)

    def __init__(self, parent):
        super().__init__(parent)

        self.horizontalScrollBar().setVisible(True)

        self.menu = ModuleListViewMenu(self)

        self._connect_signals()

    def _connect_signals(self) -> None:
        """connect child signals"""

        # menu signals
        self.menu.deleteSel.triggered.connect(self.deleteRow)
        self.menu.editSel.triggered.connect(self.editRow)
        self.menu.moveUp.triggered.connect(self.moveUp)
        self.menu.moveDown.triggered.connect(self.moveDown)
        self.menu.activate.triggered.connect(self.activate)
        self.menu.deactivate.triggered.connect(self.deactivate)

        # list signals
        self.moveUp.connect(self._move_up)
        self.moveDown.connect(self._move_down)
        self.activate.connect(self._activate)
        self.deactivate.connect(self._deactivate)
        self.updateIndex.connect(self._update_index)
        self.addNewModule.connect(self.add_new_module)

    def contextMenuEvent(self, e: QContextMenuEvent):
        """execute context menu at the given global pos"""
        if len(self.selectionModel().selectedRows()) == 1:
            # check if the given module has to be activated
            idx = self.selectedIndexes()[0]
            if idx is not None:
                data: WfkModule = idx.data(USER_ROLE_ROW_DATA)
                # run option
                if data.run:
                    self.menu.activate.setDisabled(True)
                    self.menu.deactivate.setDisabled(False)
                else:
                    self.menu.activate.setDisabled(False)
                    self.menu.deactivate.setDisabled(True)

                # move options
                if idx.row() == self.model().rowCount() - 1:
                    self.menu.moveDown.setDisabled(True)
                    self.menu.moveUp.setDisabled(False)
                elif idx.row() == 0:
                    self.menu.moveDown.setDisabled(False)
                    self.menu.moveUp.setDisabled(True)
                else:
                    self.menu.moveUp.setDisabled(False)
                    self.menu.moveDown.setDisabled(False)
            self.menu.exec_(e.globalPos())

    # #################################################################################################
    # help function

    def _is_module_inside(self, p_module_to_check: WfkModule) -> QModelIndex:
        """check if the given module exist in the root model"""
        if p_module_to_check is None:
            return None
        for row in range(self.model().rowCount()):
            idx = self.model().index(row, 0)
            if idx is not None:
                module: WfkModule = idx.data(USER_ROLE_ROW_DATA)
                if module == p_module_to_check:
                    return idx
        return None

    # #################################################################################################

    @pyqtSlot(WfkModule)
    def add_new_module(self, p_new_module: WfkModule) -> QModelIndex:
        """add a new wfk module to the list if it does not exist"""
        if p_new_module is None:
            return None
        # iterate over module and compare to each item if it is the same
        idx: QModelIndex = self._is_module_inside(p_module_to_check=p_new_module)
        if idx is None:
            item = QStandardItem(p_new_module.name)
            item.setData(p_new_module, USER_ROLE_ROW_DATA)
            self.model().appendRow(item)
            idx = self.model().index(self.model().rowCount() - 1, 0)
        # if module exists, update it
        self._update_index({"index": idx, "data": p_new_module})
        # scroll to the updated or added index
        self.scrollTo(idx)
        self.selectionModel().select(idx, QItemSelectionModel.ClearAndSelect)
        return idx

    @pyqtSlot(dict)
    def _update_index(self, p_data: dict) -> None:
        """update the given index with the given data from tuple"""
        if p_data is not None:
            if len(p_data) == 2:
                idx = p_data.get("index", None)
                new_data: WfkModule = p_data.get("data", None)

                if idx is not None and new_data is not None:
                    # get item at given index
                    item = self.model().item(idx.row(), 0)
                    if item is not None:
                        item.setText(new_data.name)
                        item.setData(new_data, USER_ROLE_ROW_DATA)
                        item.setToolTip("")

                        if not new_data.run:
                            font = item.font()
                            font.setItalic(True)
                            font.setStrikeOut(True)
                            item.setFont(font)
                            item.setData(QBrush(QColor(30, 35, 40, 150)), Qt.ForegroundRole)
                            item.setToolTip(self.tr("Modul ist deaktiviert"))
                        else:
                            item.setFont(self.font())
                            item.setData(QBrush(QColor(30, 35, 40)), Qt.ForegroundRole)

    @pyqtSlot()
    def _activate(self) -> None:
        """activate the selected module"""
        indexes = self.selectedIndexes()
        if len(indexes) > 0:
            try:
                indexes.sort()
                for idx in indexes:
                    if idx is not None:
                        data: WfkModule = idx.data(USER_ROLE_ROW_DATA)
                        data.run = True
                        self._update_index({"index": idx, "data": data})
            except Exception as e:
                print(e)

    @pyqtSlot()
    def _deactivate(self) -> None:
        """activate the selected module"""
        indexes = self.selectedIndexes()
        if len(indexes) > 0:
            try:
                indexes.sort()
                for idx in indexes:
                    if idx is not None:
                        data: WfkModule = idx.data(USER_ROLE_ROW_DATA)
                        data.run = False
                        self._update_index({"index": idx, "data": data})
            except Exception as e:
                print(e)

    @pyqtSlot()
    def _move_up(self) -> None:
        """move selected item up"""
        indexes = self.selectedIndexes()
        if len(indexes) > 0:
            try:
                indexes.sort()
                first_row = indexes[0].row() - 1
                if first_row >= 0:
                    for idx in indexes:
                        if idx is not None:
                            txt = idx.data()
                            data = idx.data(USER_ROLE_ROW_DATA)
                            fore_ground = idx.data(Qt.ForegroundRole)
                            tool_tip = idx.data(Qt.ToolTipRole)
                            font = idx.data(Qt.FontRole)
                            row = idx.row()

                            self.model().insertRow(row - 1)
                            pre_idx = self.model().index(row - 1, 0)
                            self.model().removeRow(row + 1)

                            # reset the data
                            self.model().setData(pre_idx, txt, Qt.EditRole)
                            self.model().setData(pre_idx, data, USER_ROLE_ROW_DATA)
                            self.model().setData(pre_idx, fore_ground, Qt.ForegroundRole)
                            self.model().setData(pre_idx, font, Qt.FontRole)

                            try:
                                if len(tool_tip) > 0:
                                    self.model().setData(pre_idx, tool_tip, Qt.ToolTipRole)
                            except Exception as e:
                                pass
                            self.selectionModel().select(pre_idx, QItemSelectionModel.ClearAndSelect)
            except Exception as e:
                print(e)

    @pyqtSlot()
    def _move_down(self) -> None:
        """move selected item down"""
        max_row = self.model().rowCount()
        indexes = self.selectedIndexes()
        if len(indexes) > 0:
            try:
                indexes.sort()
                last_row = indexes[-1].row() + 1
                if last_row < max_row:
                    for idx in reversed(indexes):
                        if idx is not None:
                            txt = idx.data()
                            data = idx.data(USER_ROLE_ROW_DATA)
                            fore_ground = idx.data(Qt.ForegroundRole)
                            tool_tip = idx.data(Qt.ToolTipRole)
                            font = idx.data(Qt.FontRole)
                            row = idx.row()

                            self.model().insertRow(row + 2)
                            post_idx = self.model().index(row + 2, 0)

                            # reset the data
                            self.model().setData(post_idx, txt, Qt.EditRole)
                            self.model().setData(post_idx, data, USER_ROLE_ROW_DATA)
                            self.model().setData(post_idx, fore_ground, Qt.ForegroundRole)
                            self.model().setData(post_idx, font, Qt.FontRole)

                            try:
                                if len(tool_tip) > 0:
                                    self.model().setData(post_idx, tool_tip, Qt.ToolTipRole)
                            except Exception as e:
                                pass

                            self.model().removeRow(row)
                            self.selectionModel().select(self.model().index(row + 1, 0),
                                                         QItemSelectionModel.ClearAndSelect)
            except Exception as e:
                print(e)
