import os

from PyQt5.QtCore import Qt, QTimer, QDir, QModelIndex, pyqtSlot, QSize
from PyQt5.QtGui import QStandardItemModel, QKeySequence, QIcon
from PyQt5.QtWidgets import QLabel, QFileDialog, QApplication, QShortcut, QFrame, \
    QHBoxLayout, QVBoxLayout

from ...common import os_utils, str_utils
from ...common.config import config
from ...common.signal_bus import signalBus

from ...components.buttons.svg_button import ThreeStateSvgButton
from ...components.dialog_box.message_box import MessageDialog

from .link_button import LinkButton
from .module_dialog import ModuleDialog
from .module_edit_bar import ModuleEditBar
from .module_list import ModuleListView
from ...common.qgis.units import AreaUnit
from ...common.wfk.wfk_config import WfkConfig
from ...common.wfk.wfk_module import WfkModule
from ...components.buttons.tooltip_button import TooltipPushButton
from ...components.widgets.combo_box import ComboBox
from ...components.widgets.flow_layout import FlowLayout
from ...components.widgets.interfaces import BaseProcessInterface
from ...components.widgets.spin_box import DoubleSpinBox
from ...components.widgets.check_box import TextCheckBox
from ...components.widgets.line_edit import LineEdit
from ...components.widgets.list_view import USER_ROLE_ROW_DATA


class CreationInterface(BaseProcessInterface):
    """creation interface"""

    def __init__(self, parent=None):
        super().__init__(parent)
        # layout
        self._layout = FlowLayout()
        # loaded configs
        self.wfk_config: WfkConfig = None

        # module wizard
        self.module_wizard = None

        # wfk label
        self.btn_return = TooltipPushButton("", self)
        self.btn_open_config_in_explorer = LinkButton(parent=self._scroll_widget,
                                                      p_images={
                                                          "hover": ":/images/line_edit/add_folder_hover.png",
                                                          "normal": ":/images/line_edit/add_folder_active.png",
                                                          "pressed": ":/images/line_edit/add_folder_pressed.png"
                                                      },
                                                      p_text=self.tr("Im Verzeichnis öffnen"))
        self.btn_save_config = ThreeStateSvgButton(parent=self,
                                                   p_images={"normal": ":/images/button/save_active.svg"},
                                                   p_text=self.tr("Speichern"))

        # #####################################################
        # common

        self.btn_root_dir = TooltipPushButton("", self._scroll_widget)
        self.le_root_dir = LineEdit(self._scroll_widget)

        self.le_wfk_layer_name = LineEdit(self._scroll_widget)

        self.le_analysis_layer_name = LineEdit(self._scroll_widget)
        self.le_config_name = LineEdit(self._scroll_widget)

        self.dsb_potential_area_size = DoubleSpinBox(self._scroll_widget)
        self.cb_potential_area_unit = ComboBox(self._scroll_widget)
        self.l_area = QLabel(self.tr("Minimale Potentialflächengröße"), self._scroll_widget)

        # activate the configuration or not
        self.check_box_activate_config = TextCheckBox(parent=self._scroll_widget)

        # ########################################################
        # module
        self.info_label_module = QLabel(self.tr("Ausschlusskriterien"), self._scroll_widget)
        self.info_label_financial_support = QLabel(self.tr("Gunstkriterien"), self._scroll_widget)
        self.info_label_consideration = QLabel(self.tr("Abwägungskriterien"), self._scroll_widget)

        self.btn_question = ThreeStateSvgButton(parent=self._scroll_widget,
                                                p_images={"normal": ":/images/button/question_active_black.svg"})

        self.btn_question_consideration = ThreeStateSvgButton(parent=self._scroll_widget,
                                                              p_images={
                                                                  "normal": ":/images/button/question_active_black.svg"})

        self.btn_question_financial_supported = ThreeStateSvgButton(parent=self._scroll_widget,
                                                                    p_images={
                                                                        "normal": ":/images/button"
                                                                                  "/question_active_black.svg"})

        # list view
        self.list_view_modules = ModuleListView(self._scroll_widget)
        self.tool_bar_modules = ModuleEditBar(parent=self._scroll_widget, iconSize=(25, 25))

        self.list_view_financial_support = ModuleListView(self._scroll_widget)
        self.tool_bar_financial_support = ModuleEditBar(parent=self._scroll_widget, iconSize=(25, 25))

        self.list_view_consideration = ModuleListView(self._scroll_widget)
        self.tool_bar_consideration = ModuleEditBar(parent=self._scroll_widget, iconSize=(25, 25))

        # init the ui
        self._init_widget()
        self._scroll_widget.adjustSize()
        self._create_actions()

    # ##############################################

    def _adjust_layout(self):
        """adjust the layout"""
        super()._adjust_layout()
        if self.module_wizard is not None:
            self.module_wizard.adjust_widget_geometry()
        self._adjust_height()

        self.btn_save_config.move(
            self.width() - self.btn_save_config.width() - 20,
            self.height() - self.btn_save_config.height() - 20
        )

    def _adjust_height(self) -> None:
        """
        adjust layout height for product cards
        :return: None
        """
        h = self._layout.heightForWidth(self.width())
        self._scroll_widget.resize(self.width(), h)

    # ##############################################

    def _create_actions(self) -> None:
        """ create and add actions """
        self.save_short = QShortcut(QKeySequence(Qt.CTRL + Qt.Key_S), self._scroll_widget)
        self.save_short.activated.connect(self._on_save_action_triggered)

    # ##############################################

    def _connect_signals(self):
        """ connect the signals to the widgets"""
        super()._connect_signals()
        # common
        self.btn_open_config_in_explorer.clicked.connect(self._on_open_config_in_explorer)
        self.btn_root_dir.clicked.connect(self._on_select_root_dir)
        self.btn_save_config.clicked.connect(self._on_save_config_clicked)
        self.btn_return.clicked.connect(signalBus.switchToWfkInterfaceSig.emit)

        self.le_config_name.textChanged.connect(self._on_config_name_changed)

        # checkbox
        self.check_box_activate_config.check_box.stateChanged.connect(self._on_check_box_status_changed)

        # normal modules
        self.list_view_modules.doubleClicked.connect(self.on_list_view_module_double_clicked)
        self.list_view_modules.editRow.connect(self.on_list_view_module_edit_row_clicked)
        self.list_view_modules.deleteRow.connect(self.on_list_view_module_delete_row)

        self.tool_bar_modules.btn_add.clicked.connect(self._on_open_module_wizard_normal_clicked)
        self.tool_bar_modules.btn_del.clicked.connect(self.on_list_view_module_delete_row)
        self.tool_bar_modules.btn_edit.clicked.connect(self.on_list_view_module_edit_row_clicked)
        self.tool_bar_modules.btn_move_down.clicked.connect(self.list_view_modules.moveDown)
        self.tool_bar_modules.btn_move_up.clicked.connect(self.list_view_modules.moveUp)

        # financial toolbar
        self.list_view_financial_support.doubleClicked.connect(self.on_list_view_module_financial_double_clicked)
        self.list_view_financial_support.editRow.connect(self.on_list_view_module_financial_support_edit_row_clicked)
        self.list_view_financial_support.deleteRow.connect(self.on_list_view_module_financial_support_delete_row)

        self.tool_bar_financial_support.btn_move_down.clicked.connect(self.list_view_financial_support.moveDown)
        self.tool_bar_financial_support.btn_move_up.clicked.connect(self.list_view_financial_support.moveUp)
        self.tool_bar_financial_support.btn_add.clicked.connect(self._on_open_module_wizard_financial_clicked)
        self.tool_bar_financial_support.btn_del.clicked.connect(self.on_list_view_module_financial_support_delete_row)
        self.tool_bar_financial_support.btn_edit.clicked.connect(
            self.on_list_view_module_financial_support_edit_row_clicked
        )

        # consideration module
        self.list_view_consideration.doubleClicked.connect(self.on_list_view_module_consideration_double_clicked)
        self.list_view_consideration.editRow.connect(self.on_list_view_module_consideration_edit_row_clicked)
        self.list_view_consideration.deleteRow.connect(self.on_list_view_module_consideration_delete_row)

        self.tool_bar_consideration.btn_add.clicked.connect(self._on_open_module_wizard_consideration_clicked)
        self.tool_bar_consideration.btn_del.clicked.connect(self.on_list_view_module_consideration_delete_row)
        self.tool_bar_consideration.btn_edit.clicked.connect(self.on_list_view_module_consideration_edit_row_clicked)
        self.tool_bar_consideration.btn_move_down.clicked.connect(self.list_view_consideration.moveDown)
        self.tool_bar_consideration.btn_move_up.clicked.connect(self.list_view_consideration.moveUp)

        # signal bus signals
        signalBus.newWfkConfigSelectedSig.connect(self._on_create_new_wfk_config)
        signalBus.editWfkConfigSig.connect(self._on_edit_wfk_config)

        signalBus.newModuleSig.connect(self._on_module_changed)

    def _init_widget(self) -> None:
        """
        init widgets

        :return: None
        """
        super()._init_widget()

        # general
        self.btn_save_config.images = {
            "hover": ":/images/button/save_hover.svg",
            "normal": ":/images/button/save_active.svg",
            "pressed": ":/images/button/save_pressed.svg"
        }

        # line edits
        self.le_config_name.setFixedHeight(50)
        self.le_config_name.setTitle(self.tr("Name der Konfiguration"))
        self.le_root_dir.setFixedHeight(50)
        self.le_root_dir.setTitle(self.tr("Root Verzeichnis"))

        self.btn_root_dir.setIcon(QIcon(":/images/utils/add_file.svg"))
        self.btn_root_dir.setIconSize(QSize(20, 20))
        self.btn_root_dir.setFixedSize(32, 32)
        self.btn_root_dir.setToolTip(self.tr("Neues Verzeichnis auswählen"))

        self.le_wfk_layer_name.setFixedHeight(50)
        self.le_wfk_layer_name.setTitle(self.tr("Wfk Layer-Name in QGis"))
        self.le_analysis_layer_name.setFixedHeight(50)
        self.le_analysis_layer_name.setTitle(self.tr("Analyse Layer-Name in QGis"))

        # init the unit of min potential area
        self.cb_potential_area_unit.addItem(str(AreaUnit.KM.value), AreaUnit.KM)
        self.cb_potential_area_unit.setCursor(Qt.PointingHandCursor)
        self.cb_potential_area_unit.setFixedHeight(45)

        # spin box
        self.dsb_potential_area_size.setFixedWidth(150)
        self.dsb_potential_area_size.setFixedHeight(self.cb_potential_area_unit.get_contents_rect().toRect().height())

        # buttons
        self.btn_open_config_in_explorer.setToolTip(
            str_utils.tooltip(p_capital=self.tr("Ablageort"), p_content=self.tr("Verzeichnis in dem die "
                                                                                "Konfiguration liegt öffnen")))
        self.btn_open_config_in_explorer.setFixedSize(200, 35)

        self.btn_save_config.resize(80, 80)

        # labels
        self.l_area.adjustSize()
        self.l_area.setFixedHeight(self.l_area.height())
        self.label_title.setText(self.tr("Konfiguration erstellen"))
        self.label_title.adjustSize()

        self.info_label_module.adjustSize()
        self.info_label_module.setMinimumWidth(
            self.info_label_module.fontMetrics().width(self.tr("Ausschlusskriterien"))
        )
        self.info_label_financial_support.adjustSize()
        self.info_label_consideration.adjustSize()

        # return button
        self.btn_return.setFixedSize(25, 25)
        self.btn_return.setToolTip(self.tr("Zurück"))
        self.btn_return.setCursor(Qt.PointingHandCursor)

        # checkboxes
        self.check_box_activate_config.setFixedHeight(self.check_box_activate_config.height())
        self.check_box_activate_config.setText(self.tr("Die aktuelle Konfiguration aktivieren"))
        self.check_box_activate_config.setToolTip(self.tr("Konfiguration aktivieren"))

        # #########################################
        # modules

        self.btn_question.images = {
            "hover": ":/images/button/question_hover_black.svg",
            "normal": ":/images/button/question_active_black.svg",
            "pressed": ":/images/button/question_hover_black.svg"
        }
        self.btn_question.setFixedSize(35, 35)
        self.btn_question.setCursor(Qt.WhatsThisCursor)
        self.btn_question.setToolTip(
            str_utils.tooltip(p_capital=self.tr("Ausschlusskriterien"),
                              p_content=self.tr("Für den Bau von Freiflächen-Photovoltaik- und Windkraftanlagen<br>"
                                                "werden bestimmte Flächen aufgrund rechtlicher oder tatsächlicher<br>"
                                                "Tatbestände ausgeschlossen. Zu den Ausschlusskriterien im<br>"
                                                "Untersuchungsraum könnten Umweltschutzgebiete, Naturschutzgebiete,<br>"
                                                "Wohngebiete, landwirtschaftliche Nutzflächen oder andere<br>geschützte "
                                                "Bereiche gehören."))
        )

        self.btn_question_financial_supported.images = {
            "hover": ":/images/button/question_hover_black.svg",
            "normal": ":/images/button/question_active_black.svg",
            "pressed": ":/images/button/question_hover_black.svg"
        }
        self.btn_question_financial_supported.setFixedSize(35, 35)
        self.btn_question_financial_supported.setCursor(Qt.WhatsThisCursor)
        self.btn_question_financial_supported.setToolTip(
            str_utils.tooltip(p_capital=self.tr("Gunstkriterien"),
                              p_content=self.tr("Neben Ausschluss- und Abwägungskriterien werden auch<br>Flächen"
                                                "hervorgehoben, die sich aufgrund ihrer Lage in bereits<br>"
                                                "vorbelasteten Landschaftsbereichen besonders gut für<br>den Bau"
                                                " von Anlagen eignen. Es wird angestrebt, die<br>Nutzung von "
                                                "erneuerbaren Energien in bereits vorbelasteten<br>Landschaftsteilen"
                                                " zu konzentrieren, um unbeeinträchtigte<br>oder gar "
                                                "unberührte Landschaftsbereiche zu erhalten.<br>Diese "
                                                "Flächen weisen nicht nur eine gute<br>Eignung aufgrund "
                                                "der vorhandenen Vorbelastung auf, sondern<br>teilweise "
                                                "erfolgt im Rahmen des EEG eine Vergütung<br>des auf diesen "
                                                "Flächen produzierten Stroms."))
        )

        self.btn_question_consideration.images = {
            "hover": ":/images/button/question_hover_black.svg",
            "normal": ":/images/button/question_active_black.svg",
            "pressed": ":/images/button/question_hover_black.svg"
        }
        self.btn_question_consideration.setFixedSize(35, 35)
        self.btn_question_consideration.setCursor(Qt.WhatsThisCursor)
        self.btn_question_consideration.setToolTip(
            str_utils.tooltip(p_capital=self.tr("Abwägungskriterien"),
                              p_content=self.tr("Flächen, die durch Restriktionskriterien beeinträchtigt<br>sind,"
                                                " gelten grundsätzlich als nur bedingt für die Errichtung<br>einer Anlage"
                                                " geeignet. Solche Flächen weisen häufig ein gewisses<br>"
                                                "Konfliktpotential gegenüber anderer Nutzung auf<br>und erfordern "
                                                "daher eine besondere Gewichtung bei der Abwägung<br>im Rahmen der "
                                                "Potentialflächenermittlung. In den meisten Fällen<br>ist eine "
                                                "standortbezogene Einzelfallprüfung<br>erforderlich, um festzustellen,"
                                                " ob der Standort dennoch für die<br>Errichtung einer "
                                                "Anlage geeignet ist."))
        )

        # list view
        self.list_view_modules.resize(270, 200)
        self.list_view_modules.setModel(QStandardItemModel())

        self.list_view_financial_support.resize(270, 200)
        self.list_view_financial_support.setModel(QStandardItemModel())

        self.list_view_consideration.resize(270, 200)
        self.list_view_consideration.setModel(QStandardItemModel())

        self._init_layout()

        # update buttons
        self._on_update_buttons()

        # set tab order
        self.setTabOrder(self.le_config_name, self.le_wfk_layer_name)
        self.setTabOrder(self.le_wfk_layer_name, self.le_analysis_layer_name)
        self.setTabOrder(self.le_analysis_layer_name, self.dsb_potential_area_size)

    def _init_layout(self) -> None:
        """ init the layout """
        super()._init_layout()
        # general
        f1 = QFrame()
        f1.setFrameShape(QFrame.Shape.NoFrame)
        l1 = QVBoxLayout()
        l1.setContentsMargins(0, 0, 0, 9)
        l1.setSpacing(6)
        l11 = QHBoxLayout()
        l11.setContentsMargins(0, 0, 0, 0)
        l11.setSpacing(6)
        l11.addWidget(self.le_root_dir)
        l12 = QVBoxLayout()
        l12.setContentsMargins(0, 0, 0, 0)
        l12.addStretch(1)
        l12.addWidget(self.btn_root_dir)
        l11.addLayout(l12)
        l13 = QHBoxLayout()
        l13.setContentsMargins(0, 0, 0, 0)
        l13.setSpacing(6)
        l13.addWidget(self.btn_return)
        l13.addWidget(self.label_title)
        l1.addLayout(l13)
        l1.addLayout(l11)
        l1.addWidget(self.le_config_name)
        l1.addWidget(self.le_wfk_layer_name)
        l1.addWidget(self.le_analysis_layer_name)
        f1.setLayout(l1)
        f1.setFixedSize(323, self.le_analysis_layer_name.height() * 5)
        # config
        f2 = QFrame()
        f2.setFrameShape(QFrame.Shape.NoFrame)
        l2 = QVBoxLayout()
        l2.setContentsMargins(0, 0, 0, 0)
        l2.setSpacing(6)
        l2.addWidget(self.l_area)
        l21 = QHBoxLayout()
        l21.setContentsMargins(0, 0, 0, 0)
        l21.setSpacing(6)
        l21.addWidget(self.dsb_potential_area_size)
        l21.addWidget(self.cb_potential_area_unit)
        l2.addLayout(l21)
        l2.addWidget(self.check_box_activate_config)
        l2.addWidget(self.btn_open_config_in_explorer)
        f2.setLayout(l2)
        f2.setFixedSize(323, self.cb_potential_area_unit.height() * 5)
        # normal modules
        height_modules = self.list_view_modules.height() + self.tool_bar_modules.height() + self.info_label_module.height()
        #
        f3 = QFrame()
        f3.setFrameShape(QFrame.Shape.NoFrame)
        l3 = QVBoxLayout()
        l3.setContentsMargins(0, 0, 0, 0)
        l3.setSpacing(6)
        l31 = QHBoxLayout()
        l31.setContentsMargins(0, 0, 0, 0)
        l31.setSpacing(6)
        l31.addWidget(self.btn_question)
        l31.addWidget(self.info_label_module)
        l31.addStretch()
        l3.addLayout(l31)
        l3.addWidget(self.tool_bar_modules)
        l3.addWidget(self.list_view_modules)
        f3.setLayout(l3)
        f3.setFixedSize(323, height_modules)
        # financial supported modules
        f4 = QFrame()
        f4.setFrameShape(QFrame.Shape.NoFrame)
        l4 = QVBoxLayout()
        l4.setContentsMargins(0, 0, 0, 0)
        l4.setSpacing(6)
        l41 = QHBoxLayout()
        l41.setContentsMargins(0, 0, 0, 0)
        l41.setSpacing(6)
        l41.addWidget(self.btn_question_financial_supported)
        l41.addWidget(self.info_label_financial_support)
        l41.addStretch(1)
        l4.addLayout(l41)
        l4.addWidget(self.tool_bar_financial_support)
        l4.addWidget(self.list_view_financial_support)
        f4.setLayout(l4)
        f4.setFixedSize(323, height_modules)
        # financial supported modules
        f5 = QFrame()
        f5.setFrameShape(QFrame.Shape.NoFrame)
        l5 = QVBoxLayout()
        l5.setContentsMargins(0, 0, 0, 0)
        l5.setSpacing(6)
        l51 = QHBoxLayout()
        l51.setContentsMargins(0, 0, 0, 0)
        l51.setSpacing(6)
        l51.addWidget(self.btn_question_consideration)
        l51.addWidget(self.info_label_consideration)
        l51.addStretch(1)
        l5.addLayout(l51)
        l5.addWidget(self.tool_bar_consideration)
        l5.addWidget(self.list_view_consideration)
        f5.setFixedSize(323, height_modules)
        f5.setLayout(l5)

        self._layout.addWidget(f1)
        self._layout.addWidget(f2)
        self._layout.addWidget(f3)
        self._layout.addWidget(f5)
        self._layout.addWidget(f4)

    def _set_qss(self) -> None:
        """ set the style sheet """
        self.btn_return.setObjectName("returnButton")
        # labels
        self.l_area.setObjectName("subTitleLabel")
        self.info_label_module.setObjectName("subTitleLabel")
        self.info_label_financial_support.setObjectName("subTitleLabel")
        self.info_label_consideration.setObjectName("subTitleLabel")

        super()._set_qss()

    # ################################################################
    # helper functions

    def _on_save_action_triggered(self) -> None:
        """short cut triggered ctrl + s"""
        if self._on_save_config_triggered():
            signalBus.showCompleteToastTip.emit(self.tr("Konfiguration gespeichert!"))
        else:
            signalBus.showErrorToastTip.emit(self.tr("Konfiguration konnte nicht gespeichert werden!"))

    def _on_update_buttons(self) -> None:
        """update the buttons after list interactions"""
        toolbars = [
            [self.list_view_modules.model().rowCount(), self.tool_bar_modules.buttons()[1:]],
            [self.list_view_financial_support.model().rowCount(), self.tool_bar_financial_support.buttons()[1:]],
            [self.list_view_consideration.model().rowCount(), self.tool_bar_consideration.buttons()[1:]]
        ]
        for toolbar in toolbars:
            for btn in toolbar[1]:
                btn.setCursor(Qt.ArrowCursor if toolbar[0] == 0 else Qt.PointingHandCursor)
                btn.setDisabled(True if toolbar[0] == 0 else False)

    def _process_root_dir(self, p_file: str) -> None:
        """process a new configuration file"""
        # if the user edited the config by itself or the directory does not exist
        if not os.path.isdir(p_file):
            return
        # get the name of the file as input text
        length, dirs = os_utils.split_directory(p_directory=QDir.toNativeSeparators(p_file))
        self.le_root_dir.setText(dirs[length - 1])
        # set the full path as input tooltip
        self.le_root_dir.setToolTip(p_file)
        # reset the line edit style
        self.le_root_dir.setProperty("isWrong", False)

    # ################################################################
    # frontend widget events

    @pyqtSlot(str)
    def _on_config_name_changed(self, p_new_str: str) -> None:
        """check fi the config name is empty"""
        if len(self.le_config_name.text()) == 0:
            self.le_config_name.setProperty("hasError", True)
        else:
            self.le_config_name.setProperty("hasError", False)
        self.setStyle(QApplication.style())

    @pyqtSlot()
    def _on_select_root_dir(self) -> None:
        """select a root folder"""
        # open the directory
        result = QFileDialog.getExistingDirectory(self,
                                                  self.tr("Rootverzeichnis auswählen"),
                                                  os.path.join(self.le_root_dir.toolTip(),
                                                               self.le_root_dir.text()
                                                               ),
                                                  QFileDialog.ShowDirsOnly
                                                  )
        if not len(result) == 0 and os.path.exists(result):
            self._process_root_dir(result)

    @pyqtSlot()
    def _on_open_config_in_explorer(self) -> None:
        """open the directory of the config"""
        if os.path.exists(config.path):
            QTimer.singleShot(0, lambda: os_utils.show_in_folder(config.path))
            signalBus.showInfoToastTip.emit(self.tr("Verzeichnis öffnen"))
        else:
            signalBus.showErrorToastTip.emit(self.tr("Verzeichnis existiert nicht mehr!"))

    @pyqtSlot()
    def _on_check_box_status_changed(self) -> None:
        """triggered if the status changed"""
        if self.check_box_activate_config.isChecked():
            self.check_box_activate_config.setText(self.tr("Die aktuelle Konfiguration deaktivieren"))
            self.check_box_activate_config.setToolTip(self.tr("Konfiguration deaktivieren"))
        else:
            self.check_box_activate_config.setText(self.tr("Die aktuelle Konfiguration aktivieren"))
            self.check_box_activate_config.setToolTip(self.tr("Konfiguration aktivieren"))

    # ################################################################
    # business logic
    # ################################################################
    # module
    # module list view functions
    # create and edit modules

    def _on_open_module_wizard_clicked(self, p_financial_support: bool = False, p_consideration: bool = False) -> None:
        """open the module creation module"""
        # if there are default parameter from the ui
        # give them the module dialog constructor
        self.module_wizard = ModuleDialog(self.window(), p_default_parameter={
            "default_buffer": 0,
            "default_buffer_distance_unit": 'm',
            "default_min_area": self.dsb_potential_area_size.value()
        }, p_financial_support_area=p_financial_support, p_consideration_criteria=p_consideration)
        self.module_wizard.exec()

    # ###############################

    @pyqtSlot()
    def on_list_view_module_delete_row(self) -> None:
        """delete selected row with message box"""
        row_data = self.list_view_modules.model().data(self.list_view_modules.currentIndex(), USER_ROLE_ROW_DATA)
        if row_data is not None:
            def delete():
                # remove the selected row
                self.list_view_modules.model().removeRow(self.list_view_modules.currentIndex().row())
                # update the tool buttons above the list
                self._on_update_buttons()
                # save the config
                self._on_save_config_triggered()
                # refocus
                self.list_view_modules.raise_()
                self.list_view_modules.setFocus()

            m = MessageDialog(title=self.tr("Abfrage"),
                              content=self.tr("Soll das Modul\"" + row_data.name + "\" wirklich gelöscht werden?"),
                              parent=self.window())
            self._process_toasts.append(m)
            m.yesSignal.connect(delete)
            m.exec()

    @pyqtSlot()
    def on_list_view_module_edit_row_clicked(self) -> None:
        """edit the module at the given index"""
        self.on_list_view_module_double_clicked(None)

    def on_list_view_module_double_clicked(self, index) -> None:
        """edit the module at the given index"""
        index = self.list_view_modules.currentIndex()
        module: WfkModule = self.list_view_modules.model().data(index, USER_ROLE_ROW_DATA)
        if module is not None:
            self.module_wizard = ModuleDialog(self.window(),
                                              p_module=module,
                                              p_index=index)
            self.module_wizard.exec()

    @pyqtSlot()
    def _on_open_module_wizard_normal_clicked(self) -> None:
        """open module dialog without financial supported area feature"""
        self._on_open_module_wizard_clicked(p_financial_support=False)

    # ############################################################
    # financial supported modules

    @pyqtSlot()
    def _on_open_module_wizard_financial_clicked(self) -> None:
        """open the module creation module"""
        # if there are default parameter from the ui
        # give them the module dialog constructor
        self._on_open_module_wizard_clicked(p_financial_support=True)

    def on_list_view_module_financial_double_clicked(self, index) -> None:
        """edit the module at the given index"""
        index = self.list_view_financial_support.currentIndex()
        module: WfkModule = self.list_view_financial_support.model().data(index, USER_ROLE_ROW_DATA)
        if module is not None:
            self.module_wizard = ModuleDialog(self.window(),
                                              p_module=module,
                                              p_index=index,
                                              p_financial_support_area=True)
            self.module_wizard.exec()

    @pyqtSlot()
    def on_list_view_module_financial_support_edit_row_clicked(self) -> None:
        """edit the module at the given index"""
        self.on_list_view_module_financial_double_clicked(None)

    @pyqtSlot()
    def on_list_view_module_financial_support_delete_row(self) -> None:
        """delete selected row with message box"""
        row_data = self.list_view_financial_support.model().data(
            self.list_view_financial_support.currentIndex(), USER_ROLE_ROW_DATA)
        if row_data is not None:
            def delete():
                # remove the selected row
                self.list_view_financial_support.model().removeRow(
                    self.list_view_financial_support.currentIndex().row())
                # update the tool buttons above the list
                self._on_update_buttons()
                # save the config
                self._on_save_config_triggered()
                # refocus
                self.list_view_financial_support.raise_()
                self.list_view_financial_support.setFocus()

            m = MessageDialog(title=self.tr("Abfrage"),
                              content=self.tr("Soll das Modul\"" + row_data.name + "\" wirklich gelöscht werden?"),
                              parent=self.window())
            self._process_toasts.append(m)
            m.yesSignal.connect(delete)
            m.exec()

    # ############################################################
    # consideration modules

    @pyqtSlot()
    def _on_open_module_wizard_consideration_clicked(self) -> None:
        """open the module creation module"""
        # if there are default parameter from the ui
        # give them the module dialog constructor
        self._on_open_module_wizard_clicked(p_consideration=True)

    def on_list_view_module_consideration_double_clicked(self, index) -> None:
        """edit the module at the given index"""
        index = self.list_view_consideration.currentIndex()
        module: WfkModule = self.list_view_consideration.model().data(index, USER_ROLE_ROW_DATA)
        if module is not None:
            self.module_wizard = ModuleDialog(self.window(),
                                              p_module=module,
                                              p_index=index,
                                              p_consideration_criteria=True)
            self.module_wizard.exec()

    @pyqtSlot()
    def on_list_view_module_consideration_edit_row_clicked(self) -> None:
        """edit the module at the given index"""
        self.on_list_view_module_consideration_double_clicked(None)

    @pyqtSlot()
    def on_list_view_module_consideration_delete_row(self) -> None:
        """delete selected row with message box"""
        row_data = self.list_view_consideration.model().data(
            self.list_view_consideration.currentIndex(), USER_ROLE_ROW_DATA)
        if row_data is not None:
            def delete():
                # remove the selected row
                self.list_view_consideration.model().removeRow(
                    self.list_view_consideration.currentIndex().row())
                # update the tool buttons above the list
                self._on_update_buttons()
                # save the config
                self._on_save_config_triggered()
                # refocus
                self.list_view_consideration.raise_()
                self.list_view_consideration.setFocus()

            m = MessageDialog(title=self.tr("Abfrage"),
                              content=self.tr("Soll das Modul\"" + row_data.name + "\" wirklich gelöscht werden?"),
                              parent=self.window())
            self._process_toasts.append(m)
            m.yesSignal.connect(delete)
            m.exec()

    # ############################################################

    @pyqtSlot(tuple)
    def _on_module_changed(self, p_new_data: tuple) -> None:
        """gets emitted if the user created new module and saved it"""
        if len(p_new_data) != 3:
            return
        # check if the created module should be processed
        if p_new_data[2]:
            # check if the data is new generated
            if p_new_data[0] is not None:
                self._change_existing_module(p_exiting_index=p_new_data[0], p_existing_module=p_new_data[1])
            else:
                self._on_add_new_module(p_new_module=p_new_data[1])
            # save the config
            self._on_save_config_triggered()

    def _change_existing_module(self, p_existing_module: WfkModule, p_exiting_index: QModelIndex) -> None:
        """set the given module at the given index"""
        if not isinstance(p_existing_module, WfkModule):
            return
        if p_exiting_index is None:
            return

        # get the right module list
        if p_existing_module.financial_supported:
            list_view: ModuleListView = self.list_view_financial_support
        elif p_existing_module.consideration_criteria:
            list_view: ModuleListView = self.list_view_consideration
        else:
            list_view: ModuleListView = self.list_view_modules
        list_view.updateIndex.emit({"index": p_exiting_index, "data": p_existing_module})

    @pyqtSlot(WfkModule)
    def _on_add_new_module(self, p_new_module: WfkModule) -> None:
        """emitted if there is a new module to add to the list"""
        if not isinstance(p_new_module, WfkModule):
            return

        # get the right module list
        if p_new_module.financial_supported:
            list_view: ModuleListView = self.list_view_financial_support
        elif p_new_module.consideration_criteria:
            list_view: ModuleListView = self.list_view_consideration
        else:
            list_view: ModuleListView = self.list_view_modules
        # add new item
        idx = list_view.add_new_module(p_new_module=p_new_module)

        # set the module dialog index to the new created item
        if self.module_wizard is not None:
            self.module_wizard.current_module_index = idx

        # update the toolbar
        self._on_update_buttons()

    # #######################################################################

    def update_window(self) -> None:
        """reset the child widgets"""
        self.dsb_potential_area_size.setValue(0)
        self.le_wfk_layer_name.setText("")
        self.le_root_dir.setText("")
        self.le_root_dir.setToolTip("")
        self.le_analysis_layer_name.setText("")
        self.le_config_name.setText("")
        self.list_view_modules.model().removeRows(0, self.list_view_modules.model().rowCount())
        self.list_view_financial_support.model().removeRows(0, self.list_view_financial_support.model().rowCount())
        self.list_view_consideration.model().removeRows(0, self.list_view_consideration.model().rowCount())
        self.check_box_activate_config.setChecked(True)
        self._on_update_buttons()

    # #######################################################################
    # configuration
    # create, edit and load configuration files into creation_interface

    @pyqtSlot(dict)
    def _on_edit_wfk_config(self, p_data: dict) -> None:
        """gets emitted if the user selected a config file and wants to edit a configuration"""
        if len(p_data) > 0:
            # emit the main window who changes the interfaces
            # call this at first
            signalBus.switchToEditConfigInterfaceSig.emit()
            self.verticalScrollBar().setValue(0)

            self.wfk_config = WfkConfig()
            self.wfk_config.from_json(p_data=p_data)

            # initiate the dialog
            self._process_root_dir(p_file=self.wfk_config.root_dir)
            self.le_config_name.setText(self.wfk_config.name)
            # self.le_crs.setText(self.wfk_config.default_crs)
            self.dsb_potential_area_size.setValue(float(self.wfk_config.default_min_area))
            self.le_wfk_layer_name.setText(self.wfk_config.wfk_layer_name)
            self.le_analysis_layer_name.setText(self.wfk_config.analysis_layer_name)

            self.check_box_activate_config.setChecked(self.wfk_config.run)

            # add item with module name and module as data to list
            for module in self.wfk_config.modules:
                self._on_add_new_module(p_new_module=module)

            # set subtitles
            self.label_title.setText(self.tr("Konfiguration berarbeiten"))
            self._on_update_buttons()

    @pyqtSlot()
    def _on_create_new_wfk_config(self) -> None:
        """gets emitted if the user wants a new configuration"""
        # emit the main window who changes the interfaces
        self.label_title.setText(self.tr("Konfiguration erstellen"))
        self.verticalScrollBar().setValue(0)

        # reset ui
        self.update_window()
        self.wfk_config = WfkConfig()
        self.dsb_potential_area_size.setValue(float(config["default_min_area"]))
        self.le_config_name.setProperty("hasError", True)

        signalBus.switchToEditConfigInterfaceSig.emit()

    def _on_save_config_clicked(self) -> None:
        """called if the user clicked on save config"""
        # if the config name is empty we can not create a new config
        if len(self.le_config_name.text()) > 0:
            if self.wfk_config is not None:
                # trigger save action
                self._on_save_config_triggered()
                # # go back to wfk interface
                signalBus.showCompleteToastTip.emit(self.tr("Konfiguration gespeichert!"))
        else:
            signalBus.showErrorToastTip.emit(self.tr("Es muss erst ein Konfigurationsname eingegeben werden!"))

    def _on_save_config_triggered(self) -> bool:
        """save the data from frontend to backend"""
        if self.wfk_config is not None:
            self.wfk_config.name = self.le_config_name.text()
            self.wfk_config.wfk_layer_name = self.le_wfk_layer_name.text()
            self.wfk_config.analysis_layer_name = self.le_analysis_layer_name.text()
            self.wfk_config.run = self.check_box_activate_config.isChecked()

            # if the root dir does not exist
            if os.path.exists(self.le_root_dir.toolTip()):
                self.wfk_config.root_dir = self.le_root_dir.toolTip()
            else:
                self.wfk_config.root_dir = ""

            self.wfk_config.default_buffer = 0
            self.wfk_config.default_min_area = self.dsb_potential_area_size.value()
            self.wfk_config.default_min_area_unit = self.cb_potential_area_unit.currentText()
            # self.wfk_config.default_crs = self.le_crs.text()

            # clear and add modules
            self.wfk_config.modules.clear()
            model = self.list_view_modules.model()
            for row in range(model.rowCount()):
                self.wfk_config.modules.append(model.data(model.index(row, 0), USER_ROLE_ROW_DATA))

            # add all financial supported modules
            model = self.list_view_financial_support.model()
            for row in range(model.rowCount()):
                self.wfk_config.modules.append(model.data(model.index(row, 0), USER_ROLE_ROW_DATA))

            # add all consideration criteria modules
            model = self.list_view_consideration.model()
            for row in range(model.rowCount()):
                self.wfk_config.modules.append(model.data(model.index(row, 0), USER_ROLE_ROW_DATA))

            # save the config
            signalBus.updateWfkConfigSig.emit(self.wfk_config.to_json(), None)
            return True
        return False
