from PyQt5.QtCore import QEvent, Qt, QModelIndex, pyqtSlot
from PyQt5.QtWidgets import QLabel, QPushButton, QVBoxLayout, QHBoxLayout, QStackedWidget, \
    QSizePolicy, QWidget, QApplication

from ...common.config import config
from ...common.qgis import DistanceUnit
from ...common.signal_bus import signalBus
from ...common.style_sheet import setStyleSheet
from ...common.wfk.wfk_factories import ConnectorFactory, CategorizationFactory, DifferenceFactory
from ...common.wfk.wfk_module import WfkModule
from ...components.dialog_box.mask_dialog_base import MaskDialogBase
from ...components.dialog_box.message_box import MessageDialog
from ...components.widgets.check_box import TextCheckBox
from ...components.widgets.combo_box import ComboBox
from ...components.widgets.line_edit import LineEdit


class ModuleDialog(MaskDialogBase):
    def __init__(self, parent=None, p_module: WfkModule = None, p_default_parameter: dict = None,
                 p_index: QModelIndex = None, p_financial_support_area: bool = False,
                 p_consideration_criteria: bool = False, p_process_new_module: bool = True,
                 p_analysis_mode: bool = False):
        super().__init__(parent)
        self.hide()

        # if the user saved the module
        self._saved_clicked: bool = False
        self._current_page: str = ""

        self._process_new_module: bool = p_process_new_module   # if false, inside wfk_interface ignore the signal
        self._analysis_mode: bool = p_analysis_mode   # if true, ignore buffer order: con - cat

        # what kind of module we should create, financial support or normal
        self._financial_support_area: bool = p_financial_support_area
        self._consideration_criteria: bool = p_consideration_criteria

        # module
        self._module_to_display: WfkModule = None  # module for visual purpose
        self._module_index: QModelIndex = p_index  # will be set if a module gets edited
        self._module_to_save: WfkModule = p_module  # module which will be saved after editing

        # default parameter for new module
        self.default_parameter: dict = {} if p_default_parameter is None else p_default_parameter

        # frontend
        self.vBoxLayout = QVBoxLayout(self.widget)

        self.message_box = None

        self.cb_run_module = TextCheckBox(parent=self.widget)

        self.le_module_legend_name = LineEdit(self.widget)

        self.combo_box_object_types = ComboBox(self.widget)

        self.btn_next = QPushButton(self.tr("Weiter"), self.widget)
        self.btn_prev = QPushButton(self.tr("Zurück"), self.widget)
        self.btn_close = QPushButton(self.tr("Abbrechen"), self.widget)
        self.btn_save = QPushButton(self.tr("Anwenden"), self.widget)

        self.stacked_widget = QStackedWidget(self.widget)

        self._init_widget()

    @property
    def current_module_index(self) -> QModelIndex:
        """returns the current module index"""
        return self._module_index

    @current_module_index.setter
    def current_module_index(self, value: QModelIndex) -> None:
        """set the current module index if the current index is none"""
        if self._module_index is None and value is not None:
            self._module_index = value

    def showEvent(self, e):
        """show event"""
        # create module to display
        self._module_to_display = WfkModule()

        # new module
        if self._module_to_save is None:
            self._module_to_display.default_min_area = float(self.default_parameter.setdefault("default_min_area", 0.0))
            self.le_module_legend_name.setFocus()
        else:
            # edit module

            # copy module data to new module
            self._module_to_display.from_json(p_data=self._module_to_save.to_json())

            self.le_module_legend_name.setText(self._module_to_display.name)

        # this member is inside every module
        self.cb_run_module.setChecked(self._module_to_display.run)

        # set the checkbox text
        if self._module_to_display.run:
            self.cb_run_module.setText("Modul deaktivieren")
        else:
            self.cb_run_module.setText("Modul aktivieren")

        # check the module types, if one is none create it
        # #########################
        if self._module_to_display.connector is None:
            self._module_to_display.connector = ConnectorFactory.build("WfkLocalShapeFileConnector")
            self._module_to_display.connector.from_json({
                "crs": self.default_parameter.setdefault("default_crs", config["default_crs"])
            })

        # #########################
        # if self._module_to_display.layer is None:
        #     self._module_to_display.layer = LayerFactory.build("WfkShapeLayer")
        #     self._module_to_display.layer.from_json({
        #         "crs": self.default_parameter.setdefault("default_crs", config["default_crs"])
        #     })
        # #########################
        if self._module_to_display.difference is None:
            self._module_to_display.difference = DifferenceFactory.build("WfkBufferDifference")
            self._module_to_display.difference.from_json({
                "buffer": float(self.default_parameter.setdefault("default_buffer", 0)),
                "distance_unit": self.default_parameter.setdefault("default_buffer_distance_unit",
                                                                   DistanceUnit.M.value),
                "crs": self.default_parameter.setdefault("default_crs", config["default_crs"])
            })
        # #########################
        if self._module_to_display.categorization is None:
            self._module_to_display.categorization = CategorizationFactory.build("WfkByDistanceCategorization")
        # #########################
        # at this point the module is never None
        # select the right connector for the first page
        self._on_select_right_class(p_class=type(self._module_to_display.connector).__name__)
        self._on_type_object_changed()

        super(ModuleDialog, self).showEvent(e)

    # ##########################################################
    # frontend dialog events

    def resizeEvent(self, e) -> None:
        """resize"""
        if self.message_box is not None:
            self.message_box.adjust_widget_geometry()
        if self._module_to_display is not None:
            if self._module_to_display.difference is not None:
                self._module_to_display.difference.adjust_layout_of_masks()
            if self._module_to_display.connector is not None:
                self._module_to_display.connector.adjust_layout_of_masks()

    def eventFilter(self, _object, event) -> bool:
        """filter events"""
        if _object == self.windowMask:
            if event.type() == QEvent.MouseButtonPress:
                self._try_to_close()
        return super(ModuleDialog, self).eventFilter(_object, event)

    def keyPressEvent(self, event) -> None:
        """catch keys"""
        # if the user wants to exit the dialog with keys
        if event.key() in [Qt.Key_Return, Qt.Key_Enter, Qt.Key_Escape]:
            self._try_to_close()
        else:
            super(ModuleDialog, self).keyPressEvent(event)

    # ######################################

    def _init_widget(self) -> None:
        """init widget"""
        self._saved_clicked = False  # the user did not save the modules

        self.windowMask.resize(self.size())
        self.windowMask.setMouseTracking(True)

        self._set_qss()

        self.le_module_legend_name.setTitle(self.tr("Legendenname"))
        self._current_page = self.tr("Konnektor:")

        self.combo_box_object_types.setTitle(self.tr("Kategorie"))

        self.cb_run_module.setFixedSize(200, 30)

        self.btn_next.setFixedSize(100, 30)
        self.btn_prev.setFixedSize(100, 30)
        self.btn_close.setFixedSize(100, 30)
        self.btn_save.setFixedSize(100, 30)

        self.stacked_widget.setSizePolicy(QSizePolicy(QSizePolicy.Expanding,
                                                      QSizePolicy.Preferred))

        # init layout
        self._init_layout()

        # add possible connector classes to start
        for key, val in ConnectorFactory.registry.items():
            self.combo_box_object_types.addItem(val.setdefault('desc', 'not given!'), key)

        # init ui
        self.btn_prev.setDisabled(True)
        self.stacked_widget.show()

        self._connect_signals()

        # handle events from child
        self.windowMask.installEventFilter(self)
        self.widget.installEventFilter(self)

        # set tab order
        self.setTabOrder(self.le_module_legend_name, self.combo_box_object_types)

        self.raise_()
        self.setFocus()

    def _init_layout(self) -> None:
        """init the layout"""
        self.vBoxLayout.setContentsMargins(30, 20, 30, 20)
        self.vBoxLayout.setSizeConstraint(QVBoxLayout.SetFixedSize)
        self.vBoxLayout.setAlignment(Qt.AlignTop)

        self.vBoxLayout.addWidget(self.le_module_legend_name, 0)
        self.vBoxLayout.addWidget(self.combo_box_object_types, 0)
        self.vBoxLayout.addSpacing(15)
        self.vBoxLayout.addWidget(self.cb_run_module)

        # stacked widget
        self.vBoxLayout.addSpacing(10)
        self.vBoxLayout.addWidget(self.stacked_widget, 0)

        # buttons
        layout_1 = QHBoxLayout()
        layout_1.setContentsMargins(0, 0, 0, 0)
        layout_1.setSpacing(7)
        layout_1.addWidget(self.btn_close, 0)
        layout_1.addWidget(self.btn_save, 0)
        layout_1.addWidget(self.btn_prev, 0, Qt.AlignRight)
        layout_1.addWidget(self.btn_next, 0)

        self.vBoxLayout.addSpacing(5)
        self.vBoxLayout.addLayout(layout_1, 0)

    def _set_qss(self) -> None:
        """set qss"""
        self.windowMask.setObjectName('windowMask')
        setStyleSheet(self, "module_dialog")

    def _connect_signals(self) -> None:
        """connect the signals"""
        self.combo_box_object_types.currentTextChanged.connect(self._on_type_object_changed)

        self.cb_run_module.check_box.stateChanged.connect(self.on_module_state_changed)

        self.le_module_legend_name.textChanged.connect(self._on_module_legend_name_changed)
        self.btn_next.clicked.connect(self.on_next_clicked)
        self.btn_prev.clicked.connect(self.on_prev_clicked)
        self.btn_close.clicked.connect(self._try_to_close)
        self.btn_save.clicked.connect(self.on_save_clicked)


    # ######################################
    # frontend widget events

    @pyqtSlot()
    def _on_module_legend_name_changed(self) -> None:
        """if the module name is empty set the field red"""
        if len(self.le_module_legend_name.text()) == 0:
            self.le_module_legend_name.setProperty("hasError", True)
        else:
            self.le_module_legend_name.setProperty("hasError", False)
        self.setStyle(QApplication.style())

    @pyqtSlot()
    def _try_to_close(self) -> None:
        """try to close the dialog and aks for permission"""
        if not self._saved_clicked:
            self.message_box = MessageDialog(title=self.tr("Abfrage"),
                              content=self.tr("Das Modul wurde noch nicht gespeichert.<br>Trotzdem beenden?"),
                              parent=self.window())
            self.message_box.yesSignal.connect(self.hide)
            self.message_box.show()
        else:
            # hide will trigger delete later after animation finished
            self.hide()

    # ##########################################################
    # help functions

    def _add_items_from_factory(self, p_factory_dict: dict) -> None:
        """add the items out of the factory to combo box"""
        self.combo_box_object_types.clear()
        for key, val in p_factory_dict.items():
            self.combo_box_object_types.addItem(val.setdefault('desc', 'not given!'), key)

    def _add_financial_supported_class(self) -> None:
        """add financial supported classes to combo box"""
        self.combo_box_object_types.clear()
        # add min/ max buffer
        min_max: dict = DifferenceFactory.registry["WfkFinancialSupportedDifference"]

        # add intersection wfk
        intersect: dict = DifferenceFactory.registry["WfkFinancialSupportedIntersection"]

        # add to combobox
        self.combo_box_object_types.addItem(intersect["desc"], "WfkFinancialSupportedIntersection")
        self.combo_box_object_types.addItem(min_max["desc"], "WfkFinancialSupportedDifference")

    def _on_select_right_class(self, p_class: str) -> None:
        """selects the right str inside the combobox"""
        if len(p_class) == 0:
            return
        for i in range(self.combo_box_object_types.count()):
            if p_class == self.combo_box_object_types.itemData(i):
                self.combo_box_object_types.setCurrentIndex(i)

    def _save_module(self) -> None:
        """saves the module"""
        if self._module_to_display is None:
            return
        # save all properties from frontend into backend
        if hasattr(self._module_to_display.connector, "widget"):
            self._module_to_display.connector.from_view()

        if hasattr(self._module_to_display.categorization, "widget"):
            self._module_to_display.categorization.from_view()

        if hasattr(self._module_to_display.difference, "widget"):
            self._module_to_display.difference.from_view()

        self._module_to_display.name = self.le_module_legend_name.text()
        self._module_to_display.run = self.cb_run_module.isChecked()

        self._saved_clicked = False

    # ##########################################################
    # business logic

    def on_save_clicked(self) -> None:
        """save the module and its data from the ui"""
        # check if we did not save
        if len(self.le_module_legend_name.text()) > 0:
            # save the current module
            self._save_module()
            # user clicked save, set the value after save modules, in that
            # function the flag gets reset
            self._saved_clicked = True
            self._module_to_save = self._module_to_display

            # set value if the module is financial supported or not
            self._module_to_save.financial_supported = self._financial_support_area
            self._module_to_save.consideration_criteria = self._consideration_criteria

            signalBus.newModuleSig.emit((self._module_index, self._module_to_save, self._process_new_module))

            # self._module_to_display = None
            # self._module_to_save = None

            # we saved the module
            signalBus.showInfoToastTip.emit(self.tr("Modul gespeichert!"))
            # close module dialog after saving
            self._try_to_close()
        else:
            signalBus.showInfoToastTip.emit(self.tr("Bitte fehlende Felder befüllen!"))
        # update the border color of the buttons
        self._on_module_legend_name_changed()

    # ######################################
    # movement

    def on_prev_clicked(self) -> None:
        """got to previous page"""
        self.combo_box_object_types.currentTextChanged.disconnect()
        self.combo_box_object_types.clear()
        cur_label_txt: str = self._current_page
        if self.tr("Pufferart:") == cur_label_txt or self.tr("Suchfunktion EEG-Fläche:") == cur_label_txt:
            class_name = self.go_to_connector()
        elif self.tr("Kategorie:") == cur_label_txt:
            if not self._analysis_mode:
                self._current_page = self.tr("Pufferart:")

                # if we have a financial supported module, there are limited buffer which the user can select
                if self._financial_support_area:
                    self._current_page = self.tr("Suchfunktion EEG-Flächen:")
                    self._add_financial_supported_class()
                else:
                    self._add_items_from_factory(p_factory_dict=DifferenceFactory.registry)
                    # feature 12.01.2023
                    # there is no need for the last two objects
                    # WfkFinancialSupportedIntersection and WfkFinancialSupportedDifference
                    self.combo_box_object_types.removeItem(self.combo_box_object_types.count() - 1)
                    self.combo_box_object_types.removeItem(self.combo_box_object_types.count() - 1)

                class_name = type(self._module_to_display.difference).__name__
                self.btn_next.setDisabled(False)
            else:
                class_name = self.go_to_connector()
        else:
            self.combo_box_object_types.currentTextChanged.connect(self._on_type_object_changed)
            return
        self._on_select_right_class(p_class=class_name)
        self._on_type_object_changed()
        self.adjust_labels()
        self._save_module()
        # connect after we inserted the new strings to combobox
        self.combo_box_object_types.currentTextChanged.connect(self._on_type_object_changed)

    def go_to_connector(self) -> str:
        """
        :return:
        """
        self._current_page = self.tr("Konnektor:")
        self.btn_prev.setDisabled(True)
        self.btn_next.setDisabled(False)
        self._add_items_from_factory(p_factory_dict=ConnectorFactory.registry)
        return type(self._module_to_display.connector).__name__

    def got_to_buffer(self) -> str:
        """
        got to buffer interface
        :return: buffer class name
        """
        self._current_page = self.tr("Pufferart:")

        if self._financial_support_area:
            self._current_page = self.tr("Suchfunktion EEG-Fläche:")
            self._add_financial_supported_class()
        else:
            self._add_items_from_factory(p_factory_dict=DifferenceFactory.registry)
            # feature 12.01.2023
            # there is no need for the last two objects
            # WfkFinancialSupportedIntersection and WfkFinancialSupportedDifference
            self.combo_box_object_types.removeItem(self.combo_box_object_types.count() - 1)
            self.combo_box_object_types.removeItem(self.combo_box_object_types.count() - 1)
        return type(self._module_to_display.difference).__name__

    def go_to_category(self) -> str:
        """
        got to category interface
        :return: buffer class name
        """
        self._current_page = self.tr("Kategorie:")
        self._add_items_from_factory(p_factory_dict=CategorizationFactory.registry)
        self.btn_next.setDisabled(True)
        return type(self._module_to_display.categorization).__name__

    def on_next_clicked(self) -> None:
        """got to next page"""
        self._save_module()
        self.combo_box_object_types.currentTextChanged.disconnect()
        self.combo_box_object_types.clear()
        cur_label_txt: str = self._current_page
        # switch and case for the next page
        # set the label name, set the cb items
        if self.tr("Konnektor:") == cur_label_txt:
            if not self._analysis_mode:
                class_name = self.got_to_buffer()
                self.btn_prev.setDisabled(False)
                self.btn_next.setDisabled(True)
            else:
                class_name = self.go_to_category()
                self.btn_next.setDisabled(True)
                self.btn_prev.setDisabled(False)
        # elif self.tr("Pufferart:") == cur_label_txt or self.tr("Suchfunktion EEG-Flächen:") == cur_label_txt:
        #     if self._analysis_mode
        #     class_name = self.go_to_category()
        else:
            self.combo_box_object_types.currentTextChanged.connect(self._on_type_object_changed)
            return
        # select the right class name inside the cb
        self._on_select_right_class(p_class=class_name)
        # create a new page and select it
        self._on_type_object_changed()
        # connect after we inserted the new strings to combobox
        self.combo_box_object_types.currentTextChanged.connect(self._on_type_object_changed)
        # adjust labels
        self.adjust_labels()

    # ######################################
    #  user interaction

    # ######################################
    # select and create a new view

    def _delete_current_page_(self):
        """delete the current page"""
        w = self.stacked_widget.currentWidget()
        self.stacked_widget.removeWidget(w)
        w.deleteLater()

    def _reload_view(self, p_old, p_new):
        """reloads the data from old to new"""
        if p_old is not None:
            p_new.from_json(p_data=p_old.to_json())

    def _create_view(self, p_type: str) -> QWidget:
        """creates the widget for the given string"""
        widget: QWidget = None
        if p_type in ConnectorFactory.registry:
            old = self._module_to_display.connector
            # if the type is another class
            if p_type != self._module_to_display.connector.__class__.__name__:
                if hasattr(self._module_to_display.connector, "widget"):
                    self._module_to_display.connector.widget.deleteLater()
                    self._module_to_display.connector = None
                self._module_to_display.connector = ConnectorFactory.build(p_type)
            # reload data
            self._reload_view(p_old=old, p_new=self._module_to_display.connector)
            # create the view
            self._module_to_display.connector.create_view(self.stacked_widget)
            widget = self._module_to_display.connector.widget
        elif p_type in DifferenceFactory.registry:
            old = self._module_to_display.difference
            # build the new selected class
            self._module_to_display.difference = DifferenceFactory.build(p_type)
            # reload data
            self._reload_view(p_old=old, p_new=self._module_to_display.difference)
            # create the view
            self._module_to_display.difference.create_view(self.stacked_widget)
            widget = self._module_to_display.difference.widget
        elif p_type in CategorizationFactory.registry:
            old = self._module_to_display.categorization
            # build the new selected class
            self._module_to_display.categorization = CategorizationFactory.build(p_type)
            # reload data
            self._reload_view(p_old=old, p_new=self._module_to_display.categorization)
            # create the view
            self._module_to_display.categorization.create_view(self.stacked_widget)
            widget = self._module_to_display.categorization.widget

        return widget

    def _on_type_object_changed(self, p_type_change=""):
        """called if we need a new widget view"""
        w: QWidget = self._create_view(p_type=self.combo_box_object_types.currentData())
        if w is not None:
            self.stacked_widget.addWidget(w)
            self.stacked_widget.setFixedSize(w.size())
            self.stacked_widget.setCurrentWidget(w)

    def on_module_state_changed(self, p_state):
        """set text of module"""
        if p_state:
            self.cb_run_module.setText("Module deaktivieren")
        else:
            self.cb_run_module.setText("Module aktivieren")

    # #########################################
    # adjust layout

    def adjust_labels(self):
        """adjust the label width"""
        pass
