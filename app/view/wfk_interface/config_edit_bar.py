from PyQt5.QtCore import Qt
from PyQt5.QtGui import QResizeEvent

from app.components.buttons.svg_button import SvgPaintButton
from app.components.toolbar.edit_menu_bar import EditMenuBarSvg


class ConfigEditBar(EditMenuBarSvg):
    """Config edit/toolbar"""

    def __init__(self, p_seperator_color: str = "#FFFFFF", p_background_color: str = "#6481e5",
                 iconSize: tuple = (40, 40), parent=None):
        super().__init__(p_seperator_color, p_background_color, iconSize, parent)

        self.btn_new_file = SvgPaintButton(p_svg_path=":/images/button/file_active_white.svg", parent=self)
        self.btn_new_file.resize(*self._icon_size)

        self._buttons.insert(0, self.btn_new_file)
        self.btn_new_file.setCursor(Qt.PointingHandCursor)

    def resizeEvent(self, e: QResizeEvent) -> None:
        """resize three buttons"""
        super(ConfigEditBar, self).resizeEvent(e)

        self.btn_new_file.move(self._margin[0], self._margin[1])
        self.btn_add.move(self.btn_new_file.geometry().bottomRight().x() + self._padding, self._margin[1])

        self.btn_edit.move(self.btn_add.geometry().bottomRight().x() + self._padding, self._margin[1])
        self.btn_del.move(self.btn_edit.geometry().bottomRight().x() + self._padding, self._margin[1])

        self.setFixedSize(self.btn_del.geometry().bottomRight().x() + self._margin[2],
                          self.btn_del.geometry().bottomRight().y() + self._margin[3])
