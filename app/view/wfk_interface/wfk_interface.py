import os

from PyQt5.QtCore import *
from PyQt5.QtGui import QStandardItemModel, QKeySequence, QStandardItem, QBrush, QColor, QIcon
from PyQt5.QtWidgets import QLabel, QFileDialog, QShortcut, QFrame, QHBoxLayout, QVBoxLayout

from ...common import os_utils, str_utils
from ...common.config import config
from ...common.logger import logger
from ...common.signal_bus import signalBus
from ...common.wfk.tasks.task_info import TaskInfo
from ...common.wfk.tasks.task_result import WfkTaskResult
from ...common.wfk.wfk_config import WfkConfig
from ...common.wfk.wfk_legend_group import WfkLegendGroup
from ...components.buttons.tooltip_button import TooltipPushButton
from ...components.dialog_box.message_box import MessageDialog
from ...components.widgets.combo_box import ComboBox, USER_ROLE_LAYER_DATA
from ...components.widgets.frame import DragNDropFrame
from ...components.widgets.interfaces import BaseProcessInterface
from ...components.widgets.tooltip import QGisTaskStateToolTip

try:
    from qgis.core import QgsProject, QgsApplication
    from ...common.qgis import QGisProjectWrapper
    from ...common.wfk.tasks.wfk_task import WfkTask
    from ...common.wfk.not_used import AnalysisTask
    from ...common.qgis import QGisApiWrapper
except ImportError:
    pass


class WfkInterface(BaseProcessInterface):
    """wfk interface"""

    def __init__(self, parent=None):
        super().__init__(parent)
        # layout
        self._layout = QVBoxLayout()
        # layer
        self.cb_qgis_layer = ComboBox(self._scroll_widget)

        # config
        self.label_config = QLabel(self.tr("Neue Konfigurationsdatei"), self._scroll_widget)
        # drag and drop frame
        self.cb_select_config = ComboBox(self._scroll_widget)
        self.drag_drop_frame = DragNDropFrame(parent=self._scroll_widget, mask_parent=parent.parent())

        # buttons
        self.btn_refresh_layer = TooltipPushButton("", self._scroll_widget)
        self.btn_add_new_single_config = TooltipPushButton("", parent=self._scroll_widget)
        self.btn_remove_single_config = TooltipPushButton("", parent=self._scroll_widget)
        self.btn_edit_single_config = TooltipPushButton("", parent=self._scroll_widget)

        self.btn_start_wfk = TooltipPushButton(self.tr("Start"), self._scroll_widget)
        self.btn_close = TooltipPushButton(self.tr("Schließen"), self._scroll_widget)
        self.btn_create_config = TooltipPushButton(self.tr("Erstellen"), self._scroll_widget)

        # events
        self.installEventFilter(self)
        self.setAcceptDrops(True)
        self._init_widget()

    # ##############################################

    def _adjust_layout(self):
        """adjust the layout"""
        super()._adjust_layout()
        self.drag_drop_frame.adjust_layout()

    # ##############################################

    def _init_widget(self) -> None:
        """
        init widgets

        :return: None
        """
        super()._init_widget()
        self.label_title.hide()

        layer_desc = str_utils.tooltip(p_capital=self.tr("Untersuchungsgebiet"),
                                       p_content=self.tr("Im ersten Schritt wird das zu untersuchende Zielgebiet "
                                                         "ausgewählt.\nDazu muss der zugehörige Layer in QGIS "
                                                         "hinzugefügt und aktiviert werden, damit diese erkannt wird."))
        self.cb_qgis_layer.setToolTip(layer_desc)

        self.cb_select_config.setToolTip(
            str_utils.tooltip(p_capital=self.tr("Konfigurationen"),
                              p_content=self.tr("Eine Konfigurationsdatei kann mehrere Konfigurationen beinhalten.\n"
                                                "Es besteht die Möglichkeit für mehrere Szenarien mit verschiedenen "
                                                "Anforderungen \nund Kriterien jeweils eine Konfiguration zu erstellen.")))

        self.btn_start_wfk.setToolTip(
            str_utils.tooltip(p_capital=self.tr("Kartierung"),
                              p_content=self.tr("Starten/Anhalten der Weißflächenkartierung"))
        )

        self.btn_close.setToolTip(self.tr("Schließen"))

        # init widgets

        # config combo box layer
        # self.cb_qgis_layer.setFixedHeight(55)
        self.cb_qgis_layer.setTitle(self.tr("Untersuchungsgebiet"))
        self.cb_qgis_layer.setModel(QStandardItemModel())

        # config combo box
        #self.cb_select_config.setFixedHeight(55)
        self.cb_select_config.setTitle(self.tr("Konfiguration"))
        self.cb_select_config.setModel(QStandardItemModel())

        # buttons
        self.btn_create_config.setLayoutDirection(Qt.RightToLeft)
        self.btn_create_config.setFixedSize(100, 30)
        self.btn_create_config.setToolTip(self.tr("Eine neue Konfiguration erstellen"))

        self.btn_add_new_single_config.setIcon(QIcon(":/images/utils/config_add_normal.svg"))
        self.btn_add_new_single_config.setIconSize(QSize(16, 16))
        self.btn_add_new_single_config.setFixedSize(24, 24)
        self.btn_add_new_single_config.setToolTip(self.tr("Innerhalb der gewählten<br>Konfigurationsdatei eine neue<br>"
                                                          "Konfiguration erstellen"))

        self.btn_remove_single_config.setIcon(QIcon(":/images/utils/config_delete_normal.svg"))
        self.btn_remove_single_config.setIconSize(QSize(16, 16))
        self.btn_remove_single_config.setFixedSize(24, 24)
        self.btn_remove_single_config.setToolTip(self.tr("Innerhalb der gewählten<br>Konfigurationsdatei eine<br>"
                                                          "bestehende Konfiguration löschen"))

        self.btn_edit_single_config.setIcon(QIcon(":/images/utils/config_edit_hover.svg"))
        self.btn_edit_single_config.setIconSize(QSize(16, 16))
        self.btn_edit_single_config.setFixedSize(24, 24)
        self.btn_edit_single_config.setToolTip(self.tr("Bereits gespeicherte<br>Konfiguration bearbeiten"))

        self.btn_refresh_layer.setIcon(QIcon(":/images/utils/refresh_hover.svg"))
        self.btn_refresh_layer.setIconSize(QSize(20, 20))
        self.btn_refresh_layer.setFixedSize(32, 32)
        self.btn_refresh_layer.setToolTip(self.tr("Innerhalb der gewählten<br>Konfigurationsdatei eine<br>"
                                                         "bestehende Konfiguration löschen"))

        self.btn_start_wfk.setFixedSize(100, 30)
        self.btn_close.setFixedSize(100, 30)

        # adjust label size
        self.label_config.adjustSize()

        self._init_layout()
        self._load_config_with_settings()

    def _connect_signals(self) -> None:
        """
        connects all signals from the settings

        :return: None
        """
        super()._connect_signals()
        signalBus.refreshLayerSelection.connect(self._on_refresh_layer_selection_clicked)
        signalBus.updateWfkConfigSig.connect(self.on_update_wfk_config)

        # layer
        if config.qgis_interface is not None:
            config.qgis_interface.mapCanvas().mapCanvasRefreshed.connect(signalBus.refreshLayerSelection)
        self.btn_refresh_layer.clicked.connect(signalBus.refreshLayerSelection)
        self.cb_qgis_layer.currentIndexChanged.connect(signalBus.newLayerSelected)

        # config
        self.cb_select_config.currentIndexChanged.connect(self._on_config_changed)
        self.drag_drop_frame.dropped.connect(self._on_config_file_dropped)
        self.drag_drop_frame.clicked.connect(self._on_select_config_clicked)
        self.btn_create_config.clicked.connect(self._on_create_new_config)
        self.btn_edit_single_config.clicked.connect(self._on_edit_config)
        self.btn_add_new_single_config.clicked.connect(self._on_new_config)
        self.btn_remove_single_config.clicked.connect(self._on_delete_config)
        self.btn_close.clicked.connect(self._try_to_close)
        self.btn_start_wfk.clicked.connect(self._on_start_wfk_clicked)

    def _init_layout(self) -> None:
        """
        init the layout

        :return: None
        """
        self._layout.setContentsMargins(0, 0, 100, 0)
        self._layout.setSpacing(6)
        # refresh and select layer
        l1 = QHBoxLayout()
        l1.setContentsMargins(0, 0, 0, 6)
        l1.setSpacing(6)
        l11 = QVBoxLayout()
        l11.setContentsMargins(0, 0, 0, 7)
        l11.addStretch(1)
        l11.addWidget(self.btn_refresh_layer)
        l1.addWidget(self.cb_qgis_layer)
        l1.addLayout(l11)

        # create and drop config file
        f2 = QFrame()
        f2.setFrameShape(QFrame.Shape.NoFrame)
        l21 = QHBoxLayout()
        l21.setContentsMargins(0, 0 ,0 ,0)
        l21.setSpacing(0)
        l21.addWidget(self.label_config)
        l21.addWidget(self.btn_create_config)
        l2 = QVBoxLayout()
        l2.setContentsMargins(0, 0, 0, 0)
        l2.addLayout(l21)
        l2.addWidget(self.drag_drop_frame)
        l2.addSpacing(20)
        f2.setFixedHeight(self.btn_create_config.height() + self.drag_drop_frame.height() + 20)
        f2.setLayout(l2)
        # manipulate config
        f3 = QFrame()
        f3.setFrameShape(QFrame.Shape.NoFrame)
        l31 = QHBoxLayout()
        l31.setContentsMargins(0, 0 ,0 ,0)
        l31.setSpacing(3)
        l31.addStretch(1)
        l31.addWidget(self.btn_add_new_single_config)
        l31.addWidget(self.btn_edit_single_config)
        l31.addWidget(self.btn_remove_single_config)
        l3 = QVBoxLayout()
        l3.setContentsMargins(0, 0, 0, 0)
        l3.setSpacing(0)
        l3.addLayout(l31)
        l3.addWidget(self.cb_select_config)
        l3.addSpacing(5)
        l31 = QHBoxLayout()
        l31.setContentsMargins(0, 5, 0, 0)
        l31.setSpacing(10)
        l31.addStretch(1)
        l31.addWidget(self.btn_close)
        l31.addWidget(self.btn_start_wfk)
        l3.addLayout(l31)
        f3.setFixedHeight(self.cb_select_config.height() + self.btn_edit_single_config.height() + 15 +
                        self.btn_close.height())
        f3.setLayout(l3)

        self._layout.addLayout(l1)
        self._layout.addWidget(f2)
        self._layout.addWidget(f3)
        self._scroll_widget.adjustSize()

    def _set_qss(self) -> None:
        """sets the qss"""
        # labels
        self.label_config.setObjectName("subTitleLabel")
        # buttons
        self.btn_remove_single_config.setObjectName("remSingleConfigButton")
        self.btn_edit_single_config.setObjectName("editSingleConfigButton")
        self.btn_create_config.setObjectName("createConfigButton")
        self.btn_start_wfk.setObjectName("primaryButton")
        self.btn_close.setObjectName("secondaryButton")
        super()._set_qss()

    def _try_to_close(self) -> None:
        """
        :return: None
        """
        if self.is_busy():
            signalBus.showErrorToastTip.emit(self.tr("Es laufen noch Prozesse!"))
        else:
            signalBus.closeMainWindowSig.emit()

    # ##############################################
    # update available layer
    @pyqtSlot()
    def _on_refresh_layer_selection_clicked(self):
        """refresh the layer selection with the qgis interface"""
        try:
            text = self.cb_qgis_layer.currentText()
            self.cb_qgis_layer.clear()
            for layer in QGisProjectWrapper.get_current_available_layer(p_interface=config.qgis_interface):
                try:
                    if layer.sourceCrs().authid():
                        self.cb_qgis_layer.addItem(f"{layer.name()}  ({layer.sourceCrs().authid()})", layer)
                    else:
                        self.cb_qgis_layer.addItem(f"{layer.name()}", layer)
                except:
                    self.cb_qgis_layer.addItem(f"{layer.name()}", layer)

            if not text:
                self.cb_qgis_layer.setCurrentIndex(0)
            else:
                if self.cb_qgis_layer.index(text) >= 0:
                    self.cb_qgis_layer.setCurrentText(text)

            # emit the layer list got refreshed
            signalBus.layerSectionRefreshed.emit(
                QGisProjectWrapper.get_current_available_layer(p_interface=config.qgis_interface))
        except (ImportError, NameError) as e:
            logger.warning(str(e))
            signalBus.showErrorToastTip.emit(self.tr("Die QGIS-Ressourcen sind aktuell nicht verfügbar!"))
        except Exception as e:
            logger.warning(e)
            signalBus.showErrorToastTip.emit(self.tr("Aktuell kann nicht aktualisiert werden!"))

    # ##################################################
    # WFK

    def _on_process_finished(self) -> None:
        """
        emitted if the task should stop
        :return: None
        """
        super()._on_process_finished()
        self.btn_start_wfk.setText(self.tr("Start"))

    def _on_process_result(self, p_pr: WfkTaskResult) -> None:
        """
        process wfk process results
        :param p_pr: process information
        :return: None
        """
        if not super()._on_process_result(p_pr):
            return

        # there is text to show
        if len(p_pr.txt) > 0:
            m = MessageDialog(title=self.tr("Meldung"),
                              content=self.tr(p_pr.txt),
                              parent=self.window())
            m.cancelButton.hide()
            self._process_toasts.append(m)
            m.exec()

        if len(p_pr.layer_groups_to_protocol) == 0:
            signalBus.showErrorToastTip.emit(self.tr("Es wurden keine Ergebnis Layer erstellt!"))
            logger.debug("There are zero result layer!")
        else:
            # add layer group to map
            for group in p_pr.layer_groups_to_protocol:
                if not isinstance(group, WfkLegendGroup):
                    continue
                if not len(group.layers):
                    logger.debug(f"There are no result layers to add to group: {group.name}!")
                    continue
                if not QGisProjectWrapper.add_layers_to_group(group.name, group.layers):
                    logger.debug(f"Can not add group {group.name} with layers to qgis!")
                    signalBus.showErrorToastTip.emit(self.tr(f"{group.name} Gruppe konnte nicht hinzugefügt werden!"))

            # update specific config
            # wfk config will be saved as json and qgs vector layer can not be converted to json
            self.cb_select_config.setItemData(
                self.cb_select_config.currentIndex(),
                p_pr.wfk_config.processed_wfk_layer,
                USER_ROLE_LAYER_DATA
            )

            # inform analysis interface wfk process finished with good results
            signalBus.wfkResultSig.emit(p_pr)

            # check if the crs of the project is identical with the settings
            if p_pr.wfk_config.default_crs != QGisProjectWrapper.get_project_crs(
                    p_interface=config.qgis_interface):
                logger.warning(
                    self.tr("Das Default CRS und das QGis Projekt CRS sind unterschiedlich!"))

    def _on_start_wfk_clicked(self) -> None:
        """user clicked on wfk button"""
        if self.btn_start_wfk.text() == self.tr("Anhalten"):
            signalBus.stopWfkSig.emit()
            return

        if self.cb_qgis_layer.count() == 0:
            signalBus.showErrorToastTip.emit(self.tr("Es muss erst ein Layer ausgewählt werden!"))
            return

        # check if the qgis interface is not none
        if config.qgis_interface is None:
            signalBus.showErrorToastTip.emit(self.tr("Das QGis interface ist nicht gegeben!"))
            return

        if self.cb_select_config.count() == 0:
            signalBus.showErrorToastTip.emit(self.tr("Es sind keine Konfiguration/en erstellt worden!"))
            return

        # get the wfk configuration
        wfk_config = WfkConfig(
            QGisProjectWrapper.get_layer_by_id(p_id=self.cb_qgis_layer.currentIndex(),
                                               p_interface=config.qgis_interface)
        )
        wfk_config.from_json(p_data=self.cb_select_config.currentData())

        # check module count
        if len(wfk_config.modules) == 0:
            signalBus.showErrorToastTip.emit(self.tr("Es sind keine Module vorhanden!"))
            return
        self.btn_start_wfk.setText(self.tr("Anhalten"))

        # create wfk proces
        self._process_running = True
        task = WfkTask(p_task_info=TaskInfo(wfk_config, "WfkTask"))
        self._process_tasks.append(task)

        # connect signals
        task.progressChanged.connect(self._on_progress_changed)
        task.result.connect(self._on_process_result)
        signalBus.wfkFinishedSig.connect(self._on_process_finished)
        signalBus.stopWfkSig.connect(self._on_stop_tasks)

        # create loading widget
        self._process_toasts.append(
            QGisTaskStateToolTip(
                title=self.tr("WFK-Prozess"), content=self.tr("Prozess zu 0% ausgeführt"), parent=self.window()
            )
        )
        QgsApplication.taskManager().addTask(task)

    # ################################################################
    # configuration manipulation

    def _load_config_with_settings(self) -> None:
        """
        :return: None
        """
        # check if the search inside project flag is activated
        config_path: str = ""
        if config["search_config_in_project_dir"]:
            config_path = os.path.join(QgsProject.instance().absolutePath(), "configuration.cfg")

        # check if there is a config inside the plugin dir
        if config["search_config_in_plugin_dir"]:
            config_path = os.path.join(config.plugin_dir, "configuration.cfg")

        # the project and plugin configurations have the priority
        if os.path.exists(config_path):
            config.path = config_path

        # if there is no configuration, check the last config path
        if config.set_default("open_last_config", False):
            config_path = config.path

        # process the config
        if os.path.exists(config_path):
            self._process_config_file(p_file=config_path)
        else:
            config.path = ""

    ##################################
    # edit config
    def _process_config_file(self, p_file: str) -> bool:
        """process a new configuration file"""
        if self.is_busy():
            signalBus.showInfoToastTip.emit(self.tr("Kann aktuell nicht gemacht werden!"))
            return True
        # get the name of the file as input text
        d, f = os_utils.split_file_path(p_file=QDir.toNativeSeparators(p_file))
        if len(f) > 0:
            # try to load json into config
            if config.read_config(p_path=p_file):
                # clear view
                self.cb_select_config.clear()
                # update view
                if not self._update_view_wfk_config():
                    signalBus.showInfoToastTip.emit(self.tr("Es sind Konfigurationen ohne Namen vorhanden!"))
                    return False
                return True
            else:
                signalBus.showErrorToastTip.emit(self.tr("Die Konfiguration ist beschädigt!"))
        else:
            signalBus.showErrorToastTip.emit(self.tr("Der Pfad existiert nicht mehr!"))
        return False

    def _create_config_item(self, p_config: dict) -> QStandardItem:
        """
        create well-formed item
        :param p_config:
        :return: QStandardItem
        """
        item = QStandardItem(p_config.setdefault("name", "").rstrip().lstrip())
        item.setData(p_config, Qt.UserRole)

        # set text style if the module is deactivated / activated
        if not p_config.setdefault("run", True):
            font = item.font()
            font.setItalic(True)
            font.setStrikeOut(True)
            item.setFont(font)
            item.setData(QBrush(QColor(30, 35, 40, 150)), Qt.ForegroundRole)
            item.setToolTip(self.tr("Konfiguration ist deaktiviert"))
        else:
            item.setToolTip("")
            item.setFont(self.font())
            item.setData(QBrush(QColor(30, 35, 40)), Qt.ForegroundRole)
        return item

    def _update_view_wfk_config(self) -> bool:
        """updates the view, refreshing from config"""
        well_cfg_names: bool = True
        for new_config in config["configuration"]:
            if isinstance(new_config, dict):
                new_config_name = new_config.setdefault("name", "").rstrip().lstrip()
                if len(new_config_name) == 0:
                    well_cfg_names = False
                    continue
                pos: int = self._config_pos(new_config_name)
                # check if the new config is inside
                if pos == -1:
                    # add new item and create layer out of config
                    self.cb_select_config.model().appendRow(self._create_config_item(p_config=new_config))
                    self.cb_select_config.setItemData(
                        self.cb_select_config.model().rowCount()-1,
                        new_config.get("wfk_layer_path", None),
                        USER_ROLE_LAYER_DATA
                    )
                else:
                    # get the old config and save the changed
                    index: QModelIndex = self.cb_select_config.model().index(pos, 0)
                    if index:
                        if not new_config.setdefault("run", True):
                            font = self.font()
                            font.setItalic(True)
                            font.setStrikeOut(True)
                            self.cb_select_config.setItemData(pos, font, Qt.FontRole)
                            self.cb_select_config.setItemData(pos, QBrush(QColor(30, 35, 40, 150)), Qt.ForegroundRole)
                            self.cb_select_config.setItemData(pos, self.tr("Konfiguration ist deaktiviert"),
                                                              Qt.ToolTipRole)
                        else:
                            self.cb_select_config.setItemData(pos, self.font(), Qt.FontRole)
                            self.cb_select_config.setItemData(pos, QBrush(QColor(30, 35, 40)), Qt.ForegroundRole)
                            self.cb_select_config.setItemData(pos, "", Qt.ToolTipRole)

                        self.cb_select_config.setItemText(pos, new_config_name)
                        self.cb_select_config.setItemData(pos, new_config, Qt.UserRole)
                        self.cb_select_config.setItemData(pos, index.data(USER_ROLE_LAYER_DATA), USER_ROLE_LAYER_DATA)
        return well_cfg_names

    ##################################
    # config connectors

    @pyqtSlot(dict, object)
    def on_update_wfk_config(self, p_data: dict, p_layer: object) -> None:
        """
        Update the config with wfk_config
        :param p_data: given data
        :param p_layer: given layer
        :return: None
        """
        # set new QVector layer to combobox
        if p_layer is not None:
            pos: int = self._config_pos(p_data.setdefault("name", "").rstrip().lstrip())
            self.cb_select_config.setItemData(pos, p_layer, USER_ROLE_LAYER_DATA)

        # if config path does not exist get new one
        if not os.path.exists(config.path):
            # there are no wfk configurations, we have to create one
            result = QFileDialog.getSaveFileName(self,
                                                 self.tr("Neue Konfiguration erstellen..."),
                                                 filter=self.tr("Config Datei (*.cfg)"))[0]

            if len(result) > 0:
                # save the default data into the json file
                config.save(p_path=result)
                # process the path routine
                if self._process_config_file(result):
                    signalBus.showCompleteToastTip.emit(self.tr("Konfiguration erstellt"))

        # update internal config
        config.update_wfk_config(p_config=p_data)
        # save internal config
        config.save(p_path=config.path)
        # update view
        self._update_view_wfk_config()
        # select the last edited config
        self.cb_select_config.setCurrentText(p_data.setdefault('name', ''))

    @pyqtSlot(int)
    def _on_config_changed(self, index: int) -> None:
        """
        emitted if the user changed the config
        """
        if self.cb_select_config.currentData() is not None:
            signalBus.wfkConfigChangedSig.emit(
                self.cb_select_config.currentData(Qt.UserRole),
                self.cb_select_config.currentData(USER_ROLE_LAYER_DATA)
            )

    @pyqtSlot(list)
    def _on_config_file_dropped(self, configs: list) -> None:
        """
        :param configs: list with configs
        :return: None
        """
        if len(configs):
            if self._process_config_file(configs[0]):
                signalBus.showInfoToastTip.emit(self.tr("Konfiguration geladen!"))

    def _config_pos(self, p_config_name: str) -> int:
        """
        :param p_config_name: name
        :return: pos inside config combobox
        """
        config_name = p_config_name.rstrip().lstrip()
        for i in range(self.cb_select_config.model().rowCount()):
            index: QModelIndex = self.cb_select_config.model().index(i, 0)
            data: dict = index.data(Qt.UserRole)
            if data is not None:
                name: str = data.setdefault("name", "").rstrip().lstrip()
                if config_name == name:
                    return i
        return -1

    def _on_edit_config(self) -> None:
        """show the configuration dialog"""
        # always edit the first config if none was selected
        # if there is no config, send an empty dict
        if self.cb_select_config.count() == 0:
            signalBus.showInfoToastTip.emit(self.tr("Es sind keine Konfigurationen vorhanden!"))
            return
        if len(self.cb_select_config.currentData()):
            # send current wfk config json
            signalBus.editWfkConfigSig.emit(self.cb_select_config.currentData())
        else:
            signalBus.showInfoToastTip.emit(self.tr("Es sind keine Konfigurationen vorhanden!"))

    def _on_create_new_config(self) -> None:
        """create a new config file"""
        self._on_new_config(p_new_config=True)   # create new file

    def _on_save_config(self) -> None:
        """
        save current config to file
        :return: None
        """
        if not config.path:
            signalBus.showCompleteToastTip.emit(self.tr("Es gibt noch keine Konfiguration!"))
        else:
            config.save(p_path=config.path)

    def _on_new_config(self, p_new_config: bool = False) -> None:
        """emit we need a new configuration inside the file"""
        # there is no config file selected -> create a new file
        if not config.path or p_new_config:
            # there are no wfk configurations, we have to create one
            result = QFileDialog.getSaveFileName(self.window(),
                                                 self.tr("Neue Konfiguration erstellen..."),
                                                 filter=self.tr("Config Datei (*.cfg)"))[0]

            if len(result) > 0:
                # reset
                config.reset_configuration()
                # save the default data into the json file
                config.save(p_path=result)
                # process the path routine
                if self._process_config_file(result):
                    signalBus.showCompleteToastTip.emit(self.tr("Konfiguration erstellt"))
        else:
            # we have a configuration file, we want a new configuration inside it
            signalBus.newWfkConfigSelectedSig.emit()

    def _on_delete_config(self) -> None:
        """delete the current selected configuration"""
        if len(self.cb_select_config.currentText()) > 0 and self.cb_select_config.count() > 0:
            name_to_delete: str = self.cb_select_config.currentData()["name"]
            def delete():
                if config.delete_wfk_config(p_config_name=name_to_delete):
                    self.cb_select_config.clear()
                    signalBus.showCompleteToastTip.emit(self.tr("Konfiguration gelöscht"))
                    self._update_view_wfk_config()
                    config.save(p_path=config.path)

            m = MessageDialog(title=self.tr("Abfrage"),
                              content=self.tr("Soll die Konfig \"" + name_to_delete +
                                              "\" wirklich gelöscht werden?"),
                              parent=self.window())
            m.yesSignal.connect(delete)
            self._process_toasts.append(m)
            m.exec()
        else:
            signalBus.showInfoToastTip.emit(self.tr("Es sind keine Konfigurationen vorhanden!"))

    def _on_select_config_clicked(self):
        """open file selection window"""
        current_working_dir = ""
        if config["open_cur_config_dir"]:
            if len(config.path) > 0:
                current_working_dir, f_name = os.path.split(config.path)

        # open the directory
        result: str = os_utils.get_open_file_name(
            p_parent=self.window(),
            p_caption=self.tr("Konfigurationsdatei auswählen..."),
            p_filter=self.tr("Konfig Dateien (*.cfg)"),
            p_directory=current_working_dir
        )
        if result:
            if self._process_config_file(result):
                signalBus.showCompleteToastTip.emit(self.tr("Konfiguration geladen!"))
