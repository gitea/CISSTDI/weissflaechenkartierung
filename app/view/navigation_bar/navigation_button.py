# coding:utf-8
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QToolButton, QApplication, QHBoxLayout

from ...components.buttons.action_button import ActionButton


class ReturnButton(QToolButton):
    """ Title bar button """

    def __init__(self, size=(57, 30), parent=None):
        super().__init__(parent)
        self.resize(*size)
        self.isPressed = False
        self.__isWhiteIcon = False
        self.setProperty("isWhite", False)

    def setWhiteIcon(self, isWhite: bool):
        """ set icon color """
        if self.__isWhiteIcon == isWhite:
            return

        self.__isWhiteIcon = isWhite
        self.setProperty("isWhite", isWhite)
        self.setStyle(QApplication.style())

    def mousePressEvent(self, e):
        self.isPressed = True
        super().mousePressEvent(e)

    def mouseReleaseEvent(self, e):
        self.isPressed = False
        super().mouseReleaseEvent(e)


class NavigationButton(ActionButton):
    """Navigation push button"""

    """
        Navigation push button
        """

    def __init__(self, images: dict, txt: str, parent=None):
        super().__init__(images, parent)
        self.setFixedSize(230, 48)
        self._button.setFixedSize(24, 24)
        self.set_text(txt)

    def _init_layout(self) -> None:
        """
        :return: None
        """
        h_box = QHBoxLayout()
        h_box.setSpacing(14)
        h_box.setContentsMargins(28, 10, 0, 10)
        h_box.addWidget(self._button, 0, Qt.AlignLeft)
        h_box.addWidget(self._label, 0, Qt.AlignLeft)
        h_box.addStretch(2)

        self.setLayout(h_box)

    def is_checked(self) -> bool:
        """
        is checked
        :return: None
        """
        return self._checked

    def set_checked(self) -> None:
        """
        set checked or not
        :return: None
        """
        self._checked = True
        self.set_style_to_clicked()

    def deselect(self) -> None:
        """
        deselect button
        :return: None
        """
        self._checked = False
        self.set_style_to_normal()

    def set_style_to_clicked(self) -> None:
        """
        :return: None
        """
        if self._checked:
            self.setStyleSheet("background-color: #334894;")
            self._label.setStyleSheet("""
                            color: #C4CDF1;
                            font-family: "Segoe UI";
                            font-size: 14px;
                            font-weight: 400;
                        """)
            self._button.setStyleSheet(f"image: url({self._images['clicked']});")

    def set_style_to_hover(self) -> None:
        """
        :return: None
        """
        if not self._checked:
            self.setStyleSheet("QWidget#actionButton {}")
            self._label.setStyleSheet("""
                            color: rgba(255, 255, 255, 1);
                            font-family: "Segoe UI";
                            font-size: 14px;
                            font-weight: 400;
                        """)
            self._button.setStyleSheet(f"image: url({self._images['hover']});")

    def set_style_to_normal(self) -> None:
        if not self._checked:
            self.setStyleSheet("QWidget#actionButton {}")
            self._label.setStyleSheet("""
                        color: rgba(196, 205, 241, 1);
                        font-family: "Segoe UI";
                        font-size: 14px;
                        font-weight: 400;
                    """)
            self._button.setStyleSheet(f"image: url({self._images['normal']});")
