# coding:utf-8
from PyQt5.QtCore import QFile, pyqtSignal
from PyQt5.QtWidgets import QWidget

from ...common.style_sheet import setStyleSheet


class NavigationWidgetBase(QWidget):
    """
    Navigation widget base
    """

    selectedButtonChanged = pyqtSignal(str)

    def __init__(self, parent=None):
        super().__init__(parent)
        self._selectableButtons = []
        self._selectableButtonNames = []

        self.__buttonIndexes = {
            'wfkButton': 0,
            'analysisButton': 1,
            'settingsButton': 2
        }

        setStyleSheet(self, 'navigation')

    def _connectButtonClickedSigToSlot(self):
        """ _connect button clicked signal to slot """
        if len(self._selectableButtonNames) != len(self._selectableButtons):
            raise Exception(
                'The button list does not match the length of its corresponding name list.')

        for button, name in zip(self._selectableButtons, self._selectableButtonNames):
            if button.property('name') or name == 'logoButton':
                continue
            button.setProperty('name', name)
            button.clicked.connect(
                lambda name=name: self.__onButtonClicked(name))

    def setCurrentIndex(self, index: int) -> None:
        """ set selected button """
        names = [k for k, v in self.__buttonIndexes.items() if v == index and v != 0]
        if names:
            self.__updateButtonSelectedState(names[0])

    def setSelectedButton(self, buttonName: str) -> None:
        """ set selected button
        Parameters
        ----------
        buttonName: str
            button name, defined by `btn.property('name')`
        """
        self.__updateButtonSelectedState(buttonName)

    def __onButtonClicked(self, buttonName: str) -> None:
        """ button clicked slot """
        self.__updateButtonSelectedState(buttonName)
        self.selectedButtonChanged.emit(buttonName)

    def __updateButtonSelectedState(self, buttonName: str) -> None:
        """ update selected state of all buttons """
        if buttonName == 'logoButton':
            return
        for btn in self._selectableButtons:
            if btn.property('name') != buttonName:
                btn.deselect()
        self._selectableButtons[self.__buttonIndexes[buttonName]].set_checked()
