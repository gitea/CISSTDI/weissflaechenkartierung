# coding:utf-8
from PyQt5.QtCore import Qt, QTimer, QUrl
from PyQt5.QtGui import QDesktopServices
from PyQt5.QtWidgets import QVBoxLayout, QLabel, QPushButton, QSpacerItem, QSizePolicy, QApplication

from .navigation_button import NavigationButton
from .navigation_widget_base import NavigationWidgetBase
from ...common import os_utils
from ...components.widgets.line_widget import SeperatorWidget


class NavigationBar(NavigationWidgetBase):
    """Navigation bar"""

    def __init__(self, parent=None):
        super().__init__(parent)
        self.__createButtons()
        self.vBox = QVBoxLayout()
        self.__initWidget()

    def __createButtons(self):
        """create buttons """
        # color = "white" if config.theme == 'dark' else 'black'

        self.seperator = SeperatorWidget(parent=self, p_line_color="#758DE7")
        self.label_logo = QLabel(parent=self)
        self.label_dev = QLabel(self.tr("Entwickelt von"), parent=self)
        self.btn_shop = QPushButton(self)
        self.btn_shop.clicked.connect(lambda:
            QTimer.singleShot(
                800,
                lambda:QDesktopServices.openUrl(QUrl("https://shop.ciss.de"))
            )
        )
        self.btn_website = QPushButton(self)
        self.btn_website.clicked.connect(lambda:
            QTimer.singleShot(
                800,
                os_utils.open_email_launcher
            )
        )

        self.wfkButton = NavigationButton({
                "normal": ":/images/navigation_interface/wfk_nav_button_normal.svg",
                "hover":  ":/images/navigation_interface/wfk_nav_button_hover.svg",
                "clicked":  ":/images/navigation_interface/wfk_nav_button_clicked.svg"
            }, self.tr("Konfiguration"), self)

        self.analysisButton = NavigationButton({
                "normal": ":/images/navigation_interface/analysis_nav_button_normal.svg",
                "hover": ":/images/navigation_interface/analysis_nav_button_hover.svg",
                "clicked": ":/images/navigation_interface/analysis_nav_button_clicked.svg",
            }, self.tr("WFK-Analyse"),self)

        self.settingsButton = NavigationButton({
                "normal": ":/images/navigation_interface/settings_nav_button_normal.svg",
                "hover": ":/images/navigation_interface/settings_nav_button_hover.svg",
                "clicked": ":/images/navigation_interface/settings_nav_button_clicked.svg"
            }, self.tr("Einstellungen"),self)


        self.buttons = [
            self.wfkButton, self.analysisButton, self.settingsButton
        ]

        self._selectableButtons = self.buttons

        self._selectableButtonNames = [
            'wfkButton', 'analysisButton', 'settingsButton'
        ]

    def __initWidget(self):
        """initialize widgets"""
        self.setFixedWidth(201)
        self.setAttribute(Qt.WA_StyledBackground)
        self._connectButtonClickedSigToSlot()

        self.seperator.setFixedWidth(117)
        self.label_logo.setStyleSheet("image: url(:images/navigation_interface/ciss_logo.svg);")
        self.label_logo.setFixedSize(75, 14)

        self.btn_website.setText(self.tr("Feedback"))
        self.btn_website.setFixedSize(100, 29)
        self.btn_shop.setText(self.tr("Zum CISS Shop"))
        self.btn_shop.setFixedSize(110, 29)
        self.btn_website.setObjectName("btnWebsite")
        self.btn_shop.setObjectName("btnShop")
        self.label_dev.setFixedSize(92, 17)
        self.label_dev.setObjectName("devLabel")

        self.btn_shop.setCursor(Qt.PointingHandCursor)
        self.btn_website.setCursor(Qt.PointingHandCursor)

        self.setSelectedButton(self.wfkButton.property('name'))
        self.setStyle(QApplication.style())

        self.__initLayout()

    def __initLayout(self) -> None:
        """initialize layout"""
        self.vBox.addSpacing(55)
        self.vBox.setSpacing(0)
        self.vBox.setContentsMargins(0, 0, 0, 0)
        for button in self.buttons:
            self.vBox.addWidget(button, 0, Qt.AlignLeft)
        self.vBox.addSpacing(279)
        self.vBox.addSpacerItem(QSpacerItem(self.width(), 1, QSizePolicy.Minimum, QSizePolicy.Expanding))
        self.vBox.addWidget(self.seperator,0, Qt.AlignHCenter)
        self.vBox.addSpacing(10)
        self.vBox.addWidget(self.label_dev,0, Qt.AlignHCenter)
        self.vBox.addWidget(self.label_logo,0, Qt.AlignHCenter)
        self.vBox.addSpacing(10)
        self.vBox.addWidget(self.btn_website,0, Qt.AlignHCenter)
        self.vBox.addSpacing(5)
        self.vBox.addWidget(self.btn_shop,0, Qt.AlignHCenter)
        self.vBox.addSpacing(15)
        self.setLayout(self.vBox)
