from app.common.signal_bus import signalBus
from PyQt5.QtWidgets import QWidget

from .navigation_bar import NavigationBar


class NavigationInterface(QWidget):
    """
    Navigation interface
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.navigationBar = NavigationBar(self)
        self.__initWidget()
        self.hide()

    def __initWidget(self):
        """ initialize widgets """
        self.resize(self.navigationBar.width(), self.navigationBar.height())
        self.setCurrentIndex(0)
        self.__connectSignalToSlot()

    def __connectSignalToSlot(self) -> None:
        """connect signal to slot"""

        self.navigationBar.selectedButtonChanged.connect(
            self.__onSelectedButtonChanged)

        self.navigationBar.wfkButton.clicked.connect(
            signalBus.switchToWfkInterfaceSig
        )
        self.navigationBar.analysisButton.clicked.connect(
            signalBus.switchToAnalysisInterfaceSig
        )
        self.navigationBar.settingsButton.clicked.connect(
            signalBus.switchToSettingInterfaceSig
        )

    def resizeEvent(self, e) -> None:
        self.navigationBar.resize(self.navigationBar.width(), self.height())

    def __onSelectedButtonChanged(self, name) -> None:
        """ selected button changed slot """
        if self.navigationBar is not self.sender():
            self.navigationBar.setSelectedButton(name)

    def setCurrentIndex(self, index: int) -> None:
        """set selected button"""
        self.navigationBar.setCurrentIndex(index)
