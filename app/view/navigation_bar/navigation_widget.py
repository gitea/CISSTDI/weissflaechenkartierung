# coding:utf-8
from PyQt5 import QtWidgets

from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtGui import QPainter, QPen
from PyQt5.QtWidgets import QWidget

from app.common.config import config
from .navigation_button import PushButton, ToolButton, ReturnButton
from .navigation_widget_base import NavigationWidgetBase


class NavigationWidget(NavigationWidgetBase):
    """ Navigation widget """

    searchSig = pyqtSignal(str)

    def __init__(self, parent):
        super().__init__(parent)

        self.scrollArea = QtWidgets.QScrollArea(self)
        self.scrollWidget = ScrollWidget(self)

        # return button on the widget
        self.returnButton = ReturnButton((60, 39), self)

        self.__createButtons()
        self.__initWidget()

    def __createButtons(self):
        """ create buttons """
        color = "white" if config.theme == 'dark' else 'black'
        self.showBarButton = ToolButton(
            f":/images/navigation_interface/global_nav_button_{color}.png", parent=self)

        self.wfkButton = PushButton(
            f":/images/navigation_interface/wfk_{color}.png", self.tr("Weißflächenkartierung"), (300, 60),
            self.scrollWidget)
        self.settingButton = PushButton(
            f":/images/navigation_interface/settings_{color}.png", self.tr("Einstellungen"),
            (300, 60), self.scrollWidget)

        self.cissButton = PushButton(
            f":/images/navigation_interface/ciss_{color}.png", self.tr("Über Ciss"), (300, 60), self)

        self.currentButton = self.wfkButton
        self.lastButton = self.wfkButton

        self._selectableButtons = [
            self.wfkButton,
            self.settingButton,
            self.cissButton,
        ]

        self._selectableButtonNames = [
            "wfkButton",
            "settingButton",
            "cissButton",
        ]

    def __initWidget(self):
        """ initialize widgets """
        self.resize(300, 800)
        self.setAttribute(Qt.WA_StyledBackground)
        self.setSelectedButton(self.wfkButton.property('name'))

        # connect signal to slot
        self._connectButtonClickedSigToSlot()

        # set object name
        self.returnButton.setObjectName("returnButton")
        self.returnButton.setWhiteIcon(True)
        self.returnButton.show()

        self.__initLayout()

    def __initLayout(self):
        """ initialize layout """
        self.scrollWidget.move(0, 40)

        self.showBarButton.move(0, 40)
        self.returnButton.move(self.width() - self.returnButton.width(), 40)

        self.wfkButton.move(0, 61)
        self.settingButton.move(0, 122)

        self.__adjustScrollWidgetHeight()

    def resizeEvent(self, e):
        self.scrollArea.resize(self.width(), self.height() - 347)
        self.scrollWidget.resize(self.width(), self.scrollWidget.height())

        # move the ciss button into the bottom left corner
        self.cissButton.move(0, self.height() - self.cissButton.height() - 60)

    def paintEvent(self, e):
        """ paint seperator """
        painter = QPainter(self)
        painter.setPen(Qt.white)
        # we can not automate these y value to draw the line
        # the scroll view is smaller than the widget
        y = 222
        lp: int = 15
        painter.drawLine(lp, y,
                         self.width() - lp, y)

    def __adjustScrollWidgetHeight(self):
        """ adjust the height of scroll widget """
        buttonHeight = 246 + 62 * 0
        height = self.height() - 346 if self.height() - 346 > buttonHeight else buttonHeight
        self.scrollWidget.resize(300, height)

    def updateWindow(self):
        """ update window """
        # create new buttons
        self._connectButtonClickedSigToSlot()

        # move buttons
        self.__adjustScrollWidgetHeight()
        self.update()


class ScrollWidget(QWidget):
    """ Scroll widget """

    def paintEvent(self, e):
        """ paint seperator """
        painter = QPainter(self)
        painter.setPen(QPen(Qt.white))
        painter.drawLine(15, 182, self.width() - 15, 182)
