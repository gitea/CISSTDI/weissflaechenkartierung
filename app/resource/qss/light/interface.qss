
QWidget {
    background-color: rgba(255, 255, 255, 1);
}

/****************************************************************************/

QScrollArea {
    background-color: rgba(255, 255, 255, 1);
    border: none;
}

/****************************************************************************/

QWidget#windowMask {
    background-color: rgba(255, 255, 255, 0.6);
    border: none;
}

/****************************************************************************/

QToolTip {
    background-color: white;
}

/****************************************************************************/

#dragNdropFrame {
    background-color: transparent;
    border: 2px solid rgba(178, 178, 178, 0.80);
    border-radius: 4px;
    border-style : dashed;
}

#dragNdropFrame:hover {
    background-color: rgba(178, 178, 178, 0.20);
}

/****************************************************************************/

QPushButton {
    border-radius: 4px;
    border: 1px solid #758DE7;
    background: #758DE7;
    color: white;
    font-family: "Segoe UI";
    font-size: 14px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
    text-align: center;
}

QPushButton:hover {
    background: #3F57AD;
    font-size: 14px;
    border: 1px solid #3F57AD;
}

QPushButton:disabled {
    background: #636AC5;
    border: 1px solid #636AC5;
}

/****************************************************************************/

QGroupBox QLineEdit {
    border:none;
    background-color:transparent;
    padding-left:14px;
    color:#606060;
    selection-background-color: #758DE7;
}

QGroupBox QLineEdit QPushButton{
    border:none;
    background-color:transparent;
}

QGroupBox QLineEdit QPushButton:hover{
    border:none;
    background-color:transparent;
}

QGroupBox QComboBox {
    border:none;
    background-color:transparent;
    color:#606060;
}

/****************************************************************************/

QLineEdit[hasError=true] {
    color: #ff2b41;
    font-style: italic;
}

/****************************************************************************/

QLabel {
    background-color: transparent;
    color: #191C2C;
    border: none;
}

QLabel#titleLabel {
    font: 17px "Segoe UI";
    /*font: 17px "FiraGo", "Microsoft YaHei Light"; */
    font-weight: bold;
}

QLabel#subTitleLabel {
    font: 15px "Segoe UI";
}

QLabel#underSubTitleLabel {
    font: 13px "Segoe UI Light";
}

QLabel#dragDropIconLabel {
     image: url(:/images/utils/add_drag_n_drop.svg);
}

QLabel#dragDropTextLabel {
    color: #606060;
    text-align: center;
    font-family: "Segoe UI";
    font-size: 12px;
    font-style: normal;
    font-weight: 400;
}

/****************************************************************************/

#infoBtn {
    border-radius: 10px;
    font-size: 18px;
    text-align: center;
}

/****************************************************************************/

#goUpButton {
    image: url(:/images/combo_box/drop_up_blue.svg);
    border-radius: 4px;
	border: 1px solid #B2B2B2;
	background: transparent;
}

#goUpButton:hover {
    border: 1px solid #758DE7;
}

#goDownButton {
    image: url(:/images/combo_box/drop_down_blue.svg);
    border-radius: 4px;
	border: 1px solid #B2B2B2;
    background: transparent;
}

#goDownButton:hover {
    border: 1px solid #758DE7;
}

/****************************************************************************/

#secondaryButton {
    border-radius: 15px;
    background-color: white;
    color: #3F57AD;
    border: 1.5px solid #3F57AD;
}

#secondaryButton:hover {
    border: 1.5px solid #3F57AD;
    background: #3F57AD;
    color: white;
}

#primaryButton {
    border: 1.5px solid #758DE7;
    border-radius: 15px;
    background-color:white;
    color: #758DE7;
}

#primaryButton:hover {
    border: 1.5px solid #758DE7;
    background-color: #758DE7;
    color: white;
}

/****************************************************************************/

#createConfigButton {
	background: transparent;
	color: #3F57AD;
	border-radius: 15px;
	border: 1.5px solid #3F57AD;
    text-align: left;

	image: url(:/images/utils/add_file_format_normal.svg);
	image-position: right;
	padding-right: 8px;
	padding-left: 8px;
}

#createConfigButton:hover {
	background: #3F57AD;
	color: white;
	image: url(:/images/utils/add_file_format_hover.svg);
}

/****************************************************************************/

#refreshAnalysisButton {
    background: white;
	color: #3F57AD;
	/*border-radius: 15px;*/
	border: 1.5px solid #3F57AD;
    text-align: left;

	image: url(:/images/button/refresh_analysis_normal.svg);
	image-position: right;
	padding-right: 8px;
	padding-left: 8px;
}

#refreshAnalysisButton:hover {
	background: #3F57AD;
	color: white;
	image: url(:/images/button/refresh_analysis_hover.svg);
}


/****************************************************************************/

#returnButton {
    border: 1px solid #B2B2B2;
    image: url(:/images/utils/arrow_left_hover.svg);
    background: transparent;
}

#returnButton:hover {
    border: none;
    border: 1px solid #758DE7;
    background: transparent;
}

/****************************************************************************/

QComboBox{
	background: none;
    box-shadow: transparent;

    border:none;
    background-color:transparent;
    color:#606060;

	combobox-popup: 0;

	font-family: "Segoe UI";
    font-size: 12px;
    font-style: normal;
    font-weight: 400;
}

QComboBox::drop-down
{
     subcontrol-origin: padding;
     subcontrol-position: top right;
     width: 22px;
}

QComboBox::down-arrow {
	image: url(:/images/combo_box/drop_down_blue.svg);
    width: 12px;
    height: 6px;
}

QComboBox::down-arrow:on {
	image: url(:/images/combo_box/drop_up_blue.svg);
    width: 12px;
    height: 6px;
}

/* ********************************* */

QComboBox QAbstractItemView {
	outline: none;
	box-shadow: transparent;
	background-color: white;

	color: #606060;

	border-radius: 4px;
    border: 1px solid #B2B2B2;
	padding: 5px 2px 2px 2px;

	font-family: "Segoe UI";
    font-size: 11px;
    font-style: normal;
    font-weight: 400;
}

QComboBox QListView {
    box-shadow: transparent;
}

/* style list item */
QComboBox QListView:item {
    box-shadow: transparent;
    padding-left: 10px;
    background-color: white;
}

QComboBox QListView:item:hover {
    padding-left: 10px;
    background-color: white;
}

/* *********************************************************************** */

QScrollBar {
    background: transparent;
    width: 6px;
    margin-top: 40px;
    padding-right: 2px;
}

QScrollBar:horizontal {
    margin-top: 2px;
    height: 6px;
}

QScrollBar::sub-line {
    background: transparent;
}

QScrollBar::add-line {
    background: transparent;
}

QScrollBar::handle {
    background: rgb(122, 122, 122);
    border: 2px solid rgb(128, 128, 128);
    border-radius: 1px;
    min-height: 40px;
}

QScrollBar::add-page:vertical,
QScrollBar::sub-page:vertical {
    background: none;
}

/* *********************************************************************** */

QSpinBox,
QDoubleSpinBox {
    padding-left: 5px; /* make room for the arrows */
	border-radius: 4px;
	border: 1px solid #B2B2B2;
    color:#606060;
	selection-background-color: #758DE7;
 }

QSpinBox:hover,
QDoubleSpinBox:hover {
    border: 1px solid #758DE7;
}

/* right button */
QSpinBox::up-button,
 QDoubleSpinBox::up-button {
     subcontrol-origin: border;
     subcontrol-position: right; /* position at the top right corner */
	 width: 20px;
	 height: 27px;
	 border-left: 1px solid #B2B2B2;
     padding-right: 2px;
 }

QSpinBox::up-arrow:hover,
QDoubleSpinBox::up-arrow:hover {
	image: url(:/images/utils/arrow_right_hover.svg);
}

QSpinBox::up-arrow,
QDoubleSpinBox::up-arrow {
    image: url(:/images/utils/arrow_right_normal.svg);
    width: 11px;
    height: 12px;
}

/* left button */
QSpinBox::down-button,
 QDoubleSpinBox::down-button {
     subcontrol-origin: border;
     subcontrol-position: left; /* position at bottom left corner */
	 width: 20px;
	 height: 27px;
	 border-right: 1px solid #B2B2B2;
	 padding-left: 2px;
 }

QSpinBox::down-arrow:hover,
QDoubleSpinBox::down-arrow:hover {
	image: url(:/images/utils/arrow_left_hover.svg);
}

QSpinBox::down-arrow,
QDoubleSpinBox::down-arrow {
    image: url(:/images/utils/arrow_left_normal.svg);
    width: 11px;
    height: 12px;
}


/****************************************************************************/

QListView QScrollBar{
    margin-top: 5px;
    margin-bottom: 5px;
}

QListView QScrollBar:horizontal {
    margin-top: 0px;
    height: 10px;
}

QListView {
    border: 1px solid rgba(54, 69, 83, 25%);
    background-color: rgb(255, 255, 255);
	selection-background-color: rgb(211, 220, 230);
	alternate-background-color: rgb(246, 248, 250);
    color: rgb(30, 35, 40);
    border-radius: 0px;
	padding-left: 2px;
	padding-right: 2px;
	font-size: 9pt;
}

QListView::item:alternate {
}

QListView::item:selected {
}

QListView::item:selected:!active {
}

QListView::item:selected:active {
}

QListView::item:hover {
}

/* ************************************************/

QTableView#resultTableView {
    background: transparent;
    gridline-color: none;
    border: none;
    selection-background-color: white;
	alternate-background-color: white;
}

QTableView#resultTableView::item {
    border: 1px solid #B2B2B2;
    border-radius: 4px;
    padding-left: 5px;
    margin: 5px;
}

QTableView#resultTableView::item:selected {
    selection-background-color: white;
    border: none;
}

QTableView#resultTableView QTableCornerButton::section{
    background-color: white;
    border: none;
}

/* ***********************/

QTableView#resultTableView QHeaderView {
    border: none;
    background: transparent;
}

QTableView#resultTableView QHeaderView::section  {
    border: none;
    border-radius: 0px;
    background-color: white;
    text-align: center;
}

QTableView#resultTableView QHeaderView::section:horizontal {
    border-left: 1px solid white;
}

QTableView#resultTableView QHeaderView::section:checked {
    background: transparent;
    border: none;
}
