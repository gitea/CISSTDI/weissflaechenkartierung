from PyQt5.QtCore import pyqtSignal, Qt
from PyQt5.QtGui import QCursor
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QPushButton, QLabel


class ActionButton(QWidget):
    """
    action button
    """
    clicked = pyqtSignal()

    def __init__(self, images: dict, parent=None):
        super().__init__(parent)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_StyledBackground)
        self.setCursor(QCursor(Qt.PointingHandCursor))
        self.setAutoFillBackground(False)
        self._checked: bool = False
        self._images = images

        self._button = QPushButton()
        self._button.setAttribute(Qt.WA_TransparentForMouseEvents)
        self._label = QLabel()

        self._button.setFixedSize(22, 18)
        self._button.setStyleSheet(f"image: url({images['normal']});")
        self._label.adjustSize()

        self._set_qss()
        self.set_style_to_normal()

        self._init_layout()

    def _init_layout(self) -> None:
        """
        :return: None
        """
        vBox = QVBoxLayout()
        vBox.setSpacing(0)
        vBox.setContentsMargins(0, 0, 0, 0)
        vBox.addWidget(self._button, 0, Qt.AlignHCenter)
        vBox.addWidget(self._label, 0, Qt.AlignHCenter)
        self.setLayout(vBox)

    def set_style_to_normal(self) -> None:
        """
        :return: None
        """
        self._label.setStyleSheet("""
            color: #899EE7;
            font-family: "Segoe UI";
            font-size: 12px;
            font-weight: 400;
        """)
        self._button.setStyleSheet(f"image: url({self._images['normal']});")

    def set_style_to_hover(self) -> None:
        """
        :return: None
        """
        self._label.setStyleSheet("""
                    color: #617DDF;
                    font-family: "Segoe UI";
                    font-size: 12px;
                    font-weight: 400;
                """)
        self._button.setStyleSheet(f"image: url({self._images['hover']});")

    def set_style_to_clicked(self) -> None:
        """
        :return: None
        """
        self._label.setStyleSheet("""
                    color: #617DDF;
                    font-family: "Segoe UI";
                    font-size: 12px;
                    font-weight: 400;
                """)
        self._button.setStyleSheet(f"image: url({self._images['clicked']});")

    def enterEvent(self, e) -> None:
        """
        :param e:
        :return: None
        """
        super().enterEvent(e)
        self.set_style_to_hover()

    def leaveEvent(self, e) -> None:
        """
        :param e:
        :return: None
        """
        super().leaveEvent(e)
        self.set_style_to_normal()

    def mouseReleaseEvent(self, e) -> None:
        """
        mouse release event
        :param e: mouse event
        :return: None
        """
        super().mouseReleaseEvent(e)
        if e.button() == Qt.LeftButton:
            self.clicked.emit()

    def set_text(self, value: str) -> None:
        """
        set new text
        :param value: new string
        :return: None
        """
        self._label.setText(value)
        self._label.adjustSize()

    def _set_qss(self) -> None:
        """
        set qss
        :return:
        """
        self.setObjectName('actionButton')
