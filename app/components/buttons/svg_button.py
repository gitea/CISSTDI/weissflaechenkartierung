from PyQt5 import QtSvg, QtCore
from PyQt5.QtCore import pyqtSignal, QEvent, Qt
from PyQt5.QtGui import QResizeEvent, QPaintEvent, QPainter, QColor
from PyQt5.QtWidgets import QLabel, QGraphicsDropShadowEffect, QPushButton

from app.components.buttons.tooltip_button import TooltipPushButton


class SvgPaintWidget(TooltipPushButton):
    def __init__(self, p_svg_path: str, parent=None):
        super().__init__("", parent)

        self._svg_path: str = p_svg_path

        self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)

    def paintEvent(self, e: QPaintEvent) -> None:
        """paint rounded border"""
        # super().paintEvent(e)
        p = QPainter(self)
        p.restore()
        svg = QtSvg.QSvgRenderer(self._svg_path)
        svg.render(p)
        p.end()

    @property
    def svg_path(self) -> str:
        """return path to the svg file"""
        return self._svg_path

    @svg_path.setter
    def svg_path(self, value: str):
        """set a new path"""
        self._svg_path = value
        self.update()


class SvgPaintButton(SvgPaintWidget):

    def __init__(self, p_svg_path: str, parent=None):
        super().__init__(p_svg_path, parent)
        self.installEventFilter(self)


class ThreeStateSvgButton(TooltipPushButton):

    def __init__(self, p_text: str = "", p_images=None, parent=None):
        super().__init__("", parent)

        if p_images is None:
            p_images = {}
        # images to draw
        self._images: dict = p_images

        self._svg_widget = SvgPaintWidget(p_svg_path=p_images.setdefault('normal', ''), parent=self)
        self._label = QLabel(p_text, self)

        # left, top, right, bottom
        self._margin: list = [5, 5, 5, 5]
        self.setStyleSheet("border:none;background: transparent;")

        self._init_widget()

    @property
    def images(self) -> dict:
        """images as dict"""
        return self._images

    @images.setter
    def images(self, value: dict):
        """set image dict"""
        if len(value) == 0:
            return
        self._images = value

    def _init_widget(self) -> None:
        """init the sub controls"""
        self.installEventFilter(self)
        self.setCursor(Qt.PointingHandCursor)
        self._svg_widget.setAttribute(Qt.WA_TransparentForMouseEvents)
        self._label.adjustSize()

    def eventFilter(self, obj, e):
        """filter mouse interactions"""
        if obj is self:
            if e.type() in [QEvent.Enter, QEvent.HoverMove]:
                self._svg_widget.svg_path = self._images.setdefault('hover', '')
            elif e.type() in [QEvent.Leave, QEvent.MouseButtonRelease]:
                self._svg_widget.svg_path = self._images.setdefault('normal', '')
            elif e.type() == QEvent.MouseButtonPress:
                self._svg_widget.svg_path = self._images.setdefault('pressed', '')
        return super().eventFilter(obj, e)

    def resizeEvent(self, e: QResizeEvent) -> None:
        """move the child svg"""
        # resize svg widget
        self._svg_widget.resize(self.width() // 2, self.height() // 2)
        # move image
        self._svg_widget.move(self.width() // 2 - self._svg_widget.width() // 2, self._margin[1])

        # move label
        if len(self._label.text()) == 0:
            self._label.hide()
            self.setFixedSize(self._svg_widget.geometry().bottomRight().x() + self._margin[2],
                              self._svg_widget.geometry().bottomRight().y() + self._margin[1])
        else:
            self._label.move(self.width() // 2 - self._label.width() // 2,
                             self._svg_widget.geometry().bottomLeft().y() + 6)
