from PyQt5.QtCore import QPropertyAnimation, QEasingCurve
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import QPushButton, QGraphicsDropShadowEffect, QGraphicsOpacityEffect

from app.components.buttons.tooltip_button import TooltipPushButton


class OpacityButton(TooltipPushButton):
    def __init__(self, parent=None):
        super().__init__("", parent)

    def showEvent(self, e):
        """fade in"""
        opacity_ani = self._create_opacity_anim(0, 1, 200, True)
        opacity_ani.setEasingCurve(QEasingCurve.InSine)
        opacity_ani.start()
        super().showEvent(e)

    def hideEvent(self, e) -> None:
        """fade out"""
        self.setGraphicsEffect(None)
        opacity_ani = self._create_opacity_anim(1, 0, 100)
        opacity_ani.setEasingCurve(QEasingCurve.OutCubic)
        # opacity_ani.finished.connect(self.deleteLater)
        opacity_ani.start()
        e.ignore()

    # ##########################################################

    def __setShadowEffect(self):
        """ add shadow to dialog """
        shadow_effect = QGraphicsDropShadowEffect(self)
        shadow_effect.setBlurRadius(60)
        shadow_effect.setOffset(0, 10)
        shadow_effect.setColor(QColor(0, 0, 0, 100))
        self.setGraphicsEffect(shadow_effect)

    def _create_opacity_anim(self, start: int, end: int, duration: int, del_effect: bool = False) -> QPropertyAnimation:
        """
        create opacity anim
        """
        opacity_effect = QGraphicsOpacityEffect(self)
        self.setGraphicsEffect(opacity_effect)
        opacity_ani = QPropertyAnimation(opacity_effect, b'opacity', self)
        opacity_ani.setStartValue(start)
        opacity_ani.setEndValue(end)
        opacity_ani.setDuration(duration)
        if del_effect:
            opacity_ani.finished.connect(opacity_effect.deleteLater)
        return opacity_ani