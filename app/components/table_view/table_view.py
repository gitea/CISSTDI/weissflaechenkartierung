from typing import List

from PyQt5.QtCore import Qt, QModelIndex, pyqtSignal, QPoint, QObject, QEvent, QItemSelectionModel, QAbstractItemModel
from PyQt5.QtGui import QKeyEvent
from PyQt5.QtWidgets import QTableView, QAbstractItemView, QFrame, QAbstractScrollArea, QHeaderView, QMenu, QApplication

from app.components.widgets.menu import ListViewMenu, CopyMenu
from app.components.table_view.table_view_delegate import TableViewDelegate, AnalysisViewDelegate
from app.components.table_view.table_view_model import EditableTableViewModel, parse_input_data, USER_ROLE_ROW_DATA, \
    AnalysisModel, NarrowableFilterProxyModel, BaseModelMixIn, CustomSortingModel


class BaseTableView(QTableView):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setFrameShape(QFrame.NoFrame)
        self.setCornerButtonEnabled(False)
        self.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.setSelectionMode(QAbstractItemView.ExtendedSelection)

    def get_min_vertical_content_height(self) -> int:
        """
        calculate height depending on its content row height
        :return: None
        """
        total_row_height = 0

        # Row height
        for i in range(self.verticalHeader().count()):
            if not self.verticalHeader().isSectionHidden(i):
                total_row_height += self.verticalHeader().sectionSize(i)

        # Check for scrollbar visibility
        if not self.horizontalScrollBar().isHidden():
            total_row_height += self.horizontalScrollBar().height()

        # Check for header visibility
        if not self.horizontalHeader().isHidden():
            total_row_height += self.horizontalHeader().height()

        return total_row_height

    def get_min_horizontal_content_width(self) -> int:
        """
        calculate width depending on its content row width
        :return:  as int
        """
        total_col_width = 0

        # Column height
        for i in range(self.horizontalHeader().count()):
            if not self.horizontalHeader().isSectionHidden(i):
                total_col_width += self.horizontalHeader().sectionSize(i)

        # Check for scrollbar visibility
        if not self.verticalScrollBar().isHidden():
            total_col_width += self.verticalScrollBar().width()

        # Check for header visibility
        if not self.verticalHeader().isHidden():
            total_col_width += self.verticalHeader().width()

        return total_col_width

class SortableTableView(BaseTableView):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.proxy_model = CustomSortingModel()
        self.setSortingEnabled(True)

    def append_row(self, p_row: list):
        """appends a row to the model"""
        if self.model() is None:
            return
        if len(p_row) > 0:
            self.proxy_model.sourceModel().addRow(p_row)

    def append_rows(self, p_rows: list) -> None:
        """appends all rows inside the list"""
        if self.model() is None:
            return
        self.proxy_model.sourceModel().addRows(p_rows)

    def setModel(self, model) -> None:
        """
        set the model
        :param model:
        :return: None
        """
        if model is None:
            return
        model.setParent(self)
        self.proxy_model.setSourceModel(model)
        super(SortableTableView, self).setModel(self.proxy_model)

    def model(self) -> QAbstractItemModel:
        """
        :return: source model
        """
        return self.proxy_model.sourceModel()

class TableView(SortableTableView):
    copyRow = pyqtSignal(QModelIndex)
    deleteRow = pyqtSignal()
    editRow = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContentsOnFirstShow)
        self.setAlternatingRowColors(True)
        self.setTextElideMode(Qt.ElideLeft)
        self.setGridStyle(Qt.NoPen)
        self.verticalHeader().setVisible(False)
        self.horizontalHeader().setVisible(True)
        self.horizontalHeader().setDefaultAlignment(Qt.AlignCenter)

        self.setModel(EditableTableViewModel(p_data=[], p_header=[], parent=self))

        self.menu = ListViewMenu(self)
        self._row_deletable: bool = False

        # needed for context menu
        self.installEventFilter(self)
        self.setContextMenuPolicy(Qt.CustomContextMenu)


        # the header gets stretched; list width / column size
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

        self.setItemDelegate(TableViewDelegate())

        self._connect_signals()

    def _copy_cell_to_clip_board(self, index: QModelIndex) -> None:
        """
        copy selected cell to clipboard
        """
        if not index.isValid():
            return
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard)
        # parse row
        cb.setText(str(parse_input_data(index.data(USER_ROLE_ROW_DATA)[index.column()])), mode=cb.Clipboard)

    def set_row_deletable(self, p_state: bool = False) -> None:
        """
        :param p_state: state as bool
        :return: None
        """
        self._row_deletable = p_state

    def set_menu(self, p_menu: QMenu) -> None:
        """
        set a new menu
        :param p_menu: Menu
        :return: None
        """
        # del self.menu
        if p_menu is None:
            return
        self.menu = p_menu
        self.menu.setParent(self)

    def _connect_signals(self) -> None:
        """
        connect table view signals
        """
        self.customContextMenuRequested.connect(self._on_custom_context_menu_clicked)
        self.copyRow.connect(self._copy_cell_to_clip_board)

    def keyPressEvent(self, event: QKeyEvent) -> None:
        """
        key pressed
        :param event: event
        :return: None
        """
        if event.key() == Qt.Key_Up:
            indexes: list = self.selectedIndexes()
            if len(indexes) > 0:
                pre_idx = None
                first_row: int = indexes[0].row() - 1
                if first_row >= 0:
                    idx = indexes[0]
                    if idx is not None:
                        pre_idx = self.model().index(idx.row() - 1, 0)
                else:
                    pre_idx = self.model().index(self.model().rowCount() - 1, 0)
                if pre_idx is not None:
                    self.selectionModel().select(pre_idx, QItemSelectionModel.ClearAndSelect)
                    self.setCurrentIndex(pre_idx)
        elif event.key() == Qt.Key_Down:
            indexes: list = self.selectedIndexes()
            if len(indexes) > 0:
                next_idx = None
                last_row = indexes[-1].row() + 1
                max_row = self.model().rowCount()
                if last_row < max_row:
                    idx = indexes[0]
                    if idx is not None:
                        next_idx = self.model().index(idx.row() + 1, 0)
                else:
                    next_idx = self.model().index(0, 0)
                if next_idx is not None:
                    self.selectionModel().select(next_idx, QItemSelectionModel.ClearAndSelect)
                    self.setCurrentIndex(next_idx)
        elif event.key() == Qt.Key_Delete and self._row_deletable:
            indexes: list = self.selectedIndexes()
            if len(indexes) > 0:
                self.deleteRow.emit(indexes)

    def eventFilter(self, obj: QObject, event: QEvent) -> bool:
        """
        filter events
        :param obj: sender object
        :param event: event
        :return: bool
        """
        if event.type() == QEvent.KeyPress:
            if obj == self and self.currentIndex() is not None:
                if event.key() == Qt.Key_C and event.modifiers() == Qt.ControlModifier:
                    self.copyRow.emit(self.currentIndex())
                    return True
        return super().eventFilter(obj, event)

    def _on_custom_context_menu_clicked(self, p_pos) -> None:
        """
        open context menu at the given coordinates
        :param p_pos: pos
        :return: None
        """
        if len(self.selectionModel().selectedRows()) == 1:
            if self.menu is not None:
                self.menu.exec_(self.mapToGlobal(QPoint(0, 0) + p_pos))

    def append_row(self, p_row: list):
        """ appends a row to the model """
        if len(p_row) > 0:
            self.model().layoutAboutToBeChanged.emit()
            self.model().add_data([p_row])
            self.model().layoutChanged.emit()

class CopyTableView(TableView):
    def __init__(self, parent=None):
        super().__init__(parent)
        menu = CopyMenu(self)
        menu.copySel.triggered.connect(
            lambda: self.copyRow.emit(self.currentIndex())
        )
        self.set_menu(menu)

class AnalysisTableView(CopyTableView):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setItemDelegate(AnalysisViewDelegate())
        self.setAlternatingRowColors(False)
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.setSelectionMode(QAbstractItemView.SingleSelection)
        self.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.setSizeAdjustPolicy(0)
        self.horizontalHeader().setVisible(True)
        self.horizontalHeader().setMinimumSectionSize(140)
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.horizontalHeader().setFrameShape(QFrame.NoFrame)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAsNeeded)

        self.verticalHeader().setVisible(True)
        self.verticalHeader().setSectionResizeMode(QHeaderView.ResizeToContents)
        self.verticalHeader().setFrameShape(QFrame.NoFrame)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOff)


        self.std_vertical_header: List[str] = [
                "Weißflächen gesamt",
                "Weißflächen\nbedingt geeignet",
                "Weißflächen\ngut geeignet"
            ]

        model = AnalysisModel(p_data=[], p_header=[], parent=self)
        model.set_vertical_header(self.std_vertical_header)
        model.set_header(
            [
                "Potentialfläche",
                "Anteil vom\nUntersuchungsgebiet",
                "Installierbare Leistung",
                "Jährlicher Ertrag"
            ]
        )
        self.setModel(model)

    def _on_custom_context_menu_clicked(self, p_pos) -> None:
        """
        open context menu at the given coordinates
        :param p_pos: pos
        :return: None
        """
        if len(self.selectionModel().selectedIndexes()):
            if self.menu is not None:
                self.menu.exec_(self.mapToGlobal(QPoint(0, 0) + p_pos))

    def _copy_cell_to_clip_board(self, index: QModelIndex) -> None:
        """
        copy selected cell to clipboard
        """
        if not index.isValid():
            return
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard)
        cb.setText(str(" ,".join([ parse_input_data(x) for x in  index.data(USER_ROLE_ROW_DATA)])), mode=cb.Clipboard)

class FilterTableView(TableView):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWordWrap(False)
        self.setSizeAdjustPolicy(QHeaderView.AdjustToContents)
        self.horizontalHeader().setStretchLastSection(False)
        self.horizontalHeader().setDefaultSectionSize(100)

        del self.proxy_model
        self.proxy_model = NarrowableFilterProxyModel()

        self.setModel(BaseModelMixIn(p_data=[], p_header=[]))

    def source_model(self) -> QAbstractItemModel:
        """
        return the source model of the source model
        :return: QAbstractItemModel
        """
        return self.proxy_model.sourceModel().sourceModel()

    def append_row(self, p_row: list):
        """appends a row to the model"""
        if self.model() is None:
            return
        if len(p_row) > 0:
            self.source_model().addRow(p_row)

    def append_rows(self, p_row: list) -> None:
        """appends all rows inside the list"""
        if self.model() is None:
            return
        self.source_model().addRows(p_row)