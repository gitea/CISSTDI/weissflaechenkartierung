import decimal
import locale
from datetime import datetime
from typing import List

from PyQt5.QtCore import Qt, QAbstractTableModel, QModelIndex, QAbstractProxyModel, QRegExp, pyqtProperty, \
    QAbstractItemModel, QIdentityProxyModel, QSortFilterProxyModel

USER_ROLE_ROW_DATA = Qt.UserRole + 1
USER_ROLE_WHOLE_DATA = Qt.UserRole + 2

def parse_input_data(p_data_to_parse: any) -> any:
    """
    parses the input data
    """
    if isinstance(p_data_to_parse, decimal.Decimal):
        return "{0:0.0f}".format(decimal.Decimal(p_data_to_parse))
    elif isinstance(p_data_to_parse, datetime):
        # Render time to YYY-MM-DD.
        return p_data_to_parse.strftime("%H:%M:%S %Y-%m-%d")
    elif isinstance(p_data_to_parse, float):
        # Render float to 2 dp
        return "%.2f" % p_data_to_parse
    elif p_data_to_parse is None or p_data_to_parse == "None":
        return ""
    else:
        return str(p_data_to_parse)


class BaseModelMixIn(QAbstractTableModel):
    def __init__(self, p_data: list, p_header: list, parent=None):
        super(BaseModelMixIn, self).__init__(parent)
        self._data: list = []
        self._header: list = p_header
        self.addRows(p_rows=p_data)

    def addRow(self, p_row: tuple):
        """
        adds one row to the data

        :param p_row: tuple with row data
        :return: true if added otherwise false
        """
        if len(p_row) == 0:
            return
        self.layoutAboutToBeChanged.emit()
        self.beginInsertRows(self.index(len(self._data) - 1, 0), self.rowCount(), self.rowCount() + 1)
        if len(p_row) == len(self._header):
            self._data.append(p_row)
        else:
            raise IndexError("Row len is unequal to header len")
        self.endInsertRows()
        self.layoutChanged.emit()

    def addRows(self, p_rows: list):
        """
        adds multiple rows

        :param p_rows: list with row data
        :return: true if added otherwise false
        """
        if len(p_rows) == 0:
            return
        self.layoutAboutToBeChanged.emit()
        for row in p_rows:
            self.beginInsertRows(self.index(len(self._data) - 1, 0), self.rowCount(), self.rowCount() + 1)
            if len(row) == len(self._header):
                self._data.append(row)
            else:
                raise IndexError("Row len is unequal to header len")
            self.endInsertRows()
        self.layoutChanged.emit()

    def resetInternalData(self) -> None:
        """
        reset the internal custom data

        :return: None
        """
        self.beginResetModel()
        self._data.clear()
        self._header.clear()
        self.endResetModel()

    def get_header_id(self, p_header_name: str) -> int:
        """return the index of the given"""
        try:
            return self._header.index(p_header_name)
        except ValueError:
            return -1

    def get_header_value(self, p_header_name: str) -> object:
        """return the header value at the given index"""
        try:
            return self._header[self.get_header_id(p_header_name)]
        except IndexError:
            return ""

    def set_header(self, p_header: List[str]) -> None:
        """
        sets the header data
        :param p_header: list with header names
        :return: None
        """
        self._header = p_header

    def get_header(self) -> List[str]:
        """
        get the header names
        :return: list with header names
        """
        return self._header

    def removeRow(self, row: int, parent: QModelIndex = ...) -> bool:
        """
        remove the given-row
        :param row: row
        :param parent: not handled
        :return: true if successful, otherwise false
        """
        if len(self._data) > 0 and -1 < row <= len(self._data):
            self.layoutAboutToBeChanged.emit()
            self._data.pop(row)
            self.layoutChanged.emit()
            return True
        return False

    def data(self, index: QModelIndex, role=Qt.DisplayRole) -> any:
        """
        handle the data
        :param index: index of the given item
        :param role: display role
        :return: any kind of object
        """
        if index.isValid():
            if role == USER_ROLE_ROW_DATA:
                return self._data[index.row()]

            if role in [Qt.ToolTipRole, Qt.DisplayRole]:
                return parse_input_data(p_data_to_parse=self._data[index.row()][index.column()])

    def headerData(self, col: int, orientation, role=Qt.DisplayRole) -> str:
        """
        return the given header name at given col
        :param col: coll
        :param orientation: Qt.Horizontal or Qt.Vertical
        :param role: Qt.DisplayRole
        :return: str or none
        """
        if orientation == Qt.Horizontal:
            if role in [Qt.ToolTipRole, Qt.DisplayRole]:
                return self._header[col]
        return None

    def rowCount(self, index: QModelIndex = QModelIndex()) -> int:
        """
        length of the model data
        :param index: not handled
        :return: len as int
        """
        if self._data is None:
            return 0
        return len(self._data)

    def columnCount(self, index: QModelIndex = QModelIndex()) -> int:
        """
        return the column count
        :param index:
        :return:
        """
        # The following takes the first sub-list, and returns
        # the length (only works if all rows are an equal length)
        if self._data is None:
            return 0
        return 0 if len(self._data) == 0 or self._data[0] is None else len(self._data[0])

    def flags(self, index):
        return Qt.ItemIsSelectable | Qt.ItemIsEnabled

class EditableTableViewModel(QAbstractTableModel):
    def __init__(self, p_data: list, p_header: list, parent=None):
        super(EditableTableViewModel, self).__init__(parent)
        self._data: list = []
        self._header = p_header
        self.add_data(p_data)

    def add_data(self, p_data: list):
        for data in p_data:
            self._data.append(data)

    def set_header(self, p_header: list):
        self._header = p_header

    def get_horizontal_header(self) -> List[str]:
        return self._header

    def _reset_data(self) -> None:
        """
        reset the model data only
        :return: None
        """
        self._data.clear()

    def _reset_internal_data(self) -> None:
        """
        reset internal data
        :return: None
        """
        self._data.clear()
        self._header.clear()

    def removeRow(self, row: int, parent: QModelIndex = ...) -> bool:
        """ try to remove the row """
        if row != -1 and len(self._data) > 0:
            self.layoutAboutToBeChanged.emit()
            self._data.pop(row)
            self.layoutChanged.emit()
            return True
        return False

    def resetInternalData(self) -> None:
        """
        reset the internal custom data

        :return: None
        """
        self.beginResetModel()
        self._reset_internal_data()
        self.endResetModel()

    def data(self, index: QModelIndex, role=Qt.DisplayRole):
        # return the whole list with data
        if role == USER_ROLE_WHOLE_DATA:
            return self._data

        if index.isValid():
            if role == USER_ROLE_ROW_DATA:
                return self._data[index.row()]

            value = self._data[index.row()][index.column()]

            if role == Qt.EditRole:
                return value

            if role == Qt.DisplayRole:
                return parse_input_data(p_data_to_parse=value)

    def setData(self, index: QModelIndex, value: any, role: int = ...):
        if role == Qt.EditRole:
            # we need to overwrite this function if we inherit this class
            if index.column() == 0:
                if isinstance(value, str):
                    if ',' in value or '?' in value or '#' in value:
                        return False
                    if value.isnumeric():
                        return False
            elif index.column() == 1:
                value = str(value).replace(',', '.')
                try:
                    value = float(value)
                except ValueError:
                    return False
                # if there is a negative buffer do not accept it
                if value < 0:
                    return False
            else:
                return False
            self._data[index.row()][index.column()] = value
            return True
        return False

    def headerData(self, col: int, orientation, role=Qt.DisplayRole):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self._header[col]
        return None

    def rowCount(self, index: QModelIndex = QModelIndex()):
        # The length of the outer list.
        return len(self._data)

    def columnCount(self, index: QModelIndex = QModelIndex()):
        # The following takes the first sub-list, and returns
        # the length (only works if all rows are an equal length)
        if self._data is None:
            return 0
        return 0 if len(self._data) == 0 or self._data[0] is None else len(self._data[0])

    def flags(self, index):
        return Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsEditable

class AnalysisModel(EditableTableViewModel):
    def __init__(self, p_data: list, p_header: list, parent=None):
        super(AnalysisModel, self).__init__(p_data, p_header, parent)
        self._vertically_header = []

    def data(self, index: QModelIndex, role=Qt.DisplayRole):
        result = super().data(index, role)

        if index.isValid():
            if role == Qt.TextAlignmentRole:
                return Qt.AlignCenter

            if role == Qt.DisplayRole:
                value = self._data[index.row()][index.column()]
                if index.column() == 0:
                    locale.setlocale(locale.LC_NUMERIC, 'German')
                    return f"{locale.format_string('%.2f', round(float(value), 2), True)} km²"
                parsed_value = parse_input_data(p_data_to_parse=value)
                if index.column() == 1:
                    return f"{parsed_value} %"
                elif index.column() == 2:
                    return f"{locale.format_string('%.0f', value, True)} MW"
                elif index.column() == 3:
                    return f"{locale.format_string('%.0f', value, True)} MWh/a"
                return parsed_value
        return result

    def headerData(self, col: int, orientation, role=Qt.DisplayRole):
        result = super().headerData(col, orientation, role)
        if orientation == Qt.Vertical and role == Qt.DisplayRole:
            return self._vertically_header[col]
        return result

    def _reset_internal_data(self) -> None:
        """
        reset the internal custom data

        :return: None
        """
        # super()._reset_internal_data()
        # self._vertically_header.clear()
        self._reset_data()

    def set_vertical_header(self, p_header: list):
        self._vertically_header = p_header

    def get_vertical_header(self):
        return self._vertically_header

# better implementation filter https://stackoverflow.com/questions/39414607/avoid-redundant-calls-to-qsortfilterproxymodelfilteracceptsrow-if-the-filter

class CustomSortingModel(QSortFilterProxyModel):
    def lessThan(self, left: QModelIndex, right: QModelIndex) -> bool:
        data_left = float(left.data().split(' ')[0])
        data_right = float(right.data().split(' ')[0])
        if data_left is None or data_right is None:
            return False
        return data_left < data_right


class NarrowableFilterProxyModel(QIdentityProxyModel):
    def __init__(self, parent=None):
        super().__init__(parent)
        self._filter_key_column: int = -1
        self.source_model = None
        self._filter_role = Qt.DisplayRole
        self._filter_case_sensitivity = Qt.CaseInsensitive
        self._filter_reg_exp = QRegExp()
        self._filter_proxy_chain: List[CustomSortingModel] = []

    def setSourceModel(self, sourceModel: QAbstractItemModel) -> None:
        """
        set source-model
        :param sourceModel: model
        :return: None
        """
        self.source_model = sourceModel
        super(NarrowableFilterProxyModel, self).setSourceModel(sourceModel)
        for proxy_node in self._filter_proxy_chain:
            del proxy_node
        self._filter_proxy_chain.clear()
        self._apply_current_filter()

    @pyqtProperty(QRegExp)
    def filter_reg_exp(self) -> QRegExp:
        """
        :return: QRegExp
        """
        return self._filter_reg_exp

    @pyqtProperty(int)
    def filter_key_column(self) -> int:
        """
        :return: int
        """
        return self._filter_key_column

    @pyqtProperty(Qt.CaseSensitivity)
    def filter_case_sensitivity(self) -> Qt.CaseSensitivity:
        """
        :return: Qt.CaseSensitivity
        """
        return self._filter_case_sensitivity

    @pyqtProperty(int)
    def filter_role(self) -> int:
        """
        :return: int
        """
        return self._filter_role

    # ########################################################

    def set_filter_key_column(self, p_filter_key_column: int, p_narrowed: bool) -> None:
        """
        set the given key column
        :param p_filter_key_column:
        :param p_narrowed:
        :return: None
        """
        self._filter_key_column = p_filter_key_column
        self._apply_current_filter(p_narrowed)

    def set_filter_case_sensitivity(self, p_filter_case_sensitivity: Qt.CaseSensitivity, p_narrowed: bool) -> None:
        """
        :param p_filter_case_sensitivity:
        :param p_narrowed:
        :return: None
        """
        self._filter_case_sensitivity = p_filter_case_sensitivity
        self._apply_current_filter(p_narrowed)

    def set_filter_role(self, p_filter_role: int, p_narrowed: bool) -> None:
        """
        :param p_filter_role:
        :param p_narrowed:
        :return: None
        """
        self._filter_role = p_filter_role
        self._apply_current_filter(p_narrowed)

    def set_filter_regex_string(self, p_filter_reg: str, p_narrowed: bool) -> None:
        """
        :param p_filter_reg:
        :param p_narrowed:
        :return:
        """
        self._filter_reg_exp.setPatternSyntax(QRegExp.RegExp)
        self._filter_reg_exp.setPattern(p_filter_reg)
        self._apply_current_filter(p_narrowed)

    def set_filter_regex(self, p_filter_regex: QRegExp = None, p_narrowed: bool = False) -> None:
        """
        :param p_filter_regex:
        :param p_narrowed:
        :return: None
        """
        self._filter_reg_exp = p_filter_regex
        self._apply_current_filter(p_narrowed)

    def set_filter_wild_card(self, p_filter_pattern: str, p_narrowed: bool) -> None:
        """
        :param p_filter_pattern:
        :param p_narrowed:
        :return: None
        """
        self._filter_reg_exp.setPatternSyntax(QRegExp.Wildcard)
        self._filter_reg_exp.setPattern(p_filter_pattern)
        self._apply_current_filter(p_narrowed)

    def set_filter_fixed_string(self, p_filter_pattern: str, p_narrowed: bool) -> None:
        """
        :param p_filter_pattern:
        :param p_narrowed:
        :return: None
        """
        self._filter_reg_exp.setPatternSyntax(QRegExp.FixedString)
        self._filter_reg_exp.setPattern(p_filter_pattern)
        self._apply_current_filter(p_narrowed)

    # ########################################################

    def _new_proxy_node(self) -> CustomSortingModel:
        """
        return a new QSortFilterProxyModel model
        :return: QSortFilterProxyModel
        """
        proxy_node: CustomSortingModel = CustomSortingModel(self)
        proxy_node.setFilterRegExp(self.filter_reg_exp)
        proxy_node.setFilterKeyColumn(self.filter_key_column)
        proxy_node.setFilterCaseSensitivity(self.filter_case_sensitivity)
        proxy_node.setFilterRole(self.filter_role)
        return proxy_node

    def _compare_to_filter(self) -> QAbstractProxyModel:
        """
        :return:
        """
        for f in self._filter_proxy_chain:
            if self._filter_key_column == f.filterKeyColumn() and \
                    self._filter_reg_exp.pattern() == f.filterRegExp().pattern():
                return f

    def _apply_current_filter(self, p_narrow_down: bool = False) -> None:
        """
        apply filter
        :param p_narrow_down:
        :return: None
        """
        if self.source_model is None:
            return
        if p_narrow_down:
            # if the filter is being narrowed down
            # instantiate a new filter proxy model and add it to the end of the chain
            # proxy_node_source: QAbstractItemModel = self.source_model if len(self._filter_proxy_chain) == 0 else \
            #     self._filter_proxy_chain[len(self._filter_proxy_chain) - 1]
            proxy_node = self._new_proxy_node()
            proxy_node.setSourceModel(self.source_model)
            super(NarrowableFilterProxyModel, self).setSourceModel(proxy_node)
            self._filter_proxy_chain.append(proxy_node)
        else:
            # 05.02.2023 the filter is not narrowed down
            # look in the current filter chain for a filter that is similar
            # to the current filter and switch to it immediately
            # (instead of deleting the whole chain and starting a new one)
            # proxy_node = self._compare_to_filter()
            proxy_node = self._compare_to_filter()
            if proxy_node is None:
                # delete all filters from the current chain
                # and construct a new chain with the new filter in it
                proxy_node = self._new_proxy_node()
                proxy_node.setSourceModel(self.source_model)
                super(NarrowableFilterProxyModel, self).setSourceModel(proxy_node)
                for node in self._filter_proxy_chain:
                    del node
                self._filter_proxy_chain.clear()
                self._filter_proxy_chain.append(proxy_node)
            else:
                proxy_node.setSourceModel(self.source_model)
                super(NarrowableFilterProxyModel, self).setSourceModel(proxy_node)