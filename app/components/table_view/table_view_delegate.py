from PyQt5.QtCore import Qt, QModelIndex, QRect
from PyQt5.QtGui import QBrush, QColor, QPainter, QPen
from PyQt5.QtWidgets import QStyle, QStyleOptionViewItem, QStyledItemDelegate


class TableViewDelegate(QStyledItemDelegate):
    def sizeHint(self, option, index):
        size = super().sizeHint(option, index)
        size.setHeight(30)
        return size

    def initStyleOption(self, option: QStyleOptionViewItem, index: QModelIndex) -> None:
        """style the item"""
        super(TableViewDelegate, self).initStyleOption(option, index)

        # set selection style
        if option.state & QStyle.State_Selected:
            option.state &= ~ QStyle.State_Selected
            option.backgroundBrush = QBrush(QColor(178, 192, 250))   # "#B2C0FA"
        elif option.state & QStyle.State_MouseOver:
            option.state &= ~ QStyle.State_MouseOver
            option.backgroundBrush = QBrush(QColor(239, 241, 253))


class AnalysisViewDelegate(QStyledItemDelegate):
    def sizeHint(self, option, index):
        size = super().sizeHint(option, index)
        size.setHeight(40)
        return size

    def paint(self, painter: QPainter, option: 'QStyleOptionViewItem', index: QModelIndex) -> None:
        """
        custom paint item

        :param painter: painter object itself
        :param option: style option
        :param index: item index
        :return: None
        """
        painter.save()
        painter.setRenderHints(QPainter.Antialiasing)
        rect = option.rect
        rect = QRect(rect.x() + 5, rect.y() + 5, rect.width() - 10, rect.height() - 10)

        selected = False
        if option.state & QStyle.State_Selected:
            option.state &= ~ QStyle.State_Selected
            selected = True

        ## Draw text data
        pen = QPen()
        pen.setWidth(1)
        if index.column() == 0:
            pen.setColor(QColor("#758DE7"))
        else:
            pen.setColor(QColor("#B2B2B2"))

        if selected:
            painter.setBrush(QColor("#F4F4F4"))

        painter.setPen(pen)
        painter.drawRoundedRect(rect, 4, 4)

        ## draw border rect
        text = index.data(Qt.DisplayRole)
        text_alignment = index.data(Qt.TextAlignmentRole)
        text_color = index.data(Qt.TextColorRole)
        if selected:
            painter.setPen(QPen(QColor("#758DE7"), 1))
        else:
            painter.setPen(QPen(QColor("#606060"), 1))

        if text_color is not None:
            painter.setPen(QPen(text_color, 1))
        painter.setFont(option.font)
        painter.drawText(rect, text_alignment, text)

        painter.restore()

