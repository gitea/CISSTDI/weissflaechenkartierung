from PyQt5.QtCore import Qt, QObject, QEvent, pyqtSignal, pyqtSlot, QModelIndex
from PyQt5.QtGui import QGuiApplication, QKeyEvent
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QAbstractScrollArea, QApplication, QHBoxLayout, QLineEdit, \
    QPushButton, QSpacerItem, QSizePolicy

from app.components.table_view.table_view import FilterTableView
from app.components.table_view.table_view_model import parse_input_data, USER_ROLE_ROW_DATA


class FilterWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self._layout = QHBoxLayout()
        self.le_filter = QLineEdit(self)
        self.btn_close = QPushButton(self)
        self.btn_clear = QPushButton(self)

        self.le_filter.setFixedSize(250, 30)
        self.btn_close.setFixedSize(70, 30)
        self.btn_clear.setFixedSize(70, 30)

        self.btn_close.clicked.connect(self._key_press_esc_event)
        self.btn_clear.clicked.connect(self.le_filter.clear)

        self.btn_clear.setText(self.tr("Leeren"))
        self.btn_close.setText(self.tr("Schließen"))

        self.setStyleSheet(
            """
            QPushButton {
                background-color: rgb(1, 120, 229);
                color: white;
                border-top-right-radius : 5px;
                border-top-left-radius : 5px;
                border-bottom-right-radius : 5px;
                border-bottom-left-radius : 5px;
            }

            QPushButton:hover {
                background-color: rgba(1, 120, 229,100);
            }

            QPushButton:pressed {
                background-color: rgb(1, 120, 229);
            }
            """
        )

        # layout
        self._layout.setSpacing(6)
        self._layout.setContentsMargins(0, 0, 0, 0)
        self._layout.addWidget(self.le_filter)
        self._layout.addWidget(self.btn_clear)
        self._layout.addWidget(self.btn_close)
        self._layout.addItem(QSpacerItem(1, 1, QSizePolicy.Expanding, QSizePolicy.Fixed))
        self.setLayout(self._layout)

        self.setTabOrder(self.le_filter, self.btn_clear)
        self.setTabOrder(self.btn_clear, self.btn_close)

    def _key_press_esc_event(self) -> None:
        """
        send parent esc key press event
        :return: None
        """
        self.le_filter.clear()
        QGuiApplication.sendEvent(self.parent(), QKeyEvent(QEvent.KeyPress, Qt.Key_Escape, Qt.NoModifier, "Escape"))


class FilterTableWidget(QWidget):
    showFilter = pyqtSignal()
    hideFilter = pyqtSignal()
    refreshData = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.table_view = FilterTableView(self)
        self.filter_widget = FilterWidget(self)
        self._layout = QVBoxLayout()
        self._init_widget()

    def _init_widget(self) -> None:
        """
        init the widget
        :return: None
        """
        self.table_view.installEventFilter(self)
        self.filter_widget.le_filter.installEventFilter(self)
        self.installEventFilter(self)

        self.table_view.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)

        self._layout.setSpacing(6)
        self._layout.setContentsMargins(0, 0, 0, 0)
        self._layout.addWidget(self.table_view)
        self._layout.addWidget(self.filter_widget)

        self.setLayout(self._layout)

        # filter widget
        self.filter_widget.le_filter.setPlaceholderText(self.tr("Tabelle filter..."))
        self.filter_widget.hide()

        self._connect_signals()

    def _connect_signals(self) -> None:
        """
        connect signals
        :return: None
        """
        self.table_view.copyRow.connect(self._copy_cell_to_clip_board)

        self.filter_widget.le_filter.textChanged.connect(self.filter_widget.le_filter.update)
        self.filter_widget.le_filter.textChanged.connect(self._filter_text_changed)

        self.showFilter.connect(self._show_filter_widget)
        self.hideFilter.connect(self._hide_filter_widget)

    @staticmethod
    def _copy_cell_to_clip_board(index: QModelIndex) -> None:
        """
        copy selected cell to clipboard
        """
        if not index.isValid():
            return
        cb = QApplication.clipboard()
        cb.clear(mode=cb.Clipboard)
        # parse row
        cb.setText(str(parse_input_data(index.data(USER_ROLE_ROW_DATA)[index.column()])), mode=cb.Clipboard)

    @pyqtSlot(str)
    def _filter_text_changed(self, p_new_filter: str) -> None:
        """
        filter text changed
        :param p_new_filter: new filter as string
        :return: None
        """
        p_new_filter = f"^{p_new_filter}"
        narrowed: bool = p_new_filter.startswith(self.table_view.proxy_model.filter_reg_exp.pattern())
        # narrowed = False
        # match from the beginning of the name p_new_filter
        self.table_view.proxy_model.set_filter_regex_string(p_new_filter, narrowed)

    def eventFilter(self, obj: QObject, event: QEvent) -> bool:
        """
        filter events
        :param obj: sender object
        :param event: event
        :return: bool
        """
        if event.type() == QEvent.KeyPress:
            if obj in [self.filter_widget, self.table_view, self]:
                if event.key() == Qt.Key_Escape:
                    if self._layout.indexOf(self.filter_widget) > -1:
                        self._hide_filter_widget()
                        return True
            if obj == self.table_view:
                if event.key() == Qt.Key_F and event.modifiers() == Qt.ControlModifier:
                    if self.filter_widget not in self._layout.children():
                        self._show_filter_widget()
                        return True
                if event.key() == Qt.Key_F5:
                    self.refreshData.emit()
                    return True
        return super().eventFilter(obj, event)

    def _hide_filter_widget(self) -> None:
        """
        hide filter widget
        :return: None
        """
        self.filter_widget.le_filter.clear()
        self._layout.removeWidget(self.filter_widget)
        self.filter_widget.hide()

    def _show_filter_widget(self) -> None:
        """
        hide filter widget
        :return: None
        """
        self._layout.addWidget(self.filter_widget)
        self.filter_widget.show()
        self.filter_widget.le_filter.setFocus()
