from PyQt5.QtCore import Qt, QPoint
from PyQt5.QtGui import QPaintEvent, QPainter, QColor, QPen, QResizeEvent
from PyQt5.QtWidgets import QWidget

from app.components.buttons.svg_button import SvgPaintButton


class EditMenuBarSvg(QWidget):
    def __init__(self, p_seperator_color: str = "#FFFFFF", p_background_color: str = "#6481e5",
                 iconSize: tuple = (40, 40), parent=None):
        super().__init__(parent)

        self.btn_add = SvgPaintButton(p_svg_path=":/images/button/plus_white.svg", parent=self)
        self.btn_del = SvgPaintButton(p_svg_path=":/images/button/minus_white.svg", parent=self)
        self.btn_edit = SvgPaintButton(p_svg_path=":/images/button/edit_white.svg", parent=self)

        self._back_ground_color: str = p_background_color
        self._seperator_color: str = p_seperator_color

        self._padding: int = 12
        # left top right bottom
        self._margin: list = [3, 3, 3, 3]
        
        # list with tool buttons
        self._buttons: list = []

        self._icon_size: tuple = iconSize

        self._init_widget()

    def buttons(self) -> list:
        return self._buttons

    def _init_widget(self) -> None:
        """init the widgets"""
        self.setAttribute(Qt.WA_StyledBackground | Qt.WA_TranslucentBackground)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.btn_add.resize(*self._icon_size)
        self.btn_del.resize(*self._icon_size)
        self.btn_edit.resize(*self._icon_size)
        
        self._buttons.extend([
            self.btn_add,
            self.btn_del,
            self.btn_edit
        ])
        self.btn_del.setCursor(Qt.PointingHandCursor)
        self.btn_add.setCursor(Qt.PointingHandCursor)
        self.btn_edit.setCursor(Qt.PointingHandCursor)

    def resizeEvent(self, e: QResizeEvent) -> None:
        """resize three buttons"""
        self.btn_add.move(self._margin[0], self._margin[1])
        self.btn_edit.move(self.btn_add.geometry().bottomRight().x() + self._padding, self._margin[1])
        self.btn_del.move(self.btn_edit.geometry().bottomRight().x() + self._padding, self._margin[1])

        self.setFixedSize(self.btn_del.geometry().bottomRight().x() + self._margin[2],
                          self.btn_del.geometry().bottomRight().y() + self._margin[3])

    def paintEvent(self, e: QPaintEvent) -> None:
        """paint background"""
        p = QPainter(self)
        p.setRenderHint(QPainter.Antialiasing)

        # draw background
        p.setPen(Qt.NoPen)
        p.setBrush(QColor(self._back_ground_color))
        p.drawRoundedRect(self.rect(), 6, 6)

        # white rounded pen
        pen = QPen()
        pen.setColor(QColor(self._seperator_color))
        pen.setStyle(Qt.PenStyle.SolidLine)
        pen.setCapStyle(Qt.PenCapStyle.RoundCap)
        pen.setWidth(3)
        p.setPen(pen)

        # y = self.height - margin top + half margin top
        # x = child right bottom pos + half padding

        for i, button in enumerate(self._buttons, 0):
            if i < len(self._buttons):
                x: int = button.geometry().bottomRight().x() + self._padding // 2
                y: int = self.height() - self._margin[3] - self._margin[3] // 2
                p.drawLine(QPoint(x, self._margin[1] + self._margin[1] // 2), QPoint(x, y))
