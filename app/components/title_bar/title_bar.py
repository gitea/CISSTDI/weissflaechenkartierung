# coding:utf-8
import sys

from app.common.style_sheet import setStyleSheet
from app.common.utils import startSystemMove
from PyQt5.QtCore import QEvent, Qt
from PyQt5.QtGui import QResizeEvent
from PyQt5.QtWidgets import QLabel, QWidget, QPushButton

from .title_bar_buttons import MaximizeButton, TitleBarButton


class TitleBar(QWidget):
    """ Title bar """

    def __new__(cls, *args, **kwargs):
        cls = WindowsTitleBar if sys.platform == "win32" else LinuxTitleBar
        return super().__new__(cls, *args, **kwargs)

    def __init__(self, parent):
        super().__init__(parent)
        self.resize(600, 40)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.titleLabel = QLabel(self.tr(""), self)

        # 57, 40 normal
        self.minButton = TitleBarButton((57, 35), parent=self)
        self.closeButton = TitleBarButton((57, 35), parent=self)
        self.returnButton = TitleBarButton((60, 39), self)
        self.maxButton = MaximizeButton((57, 35), self)

        self.pluginButton = QPushButton(self)

        self.__initWidget()

    def set_text(self, p_txt: str) -> None:
        """ sets the titlebar text"""
        self.titleLabel.setText(p_txt)
        self.titleLabel.adjustSize()

    def __initWidget(self):
        """ initialize widgets """
        self.setFixedHeight(40)
        self.__setQss()

        self.titleLabel.hide()
        self.returnButton.hide()

        # connect signal to slot
        self.minButton.clicked.connect(self.window().showMinimized)
        self.maxButton.clicked.connect(self.__showRestoreWindow)
        # self.closeButton.clicked.connect(self.window().close)     # main window should handle this

        # plugin button
        self.pluginButton.setFixedSize(60, 39)

        self.returnButton.installEventFilter(self)
        self.titleLabel.installEventFilter(self)
        self.window().installEventFilter(self)

        self.returnButton.setToolTip(self.tr("Zurück"))
        self.closeButton.setToolTip(self.tr("Schließen"))
        self.minButton.setToolTip(self.tr("Minimieren"))

    def __setQss(self):
        """ set style sheet """
        self.titleLabel.setObjectName("titleLabel")
        self.minButton.setObjectName("minButton")
        self.maxButton.setObjectName("maxButton")
        self.closeButton.setObjectName("closeButton")
        self.returnButton.setObjectName("returnButton")
        self.pluginButton.setObjectName("pluginButton")
        setStyleSheet(self, 'title_bar')

    def resizeEvent(self, e: QResizeEvent):
        # if we want the mechanic, the return button pushes the label to the right and backward
        # self.titleLabel.move(self.returnButton.isVisible() * 60, 0)
        self.titleLabel.move(60, 0)

        # if the window is maximized we do not need the 1px border
        # if there is none there is no need for a border
        border: int = 1
        if self.window().isMaximized():
            border = 0

        self.closeButton.move(self.width() - 57 - border, border)
        self.maxButton.move(self.width() - 2 * 57, border)
        self.minButton.move(self.width() - 3 * 57, 0)

    def mouseDoubleClickEvent(self, e):
        self.__showRestoreWindow()

    def __showRestoreWindow(self):
        """ show restored window """
        if self.window().isMaximized():
            self.window().showNormal()
        else:
            self.window().showMaximized()

    def setWhiteIcon(self, isWhiteIcon: bool):
        """ set icon color """
        for button in self.findChildren(TitleBarButton):
            button.setWhiteIcon(isWhiteIcon)

    def eventFilter(self, obj, e: QEvent):
        """filter the events"""
        if obj is self.returnButton:
            pass
            # if we want the mechanic, the return button pushes the label to the right and backward
            """
            if e.type() == QEvent.Hide:
                self.titleLabel.move(0, 0)
            elif e.type() == QEvent.Show:
                self.titleLabel.move(60, 0)
            """
        elif obj is self.titleLabel:
            if e.type() == QEvent.Show:
                # if we want the mechanic, the return button pushes the label to the right and backward
                # elf.titleLabel.move(60*self.returnButton.isVisible(), 0)
                self.titleLabel.move(60, 0)
        elif obj is self.window():
            if e.type() == QEvent.WindowStateChange:
                self.maxButton.setMaxState(self.window().isMaximized())
                self.maxButton.setAttribute(Qt.WA_UnderMouse, False)
                return False
        return super().eventFilter(obj, e)

    def _isDragRegion(self, pos):
        """ Check whether the pressed point belongs to the area where dragging is allowed """
        for button in self.findChildren(TitleBarButton):
            if button.isVisible() and button.isPressed:
                return False

        left = self.returnButton.isVisible() * 60
        right = self.width() - 46 - 92 * self.minButton.isVisible()
        return left < pos.x() < right


class WindowsTitleBar(TitleBar):
    """ Title bar for Windows system """

    def mouseMoveEvent(self, event):
        if not self._isDragRegion(event.pos()):
            return

        startSystemMove(self.window(), event.globalPos())


class LinuxTitleBar(TitleBar):
    """ Title bar for Linux system """

    def mousePressEvent(self, event):
        if event.button() != Qt.LeftButton or not self._isDragRegion(event.pos()):
            return

        startSystemMove(self.window(), event.globalPos())
