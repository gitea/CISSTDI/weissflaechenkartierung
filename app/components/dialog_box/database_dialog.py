from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QVBoxLayout, QLabel, QPushButton, QLineEdit, QHBoxLayout, QApplication

from ...common.style_sheet import setStyleSheet
from ...components.dialog_box import MaskDialogBase
from ...components.widgets.check_box import TextCheckBox
from ...components.widgets.line_edit import LineEdit, PasswordLineEdit


class DatabaseConnectionDialog(MaskDialogBase):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.hide()
        self.__pass_phrase = "fe1c9463ec37ffc6055e0be1a3d45ff26b08"

        self.vBoxLayout = QVBoxLayout(self.widget)
        
        self.label_info = QLabel(self.tr("Verbindungsinformationen"), self.widget)
        self.le_name = LineEdit(parent=self.widget)
        self.le_service = LineEdit(parent=self.widget)
        self.le_host = LineEdit(parent=self.widget)
        self.le_port = LineEdit(parent=self.widget)
        self.le_database = LineEdit(parent=self.widget)

        self.label_auth = QLabel(self.tr("Authentifizierung"), self.widget)
        self.le_username = LineEdit(parent=self.widget)
        self.le_password = PasswordLineEdit(parent=self.widget)

        self.btn_ok = QPushButton(self.tr("Ok"), self.widget)
        self.btn_cancel = QPushButton(self.tr("Abbrechen"), self.widget)

        self.cb_use_pw = TextCheckBox(self.widget)

        self.btn_cancel.setDefault(True)

        self._init_widget()

    def _set_qss(self) -> None:
        """
        set the qss
        :return: None
        """
        self.label_info.setObjectName("subTitleLabel")
        self.label_auth.setObjectName("subTitleLabel")
        setStyleSheet(self, 'interface')

    def _connect_signals(self) -> None:
        """
        connect slots
        :return: None
        """
        self.btn_ok.clicked.connect(self._try_to_close)
        self.btn_cancel.clicked.connect(self.hide)

        self.cb_use_pw.check_box.stateChanged.connect(self._on_identifier_needed_clicked)

        for edit in self.widget.findChildren(LineEdit):
            edit.textChanged.connect(self._txt_changed)
            edit.setProperty("hasError", False)
            edit.update()
            QApplication.processEvents()

    def _init_widget(self) -> None:
        """
        init widget
        :return: None
        """
        self._connect_signals()
        self._set_qss()
        self.vBoxLayout.setContentsMargins(30, 30, 30, 30)
        self.vBoxLayout.setSizeConstraint(QVBoxLayout.SetFixedSize)
        self.vBoxLayout.setAlignment(Qt.AlignTop)
        self.vBoxLayout.setSpacing(0)

        self.label_info.adjustSize()
        self.label_auth.adjustSize()

        self.le_name.setTitle(self.tr("Name"))
        self.le_service.setTitle(self.tr("Dienst"))
        self.le_host.setTitle(self.tr("Host"))
        self.le_port.setTitle(self.tr("Port"))
        self.le_database.setTitle(self.tr("Datenbank"))

        self.le_username.setTitle(self.tr("Benutzername"))
        self.le_password.setTitle(self.tr("Passwort"))
        self.le_password.setEchoMode(QLineEdit.Password)

        self.btn_ok.setFixedSize(100, 30)
        self.btn_cancel.setFixedSize(100, 30)

        self.cb_use_pw.setText(self.tr("Anmeldung aktivieren"))
        self.cb_use_pw.setFixedHeight(self.cb_use_pw.height())
        self.cb_use_pw.setChecked(False)

        self.label_auth.hide()
        self.le_username.hide()
        self.le_password.hide()

        # self.setTabOrder(self.le_database.edit, )

        self._init_layout()

    def _init_layout(self) -> None:
        """
        init the layout
        :return: None
        """
        self.vBoxLayout.addWidget(self.label_info)
        self.vBoxLayout.addWidget(self.le_name)
        self.vBoxLayout.addWidget(self.le_service)
        self.vBoxLayout.addWidget(self.le_host)
        self.vBoxLayout.addWidget(self.le_port)
        self.vBoxLayout.addWidget(self.le_database)
        self.vBoxLayout.addSpacing(10)
        self.vBoxLayout.addWidget(self.cb_use_pw)
        self.vBoxLayout.addSpacing(20)

        l = QHBoxLayout()
        l.setContentsMargins(0, 0, 0, 0)
        l.setSpacing(6)
        l.addStretch(1)
        l.addWidget(self.btn_ok)
        l.addWidget(self.btn_cancel)
        self.vBoxLayout.addSpacing(10)
        self.vBoxLayout.addLayout(l)
        # self.widget.setLayout(self.vBoxLayout)
        # self.adjustSize()

    # ###############################################################################
    # slots

    def _on_identifier_needed_clicked(self) -> None:
        """
        :return:
        """
        # show
        if self.cb_use_pw.check_box.isChecked():
            self.cb_use_pw.setText(self.tr("Anmeldung deaktivieren"))
            self.label_auth.show()
            self.le_username.show()
            self.le_password.show()
            idx: int =  self.vBoxLayout.indexOf(self.cb_use_pw)
            self.vBoxLayout.insertWidget(idx + 1, self.label_auth)
            self.vBoxLayout.insertWidget(idx + 2, self.le_username)
            self.vBoxLayout.insertWidget(idx + 3, self.le_password)
        else:
            self.cb_use_pw.setText(self.tr("Anmeldung aktivieren"))
            self.label_auth.hide()
            self.le_username.hide()
            self.le_password.hide()
            self.vBoxLayout.removeWidget(self.label_auth)
            self.vBoxLayout.removeWidget(self.le_username)
            self.vBoxLayout.removeWidget(self.le_password)

    def _txt_changed(self) -> None:
        """
        text changed
        :return:
        """
        edit = self.sender()
        if isinstance(edit, LineEdit):
            if not self.cb_use_pw.isChecked() and edit in [self.le_password, self.le_username]:
                return
            if edit == self.le_service:
                return
            if not edit.text():
                edit.setProperty("hasError", True)
            else:
                edit.setProperty("hasError", False)
            edit.update()
        QApplication.processEvents()

    def _try_to_close(self) -> None:
        """
        check given input and close then
        :return: None
        """
        close: bool = True
        for edit in self.widget.findChildren(LineEdit):
            if not self.cb_use_pw.isChecked() and edit in [self.le_password, self.le_username]:
                continue
            if edit == self.le_service:
                continue
            if not edit.text():
                close = False
                edit.setProperty("hasError", True)
                edit.update()
                QApplication.processEvents()
        if close:
            self.hide()

    def set_connection(self, p_con: dict) -> None:
        """
        set connection with given dict
        :param p_con:
        :return: None
        """
        if isinstance(p_con, list) and not len(p_con):
            return
        self.le_name.setText(p_con.setdefault("name", ""))
        self.le_service.setText(p_con.setdefault("service", ""))
        self.le_host.setText(p_con.setdefault("host", ""))
        self.le_port.setText(p_con.setdefault("port", "5432"))
        self.le_database.setText(p_con.setdefault("database", ""))
        self.le_username.setText(p_con.setdefault("user_name", ""))
        self.le_password.setText(p_con.setdefault("password", ""))
        self.cb_use_pw.setChecked(p_con.setdefault("need_auth", False))

    def get_connection(self) -> dict:
        """
        :return: connection information as dict
        """
        # password = self.le_password.text()
        # if password:
        #     password = os_utils.encrypt_password(password, self.__pass_phrase)
        return {
            "name": self.le_name.text(),
            "service": self.le_service.text(),
            "host": self.le_host.text(),
            "port": self.le_port.text(),
            "database": self.le_database.text(),
            "user_name": self.le_username.text(),
            "password": self.le_password.text(),
            "need_auth": self.cb_use_pw.isChecked()
        }




