from ...common.auto_wrap import auto_wrap
from ...common.style_sheet import setStyleSheet
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QLabel, QPushButton

from .mask_dialog_base import MaskDialogBase


class MessageDialog(MaskDialogBase):
    """Message dialog box with a mask"""

    yesSignal = pyqtSignal()
    cancelSignal = pyqtSignal()

    def __init__(self, title: str, content: str, parent):
        super().__init__(parent=parent)
        self.content = content
        self.titleLabel = QLabel(title, self.widget)
        self.contentLabel = QLabel(content, self.widget)
        self.yesButton = QPushButton(self.tr('Ok'), self.widget)
        self.cancelButton = QPushButton(
            self.tr('Abbrechen'), self.widget)
        self.yes_clicked: bool = False
        self.no_clicked: bool = False
        self.__initWidget()

    def __initWidget(self):
        """ initialize widgets """
        self.windowMask.resize(self.size())
        self.widget.setMaximumWidth(500)
        self.titleLabel.move(30, 30)
        self.contentLabel.move(30, 70)
        self.contentLabel.setText(auto_wrap(self.content, 71)[0])

        self.__setQss()
        self.__initLayout()

        # connect signal to slot
        self.yesButton.clicked.connect(self.__onYesButtonClicked)
        self.cancelButton.clicked.connect(self.__onCancelButtonClicked)

        self.cancelButton.raise_()
        self.cancelButton.setFocus()

    def __initLayout(self):
        """ initialize layout """
        self.contentLabel.adjustSize()
        # self.widget.setFixedSize(350, 150)
        # width and height of the widget
        self.widget.setFixedSize(60 + self.contentLabel.width(),
                                 self.contentLabel.y() + self.contentLabel.height() + 115)

        self.yesButton.resize((self.widget.width() - 68) // 2, 35)
        self.cancelButton.resize(self.yesButton.width(), 35)

        self.yesButton.move(30, self.widget.height() - 70)
        self.cancelButton.move(
            self.widget.width() - 30 - self.cancelButton.width(), self.widget.height() - 70)

    def __onCancelButtonClicked(self):
        self.cancelSignal.emit()
        self.no_clicked = True
        self.hide()

    def __onYesButtonClicked(self):
        self.setEnabled(False)
        self.yes_clicked = True
        self.yesSignal.emit()
        self.hide()

    def __setQss(self):
        """ set style sheet """
        self.windowMask.setObjectName('windowMask')
        self.titleLabel.setObjectName('titleLabel')
        self.contentLabel.setObjectName('contentLabel')
        self.cancelButton.setObjectName('cancelButton')
        setStyleSheet(self, 'message_dialog')
