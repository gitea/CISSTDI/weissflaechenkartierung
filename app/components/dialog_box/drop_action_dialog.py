from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QLabel, QVBoxLayout

from ...common.style_sheet import setStyleSheet
from ...components.dialog_box.mask_dialog_base import MaskDialogBase
from ...components.widgets.label import UploadLogoLabel


class DropActionDialog(MaskDialogBase):
    """DropActionDialog with mask background"""

    def __init__(self, parent=None):
        super().__init__(parent)

        self.titleLabel = QLabel(self.tr("Konfigurationsdatei"), self.widget)
        self.logoLabel = UploadLogoLabel(self.widget)
        self.__initWidget()

    def __setQss(self):
        """set style sheet"""
        self.windowMask.setObjectName('windowMask')
        self.titleLabel.setObjectName('titleLabel')
        setStyleSheet(self, 'message_dialog')

    def __initWidget(self):
        """initialize widget"""
        self.__setQss()
        self.setAttribute(Qt.WA_TransparentForMouseEvents)

        self.windowMask.resize(self.size())
        self.titleLabel.adjustSize()
        self.titleLabel.setFixedWidth(self.titleLabel.fontMetrics().width(self.titleLabel.text()))
        self.logoLabel.setFixedSize(45, 45)

        self.__initLayout()

    def __initLayout(self):
        """initialize layout"""
        self.widget.setFixedSize(self.titleLabel.width() +  60,
                                 self.logoLabel.geometry().bottomLeft().y() + 60)
        l = QVBoxLayout()
        l.setContentsMargins(20, 20, 20, 20)
        l.addWidget(self.titleLabel)
        l.addWidget(self.logoLabel, 0, Qt.AlignHCenter)
        self.widget.setLayout(l)
