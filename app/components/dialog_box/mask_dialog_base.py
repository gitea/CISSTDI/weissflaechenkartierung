from PyQt5.QtCore import QEasingCurve, QPropertyAnimation, Qt, QPoint, QParallelAnimationGroup
from PyQt5.QtGui import QColor
from PyQt5.QtWidgets import (QDialog, QGraphicsDropShadowEffect,
                             QGraphicsOpacityEffect, QHBoxLayout, QWidget)

from ...common.config import config


class MaskDialogBase(QDialog):
    """Dialog box base class with a mask"""

    def __init__(self, parent):
        super().__init__(parent=parent)
        self.__hBoxLayout = QHBoxLayout(self)
        self.windowMask = QWidget(self)

        # dialog box in the center of mask, all widgets take it as parent
        self.widget = QWidget(self, objectName='centerWidget')
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.setGeometry(0, 0, parent.width(), parent.height())

        c = 0 if config.theme == 'dark' else 255
        self.windowMask.resize(self.size())
        self.windowMask.setStyleSheet(f'background:rgba({c}, {c}, {c}, 0.6)')
        self.__hBoxLayout.addWidget(self.widget)
        self.__setShadowEffect()
        self._anim_group = QParallelAnimationGroup()

    def __setShadowEffect(self):
        """ add shadow to dialog """
        shadowEffect = QGraphicsDropShadowEffect(self.widget)
        shadowEffect.setBlurRadius(60)
        shadowEffect.setOffset(0, 10)
        shadowEffect.setColor(QColor(0, 0, 0, 100))
        self.widget.setGraphicsEffect(shadowEffect)

    def _create_opacity_anim(self, start: int, end: int, duration: int, del_effect: bool = False) -> QPropertyAnimation:
        """
        create opacity anim
        """
        opacityEffect = QGraphicsOpacityEffect(self)
        self.setGraphicsEffect(opacityEffect)
        opacity_ani = QPropertyAnimation(opacityEffect, b'opacity', self)
        opacity_ani.setStartValue(start)
        opacity_ani.setEndValue(end)
        opacity_ani.setDuration(duration)
        if del_effect:
            opacity_ani.finished.connect(opacityEffect.deleteLater)
        return opacity_ani

    def _create_pos_anim(self, start: QPoint, end: QPoint, duration: int) -> QPropertyAnimation:
        """
        create pos animation
        """
        slide_anim = QPropertyAnimation(self.widget, b'pos')
        slide_anim.setStartValue(start)
        slide_anim.setEndValue(end)
        slide_anim.setDuration(duration)
        return slide_anim

    def adjust_widget_geometry(self):
        """adjust the layout"""
        # labels calc text len
        self.move(0, 0)
        self.resize(self.parent().size())
        self.windowMask.resize(self.parent().size())

    def showEvent(self, e):
        """fade in"""
        opacity_ani = self._create_opacity_anim(0, 1, 200, True)
        opacity_ani.setEasingCurve(QEasingCurve.InSine)

        x = self.parent().geometry().width() // 2 - self.widget.width() // 2
        y = self.parent().geometry().height() // 2 - self.widget.height() // 2

        slide_anim = self._create_pos_anim(QPoint(x, y) - QPoint(0, 10), QPoint(x, y), 250)
        slide_anim.setEasingCurve(QEasingCurve.InOutCubic)

        self._anim_group.clear()
        self._anim_group.addAnimation(opacity_ani)
        self._anim_group.addAnimation(slide_anim)
        self._anim_group.start()
        super().showEvent(e)

    def closeEvent(self, e):
        """fade out"""
        self.widget.setGraphicsEffect(None)
        opacity_ani = self._create_opacity_anim(1, 0, 100)
        opacity_ani.setEasingCurve(QEasingCurve.OutCubic)
        opacity_ani.finished.connect(self.deleteLater)

        self._anim_group.clear()
        self._anim_group.addAnimation(opacity_ani)
        self._anim_group.start()
        e.ignore()

    def hideEvent(self, e) -> None:
        """fade out"""
        self.widget.setGraphicsEffect(None)
        opacity_ani = self._create_opacity_anim(1, 0, 100)
        opacity_ani.setEasingCurve(QEasingCurve.OutCubic)
        # opacity_ani.finished.connect(self.deleteLater)

        self._anim_group.clear()
        self._anim_group.addAnimation(opacity_ani)
        self._anim_group.start()
        e.ignore()
