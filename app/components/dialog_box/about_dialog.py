from PyQt5.QtCore import QEvent, Qt, QTimer
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QLabel, QVBoxLayout, QHBoxLayout, QPushButton

from ...common import os_utils
from ...common.config import config
from ...common.style_sheet import setStyleSheet
from ...components.dialog_box.mask_dialog_base import MaskDialogBase
from ...components.widgets.label import WfkLogoLabel, ClickableLogoLabel


class AboutDialog(MaskDialogBase):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.hide()

        self.hBoxLayout = QHBoxLayout(self.widget)

        self.logoLabel = WfkLogoLabel(parent=self.widget)
        self.titleLabel = QLabel(self.tr("Weißflächenkartierung"), self.widget)
        self.buildLabel = QLabel(self.tr("Version {}".format(config.version)), self.widget)
        hyperlink = "<a href=\"http://www.ciss.de\">'CISS TDI GmbH'</a>"
        self.poweredByLabel = QLabel(hyperlink, self.widget)
        self.poweredByLabel.setOpenExternalLinks(True)
        self.poweredByLabel.setToolTip("http://www.ciss.de")
        self.contentLabel = QLabel(self.widget)
        self.contentLabel.setText(self.tr("Die CISS TDI GmbH ist eine Technologiegesellschaft,<br> die für Unternehmen "
                                          "und den öffentlichen Dienst <br>Geodaten beschafft, mittels eigener "
                                          "Werkzeuge veredelt <br>und über Plattformen bereitstellt."))
        self.copyRightLabel = QLabel(self.tr("GNU General Public License Version 2"), self.widget)

        self.gitHubLogoLabel = ClickableLogoLabel(self.widget)
        self.xingLogoLabel = ClickableLogoLabel(self.widget)

        self.btn_close = QPushButton(self.tr("Schließen"), self.widget)

        self.__init_widgets()

    def __init_widgets(self):
        """ init the widgets """
        self.__set_qss()

        self.widget.setFixedSize(self.logoLabel.width() + self.copyRightLabel.width() + 60,
                                 self.contentLabel.width())

        self.windowMask.resize(self.parent().size())
        self.windowMask.installEventFilter(self)
        self.windowMask.setMouseTracking(True)

        self.logoLabel.setFixedSize(70, 70)

        self.gitHubLogoLabel.setPixmap(QPixmap(":/images/about_dialog/github.png"))
        self.gitHubLogoLabel.clicked.connect(
            lambda: QTimer.singleShot(0, lambda: os_utils.show_in_folder("https://github.com/cisstdi"))
        )
        self.gitHubLogoLabel.setFixedSize(28, 28)
        self.gitHubLogoLabel.setToolTip("https://github.com/cisstdi")

        self.xingLogoLabel.setPixmap(QPixmap(":/images/about_dialog/xing.png"))
        self.xingLogoLabel.clicked.connect(
            lambda: QTimer.singleShot(0, lambda: os_utils.show_in_folder("https://www.xing.com/pages/ciss-tdi-gmbh"))
        )
        self.xingLogoLabel.setFixedSize(31, 31)
        self.xingLogoLabel.setToolTip("https://www.xing.com/pages/ciss-tdi-gmbh")

        self.btn_close.setFixedSize(100, 25)
        self.btn_close.setToolTip(self.tr("Schließen"))
        self.btn_close.clicked.connect(self.hide)

        self.__init_layout()

    def __init_layout(self):
        """ init the layout """
        self.hBoxLayout.setContentsMargins(30, 30, 30, 30)
        self.hBoxLayout.setSizeConstraint(QVBoxLayout.SetFixedSize)
        self.hBoxLayout.setAlignment(Qt.AlignTop)
        self.hBoxLayout.setSpacing(6)

        # logo
        self.hBoxLayout.addWidget(self.logoLabel, 0, Qt.AlignTop)
        self.hBoxLayout.addSpacing(15)

        # labels
        layout_1 = QVBoxLayout()
        layout_1.setContentsMargins(0, 0, 0, 0)
        layout_1.setSpacing(7)
        layout_1.addWidget(self.titleLabel, 0, Qt.AlignTop)
        layout_1.addWidget(self.contentLabel, 0, Qt.AlignTop)
        layout_1.addWidget(self.buildLabel, 0, Qt.AlignTop)
        layout_1.addWidget(self.poweredByLabel, 0, Qt.AlignTop)
        layout_1.addWidget(self.copyRightLabel, 0, Qt.AlignTop)

        # contact logos
        layout_2 = QHBoxLayout()
        layout_2.setContentsMargins(0, 0, 0, 0)
        layout_2.setSpacing(7)
        layout_2.addWidget(self.xingLogoLabel, 0, Qt.AlignRight)
        layout_2.addWidget(self.gitHubLogoLabel, 0)
        layout_1.addLayout(layout_2)

        # close button
        layout_1.addSpacing(20)
        layout_1.addWidget(self.btn_close, 0, Qt.AlignRight)

        self.hBoxLayout.addLayout(layout_1, 0)

    def __set_qss(self):
        """ set the qss """
        self.titleLabel.setObjectName('titleLabel')
        self.buildLabel.setObjectName('buildLabel')
        self.poweredByLabel.setObjectName('poweredByLabel')
        self.copyRightLabel.setObjectName('copyRightLabel')

        setStyleSheet(self, 'dialog')

    def eventFilter(self, _object, event) -> bool:
        if _object == self.windowMask:
            if event.type() == QEvent.MouseButtonPress:
                self.hide()
        return super(AboutDialog, self).eventFilter(_object, event)
