from PyQt5.QtCore import Qt, QRect
from PyQt5.QtGui import QPainter, QColor
from PyQt5.QtWidgets import QWidget

from app.common.config import config


class BorderWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.windowMask = QWidget(self)
        self.setAttribute(Qt.WA_NoSystemBackground)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.setAttribute(Qt.WA_TransparentForMouseEvents)
        self.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)

        self.setStyleSheet('QWidget#Mask{background:transparent;}')

        self.draw_focus_line: bool = True
        # we do net get the expected color from this
        # maybe HKEY_CURRENT_USER\SOFTWARE\Microsoft\Windows\DWM give us that
        # self._border_color: QColor = self.window().palette().color(QPalette.Window)
        self._border_color = QColor(config.active_border_color)
        self._inactive_border_color = QColor(config.inactive_border_color)

    def update(self) -> None:
        self.resize(self.window().size())
        self.windowMask.resize(self.window().size())
        super(BorderWidget, self).update()

    def paintEvent(self, e) -> None:
        """draw rect around the given geometry"""
        p = QPainter(self)
        p.setRenderHint(QPainter.Antialiasing)
        if self.draw_focus_line:
            p.setPen(self._border_color)    # draw line
        else:
            p.setPen(self._inactive_border_color)    # draw line
        # rect to draw -> myself rect
        rect = QRect(0, 0, self.width(), self.height())
        p.drawRect(rect)
        p.end()
