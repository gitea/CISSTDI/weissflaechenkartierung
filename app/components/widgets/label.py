from PyQt5 import QtSvg
from PyQt5.QtGui import QPixmap, QPainter, QMouseEvent, QPaintEvent
from PyQt5.QtWidgets import QLabel, QWidget
from PyQt5.QtCore import Qt, pyqtSignal, QRect


class ClickableLabel(QLabel):
    """ Clickable label """

    clicked = pyqtSignal()
    enter = pyqtSignal()
    leave = pyqtSignal()

    def __init__(self, text="", parent=None, isSendEventToParent: bool = True):
        super().__init__(text, parent)
        self.isSendEventToParent = isSendEventToParent

    def enterEvent(self, e) -> None:
        if self.isSendEventToParent:
            super(ClickableLabel, self).enterEvent(e)
        self.enter.emit()

    def leaveEvent(self, e) -> None:
        if self.isSendEventToParent:
            super(ClickableLabel, self).leaveEvent(e)
        self.leave.emit()

    def mousePressEvent(self, e):
        if self.isSendEventToParent:
            super().mousePressEvent(e)

    def mouseReleaseEvent(self, event: QMouseEvent):
        if self.isSendEventToParent:
            super().mouseReleaseEvent(event)
        if event.button() == Qt.LeftButton:
            self.clicked.emit()


class PixmapLabel(QLabel):
    """ Label for high dpi pixmap """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.__pixmap = QPixmap()

    def setPixmap(self, pixmap: QPixmap):
        self.__pixmap = pixmap
        self.setFixedSize(self.size())
        self.update()

    def pixmap(self):
        return self.__pixmap

    def paintEvent(self, e):
        if self.__pixmap.isNull():
            return

        painter = QPainter(self)
        painter.setRenderHints(QPainter.Antialiasing |
                               QPainter.SmoothPixmapTransform)
        painter.setPen(Qt.NoPen)
        painter.drawPixmap(self.rect(), self.__pixmap)


class SvgLabel(ClickableLabel):
    def __init__(self, parent=None):
        super().__init__(parent=parent, text="")
        self._svg_path: str = ""

    @property
    def svg_path(self) -> str:
        """return path to the svg file"""
        return self._svg_path

    @svg_path.setter
    def svg_path(self, value: str):
        """set a new path"""
        self._svg_path = value
        self.update()

    def paintEvent(self, event: QPaintEvent) -> None:
        """paint svg"""
        painter = QPainter(self)
        painter.setRenderHints(QPainter.Antialiasing |
                               QPainter.SmoothPixmapTransform)
        svg = QtSvg.QSvgRenderer(self._svg_path)
        svg.setViewBox(QRect())
        svg.render(painter)
        painter.end()


class WfkLogoLabel(SvgLabel):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.svg_path = ":/images/about_dialog/wind_wheel_rotate.svg"

class UploadLogoLabel(SvgLabel):
    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.svg_path = ":/images/wfk_interface/icon_upload.svg"


class ErrorIcon(PixmapLabel):
    def __init__(self, parent=None):
        super(ErrorIcon, self).__init__(parent)
        self.setPixmap(QPixmap(":/images/main_window/ciss.png"))


class ClickableLogoLabel(PixmapLabel):

    clicked = pyqtSignal()

    def __init__(self, parent=None, isSendEventToParent: bool = True):
        super().__init__(parent)
        self.isSendEventToParent = isSendEventToParent
        self.setCursor(Qt.PointingHandCursor)

    def mousePressEvent(self, e):
        if self.isSendEventToParent:
            super().mousePressEvent(e)

    def mouseReleaseEvent(self, event: QMouseEvent):
        if self.isSendEventToParent:
            super().mouseReleaseEvent(event)
        if event.button() == Qt.LeftButton:
            self.clicked.emit()


class ClickableIconLabel(QWidget):
    def __init__(self, parent=None, isSendEventToParent: bool = True):
        super().__init__(parent)
        self.label = ClickableLabel(parent=self, isSendEventToParent=isSendEventToParent)
        self.icon_label = PixmapLabel(parent=self)

        self.show_icon: bool = True

        self.__init_widget()

    def setText(self, p_txt: str) -> None:
        """ sets the new text and adjusts the size"""
        self.label.setText(p_txt)
        self.label.adjustSize()
        self._adjust_layout()

    def setIcon(self, p_path: str) -> None:
        """ sets the icon path """
        self.icon_label.setPixmap(QPixmap(p_path))

    def showIcon(self, p_show: bool) -> None:
        """ set the show icon flag """
        self.show_icon = p_show
        self.icon_label.hide()
        self.label.adjustSize()
        self._adjust_layout()

    def __init_widget(self):
        self.resize(200, 31)
        self.icon_label.setFixedSize(25, 25)
        self.label.setCursor(Qt.PointingHandCursor)

    def _adjust_layout(self) -> None:
        """ init the layout """
        if self.show_icon:
            self.icon_label.move(0, 0)
            self.label.move(0 + self.icon_label.width() + 15,
                            int(self.icon_label.height()/2) - int(self.label.height()/2))
        else:
            self.label.move(0, 0)
        self.adjustSize()

    def resizeEvent(self, e):
        self._adjust_layout()
