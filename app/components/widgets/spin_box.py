from PyQt5.QtCore import QPoint, QObject, QEvent
from PyQt5.QtWidgets import QDoubleSpinBox, QSpinBox

from app.common.config import config
from app.components.widgets.tooltip import Tooltip


class SpinBox(QSpinBox):
    """
    Spinbox with modern shadow
    """
    def __init__(self, parent=None):
        super().__init__(parent)
        self.__tooltip = None
        self.__darkTooltip = config.theme == 'dark'
        self.setSingleStep(1)
        self.setMaximum(50)
        self.setMinimum(1)
        self.installEventFilter(self)

    def eventFilter(self, obj: QObject, e: QEvent) -> bool:
        """
        filter tooltip event
        :param obj:
        :param e:
        :return:
        """
        if obj is self:
            if e.type() == QEvent.ToolTip:
                return True
        return super().eventFilter(obj, e)

    def enterEvent(self, e) -> None:
        """
        show tooltip
        :param e: enter event
        :return: None
        """
        super().enterEvent(e)
        if not self.toolTip():
            return

        if self.__tooltip is None:
            self.__tooltip = Tooltip(self.toolTip(), self.window())
            self.__tooltip.setDarkTheme(self.__darkTooltip)

        # update tooltip
        if self.__tooltip.text != self.toolTip():
            self.__tooltip.setText(self.toolTip())

        # the tooltip must be moved outside the button area
        pos = self.mapTo(self.window(), QPoint(0, 0))
        x = pos.x() + self.width()//2 - self.__tooltip.width()//2
        y = pos.y() - 2 - self.__tooltip.height()

        # adjust position to prevent tooltips from appearing outside the window
        x = min(max(5, x), self.window().width() - self.__tooltip.width() - 5)

        self.__tooltip.move(x, y)
        self.__tooltip.show()

    def leaveEvent(self, e) -> None:
        super().leaveEvent(e)
        if self.__tooltip:
            self.__tooltip.hide()

    def hideEvent(self, e) -> None:
        super().hideEvent(e)
        if self.__tooltip:
            self.__tooltip.hide()

    def hideToolTip(self) -> None:
        """ hide tooltip """
        if self.__tooltip:
            self.__tooltip.hide()

class DoubleSpinBox(QDoubleSpinBox):
    """
    Spinbox with modern shadow
    """
    def __init__(self, parent=None):
        super().__init__(parent)
        self.__tooltip = None
        self.__darkTooltip = config.theme == 'dark'
        self.setSingleStep(0.25)
        self.setMaximum(50000)
        self.installEventFilter(self)

    def eventFilter(self, obj: QObject, e: QEvent) -> bool:
        """
        filter tooltip event
        :param obj:
        :param e:
        :return:
        """
        if obj is self:
            if e.type() == QEvent.ToolTip:
                return True
        return super().eventFilter(obj, e)

    def enterEvent(self, e) -> None:
        """
        show tooltip
        :param e: enter event
        :return: None
        """
        super().enterEvent(e)
        if not self.toolTip():
            return

        if self.__tooltip is None:
            self.__tooltip = Tooltip(self.toolTip(), self.window())
            self.__tooltip.setDarkTheme(self.__darkTooltip)

        # update tooltip
        if self.__tooltip.text != self.toolTip():
            self.__tooltip.setText(self.toolTip())

        # the tooltip must be moved outside the button area
        pos = self.mapTo(self.window(), QPoint(0, 0))
        x = pos.x() + self.width()//2 - self.__tooltip.width()//2
        y = pos.y() - 2 - self.__tooltip.height()

        # adjust position to prevent tooltips from appearing outside the window
        x = min(max(5, x), self.window().width() - self.__tooltip.width() - 5)

        self.__tooltip.move(x, y)
        self.__tooltip.show()

    def leaveEvent(self, e) -> None:
        super().leaveEvent(e)
        if self.__tooltip:
            self.__tooltip.hide()

    def hideEvent(self, e) -> None:
        super().hideEvent(e)
        if self.__tooltip:
            self.__tooltip.hide()

    def hideToolTip(self) -> None:
        """ hide tooltip """
        if self.__tooltip:
            self.__tooltip.hide()