from PyQt5.QtGui import QFontMetrics
from PyQt5.QtWidgets import QPushButton


class Button(QPushButton):
    """ button which can set its size bigger to fit in the text no wrap """
    def __init__(self, parent=None):
        super().__init__(parent)

    def setText(self, text: str) -> None:
        super(Button, self).setText(text)
        # calculate the text metrics
        width = self.fontMetrics().width(text) + 20
        height = QFontMetrics(self.fontMetrics()).height() + 8
        # check if the text fits inside the button, 20 is margin
        if self.width() < width or self.height() < height:
            self.setFixedSize(width, height)

