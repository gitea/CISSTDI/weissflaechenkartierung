from typing import Any

from PyQt5.QtCore import QModelIndex, Qt, QEvent, pyqtSignal, QEasingCurve, QAbstractAnimation, QSizeF, \
    QPointF, pyqtSlot
from PyQt5.QtGui import QBrush, QColor, QFontMetrics, QFont, QPalette
from PyQt5.QtWidgets import QStyledItemDelegate, QComboBox, QStyleOptionViewItem, QStyle, QFrame, QWidget, QLineEdit

from app.common.str_utils import adapt_widget_font_size
from app.components.widgets.group_box import GroupBoxWidget

USER_ROLE_LAYER_DATA = Qt.UserRole + 1

class FormatDelegate(QStyledItemDelegate):
    def sizeHint(self, option, index):
        size = super().sizeHint(option, index)
        size.setHeight(20)
        size.setWidth(QFontMetrics(option.font).width(index.data()))
        return size

    def initStyleOption(self, option: QStyleOptionViewItem, index: QModelIndex) -> None:
        """style the item"""
        super(FormatDelegate, self).initStyleOption(option, index)
        color: QColor = QColor("#606060")

        # set selection style
        if option.state & QStyle.State_Selected:
            color = QColor("#191C2C")
            option.state &= ~ QStyle.State_Selected
            # font
            option.font = QFont(option.font)
            option.fontMetrics = QFontMetrics(QFont(option.font))

            # color
            option.backgroundBrush = QBrush(QColor("#efefef"))   # #B2C0FA

        option.palette.setBrush(QPalette.Text, color)


class ComboBoxBase(QComboBox):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.view().window().setWindowFlags(Qt.Popup | Qt.FramelessWindowHint | Qt.NoDropShadowWindowHint)
        self.view().window().setAttribute(Qt.WA_TranslucentBackground)    # hides square box behind widget when borders
        self.setItemDelegate(FormatDelegate())

        # if the edit is disabled, set the style
        edit = QLineEdit(self)
        edit.setStyleSheet("background-color: white;border: none;"
                           "padding-top:0px;padding-bottom:0px;margin-top:0px;margin-bottom:0px;")
        self.setLineEdit(edit)
        self.distance: int = 3

        self.setEditable(True)
        self.lineEdit().setReadOnly(True)
        self.lineEdit().setDisabled(True)
        self._close_on_line_edit_click = False

        self.installEventFilter(self)
        self.view().installEventFilter(self)
        self.lineEdit().installEventFilter(self)

    def eventFilter(self, obj, e: QEvent) -> bool:
        """
        :param obj: sender object
        :param e: event
        :return: handled
        """
        if obj in [self.lineEdit(), self]:
            if e.type() == QEvent.MouseButtonRelease:
                if self._close_on_line_edit_click:
                    self.hidePopup()
                else:
                    self.showPopup()
                return True
        if e.type() == QEvent.Show and obj == self.view():
            frame: QWidget = self.findChild(QFrame)
            if frame is not None:
                # place QAbstractItemView under the widget with 2px space
                y = self.mapToGlobal(self.lineEdit().geometry().bottomLeft()).y() + self.distance
                frame.move(frame.x(), y)
                return True
        return super().eventFilter(obj, e)

    def showPopup(self) -> None:
        """
        update flag before show popup
        :return: None
        """
        super().showPopup()
        self._close_on_line_edit_click = True

    def hidePopup(self) -> None:
        """
        reset after hide
        :return: None
        """
        super().hidePopup()
        self.startTimer(100)

    def timerEvent(self, event):
        # After timeout, kill timer, and re-enable click online edit
        self.killTimer(event.timerId())
        self._close_on_line_edit_click = False

    def index(self, p_txt: str, p_role: int = Qt.DisplayRole) -> int:
        """
        return the index of the given string
        :param p_role: int
        :param p_txt: str
        :return: int
        """
        if self.model() is None:
            return -1
        for i in range(self.model().rowCount()):
            index: QModelIndex = self.model().index(i, 0)
            if index:
                if index.data(p_role) == p_txt:
                    return i
        return -1


class ComboBox(GroupBoxWidget):
    """
    Outline Combobox
    """
    currentIndexChanged = pyqtSignal(int)
    currentTextChanged = pyqtSignal(str)

    def __init__(self, parent, text: str = "", angle_start: int = 6, easing_curve = QEasingCurve.Linear):
        super().__init__(parent, text, angle_start, easing_curve)
        self.combo_box = ComboBoxBase(parent=self)
        self._user_changed: bool = False
        self._place_holder_text: str = "PlaceHolder"
        self._init_widget()

    def setProperty(self, name: str, value: Any) -> bool:
        r = super().setProperty(name, value)
        if r:
            self.combo_box.setProperty(name, value)
        return r

    def resizeEvent(self, e) -> None:
        self.update_text()
        super().resizeEvent(e)
        self._adjust_layout()

    def eventFilter(self, obj, e: QEvent) -> bool:
        """
        :param obj:
        :param e:
        :return:
        """
        if obj == self:
            if e.type() == QEvent.ToolTip:
                if self._hit_box().contains(e.pos()):
                    return False
                return True
            if e.type() == QEvent.Leave:
                self._hover = False
                self.update()
            elif e.type() in [
                QEvent.MouseMove,
                QEvent.MouseButtonRelease,
                QEvent.MouseButtonPress
            ]:
                if self._hit_box().contains(e.pos()):
                    self._hover = True
                else:
                    self.setCursor(Qt.ArrowCursor)
                    self._hover = False
                self.update()
                return False
        if obj == self.combo_box:
            if e.type() in [
                QEvent.MouseMove,
                QEvent.MouseButtonRelease,
                QEvent.MouseButtonPress
            ]:
                self._hover = True
                return False
        return super().eventFilter(obj, e)

    # ###########################################################################

    def _adjust_layout(self) -> None:
        """
        :return: None
        """
        cr = self._contents_rect().toRect()
        cr.setHeight(cr.height() - self.fontMetrics().height() // 2 + 4) # 4
        cr.setY(cr.y() + self.fontMetrics().height() // 2 - 1)
        cr.setWidth(cr.width() - 4) # 3
        self.combo_box.setFixedSize(cr.size())
        self.combo_box.move(
            self.contentsMargins().left() + 1,
            cr.y()
        )

    def _compute_min_size(self) -> QSizeF:
        r = super()._compute_min_size()
        m = self.contentsMargins()
        r.setHeight(
            m.bottom() + m.top() + self.combo_box.height()  + 6
        )
        return r

    def _connect_signals(self) -> None:
        """
        :return: None
        """
        self.combo_box.currentTextChanged.connect(self.currentTextChanged)
        self.combo_box.currentIndexChanged.connect(self.currentIndexChanged)

        self.combo_box.model().dataChanged.connect(self.update_text)
        self.combo_box.currentTextChanged.connect(self.current_text_changed)

    def _init_widget(self) -> None:
        """
        :return: None
        """
        super()._init_widget()
        self.combo_box.show()
        self.combo_box.setMinimumSize(self._contents_rect().toRect().size())
        self.combo_box.installEventFilter(self)
        font = self.font()
        font.setPixelSize(int(
            adapt_widget_font_size(self.combo_box, 0, self.combo_box.lineEdit().rect(), self.combo_box.currentText(),
                                   font))
        )
        self.combo_box.setFont(font)
        self.combo_box.setCursor(Qt.ArrowCursor)
        self.combo_box.setAttribute(Qt.WA_NoSystemBackground)
        self.combo_box.distance = 7
        self.setCursor(Qt.ArrowCursor)
        self._title_up = True   # the title is up, do not draw complete border
        self.start_transition(QAbstractAnimation.Forward)
        self._connect_signals()

    @pyqtSlot(int)
    def start_transition(self, p_direction: int) -> None:
        """
        start transition
        :return: None
        """
        self._pos_animation.stop()
        start = QPointF(
            self._text_padding,
            self.contentsMargins().top() + (
                    (self.height() - self.contentsMargins().top()) / 2) - self.fontMetrics().height() / 2 + 1
        )
        try:
            from qgis.core import QgsStyle
            offset = 5
        except:
            offset = 0

        end = QPointF(
            self._angle_start + self._title_padding,
            self.contentsMargins().top() - self.fontMetrics().capHeight() + offset
        )

        if p_direction == QAbstractAnimation.Forward:
            self._pos_animation.setStartValue(start)
            self._pos_animation.setEndValue(end)
        else:
            self._pos_animation.setStartValue(end)
            self._pos_animation.setEndValue(start)
        self._pos_animation.start()

    # ###########################################################################

    def current_text_changed(self, p_new_text: str) -> None:
        """
        current text changed
        :param p_new_text: new text as string
        :return: None
        """
        self._user_changed = True

    def update_text(self) -> None:
        """
        update text
        :return: None
        """
        if self.model().item(0, 0) is None:
            return

        if not self._user_changed:
            self.combo_box.lineEdit().setText(self._place_holder_text)
            return
        self.combo_box.lineEdit().setText(self.currentText())

    # ###########################################################################

    def itemData(self, index, role=Qt.ItemDataRole.UserRole) -> any:
        return self.combo_box.itemData(index, role)

    def index(self, text: str, role=Qt.ItemDataRole.UserRole ) -> int:
        return self.combo_box.index(text, role)

    def setItemText(self, index: int, text: str) -> None:
        self.combo_box.setItemText(index, text)

    def removeItem(self, index: int) -> None:
        self.combo_box.removeItem(index)

    def setPlaceHolderText(self, text: str) -> None:
        """
        new placeholder
        :param text: text as str
        :return: None
        """
        self._place_holder_text = text
        self.update()

    def addItem(self, text: str, userData: Any = None) -> None:
        self.combo_box.addItem(text, userData)

    def setCurrentIndex(self, index: int) -> None:
        self.combo_box.setCurrentIndex(index)

    def setCurrentText(self, text: str) -> None:
        self.combo_box.setCurrentText(text)

    def setItemData(self, index, value, role=Qt.ItemDataRole.UserRole):
        self.combo_box.setItemData(index, value, role)

    def clear(self) -> None:
        self.combo_box.clear()

    def setModel(self, model) -> None:
        self.combo_box.setModel(model)

    def model(self) -> Any:
        return self.combo_box.model()

    def count(self) -> int:
        return self.combo_box.count()

    def currentIndex(self) -> int:
        return self.combo_box.currentIndex()

    def currentData(self, role=Qt.ItemDataRole.UserRole) -> Any:
        return self.combo_box.currentData(role)

    def currentText(self) -> str:
        return self.combo_box.currentText()