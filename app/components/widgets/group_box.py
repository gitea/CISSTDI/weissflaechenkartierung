from typing import Any

from PyQt5.QtCore import QPointF, Qt, QRectF, QEvent, pyqtSlot, QPropertyAnimation, QEasingCurve, pyqtProperty, \
    QAbstractAnimation, QSizeF
from PyQt5.QtGui import QPainterPath, QPainter, QPen, QFont, QColor, QFontMetrics
from PyQt5.QtWidgets import QGroupBox


class GroupBoxWidget(QGroupBox):
    def __init__(self, parent=None, text: str = "", angle_start: int = 4,
                 easing_curve: QEasingCurve = QEasingCurve.Linear):
        super(GroupBoxWidget, self).__init__(parent)
        self._hover: bool = False
        self._angle_start: int = angle_start
        self._title_padding: int = 4
        self._title_up: bool = False
        self._text_padding: int = 18
        self._title: str = text
        self._text_point: QPointF = QPointF()
        self._easing_curve: QEasingCurve = easing_curve
        self._border_color: QColor = QColor("#B2B2B2")
        self._border_color_hover: QColor = QColor("#758DE7")
        self._color_error: QColor = QColor("#ff2b41")

        self._pos_animation = QPropertyAnimation(self, b"text_position", self)
        self._pos_animation.setEasingCurve(self._easing_curve)
        self._pos_animation.setDuration(220)

    def paintEvent(self, event) -> None:
        painter = QPainter(self)
        painter.setRenderHints(
            QPainter.Antialiasing
            | QPainter.TextAntialiasing
        )
        painter.setFont(self.font())

        border_color = self._border_color
        if self._hover:
            border_color = self._border_color_hover

        text_color = self._border_color_hover

        if self.property("hasError"):
            border_color = self._color_error
            text_color = self._color_error

        # draw border
        if self._title_up and self.title():
            self._draw_border_with_text(painter, border_color)
        else:
            painter.setPen(border_color)
            painter.drawRoundedRect(
                self._contents_rect(),
                self._angle_start, self._angle_start
            )
        self._draw_text(painter, text_color)
        painter.end()

    def eventFilter(self, obj, e: QEvent) -> bool:
        """
        :param obj: obj
        :param e: event
        :return: bool
        """
        if obj == self:
            if e.type() == QEvent.Leave:
                self._hover = False
                self.update()
                return True
            elif e.type() == QEvent.MouseMove:
                if self._hit_box().contains(e.pos()):
                    self._hover = True
                else:
                    self._hover = False
                self.update()
                return True
        return super().eventFilter(obj, e)

    def focusOutEvent(self, e) -> None:
        """
        :param e:
        :return: None
        """
        self._hover = False
        self.update()

    def focusInEvent(self, e) -> None:
        """
        :param e:
        :return: None
        """
        self._hover = True
        self.update()

    def setTitle(self, title: str) -> None:
        """
        new title
        :param title: string
        :return: None
        """
        super().setTitle(title)
        r = self._compute_min_size().toSize()
        self.setMinimumSize(
            r.width(),
            r.height())

    def resizeEvent(self, e) -> None:
        if self._pos_animation.state() == QAbstractAnimation.State.Stopped and not self._title_up:
            self._text_point = QPointF(
                self._text_padding,
                self.contentsMargins().top() + (
                            (self.height() - self.contentsMargins().top()) / 2) - self.fontMetrics().height() / 2
            )
        super().resizeEvent(e)

    # ############################################################

    def _set_qss(self) -> None:
        """
        :return: None
        """
        self.setObjectName("groupBoxWidget")

    def _init_widget(self) -> None:
        """
        :return: None
        """
        self._set_qss()
        self.setAttribute(Qt.WA_StyledBackground | Qt.WA_TranslucentBackground)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.installEventFilter(self)
        self.setMouseTracking(True)
        font = QFont("Segoe UI")
        font.setPixelSize(14)
        font.setWeight(400)
        self.setFont(font)
        self.setContentsMargins(1, self.fontMetrics().height(), 1, 1)
        r = self._compute_min_size().toSize()
        self.setMinimumSize(
            r.width(),
            r.height())
        self._text_point = QPointF(
            self._text_padding,
            self.contentsMargins().top() + self._contents_rect().height() / 2
        )
        self.setTitle(self._title)

    # ############################################################

    @pyqtProperty(QPointF)
    def text_position(self):
        return self._text_point

    @text_position.setter
    def text_position(self, value):
        self._text_point = value
        self.update()

    @pyqtSlot(int)
    def start_transition(self, p_direction: int) -> None:
        """
        start transition
        :return: None
        """
        self._pos_animation.stop()
        start = QPointF(
            self._text_padding,
            self.contentsMargins().top() + (
                        (self.height() - self.contentsMargins().top()) / 2) - self.fontMetrics().height() / 2 - 1
        )
        end = QPointF(
            self._angle_start + self._title_padding,
            self.contentsMargins().top() - self.fontMetrics().capHeight() + 1
        )
        if p_direction == QAbstractAnimation.Forward:
            self._pos_animation.setStartValue(start)
            self._pos_animation.setEndValue(end)
        else:
            self._pos_animation.setStartValue(end)
            self._pos_animation.setEndValue(start)
        self._pos_animation.start()

    # ############################################################

    def _hit_box(self) -> QRectF:
        """
        hit box of area
        :return: QRectF
        """
        return QRectF(0, self.contentsMargins().top(), self.width(), self.height() - self.contentsMargins().top())

    def _contents_rect(self) -> QRectF:
        """
        :return: contents area
        """
        return QRectF(
            self.contentsMargins().left(),
            self.contentsMargins().top(),
            self.width() - self.contentsMargins().left() - self.contentsMargins().right(),
            self.height() - self.contentsMargins().top() - self.contentsMargins().bottom()
        )

    def get_contents_rect(self) -> QRectF:
        """
        :return: rect
        """
        return self._contents_rect()

    def _compute_min_size(self) -> QSizeF:
        """
        :return: height as integer
        """
        rect = self._hit_box()
        if not self.title():
            return rect.size()
        metrics = QFontMetrics(self.font())
        m = self.contentsMargins()
        rect.setHeight(metrics.height() + m.bottom() + 12 + m.top())
        rect.setWidth(metrics.width(self.title()) + self._text_padding * 2 + m.left() + m.right())
        return rect.size()

    def _draw_border_with_text(self, painter: QPainter, color: QColor) -> None:
        """
        :param painter:
        :param color:
        :return: None
        """
        txt_width = painter.fontMetrics().width(self.title())
        cm = self.contentsMargins()
        pt = QPainterPath(QPointF(self._angle_start + cm.left(), cm.top()))
        pt.quadTo(
            QPointF(cm.left(), self.contentsMargins().top()),
            QPointF(cm.left(), self._angle_start + self.contentsMargins().top())
        )
        pt.lineTo(QPointF(cm.left(), self.height() - self._angle_start - cm.bottom()))
        pt.quadTo(
            QPointF(cm.left(), self.height() - cm.bottom()),
            QPointF(self._angle_start + cm.left(), self.height() - cm.bottom())
        )
        pt.lineTo(QPointF(self.width() - self._angle_start * 2 - cm.right(), self.height() - cm.bottom()))
        pt.quadTo(
            QPointF(self.width() - cm.right(), self.height() - cm.bottom()),
            QPointF(self.width() - cm.right(), self.height() - self._angle_start - cm.bottom())
        )
        pt.lineTo(QPointF(self.width() - cm.right(), self._angle_start + cm.top()))
        pt.quadTo(
            QPointF(self.width() - cm.right(), cm.top()),
            QPointF(self.width() - self._angle_start - cm.right(), cm.top())
        )
        pt.lineTo(QPointF(self._angle_start + txt_width + int(self._title_padding * 2), cm.top()))

        painter.setBrush(Qt.NoBrush)
        painter.setPen(QPen(color, 1, Qt.SolidLine))
        painter.drawPath(pt)

    def _draw_text(self, painter: QPainter, color: QColor) -> None:
        """
        :param painter:
        :param color:
        :return:
        """
        txt_width = painter.fontMetrics().width(self.title())
        txt_rect = QRectF(
            self._text_point.x(),
            self._text_point.y(),
            txt_width + int(self._title_padding * 2),
            painter.fontMetrics().height()
        )
        painter.setPen(QPen(color, 1, Qt.SolidLine))
        painter.drawText(txt_rect, self.title())
