from typing import Any

from PyQt5.QtCore import QEvent, Qt, QEasingCurve, QAbstractAnimation, pyqtSignal, QSizeF
from PyQt5.QtGui import QDoubleValidator, QRegExpValidator
from PyQt5.QtWidgets import QApplication, QLineEdit, QPushButton

from ...common.str_utils import adapt_widget_font_size
from ..buttons.tooltip_button import TooltipPushButton
from ...components.buttons.three_state_button import ThreeStateButton
from ...components.widgets.group_box import GroupBoxWidget
from ...components.widgets.label import ErrorIcon


class ValidateButtonLineEdit(QLineEdit):
    def __init__(self, parent=None):
        super().__init__(parent)

        images = {
            "normal": f":/images/line_edit/http_active.png",
            "hover": ":/images/line_edit/http_hover.png",
            "pressed": ":/images/line_edit/http_pressed.png",
        }
        self.btn_browse = ThreeStateButton(iconPaths=images, iconSize=(28, 28), parent=self)
        self.installEventFilter(self)
        self._init_widget()

    def deactivate_browsing(self) -> None:
        """ deactivates the browsing functionality """
        self.btn_browse.hide()
        self.setTextMargins(0, 0, 0, 0)

    def activate_browsing(self) -> None:
        """ activate the browsing functionality """
        self.btn_browse.show()
        self.setTextMargins(0, 0, self.btn_browse.width(), 0)

    def _init_widget(self) -> None:
        """ initialize the widget """
        self.resize(150, 30)
        self.setCursorPosition(0)
        self.setTextMargins(0, 0, self.btn_browse.width(), 0)

    def resizeEvent(self, e):
        self.btn_browse.move(self.width() - self.btn_browse.width() - 1, 1)

    def eventFilter(self, o, e: QEvent) -> bool:
        if e.type() == QEvent.MouseButtonDblClick:
            self.btn_browse.clicked.emit()
        return super(ValidateButtonLineEdit, self).eventFilter(o, e)


class BrowseLineEdit(QLineEdit):
    def __init__(self, parent=None, p_file: bool = False):
        super().__init__(parent)

        if not p_file:
            images = {
                "normal": f":/images/line_edit/add_folder_active.png",
                "hover": ":/images/line_edit/add_folder_hover.png",
                "pressed": ":/images/line_edit/add_folder_pressed.png",
            }
        else:
            images = {
                "normal": f":/images/line_edit/add_file_active.png",
                "hover": ":/images/line_edit/add_file_hover.png",
                "pressed": ":/images/line_edit/add_file_pressed.png",
            }
        self.btn_browse = ThreeStateButton(iconPaths=images, iconSize=(28, 28), parent=self)
        self.installEventFilter(self)
        self._init_widget()

    def deactivate_browsing(self) -> None:
        """ deactivates the browsing functionality """
        self.btn_browse.hide()
        self.setTextMargins(0, 0, 0, 0)

    def activate_browsing(self) -> None:
        """ activate the browsing functionality """
        self.btn_browse.show()
        self.setTextMargins(0, 0, self.btn_browse.width(), 0)

    def _init_widget(self) -> None:
        """ initialize the widget """
        self.resize(150, 30)
        self.setCursorPosition(0)
        self.btn_browse.setCursor(Qt.PointingHandCursor)
        self.setTextMargins(0, 0, self.btn_browse.width(), 0)

    def resizeEvent(self, e):
        self.btn_browse.move(self.width() - self.btn_browse.width() - 1, 1)

    def eventFilter(self, o, e: QEvent) -> bool:
        if e.type() == QEvent.MouseButtonDblClick:
            self.btn_browse.clicked.emit()
        return super(BrowseLineEdit, self).eventFilter(o, e)


class LineEditWithRemBtn(QLineEdit):
    """  """

    def __init__(self, parent=None, string=None, needClearBtn: bool = True):
        super().__init__(parent)
        self.needClearBtn = needClearBtn
        self.clickedTime = 0

        images: dict = {
            "normal": f":/images/line_edit/delete_active.png",
            "hover": ":/images/line_edit/delete_hover.png",
            "pressed": ":/images/line_edit/delete_pressed.png",
        }
        self.clearButton = ThreeStateButton(images, self, iconSize=(28, 28))

        if string:
            self.setText(string)
        self.initWidget()

    def initWidget(self):
        """ initialize widgets """
        self.resize(150, 30)
        self.clearButton.hide()
        self.setCursorPosition(0)
        self.textChanged.connect(self.onTextChanged)
        self.clearButton.move(self.width() - self.clearButton.width() - 1, 1)
        self.clearButton.setCursor(Qt.PointingHandCursor)

        if self.needClearBtn:
            self.setTextMargins(0, 0, self.clearButton.width(), 0)

        self.clearButton.installEventFilter(self)

    def mousePressEvent(self, e):
        if e.button() == Qt.LeftButton:
            if self.clickedTime == 0:
                self.selectAll()
            else:
                super().mousePressEvent(e)

            self.setFocus()

            if self.text() and self.needClearBtn:
                self.clearButton.show()

        self.clickedTime += 1

    # def contextMenuEvent(self, e: QContextMenuEvent):
    #     self.menu.exec_(e.globalPos())

    def focusOutEvent(self, e):
        super().focusOutEvent(e)
        self.clickedTime = 0
        self.clearButton.hide()

    def onTextChanged(self) -> None:
        """
        text changed slot
        :return: None
        """
        if self.text() and not self.clearButton.isVisible() and self.needClearBtn and self.hasFocus():
            self.clearButton.show()
        # if the edit is empty hide the clear button
        if len(self.text()) == 0:
            self.clearButton.hide()

    def resizeEvent(self, e):
        # move the clear button at the right corner
        self.clearButton.move(self.width() - self.clearButton.width() - 1, 1)

    def eventFilter(self, obj, e):
        if obj == self.clearButton:
            if e.type() == QEvent.MouseButtonRelease and e.button() == Qt.LeftButton:
                self.clear()
                self.clearButton.hide()
                return True
        return super().eventFilter(obj, e)


class DoubleVLineEditor(LineEditWithRemBtn):
    def __init__(self, parent=None, string=None, needClearBtn: bool = True, values=(1, 50, 2)):
        super(DoubleVLineEditor, self).__init__(parent, string, needClearBtn)
        self.setValidator(QDoubleValidator(*values))

        self.errorIcon = ErrorIcon(parent)
        self.errorIcon.hide()

    def validate(self) -> bool:
        """ validate the input """
        state = self.validator().validate(self.text(), 0)[0]
        illegal = state != QRegExpValidator.Acceptable

        # set the visibility error icon
        x = self.pos().x() - 25
        y = self.pos().y() + self.height() // 2 - self.errorIcon.height() // 2
        self.errorIcon.move(x, y)
        self.errorIcon.setVisible(illegal)

        # update style sheet
        self.setProperty('error', illegal)
        self.setStyle(QApplication.style())

        self.needClearBtn = not illegal
        return not illegal


class LineEdit(GroupBoxWidget):
    textChanged = pyqtSignal(str)
    returnPressed = pyqtSignal()

    def __init__(self, parent, text: str = "", angle_start: int = 5, easing_curve = QEasingCurve.Linear):
        super().__init__(parent, text, angle_start, easing_curve)
        self.edit = QLineEdit(parent=self)
        self._init_widget()

    def setProperty(self, name: str, value: Any) -> bool:
        self.edit.setProperty(name, value)
        return super().setProperty(name, value)

    def resizeEvent(self, e) -> None:
        super().resizeEvent(e)
        self._adjust_layout()

    def eventFilter(self, obj, e: QEvent) -> bool:
        """
        :param obj:
        :param e:
        :return:
        """
        if obj == self.edit:
            if e.type() == QEvent.FocusAboutToChange and self._pos_animation.state() != QAbstractAnimation.Running:
                if not self.edit.text() and self._title_up:
                    self.edit.setDisabled(True)
                    self._title_up = False
                    self.edit.clearFocus()
                    self.clearFocus()
                    self.start_transition(QAbstractAnimation.Backward)
                    return False
        if obj == self:
            if e.type() == QEvent.Leave and not self.edit.hasFocus():
                self._hover = False
                self.update()
                return False
            elif e.type() == QEvent.MouseMove:
                if self._hit_box().contains(e.pos()):
                    self._hover = True
                    if not self.edit.text():
                        self.setCursor(Qt.IBeamCursor)
                else:
                    self.setCursor(Qt.ArrowCursor)
                    self._hover = False
                self.update()
                return False
        if obj in [self.edit, self]:
            if e.type() == QEvent.MouseButtonRelease and e.button() == Qt.LeftButton:
                # start transition
                if not self.edit.text() and not self._title_up:
                    self.edit.setEnabled(True)
                    self.start_transition(QAbstractAnimation.Forward)
                    self._title_up = True
                    if not self.edit.isVisible():
                        self.edit.show()
                # user clicked me focus child
                if self._hit_box().contains(e.pos()) and self._pos_animation.state() != QAbstractAnimation.Running:
                    self.edit.setFocus()
                self.update()
                return False
            elif e.type() == QEvent.MouseMove:
                self._hover = True
                self.update()
                return False
        return super().eventFilter(obj, e)

    # ###########################################################

    def _adjust_layout(self) -> None:
        """
        :return: None
        """
        cr = self._contents_rect().toRect()
        cr.setHeight(cr.height()) # 4
        cr.setY(self.height() - cr.height() - 1)
        cr.setWidth(cr.width() - 4) # 3
        self.edit.setFixedSize(cr.size())
        self.edit.move(
            self.contentsMargins().left() + 1,
            cr.y()
        )

    def _compute_min_size(self) -> QSizeF:
        r = super()._compute_min_size()
        m = self.contentsMargins()
        r.setHeight(
            m.bottom() + m.top() + self.edit.height()  + 2
        )
        return r

    # ###########################################################

    def setEchoMode(self, mode):
        self.edit.setEchoMode(mode)

    def clear(self) -> None:
        self.edit.clear()

    def setReadOnly(self, val: bool) -> None:
        """
        :param val:
        :return: None
        """
        self.edit.setReadOnly(val)

    def text(self) -> str:
        """
        :return: current text
        """
        return self.edit.text()

    def setPlaceholderText(self, text: str) -> None:
        """
        :param text: new text as str
        :return: None
        """
        self.setText(text)

    def setText(self, text: str) -> None:
        """
        :param text: new text as str
        :return: None
        """
        self.edit.setText(text)
        if not self._title_up and text:
            self.start_transition(QAbstractAnimation.Forward)
            self._title_up = True
            self.edit.show()
            self.edit.setFocus()

    def _init_widget(self) -> None:
        """
        :return: None
        """
        self._pos_animation.setDuration(80)
        super()._init_widget()
        self.edit.hide()
        self.edit.setMinimumSize(self._contents_rect().toRect().size())
        self.edit.installEventFilter(self)
        self.setTabOrder(self, None)
        font = self.font()
        font.setPixelSize(int(
            adapt_widget_font_size(self.edit, 0, self.edit.rect(), self.edit.text(),
                                   font))
        )
        self.edit.setFont(font)
        self.edit.setCursorPosition(0)
        self.edit.setCursor(Qt.IBeamCursor)
        self.setCursor(Qt.IBeamCursor)
        self._connect_signals()


    def _anim_finished(self) -> None:
        """
        anim finished
        :return: None
        """
        if self._title_up:
            self.edit.setFocus()
        else:
            self._hover = False
            self.update()
            self.edit.clearFocus()
            self.clearFocus()

    def _anim_value_changed(self, val) -> None:
        """
        :return: None
        """
        if self._pos_animation.state() == QAbstractAnimation.Backward:
            self._hover = False

    def _connect_signals(self) -> None:
        """
        :return: None
        """
        self.edit.textChanged.connect(self.textChanged)
        self.edit.returnPressed.connect(self.returnPressed)
        # fixes the bug where we're loosing focus while animation runs
        self._pos_animation.valueChanged.connect(self._anim_value_changed)
        # set focus to edit after animation
        self._pos_animation.finished.connect(self._anim_finished)

    def _set_qss(self) -> None:
        """
        :return: None
        """
        self.edit.setObjectName("lineEditGroupBox")


class PasswordLineEdit(LineEdit):
    def __init__(self, parent=None):
        super().__init__(parent)

    def _init_widget(self):
        super()._init_widget()
        self._btn_show = TooltipPushButton(text="", parent=self.edit)
        self._btn_show.setFixedSize(24, self.edit.height() - 1)
        self._btn_show.move(self.edit.width() - self._btn_show.width() - 1, 1)
        self._btn_show.setCursor(Qt.PointingHandCursor)
        self._btn_show.setToolTip(self.tr("Passwort anzeigen"))
        self._btn_show.installEventFilter(self)

        self.edit.setTextMargins(0, 0, self._btn_show.width(), 0)

    def _anim_finished(self) -> None:
        """
        anim finished
        :return: None
        """
        super()._anim_finished()
        if self._title_up:
            self._btn_show.show()
        else:
            self._btn_show.hide()

    def resizeEvent(self, e):
        super().resizeEvent(e)
        # move the echo button at the right corner
        self._btn_show.move(
            self.edit.width() - self._btn_show.width() - 1,
            self.get_contents_rect().height() // 2 - self._btn_show.height() // 2
        )

    def eventFilter(self, obj, e):
        r = super().eventFilter(obj, e)
        if obj == self and hasattr(self, "_btn_show"):
            if self.edit.echoMode() == QLineEdit.Normal:
                self._btn_show.setStyleSheet("image: url(:/images/line_edit/password_hidden_normal.svg);")
            else:
                self._btn_show.setStyleSheet("image: url(:/images/line_edit/password_visible_normal.svg);")
        if hasattr(self, "_btn_show") and obj == self._btn_show:
            if e.type() == QEvent.MouseButtonRelease and e.button() == Qt.LeftButton:
                if self.edit.echoMode() == QLineEdit.Normal:
                    self.setEchoMode(QLineEdit.Password)
                    self._btn_show.setToolTip(self.tr("Passwort anzeigen"))
                else:
                    self.setEchoMode(QLineEdit.EchoMode.Normal)
                    self._btn_show.setToolTip(self.tr("Passwort verstecken"))
        return r
