from typing import List

import numpy as np
from PyQt5.QtWidgets import QWidget, QSizePolicy, QHBoxLayout, QVBoxLayout
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure

import matplotlib.pyplot as plt
import matplotlib as mpl

from decimal import Decimal
from app.common.logger import logger

params = {
    'font.family': 'Segoe UI',
    'font.size': 8,

    'figure.figsize': (11, 4),
    'figure.dpi': 100
}
mpl.rcParams.update(mpl.rcParamsDefault)
mpl.rcParams.update(params)

# Matplotlib canvas class to create figure
class MplCanvas(FigureCanvas):
    def __init__(self, parent):
        self.fig = Figure(constrained_layout=True)
        # self.fig.set_tight_layout('none')
        self.ax = self.fig.add_subplot(121)

        super(MplCanvas, self).__init__(self.fig)
        self.setParent(parent)
        FigureCanvas.setSizePolicy(self, QSizePolicy.Fixed, QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

# Matplotlib widget
class MatPlotWidget(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)   # Inherit from QWidget
        self.grid_color: str = 'grey'
        self.annotation_color: str = 'grey'

        self.canvas = MplCanvas(self) # Create canvas object
        self.canvas.fig.set_dpi(100)
        self.vbl = QHBoxLayout()         # Set box for plotting
        self.vbl.setContentsMargins(0, 0, 0, 0)
        self.vbl.addWidget(self.canvas)
        self.setLayout(self.vbl)

    def setTitle(self, title: str, **kwargs) -> None:
        self.canvas.fig.suptitle(title, **kwargs)

    def setHTitle(self, title: str, **kwargs) -> None:
        self.canvas.fig.supxlabel(title, **kwargs)

    def setVTitle(self, title: str, **kwargs) -> None:
        self.canvas.fig.supylabel(title, **kwargs)

    def addHBars(self, names: List[str], values: List, colors: List[str]) -> None:
        self.canvas.ax.barh(names, values, color=colors)

    def addHBar(self, value: str, name: str, color: str) -> None:
        self.canvas.ax.barh(value, name, color=color)

    def setWatermarkText(self, text: str, color: str = 'grey') -> None:
        self.canvas.fig.text(0.5, 0.15, text, fontsize=12,
                             color=color, ha='right', va='bottom',
                             alpha=0.7)
    def addGrid(self, value: bool = True) -> None:
        if value:
            self.canvas.ax.grid(visible=True, color=self.grid_color, linestyle='-.', linewidth=0.5, alpha=0.2)
        else:
            self.canvas.ax.grid(visible=False)

    def addAnnotation(self) -> None:
        for i in self.canvas.ax.patches:
            self.canvas.ax.text(i.get_width() + 0.2, i.get_y() + 0.5,
                     str(round((i.get_width()), 3)),
                     fontsize=8, fontweight='bold',
                     color=self.annotation_color)

class AnalysisPieWidget(QWidget):
    """
    plotted analyzed data inside pie chart
    """
    def __init__(self, parent=None):
        super().__init__(parent)

        self.figure, self.ax = plt.subplots()
        self.canvas = FigureCanvas(self.figure)
        self.pie = None

        layout = QVBoxLayout()
        layout.addWidget(self.canvas)
        self.setLayout(layout)

    def plot(self, names: List[str], values: List, colors: List[str], explode: str= "", text_props: dict = {}) -> None:
        """
        plot data into pie
        :param text_props:
        :param explode: which slide should be exploded
        :param names: vertically names
        :param values: bar values
        :param colors: bar colors
        :return: None
        """
        if len(explode):
            idx = names.index(explode)
            explode: List = []
            for i in range(len(names)):
                if idx == i:
                    explode.append(0.2)
                else:
                    explode.append(0.0)
        else:
            explode = None
        self.ax.clear()
        self.ax.set_aspect(1)
        self.pie = self.ax.pie(values, labels=names, explode=explode, autopct='% 1.1f %%',
                               shadow=True, colors=colors, startangle=90, textprops=text_props)
        self.canvas.draw()

    def plot2(self, names: List[str], values: List, colors: List[str], explode: str= "", text_props: dict = {},
              p_legend_name: str = "") -> None:
        if len(explode):
            idx = names.index(explode)
            explode: List = []
            for i in range(len(names)):
                if idx == i:
                    explode.append(0.2)
                else:
                    explode.append(0.0)
        else:
            explode = None
        self.ax.clear()
        self.ax.set_aspect(1)   # make circle round
        wedges, texts, autotexts = self.ax.pie(
            values, explode=explode, autopct=lambda p: '{:.2f}%'.format(round(p, 2)) if p > 0 else '',
            radius=1.3, shadow=False, pctdistance=1.1, center=(-5, -1), colors=colors,
                                               startangle=90, textprops=text_props)
        self.ax.legend(wedges, names,
                       title=p_legend_name, bbox_to_anchor=(1.3, 0.6, 0.5, 0.5), loc="upper left", fontsize=8)
        self.canvas.figure.subplots_adjust(left=-0.4)
        self.canvas.draw()


class AnalysisBarPlotWidget(MatPlotWidget):
    """
    plotted analyzed data inside bar chart
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.add_annotations: bool = True
        self.data = []

    def plot(self, names: List[str], values: List, colors: List[str]) -> None:
        """
        plot horizontal bar chart diagram
        :param names: vertically names
        :param values: bar values
        :param colors: bar colors
        :return: None
        """

        self.data.clear()
        self.data = [names, values, colors]
        self.canvas.ax.clear()
        # add data
        y_pos = np.arange(len(names))
        self.canvas.ax.barh(y_pos, values, color=colors)

        self.canvas.ax.xaxis.set_ticks_position('none')
        self.canvas.ax.yaxis.set_ticks_position('none')

        # Remove axes splines
        for s in ['top', 'bottom', 'left', 'right']:
            self.canvas.ax.spines[s].set_visible(False)

        # Add padding between axes and labels
        self.canvas.ax.xaxis.set_tick_params(pad=5)
        self.canvas.ax.yaxis.set_tick_params(pad=5)
        self.addGrid()
        self.canvas.ax.invert_yaxis()

        self.canvas.fig.set_figwidth(13)
        # self.setWatermarkText("CISS TDI")
        self.canvas.ax.set_yticklabels([]) # deactivate automatic generated tic labels
        self.canvas.ax.set_xlabel('km²')

        for rec, name, val in zip(self.canvas.ax.patches, names, values):
            self.canvas.ax.text(
                rec.get_width() + 0.1, rec.get_y() + 0.4,
                f"{rec.get_width()} km² ({names[names.index(name)]})",  # y-tic text
                va='center', fontfamily="Segoe UI", fontsize=8, color=self.annotation_color, ha='left',
                bbox=dict(facecolor='white', alpha=0.5)
                )
        self.canvas.draw()
