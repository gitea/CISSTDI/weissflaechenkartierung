from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt, pyqtSignal, QItemSelectionModel, QModelIndex
from PyQt5.QtGui import QContextMenuEvent, QBrush, QColor, QFontMetrics
from PyQt5.QtWidgets import QListView, QAbstractItemView, QStyledItemDelegate, QStyleOptionViewItem, QStyle

from app.components.widgets.menu import ListViewMenu

USER_ROLE_ROW_DATA = Qt.UserRole + 1


class ListViewDelegate(QStyledItemDelegate):
    def sizeHint(self, option, index):
        size = super().sizeHint(option, index)
        size.setHeight(25)
        size.setWidth(QFontMetrics(option.font).width(index.data()))
        return size

    def initStyleOption(self, option: QStyleOptionViewItem, index: QModelIndex) -> None:
        """style the item"""
        super(ListViewDelegate, self).initStyleOption(option, index)

        # set selection style
        if option.state & QStyle.State_Selected:
            option.state &= ~ QStyle.State_Selected
            option.backgroundBrush = QBrush(QColor(178, 192, 250))   # #B2C0FA
        elif option.state & QStyle.State_MouseOver:
            option.state &= ~ QStyle.State_MouseOver
            option.backgroundBrush = QBrush(QColor(239, 241, 253))


class ListView(QListView):
    deleteRow = pyqtSignal()
    editRow = pyqtSignal()

    def __init__(self, parent):
        super().__init__(parent)
        self.menu = ListViewMenu(self)
        self._init_widget()

    def _init_widget(self):
        """ init the children """
        self.setItemDelegate(ListViewDelegate(self))
        self.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.setSelectionMode(QAbstractItemView.SingleSelection)
        self.setAlternatingRowColors(True)

        self.setSizeAdjustPolicy(
            QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.horizontalScrollBar().setVisible(True)
        self.setWordWrap(False)

    def contextMenuEvent(self, e: QContextMenuEvent):
        """execute context menu at the given global pos"""
        if len(self.selectionModel().selectedRows()) == 1:
            self.menu.exec_(e.globalPos())

    def keyPressEvent(self, event):
        """catch key press events"""
        if event.key() == Qt.Key_Delete:
            self.deleteRow.emit()
        elif event.key() in [Qt.Key_F2, Qt.Key_Enter, Qt.Key_Return]:
            self.editRow.emit()
        elif event.key() == Qt.Key_Up:
            indexes: list = self.selectedIndexes()
            if len(indexes) > 0:
                pre_idx = None
                first_row: int = indexes[0].row() - 1
                if first_row >= 0:
                    idx = indexes[0]
                    if idx is not None:
                        pre_idx = self.model().index(idx.row() - 1, 0)
                else:
                    pre_idx = self.model().index(self.model().rowCount() - 1, 0)
                if pre_idx is not None:
                    self.selectionModel().select(pre_idx, QItemSelectionModel.ClearAndSelect)
                    self.setCurrentIndex(pre_idx)
        elif event.key() == Qt.Key_Down:
            indexes: list = self.selectedIndexes()
            if len(indexes) > 0:
                next_idx = None
                last_row = indexes[-1].row() + 1
                max_row = self.model().rowCount()
                if last_row < max_row:
                    idx = indexes[0]
                    if idx is not None:
                        next_idx = self.model().index(idx.row() + 1, 0)
                else:
                    next_idx = self.model().index(0, 0)
                if next_idx is not None:
                    self.selectionModel().select(next_idx, QItemSelectionModel.ClearAndSelect)
                    self.setCurrentIndex(next_idx)
