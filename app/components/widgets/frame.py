from PyQt5.QtCore import pyqtBoundSignal, pyqtSignal, Qt, QRectF, QPropertyAnimation, QEasingCurve, QEvent, QObject
from PyQt5.QtGui import QPaintEvent, QPainter, QColor, QPainterPath, QPen, QMouseEvent
from PyQt5.QtWidgets import QFrame, QLabel, QWidget, QGraphicsOpacityEffect


class Mask(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setAttribute(Qt.WA_TransparentForMouseEvents)
        self.inner_rect: QRectF = QRectF()
        self.hide()
        self.setObjectName("windowMask")

    def closeEvent(self, e):
        """fade out"""
        opacity_ani = self._create_opacity_anim(1, 0, 100)
        opacity_ani.setEasingCurve(QEasingCurve.OutCubic)
        opacity_ani.start()
        e.ignore()

    def hideEvent(self, e) -> None:
        """fade out"""
        opacity_ani = self._create_opacity_anim(1, 0, 100)
        opacity_ani.setEasingCurve(QEasingCurve.OutCubic)
        opacity_ani.start()
        e.ignore()

    def showEvent(self, e):
        """fade in"""
        opacity_ani = self._create_opacity_anim(0, 1, 200, True)
        opacity_ani.setEasingCurve(QEasingCurve.InSine)
        opacity_ani.start()
        super().showEvent(e)

    def paintEvent(self, e: QPaintEvent) -> None:
        super().paintEvent(e)

        if self.isVisible():
            p = QPainter(self)
            p.setRenderHint(QPainter.Antialiasing)
            rect = self.parent().window().rect()
            extern_rect = QRectF(rect.x(), rect.y(), rect.width(), rect.height())
            p.setBrush(QColor(0, 0, 0, 100))
            p.setPen(QPen(Qt.NoPen))
            path = QPainterPath()
            path.addRect(extern_rect)
            if self.inner_rect:
                path.addRect(self.inner_rect)
            p.drawPath(path)
            p.end()

    def _create_opacity_anim(self, start: int, end: int, duration: int, del_effect: bool = False) -> QPropertyAnimation:
        """
        create opacity anim
        """
        opacity_effect = QGraphicsOpacityEffect(self)
        self.setGraphicsEffect(opacity_effect)
        opacity_ani = QPropertyAnimation(opacity_effect, b'opacity', self)
        opacity_ani.setStartValue(start)
        opacity_ani.setEndValue(end)
        opacity_ani.setDuration(duration)
        if del_effect:
            opacity_ani.finished.connect(opacity_effect.deleteLater)
        return opacity_ani


class DragNDropFrame(QFrame):
    dropped: pyqtSignal = pyqtSignal(list)
    clicked: pyqtSignal = pyqtSignal()

    def __init__(self, parent=None, mask_parent=None):
        super().__init__(parent)
        self._label_icon = QLabel(self)
        self._label_text = QLabel(self)
        self._mask = Mask(mask_parent)
        self._init_widget()

    def resizeEvent(self, e) -> None:
        """
        :param e:
        :return: None
        """
        super().resizeEvent(e)
        self._label_icon.move(
            self.width() // 2 - self._label_icon.width() // 2,
            self.contentsMargins().top()
        )
        self._label_text.move(
            self.width() // 2 - self._label_text.width() // 2,
            self._label_icon.geometry().bottomLeft().y() + 10
        )

    def adjust_layout(self) -> None:
        """
        adjust mask
        :return: None
        """
        self._mask.resize(self._mask.parent().size())
        self._mask.move(0, 0)
        point = self.mapTo(self._mask.parent(), self.rect().topLeft())
        self._mask.inner_rect = QRectF(
            point.x(),
            point.y(),
            self.width(),
            self.height()
        )

    def dragEnterEvent(self, event):
        """triggered if a file dragged over the ui"""
        if event.mimeData().hasUrls():
            urls = event.mimeData().urls()
            if isinstance(urls, list):
                first_url = str(urls[0].toLocalFile())
                if first_url.endswith(".cfg"):
                    self._mask.show()
                    event.accept()
                else:
                    event.ignore()
        else:
            event.ignore()

    def mouseReleaseEvent(self, event: QMouseEvent):
        if event.button() == Qt.LeftButton:
            self.clicked.emit()

    def dragLeaveEvent(self, e) -> None:
        """triggered if a dragged file leaves the ui"""
        self._mask.hide()

    def dragMoveEvent(self, event):
        """triggered if a file dragged over the ui"""
        if event.mimeData().hasUrls():
            event.setDropAction(Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

    def dropEvent(self, event):
        """triggered if a file dropped over the ui"""
        self._mask.hide()
        if event.mimeData().hasUrls():
            event.setDropAction(Qt.CopyAction)
            event.accept()
            self.dropped.emit([
                x.toLocalFile() for x in event.mimeData().urls()
            ])
        else:
            event.ignore()

    # ######################################################################

    def _set_qss(self) -> None:
        """
        :return: None
        """
        self.setObjectName("dragNdropFrame")
        self._label_icon.setObjectName("dragDropIconLabel")
        self._label_text.setObjectName("dragDropTextLabel")

    def _init_widget(self) -> None:
        """
        :return: None
        """
        self._set_qss()
        self.setContentsMargins(10, 15, 10, 10)
        self.setAcceptDrops(True)
        self.setCursor(Qt.PointingHandCursor)
        self._label_icon.setAttribute(Qt.WA_TransparentForMouseEvents)
        self._label_icon.setFixedSize(20, 25)
        self._label_text.setAttribute(Qt.WA_TransparentForMouseEvents)
        self._label_text.setWordWrap(True)
        self._label_text.setText(self.tr("Bestehende Konfigurationsdatei\nPer Drag & Drop hinzufügen oder auswählen"))
        self._label_text.setAlignment(Qt.AlignHCenter)
        self._label_text.setFixedSize(303, 36)
        self.setFixedHeight(
            self.contentsMargins().top() + self.contentsMargins().bottom() +
            self._label_icon.height() + self._label_text.height() + 10
        )
        self.setMinimumWidth(
            self.contentsMargins().left() + self.contentsMargins().right() +
            self._label_text.width()
        )
