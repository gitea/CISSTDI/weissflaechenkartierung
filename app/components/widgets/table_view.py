import decimal
from datetime import datetime

from PyQt5.QtCore import Qt, QAbstractTableModel, QModelIndex, pyqtSignal, QPoint
from PyQt5.QtGui import QContextMenuEvent, QBrush, QColor
from PyQt5.QtWidgets import QTableView, QAbstractItemView, QFrame, QAbstractScrollArea, QHeaderView, QStyle, \
    QStyleOptionViewItem, QStyledItemDelegate

from app.components.widgets.menu import ListViewMenu

USER_ROLE_ROW_DATA = Qt.UserRole + 1
USER_ROLE_WHOLE_DATA = Qt.UserRole + 2


def parse_input_data(p_data_to_parse: any) -> any:
    """
    parses the input data
    """
    if isinstance(p_data_to_parse, decimal.Decimal):
        return "{0:0.0f}".format(decimal.Decimal(p_data_to_parse))
    elif isinstance(p_data_to_parse, datetime):
        # Render time to YYY-MM-DD.
        return p_data_to_parse.strftime("%H:%M:%S %Y-%m-%d")
    elif isinstance(p_data_to_parse, float):
        # Render float to 2 dp
        return "%.2f" % p_data_to_parse
    elif p_data_to_parse is None or p_data_to_parse == "None":
        return ""
    else:
        return str(p_data_to_parse)


class EditableTableViewModel(QAbstractTableModel):
    def __init__(self, p_data: list, p_header: list, parent=None):
        super(EditableTableViewModel, self).__init__(parent)
        self._data: list = []
        self._header = p_header
        self.add_data(p_data)

    def add_data(self, p_data: list):
        for data in p_data:
            self._data.append(data)

    def set_header(self, p_header: list):
        self._header = p_header

    def get_header(self):
        return self._header

    def removeRow(self, row: int, parent: QModelIndex = ...) -> bool:
        """ try to remove the row """
        if row != -1 and len(self._data) > 0:
            self.layoutAboutToBeChanged.emit()
            self._data.pop(row)
            self.layoutChanged.emit()

    def data(self, index: QModelIndex, role=Qt.DisplayRole):
        # return the whole list with data
        if role == USER_ROLE_WHOLE_DATA:
            return self._data

        if index.isValid():
            if role == USER_ROLE_ROW_DATA:
                return self._data[index.row()]

            value = self._data[index.row()][index.column()]

            if role == Qt.EditRole:
                return value

            if role == Qt.DisplayRole:
                return parse_input_data(p_data_to_parse=value)

    def setData(self, index: QModelIndex, value: any, role: int = ...):
        if role == Qt.EditRole:
            # we need to overwrite this function if we inherit this class
            if index.column() == 0:
                if isinstance(value, str):
                    if ',' in value or '?' in value or '#' in value:
                        return False
                    if value.isnumeric():
                        return False
            elif index.column() == 1:
                value = str(value).replace(',', '.')
                try:
                    value = float(value)
                except ValueError:
                    return False
                # if there is a negative buffer do not accept it
                if value < 0:
                    return False
            else:
                return False
            self._data[index.row()][index.column()] = value
            return True
        return False

    def headerData(self, col: int, orientation, role=Qt.DisplayRole):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self._header[col]
        return None

    def rowCount(self, index: QModelIndex = QModelIndex()):
        # The length of the outer list.
        return len(self._data)

    def columnCount(self, index: QModelIndex = QModelIndex()):
        # The following takes the first sub-list, and returns
        # the length (only works if all rows are an equal length)
        if self._data is None:
            return 0
        return 0 if len(self._data) == 0 or self._data[0] is None else len(self._data[0])

    def flags(self, index):
        return Qt.ItemIsSelectable | Qt.ItemIsEnabled | Qt.ItemIsEditable


class TableViewDelegate(QStyledItemDelegate):
    def sizeHint(self, option, index):
        size = super().sizeHint(option, index)
        size.setHeight(30)
        return size

    def initStyleOption(self, option: QStyleOptionViewItem, index: QModelIndex) -> None:
        """style the item"""
        super(TableViewDelegate, self).initStyleOption(option, index)

        # set selection style
        if option.state & QStyle.State_Selected:
            option.state &= ~ QStyle.State_Selected
            option.backgroundBrush = QBrush(QColor(178, 192, 250))   # #B2C0FA
        elif option.state & QStyle.State_MouseOver:
            option.state &= ~ QStyle.State_MouseOver
            option.backgroundBrush = QBrush(QColor(239, 241, 253))


class TableView(QTableView):

    deleteRow = pyqtSignal()
    editRow = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent)
        self.menu = ListViewMenu(self)
        self.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContentsOnFirstShow)
        self.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.setCornerButtonEnabled(False)
        self.setAlternatingRowColors(True)
        self.setFrameShape(QFrame.NoFrame)
        self.setTextElideMode(Qt.ElideLeft)
        self.setGridStyle(Qt.NoPen)

        self.verticalHeader().setVisible(False)

        self.horizontalHeader().setVisible(True)
        self.horizontalHeader().setDefaultAlignment(Qt.AlignCenter)
        self.setModel(EditableTableViewModel(p_data=[], p_header=[], parent=self))

        # needed for context menu
        self.installEventFilter(self)
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self._on_custom_context_menu_clicked)

        # the header gets stretched; list width / column size
        self.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)

        self.setItemDelegate(TableViewDelegate())

    def _on_custom_context_menu_clicked(self, p_pos):
        if len(self.selectionModel().selectedRows()) == 1:
            if self.menu is not None:
                self.menu.exec_(self.mapToGlobal(QPoint(0, 0) + p_pos))

    def append_row(self, p_row: list):
        """ appends a row to the model """
        if len(p_row) > 0:
            self.model().layoutAboutToBeChanged.emit()
            self.model().add_data([p_row])
            self.model().layoutChanged.emit()
