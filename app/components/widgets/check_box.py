from PyQt5 import QtWidgets
from PyQt5.QtCore import (
    Qt, QSize, QPoint, QPointF, QRectF,
    QEasingCurve, QPropertyAnimation, QSequentialAnimationGroup,
    pyqtSlot, pyqtProperty)
from PyQt5.QtCore import *
from PyQt5.QtWidgets import QCheckBox
from PyQt5.QtGui import QColor, QBrush, QPaintEvent, QPen, QPainter, QFontMetrics, QLinearGradient

from app.common.auto_wrap import auto_wrap
from app.common.config import config
from app.components.buttons.tooltip_button import TooltipPushButton


class AnimatedCheckBoxOverlay(QCheckBox):
    _transparent_pen = QPen(Qt.transparent)
    _light_grey_pen = QPen(QColor("#6987b4"))

    def __init__(self,
                 parent=None,
                 bar_color="#afafaf",  # background = bar
                 checked_color="#8fb7f5",  # circle = checked
                 unchecked_color="#6987b4",  # circle = not checked
                 pulse_unchecked_color="#8fb7f5",
                 pulse_checked_color="#6987b4"
                 ):
        super().__init__(parent)

        # Save our properties on the object via self, so we can access them later
        # in the paintEvent.
        self._bar_brush = QBrush(QColor(bar_color))
        # self._bar_checked_brush = QBrush(QColor(checked_color).lighter())
        self._bar_checked_brush = QBrush(QColor(checked_color).lighter())
        self._bar_checked_brush = QBrush(QColor(pulse_checked_color))

        self._handle_brush = QBrush(QColor(unchecked_color))
        self._handle_checked_brush = QBrush(QColor(checked_color))

        self._pulse_unchecked_animation = QBrush(QColor(pulse_unchecked_color))
        self._pulse_checked_animation = QBrush(QColor(pulse_checked_color))

        # set up the rest of the widget.
        self.setContentsMargins(8, 0, 8, 0)
        self._handle_position = 0

        self._pulse_radius = 0

        self.animation = QPropertyAnimation(self, b"handle_position", self)
        self.animation.setEasingCurve(QEasingCurve.InOutCubic)
        self.animation.setDuration(200)  # time in ms

        self.pulse_anim = QPropertyAnimation(self, b"pulse_radius", self)
        self.pulse_anim.setDuration(350)  # time in ms
        self.pulse_anim.setStartValue(8)
        self.pulse_anim.setEndValue(15)

        self.animations_group = QSequentialAnimationGroup()
        self.animations_group.addAnimation(self.animation)
        # self.animations_group.addAnimation(self.pulse_anim)

        self.stateChanged.connect(self.setup_animation)

        self.setMaximumSize(QSize(50, 31))

    def sizeHint(self):
        return QSize(50, 31)

    def hitButton(self, pos: QPoint):
        return self.contentsRect().contains(pos)

    @pyqtSlot(int)
    def setup_animation(self, value):
        self.animations_group.stop()
        if value:
            self.animation.setEndValue(1)
        else:
            self.animation.setEndValue(0)
        self.animations_group.start()

    def paintEvent(self, e: QPaintEvent):
        cont_rect = self.contentsRect()
        handle_radius = round(0.24 * cont_rect.height())

        p = QPainter(self)
        p.setRenderHint(QPainter.Antialiasing)

        p.setPen(self._transparent_pen)
        bar_rect = QRectF(
            0, 0,
            cont_rect.width() - handle_radius, 0.40 * cont_rect.height()
        )
        bar_rect.moveCenter(cont_rect.center())
        rounding = bar_rect.height() / 2

        # the handle will move along this line
        trailLength = cont_rect.width() - 2 * handle_radius

        xPos = cont_rect.x() + handle_radius + trailLength * self._handle_position

        if self.pulse_anim.state() == QPropertyAnimation.Running:
            p.setBrush(
                self._pulse_checked_animation if
                self.isChecked() else self._pulse_unchecked_animation)
            p.drawEllipse(QPointF(xPos, bar_rect.center().y()),
                          self._pulse_radius, self._pulse_radius)

        if self.isChecked():
            p.setBrush(self._bar_checked_brush)
            p.drawRoundedRect(bar_rect, rounding, rounding)
            p.setBrush(self._handle_checked_brush)
        else:
            p.setBrush(self._bar_brush)
            p.drawRoundedRect(bar_rect, rounding, rounding)
            p.setPen(self._light_grey_pen)
            p.setBrush(self._handle_brush)

        p.drawEllipse(
            QPointF(xPos, bar_rect.center().y()),
            handle_radius, handle_radius)

        p.end()

    @pyqtProperty(float)
    def handle_position(self):
        return self._handle_position

    @handle_position.setter
    def handle_position(self, pos):
        """change the property
        we need to trigger QWidget.update() method, either by:
            1- calling it here [ what we doing ].
            2- connecting the QPropertyAnimation.valueChanged() signal to it.
        """
        self._handle_position = pos
        self.update()

    @pyqtProperty(float)
    def pulse_radius(self):
        return self._pulse_radius

    @pulse_radius.setter
    def pulse_radius(self, pos):
        self._pulse_radius = pos
        self.update()


class AnimatedCheckBoxInner(QCheckBox):
    def __init__(self,
                 parent=None,
                 width: int = 60,
                 height: int = 28,
                 bg_color: QColor = "#777",
                 circle_color: QColor = "#DDD",
                 active_color: QColor = "#00BCff",
                 animation_curve: QEasingCurve = QEasingCurve.OutBounce,
                 animation_duration: int = 500):
        super(AnimatedCheckBoxInner, self).__init__(parent)

        # set default parameters
        self.setFixedSize(width, height)
        self.setCursor(Qt.PointingHandCursor)

        self._bg_color: QColor = bg_color
        self._active_color: QColor = active_color
        self._circle_color: QColor = circle_color

        # create animation
        self._circle_position: float = 3
        self.animation = QPropertyAnimation(self, b"circle_position", self)
        self.animation.setEasingCurve(animation_curve)
        self.animation.setDuration(animation_duration)  # duration of the animation

        # connect state changed event
        self.stateChanged.connect(self.start_transition)

    # create new set and get property
    @pyqtProperty(float)
    def circle_position(self):
        return self._circle_position

    @circle_position.setter
    def circle_position(self, value):
        self._circle_position = value
        self.update()

    def start_transition(self, value):
        self.animation.stop()  # stop animation if running
        if value:
            # left width - (height(diameter) - 2) from left to right
            self.animation.setEndValue(self.width() - (self.height() - 2))
        else:
            # right to left
            self.animation.setEndValue(3)
        # start animation
        self.animation.start()

    # set new hit area
    def hitButton(self, pos: QPoint) -> bool:
        # we want that we can hit the button in the full area
        return self.contentsRect().contains(pos)

    # draw new states
    def paintEvent(self, event) -> None:
        # set painter
        p = QPainter(self)
        p.setRenderHint(QPainter.Antialiasing)

        # set as no pen
        p.setPen(Qt.NoPen)

        # draw rectangle
        rect = QRect(0, 0, self.width(), self.height())

        circle_diameter: int = self.height() - 6

        color = self._bg_color
        if self.checkState():
            color = self._active_color

        # reverse the color if the widget is disabled
        if not self.isEnabled():
            color = self._bg_color

        # draw background
        p.setBrush(color)
        p.drawRoundedRect(0, 0, rect.width(), self.height(), self.height() / 2, self.height() / 2)
        # draw circle
        p.setBrush(self._circle_color)
        p.drawEllipse(int(self._circle_position), 3, circle_diameter, circle_diameter)
        # end draw
        p.end()


class TextCheckBox(TooltipPushButton):
    """ checkbox with label """

    def __init__(self, parent=None):
        super().__init__("", parent)
        self.init_widget()

    def isChecked(self) -> bool:
        return self.check_box.isChecked()

    def setChecked(self, p_checked: bool) -> None:
        self.check_box.setChecked(p_checked)

    def text(self) -> str:
        """return the text of the label"""
        return self.label.text()

    def setText(self, p_txt: str) -> None:
        """sets the new text and adjusts the size"""
        # if the metric from the text is larger than self.width() setWordWrap True
        if self.fontMetrics().width(self.label.text()) > self.width():
            self.label.setWordWrap(True)
        else:
            self.label.setWordWrap(False)
        self.label.setText(p_txt)
        self.label.adjustSize()
        self._adjust_layout()

    def init_widget(self):
        """initialize widgets"""
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_TranslucentBackground)
        self.setStyleSheet("border:none;background:transparent;")
        self.resize(200, 40)
        self.setCheckable(False)

        bg_color = QLinearGradient(0, 0, 0, 22)
        bg_color.setColorAt(0.0, QColor("#999A9A"))
        bg_color.setColorAt(1.0, QColor("#606060"))
        bg_color.setSpread(QLinearGradient.PadSpread)

        active_color = QLinearGradient(0, 0, 0, 22)
        active_color.setColorAt(0.0, QColor("#3F57AD"))
        active_color.setColorAt(1.0, QColor("#364680"))
        active_color.setSpread(QLinearGradient.PadSpread)

        self.check_box = AnimatedCheckBoxInner(self,
                                               active_color=active_color,
                                               bg_color=bg_color,
                                               circle_color=QColor("#ffffff"),
                                               width=40,
                                               height=22)
        self.label = QtWidgets.QLabel(self)

    def _adjust_layout(self) -> None:
        """init the layout"""
        margin = 15
        self.check_box.move(0, 0)
        self.label.move(0 + self.check_box.width() + margin,
                        int(self.check_box.height() / 2) - int(self.label.height() / 2))

        # check if we have word wrap
        height = QFontMetrics(self.label.fontMetrics()).height()
        if self.label.wordWrap():
            height = self.label.height()
            # move the label to 0 y
            self.label.move(self.label.x(), self.check_box.y())
            # pos check in middle of label
            self.check_box.move(0, int(self.label.height() / 2) - int(self.check_box.height() / 2))

        # check if the height is less than checkbox
        if height < self.check_box.height():
            height = self.check_box.height()

        self.setFixedSize(self.check_box.width() + self.label.width() + margin, height)

    def resizeEvent(self, e):
        self._adjust_layout()
