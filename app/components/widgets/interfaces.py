from abc import abstractmethod
from typing import List

from PyQt5.QtCore import pyqtSlot, Qt
from PyQt5.QtWidgets import QWidget, QLabel, QLayout

from app.common.logger import logger
from app.common.signal_bus import signalBus
from app.common.style_sheet import setStyleSheet
from app.common.wfk.tasks.task_result import TaskResult
from app.components.buttons.opacity_button import OpacityButton
from app.components.dialog_box import MessageDialog
from app.components.widgets.scroll_area import ScrollArea
from app.components.widgets.tooltip import QGisTaskStateToolTip


class BaseInterface(ScrollArea):
    """
    Interface base class
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self._scroll_widget = QWidget(self)  # parent for child elements
        self._process_toasts: List[QWidget] = []
        self.btn_go_up = OpacityButton(self)
        self.btn_go_down = OpacityButton(self)
        self._layout: QLayout() = None

    @abstractmethod
    def _connect_signals(self) -> None:
        """
        connect signals
        :return: None
        """
        self.btn_go_up.clicked.connect(self._on_go_up_clicked)
        self.btn_go_down.clicked.connect(self._on_go_down_clicked)
        self.verticalScrollBar().valueChanged.connect(self._on_show_go_up)

    @abstractmethod
    def _init_widget(self) -> None:
        """
        init the widget
        :return: None
        """
        self._set_qss()
        self._connect_signals()

        self.btn_go_up.setToolTip(self.tr("An den Annfang springen"))
        self.btn_go_up.resize(20, 20)
        self.btn_go_up.setCursor(Qt.PointingHandCursor)
        self.btn_go_up.hide()
        self.btn_go_down.setToolTip(self.tr("An das Ende springen"))
        self.btn_go_down.resize(20, 20)
        self.btn_go_down.hide()

    @abstractmethod
    def _init_layout(self) -> None:
        """
        init widget layout
        :return: None
        """

    @abstractmethod
    def _adjust_layout(self) -> None:
        """
        if needed
        :return: None
        """
        for widget in self._process_toasts:
            if isinstance(widget, QGisTaskStateToolTip):
                widget.move(self.window().width() - widget.width() - 30, widget.y())
            if isinstance(widget, MessageDialog):
                widget.resize(self.window().size())
                widget.windowMask.resize(self.window().size())

        self.btn_go_up.move(
            self.width() - self.btn_go_up.width() - 20,
            0
        )
        self.btn_go_down.move(
            self.width() - self.btn_go_up.width() - 20,
            30
        )
        self._scroll_widget.resize(self.width(), self._scroll_widget.height())

    @abstractmethod
    def _set_qss(self) -> None:
        """
        sets the qss
        :return: None
        """
        self.btn_go_up.setObjectName("goUpButton")
        self.btn_go_down.setObjectName("goDownButton")
        setStyleSheet(self, "interface")

    def _on_go_up_clicked(self) -> None:
        """
        scroll to 0
        :return: None
        """
        self.verticalScrollBar().setValue(0)

    def _on_go_down_clicked(self) -> None:
        """
        scroll to 0
        :return: None
        """
        self.verticalScrollBar().setValue(self.verticalScrollBar().maximum())

    def _on_show_go_up(self, value) -> None:
        """
        show go up button the right side of interface
        :param value:
        :return: None
        """
        if not self.verticalScrollBar().isVisible():
            self.btn_go_up.hide()
            self.btn_go_down.hide()
        elif 0 < value < self.verticalScrollBar().maximum():
            self.btn_go_up.show()
            self.btn_go_down.hide()
        elif value == self.verticalScrollBar().maximum():
            self.btn_go_down.hide()
        else:
            self.btn_go_up.hide()
            self.btn_go_down.hide()


class BaseProcessInterface(BaseInterface):
    """
    base class for process interfaces
    """

    def __init__(self, parent=None):
        super().__init__(parent)
        self.label_title = QLabel(self.tr("BaseProductInterface"), self)
        self._process_tasks: list = []
        self._process_running: bool = False

    # #############################################################

    def resizeEvent(self, e) -> None:
        """
        resize widget
        :param e:
        :return: None
        """
        super().resizeEvent(e)
        self._adjust_layout()

    # #############################################################

    def _adjust_layout(self) -> None:
        """
        needs to be overwritten
        :return: None
        """
        super()._adjust_layout()

    def _connect_signals(self) -> None:
        """
        needs to be overwritten
        :return: None
        """
        super()._connect_signals()

    def _init_layout(self) -> None:
        """
        needs to be overwritten
        :return: None
        """
        self.label_title.move(40, 60)

    def _init_widget(self) -> None:
        """
        :return: None
        """
        super()._init_widget()
        self.resize(200, 200)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAlwaysOff)
        self._scroll_widget.resize(self.width(), self.height())
        self.setViewportMargins(40, 0, 0, 40)
        self._scroll_widget.adjustSize()
        self._scroll_widget.setLayout(self._layout)
        self.setWidget(self._scroll_widget)

    def _set_qss(self) -> None:
        """
        :return: None
        """
        self.label_title.setObjectName("titleLabel")
        super()._set_qss()

    def is_busy(self) -> bool:
        """
        :return: true if process running, otherwise false
        """
        return self._process_running

    @pyqtSlot(float)
    def _on_progress_changed(self, p_change: float) -> None:
        """
        handles progres result, sets process running

        :param p_change: percentage
        :return: None
        """
        self._process_running = True
        for widget in self._process_toasts:
            if isinstance(widget, QGisTaskStateToolTip):
                widget.show()
                widget.setContent(content=self.tr(f"Prozess zu {int(p_change)}% ausgeführt"))

    @pyqtSlot()
    def _on_stop_tasks(self) -> None:
        """
        emitted if the task should stop
        :return: None
        """
        signalBus.showInfoToastTip.emit(self.tr("Prozesse werden beendet..."))
        rem_task = []
        for task in self._process_tasks:
            try:
                task.cancel()
            except Exception as e:
                # logger.error(self.tr(f"Can not cancel task: {task} - {e}"))
                rem_task.append(task)
        for x in rem_task:
            self._process_tasks.remove(x)

        if len(self._process_tasks) == 1 and self._process_running:
            signalBus.showErrorToastTip.emit(self.tr("Prozess konnte nicht beendet werden!"))

    @pyqtSlot()
    def _on_process_finished(self) -> None:
        """
        emitted if the process finished
        :return: None
        """
        rem_w = []
        for widget in self._process_toasts:
            if isinstance(widget, QGisTaskStateToolTip):
                widget.complete_task()
                rem_w.append(widget)
        for x in rem_w:
            self._process_toasts.remove(x)
        self._process_running = False

    @pyqtSlot(TaskResult)
    def _on_process_result(self, p_result: TaskResult) -> bool:
        """
        process results
        :param p_result:
        :return: None
        """
        if not p_result.result:
            signalBus.showErrorToastTip.emit(self.tr("Der Prozess meldet Fehler!"))
            m = MessageDialog(title=self.tr("Fehler"),
                              content=self.tr("Der Prozess meldet folgende Fehler:\n") + p_result.txt,
                              parent=self.window())
            self._process_toasts.append(m)
            m.exec()
            return False
        return True