from PyQt5.QtWidgets import QAction, QApplication, QMenu

from app.common.config import config
from app.common.icon import Icon
from app.common.style_sheet import setStyleSheet


class ListViewMenu(QMenu):
    def __init__(self, parent):
        super().__init__("", parent)
        self.setObjectName("listViewMenu")
        self.setQss()

        self.action_list = []

    def setQss(self):
        """ set style sheet """
        setStyleSheet(self, 'menu')

    def create_actions(self):
        """create actions"""
        color = "white" if config.theme == 'dark' else 'black'
        self.deleteSel = QAction(
            Icon(f":/images/menu/Delete_{color}.png"),
            self.tr("Löschen"),
            self,
            shortcut='Delete'
        )
        self.deleteSel.triggered.connect(self.parent().deleteRow)

        self.editSel = QAction(
            Icon(f":/images/menu/Edit_{color}.png"),
            self.tr("Bearbeiten"),
            self
        )
        self.editSel.triggered.connect(self.parent().editRow)
        self.action_list = [self.deleteSel, self.editSel]

    def exec_(self, pos):
        """create menu and animation"""
        self.clear()
        self.create_actions()
        self.addActions(self.action_list)

        self.setStyle(QApplication.style())

        super().exec_(pos)


class CopyMenu(QMenu):
    """
    common menu for copy action
    """
    def __init__(self, parent=None):
        super().__init__("", parent)
        self.setObjectName("menu")
        self.setQss()

        self.copySel = QAction(self)
        self.copySel.setText(self.tr("Kopieren"))
        self.copySel.setShortcut("Strg+C")

        self.action_list = [self.copySel]

        self.addActions(self.action_list)

    def setQss(self):
        """ set style sheet """
        setStyleSheet(self, 'menu')

    def exec_(self, pos) -> None:
        """
        show menu
        :param pos: QPoint where the menu should open
        :return: None
        """
        self.setStyle(QApplication.style())
        super().exec_(pos)
