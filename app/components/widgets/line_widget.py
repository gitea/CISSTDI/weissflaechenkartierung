from PyQt5.QtCore import QPoint, Qt
from PyQt5.QtGui import QPaintEvent, QPainter, QColor, QPen
from PyQt5.QtWidgets import QWidget


class SeperatorWidget(QWidget):
    def __init__(self, p_line_color: str, parent=None):
        super().__init__(parent)
        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_StyledBackground)
        self._line_color: str = p_line_color
        self.left_point: QPoint = QPoint(0, 0)
        self.right_point: QPoint = None

        self.setFixedHeight(2)

    @property
    def line_color(self):
        """get the line color"""
        return self._line_color

    @line_color.setter
    def line_color(self, value):
        """set the line color"""
        self._line_color = value
        self.update()

    def paintEvent(self, event: QPaintEvent) -> None:
        """paint a straight line"""
        painter = QPainter(self)
        painter.setRenderHints(QPainter.Antialiasing)
        painter.setPen(QColor(self._line_color))

        if self.right_point is None:
            self.right_point = QPoint(self.width(), 0)
        painter.drawLine(self.left_point, self.right_point)
