from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QSystemTrayIcon, QAction, QApplication, QMenu
from PyQt5.QtCore import *

from app.common.icon import Icon
from app.common.signal_bus import signalBus


class SystemTrayIcon(QSystemTrayIcon):
    """ System tray icon """

    exitSignal = pyqtSignal()

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        self.menu = SystemTrayMenu(parent)
        self.setContextMenu(self.menu)
        self.setIcon(QIcon(':/images/main_window/ciss.png'))
        self.__connectSignalToSlot()

    def __connectSignalToSlot(self):
        """ connect signal to slot """
        self.activated.connect(self.__onActivated)

        self.menu.exitAct.triggered.connect(self.exitSignal)
        self.menu.minimizeAct.triggered.connect(signalBus.showMainWindowMinSig)
        self.menu.maximizeAct.triggered.connect(signalBus.showMainWindowMaxSig)

    def __onActivated(self, reason: QSystemTrayIcon.ActivationReason):
        """ system tray icon activated slot """
        if reason == self.Trigger:
            signalBus.showMainWindowSig.emit()


class SystemTrayMenu(QMenu):
    """ System tray menu """

    def __init__(self, parent=None):
        super().__init__(parent=parent)
        # c = 'white' if config.theme == 'dark' else 'black'
        self.exitAct = QAction(
            Icon(f':/images/system_tray/close_black_57_40.png'), self.tr('Close Window'), self
        )
        self.minimizeAct = QAction(
            Icon(f':/images/system_tray/minimize_black_57_40.png'), self.tr('Minimize Window'), self
        )
        self.maximizeAct = QAction(
            Icon(f':/images/system_tray/maximize_black_57_40.png'), self.tr('Maximize Window'), self
        )
        # self.songAct = QAction(
        #     Icon(f':/images/system_tray/Music_{c}.png'), self.tr('No songs are playing'), self)

        self.addActions([
            self.exitAct, self.minimizeAct, self.maximizeAct]
        )
        # self.addSeparator()
        self.setObjectName('systemTrayMenu')
        self.setStyle(QApplication.style())
