import time
import traceback

from PyQt5.QtCore import *

from app.common.config import config
from app.common.logger import logger
from app.common.signal_bus import signalBus
from app.common.wfk.tasks.task_result import AnalysisTaskResult, WfkTaskResult
from app.common.wfk.wfk_config import WfkConfig
from app.common.wfk.wfk_module import WfkModule

try:
    from qgis.core import QgsTask, QgsStyle, QgsGraduatedSymbolRenderer
    from app.common.qgis.QGisApiWrapper import QGisApiWrapper
except:
    pass


class AnalysisTask(QgsTask):
    result = pyqtSignal(AnalysisTaskResult)

    def __init__(self, p_desc: str, p_wfk_result: WfkTaskResult, p_classes: int):
        super(AnalysisTask, self).__init__(p_desc, QgsTask.CanCancel)

        self._interface = config.qgis_interface

        self._render_classes: int = p_classes
        self._module_to_categorize: WfkModule = p_wfk_result.config.analysis_module
        self._wfk_layer = p_wfk_result.config.processed_wfk_layer

        # result
        self._task_result: AnalysisTaskResult = AnalysisTaskResult()

        # wfk result
        self._wfk_result = p_wfk_result

        # statistics
        self._start: float = 0.0

    def run(self) -> bool:
        """
        the action happens here
        """
        self._start = time.perf_counter()
        logger.info("Start AnalysisTask")

        if self._interface is None:
            logger.error("The given interface is None!")
            return False

        # check if we want to run this config
        if not self._wfk_result.config.run:
            logger.debug("The config is disabled")
            self._task_result.result = True
            self._task_result.txt = "Die Konfiguration ist deaktiviert!"
            return True

        logger.info(f"Run config: {self._wfk_result.config.name}")
        logger.info(f"Process: {str(len(self._wfk_result.config.modules))} module/s")
        logger.info(f"Run with errors: {config['run_tasks_with_errors']}")
        logger.info(f"Protocol layers: {config['protocol_layer']}")

        try:
            analyzed_layer = self._run(p_layer=self._wfk_layer, p_blacklist=False, p_wfk_config=self._wfk_result.config)
            self._task_result.layer_to_protocol.append(analyzed_layer)
        except Exception as e:
            logger.error(traceback.format_exc())
            logger.error("The configuration failed! " + str(e))
            self._task_result.txt = str(e)
            return False
        return True

    def _run(self, p_layer, p_blacklist: bool, p_wfk_config: WfkConfig):
        """
        run
        """
        if self.has_to_stop():
            logger.debug("The program has to stop")
            return p_layer
        self.setProgress(5)

        logger.debug(f"--------Start analysis for {p_wfk_config.name}")

        # clone on wfk
        if p_layer.selectedFeatureCount() == 0:
            # clone the complete layer which we want to analyze and save it temporary
            layer_to_analyze = QGisApiWrapper.clone_all_features(p_input_layer=p_layer)
        else:
            # clone the selected features
            layer_to_analyze = QGisApiWrapper.clone_selected_features(p_input_layer=p_layer)

        step = 1
        max_steps = 4
        analyzed_layer = None
        try:
            # check if the module should run
            if not self._module_to_categorize.run:
                logger.warning(f"Skipp disabled module: {self._module_to_categorize.name}")
                return layer_to_analyze

            if self.has_to_stop():
                logger.debug(f"The program has to stop after {self._module_to_categorize.name} module")
                return layer_to_analyze

            # create connector
            self._module_to_categorize.connector.root_path = p_wfk_config.root_dir
            # run connector
            logger.debug(f"Run connector: {self._module_to_categorize.connector.__class__.__name__}")
            self._module_to_categorize.connector.retrieve_data()
            self.setProgress(5 + (step * (75 / max_steps)) + (10 / max_steps))

            # create layer
            logger.debug("Run layer...")
            if self._module_to_categorize.connector.layer is None:
                logger.debug("There is no layer to run")
                return layer_to_analyze

            # set connection of layer
            self._module_to_categorize.connector.layer.file_connection = self._module_to_categorize.connector.data_connection
            self._module_to_categorize.connector.layer.clip = self._module_to_categorize.connector.clip
            self._module_to_categorize.connector.layer.crs = self._module_to_categorize.connector.crs
            self._module_to_categorize.connector.layer.qgis_layer = p_wfk_config.qgis_layer
            self._module_to_categorize.connector.layer.create_layer()
            self.setProgress(5 + (2 * (75 / max_steps)) + (20 / max_steps))

            # check if the created layer is none, continue
            if self._module_to_categorize.connector.layer.created_layer is None:
                logger.debug("Can not create the layer!")
                return layer_to_analyze
            logger.debug(f"Run difference on layer: {self._module_to_categorize.connector.layer.created_layer.name()}")

            self.setProgress(5 + (3 * (75 / max_steps)) + (20 / max_steps))
            # run algorithm
            # set ref layer for processing
            self._module_to_categorize.categorization.ref_layer = self._module_to_categorize.connector.layer.created_layer

            logger.debug("Run categorization")
            # run the categorization class
            analyzed_layer = self._module_to_categorize.categorization.run(cat_layer=layer_to_analyze)

            self.setProgress(5 + (4 * (75 / max_steps)) + (20 / max_steps))

            logger.debug("Set analysis layer name")
            # set the layer name
            if analyzed_layer is not None:
                analyzed_layer.setName(p_wfk_config.analysis_layer_name)
            else:
                logger.error("Can not rename the analysis layer! The layer is None")
                raise TypeError("The layer is none!")

            # style the layer
            # default_style = QgsStyle().defaultStyle()
            # color_ramp = default_style.colorRamp('Spectral')  # Spectral color ramp
            # color_ramp.invert()
#
            # renderer = QgsGraduatedSymbolRenderer()
            # renderer.setClassAttribute('flaeche_m2')
            # analyzed_layer.setRenderer(renderer)
            # analyzed_layer.renderer().updateClasses(analyzed_layer, QgsGraduatedSymbolRenderer.Quantile,
            #                                         self._render_classes)
            # analyzed_layer.renderer().updateColorRamp(ramp=color_ramp)
            # self._interface.layerTreeView().refreshLayerSymbology(analyzed_layer.id())
            # self._interface.mapCanvas().refreshAllLayers()

        except Exception as e:
            logger.error(traceback.format_exc())
            if config["run_tasks_with_errors"]:
                self._task_result.txt = str(e)
                logger.warning("Skipped module: " + self._module_to_categorize.name)
                logger.warning(str(e))
            else:
                raise Exception(f"Module failed: {self._module_to_categorize.name} {str(e)}")

        return analyzed_layer

    def finished(self, result) -> None:
        """the wfk task finished"""
        logger.debug("The wfk analysis finished")

        self._task_result.result = result
        self._wfk_result.config.statistic_analysis.time_elapsed = time.perf_counter() - self._start
        self._task_result.config = self._wfk_result.config

        logger.debug("Emit process finished")
        logger.info(f"The task finished in: {self._wfk_result.config.statistic_analysis.time_elapsed} seconds")
        self.result.emit(self._task_result)
        signalBus.analysisFinishedSig.emit()

    def has_to_stop(self) -> bool:
        """
        checks if the task needs to stopped
        """
        return True if self.isCanceled() else False
