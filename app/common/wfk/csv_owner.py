import logging
from itertools import count


class Owner:
    """
        class which holds information about the owners csv file
        """
    _ids = count(0)

    def __init__(self, dictionary):
        if isinstance(dictionary, dict):
            for k, v in dictionary.items():
                if k is None:
                    continue
                if v is None:
                    v = ""
                setattr(self, k, v)
                self.id = next(self._ids)

    @staticmethod
    def from_csv_to_dict(file_name: str = "", column_name: str = "") -> dict:
        """
        returns a dict which contains the owner objects as a dict
        """
        if column_name == "":
            logging.error("from_csv_to_dict column_name is empty", )
            return {}

        import csv
        import chardet
        with open(file_name, 'rb') as file:
            encoding_result = chardet.detect(file.read())

        obj_cts: dict = {}
        with open(file_name, mode='r', encoding=encoding_result['encoding']) as csv_file:
            dict_reader = csv.DictReader(csv_file, delimiter=';')
            for row in dict_reader:
                # create owner object
                try:
                    owner_obj = Owner(dictionary=row)
                except Exception as e:
                    logging.warning("unexpected error " + str(repr(e)), )
                    logging.warning("current row" + str(row), )
                    continue

                try:
                    searched_member = getattr(owner_obj, column_name)
                except AttributeError:
                    logging.warning("AttributeError " + str(repr(column_name)), )
                    logging.warning("Current row " + str(row), )
                    continue

                if searched_member in obj_cts:
                    obj_cts[searched_member].append(owner_obj)
                else:
                    obj_cts[searched_member] = [owner_obj]

        logging.debug("result: " + str(obj_cts))
        return obj_cts