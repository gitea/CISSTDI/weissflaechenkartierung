from ...common.logger import logger


class CategorizationFactory:
    registry = {}

    @staticmethod
    def register(name: str, desc: str, func):
        """
        registers a class
        """
        data = {
            'desc': desc,
            'func': func
        }
        CategorizationFactory.registry[name] = data

    @staticmethod
    def build(class_name: str, *args):
        """
        returns classes with the class name
        """
        if class_name in CategorizationFactory.registry:
            return CategorizationFactory.registry[class_name]['func']()
        else:
            logger.warning("The given class name is not registered: " + class_name)
            return None


class ConnectorFactory:
    """
    Factory which builds connector classes
    """
    registry = {}

    @staticmethod
    def register(name: str, desc: str, func):
        """
        registers a class
        """
        data = {
            'desc': desc,
            'func': func
        }
        ConnectorFactory.registry[name] = data

    @staticmethod
    def build(class_name: str, *args):
        """
        returns classes with the class name
        """
        if class_name in ConnectorFactory.registry:
            return ConnectorFactory.registry[class_name]['func']()
        else:
            logger.warning("The given class name is not registered: " + class_name)
            return None


class DifferenceFactory:
    """
    Factory which builds difference classes
    """
    registry = {}

    @staticmethod
    def register(name: str, desc: str, func):
        """
        registers a class
        """
        data = {
            'desc': desc,
            'func': func
        }
        DifferenceFactory.registry[name] = data

    @staticmethod
    def build(class_name: str, *args):
        """
        returns classes with the class name
        """
        if class_name in DifferenceFactory.registry:
            return DifferenceFactory.registry[class_name]['func']()
        else:
            logger.warning("The given class name is not registered: " + class_name)
            return None


class LayerFactory:
    """
    Factory which builds connectors
    """
    registry = {}

    @staticmethod
    def register(name: str, desc: str, func):
        """
        registers a class
        """
        data = {
            'desc': desc,
            'func': func
        }
        LayerFactory.registry[name] = data

    @staticmethod
    def build(class_name: str, *args):
        """
        returns classes with the class name
        """
        if class_name in LayerFactory.registry:
            return LayerFactory.registry[class_name]['func']()
        else:
            logger.warning("The given class name is not registered: " + class_name)
            return None


class LegendFactory:
    """
    Factory which builds connectors
    """
    registry = {}

    @staticmethod
    def register(name: str, desc: str, func):
        """
        registers a class
        """
        data = {
            'desc': desc,
            'func': func
        }
        LegendFactory.registry[name] = data

    @staticmethod
    def build(class_name: str, *args):
        """
        returns classes with the class name
        """
        if class_name in LegendFactory.registry:
            return LegendFactory.registry[class_name]['func']()
        else:
            logger.warning("The given class name is not registered: " + class_name)
            return None