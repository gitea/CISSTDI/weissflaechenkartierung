import os
import time
from abc import abstractmethod

from PyQt5.QtCore import QDir
from PyQt5.QtWidgets import QLabel

from ..exception import ModuleLayerError
from ...common.config import config
from ...common.logger import logger
from ...common.wfk.interface import JsonHandle, Construction
from ...common.wfk.wfk_factories import LayerFactory
from ...components.widgets.check_box import TextCheckBox
from ...components.widgets.line_edit import LineEdit

try:
    import xlrd
    from qgis.PyQt.QtCore import QVariant
    from qgis.core import QgsVectorLayer, QgsField, QgsGeometry, QgsPointXY, QgsFeature, QgsDataSourceUri, \
        QgsProviderRegistry, QgsCoordinateReferenceSystem
    from app.common.qgis import QGisApiWrapper
except (NameError, ImportError):
    pass


class WfkLayer(JsonHandle, Construction):
    def __init__(self):
        self.created_layer = None           # will be set during the wfk process
        self.clip: bool = True              # from view / json

        self.qgis_layer = None              # wfk task set that
        self.crs: str = ""                  # module set that
        self.file_connection = ""      # wfk task set that

    @abstractmethod
    def translated_name(self) -> str:
        """return name for the ui"""
        return "Basis Layer"

    def from_json(self, p_data: dict) -> None:
        """ initiates the current object with the data """
        if "clip" in p_data and isinstance(p_data["clip"], bool):
            self.clip = p_data["clip"]

    def to_json(self) -> dict:
        """ creates a json from the current object"""
        data = {
            "type": self.__class__.__name__,
            "clip": self.clip,
            "crs": self.crs
        }
        return data

    @abstractmethod
    def create_layer(self) -> bool:
        pass

    def _init_widgets(self, p_parent=None) -> None:
        super(WfkLayer, self)._init_widgets(p_parent)

        self.le_crs = LineEdit(self.widget)
        self.le_crs.setPlaceholderText("EPSG:32832")
        self.le_crs.resize(150, 30)
        self.l_crs = QLabel("Koordinatensystem:", self.widget)
        self.l_crs.adjustSize()
        self.cb_clip = TextCheckBox(self.widget)
        self.cb_clip.setText("Mit Zielgebiet verschneiden")
        self.cb_clip.setToolTip("Dieser Algorithmus extrahiert Features aus dem Input-Layer,<br>die innerhalb oder "
                                "teilweise innerhalb der Grenzen von<br>Features im Clip-Layer liegen.")

    def _init_layout(self) -> None:
        x = self.widget.fontMetrics().width(self.l_crs.text()) + 15  # label text metrics
        self.le_crs.move(x, 9)
        self.l_crs.move(0, 9 + int(self.le_crs.height() / 2) - int(self.l_crs.height() / 2))
        self.cb_clip.move(0, self.le_crs.y() + 42)

        # resize
        self.le_crs.setFixedSize(self.widget.width() - x, 30)
        self.widget.setFixedSize(self.widget.width(), self.cb_clip.y() + self.cb_clip.height() + 6)
        self.widget.adjustSize()

    def create_view(self, p_parent=None):
        # recreate the widgets and their layout
        super(WfkLayer, self).create_view(p_parent)
        # set the ui
        self.cb_clip.setChecked(self.clip)
        self.le_crs.setText(self.crs)

        # focus line edit
        self.le_crs.setFocus()

    def from_view(self):
        self.clip = self.cb_clip.isChecked()
        self.crs = self.le_crs.text()


class WfkShapeLayer(WfkLayer):
    def __init__(self):
        super().__init__()

    def translated_name(self) -> str:
        """return name for the ui"""
        return "Shape Layer"

    def create_layer(self) -> bool:
        """run layer"""
        logger.info("Create ShapeLayer layer")
        logger.debug(f"Path: {self.file_connection}")
        logger.debug(f"CRS: {self.crs}")

        if not os.path.isfile(path=self.file_connection) or not os.path.exists(self.file_connection):
            logger.error(f"The given path as a Shape layer is not a file: {self.file_connection}")
            raise FileNotFoundError(f"File not found: {self.file_connection}")

        # control if we want to reprocess layer
        if not config.set_default("reprocess_layer", False) and self.created_layer is not None:
            logger.info("Do not reprocess layer, take created layer!")
            return True

        file_name = self.file_connection.split(QDir.separator())
        file_name = file_name[len(file_name) - 1]

        self.created_layer = QGisApiWrapper.load_shape(p_url=self.file_connection,
                                                       set_crs=False,
                                                       p_layer_name=f"{file_name}_{time.time()}")
        layer_crs = self.created_layer.crs()
        needed_crs = QgsCoordinateReferenceSystem(self.crs)
        logger.debug(f"Layer CRS: {layer_crs}")
        logger.debug(f"Project CRS: {needed_crs}")
        if layer_crs != needed_crs:
            logger.warning("Layer geometries have a different CRS!")
            self.created_layer.setCrs(needed_crs)

        # log feature count
        if isinstance(self.created_layer, QgsVectorLayer):
            count = self.created_layer.featureCount()
            logger.debug(f"Feature count: {count}")
            if count == 0:
                raise ModuleLayerError(f"{file_name} - Layer besitzt keine Geometrien!")

        # check clip
        logger.debug(f"Clip: {self.clip}")
        if self.clip:
            if self.qgis_layer is None:
                logger.error("The given qgis layer is None!")
                raise ModuleLayerError("Der eingabe Layer is None!")
            clipped_layer = QGisApiWrapper.clip_by_layer(
                p_input_layer=self.created_layer, p_overlay_layer=self.qgis_layer,
                p_layer_name=f"{file_name}_clip_{time.time()}"
            )
            if clipped_layer is not None and clipped_layer.featureCount() > 0:
                self.created_layer = clipped_layer

        # check if none
        if self.created_layer is None:
            raise ModuleLayerError(f"Der Layer konnte nicht erstellt werden! {self.file_connection}")
        return True


class WfkEegCsvLayer(WfkLayer):
    def __init__(self):
        super().__init__()
        self.data: list = []

    def translated_name(self) -> str:
        """return name for the ui"""
        return "EEG Layer"

    def create_layer(self) -> bool:
        """create eeg remote layer"""
        if not os.path.isfile(path=self.file_connection):
            logger.error(f"The given path as a EegCsvLayer layer is not a file: {self.file_connection}")
            raise FileNotFoundError(f"The given path as a EegCsvLayer layer is not a file: {self.file_connection}")
        logger.debug("Create EegCsvLayer layer")
        logger.debug(f"File crs: {self.crs}")

        # control if we want to reprocess layer
        if not config.set_default("reprocess_layer", False) and self.created_layer is not None:
            logger.info("Do not reprocess layer, take created layer!")
            return True

        first = True
        layer_geom = None
        for f in self.qgis_layer.getFeatures():
            if first:
                layer_geom = f.geometry()
                first = False
            else:
                layer_geom = layer_geom.combine(f.geometry())

        workbook = xlrd.open_workbook(self.file_connection)
        s = workbook.sheet_by_index(0)
        for row in range(1, s.nrows):
            if s.cell_value(row, 9) == "Wind Land" and s.cell_value(row, 22) == "Nordrhein-Westfalen":
                self.data.append(s.row(row))

        vl = QgsVectorLayer("point?crs=" + self.crs, "tmp", "memory")
        pr = vl.dataProvider()
        vl.startEditing()
        pr.addAttributes([QgsField("anlagenschluessel", QVariant.String),  # 3
                          QgsField("netzbetreiber", QVariant.String),  # 31
                          QgsField("gemarkung", QVariant.String)])  # 20
        vl.updateFields()
        for row in self.data:
            try:
                geom = QgsGeometry.fromPointXY(QgsPointXY(row[24].value - 32000000, row[25].value))
                if self.clip and not geom.intersects(layer_geom):
                    continue
            except:
                continue
            fet = QgsFeature()
            fet.setGeometry(geom)
            fet.setAttributes([QVariant(row[3].value), QVariant(row[31].value), QVariant(row[20].value)])
            pr.addFeatures([fet])

        vl.commitChanges()
        vl.updateExtents()

        self.created_layer = vl
        return True

class WfkPostgreSqlLayer(WfkLayer):

    def __init__(self):
        super().__init__()
        self.created_layer = None           # will be set during the wfk process
        self.clip: bool = True              # from view / json

        self.qgis_layer = None              # wfk task set that
        self.crs: str = ""                  # module set that
        self.file_connection = ""      # wfk task set that

    def translated_name(self) -> str:
        """return name for the ui"""
        return "PostGre SQL Layer"

    def create_layer(self) -> bool:
        logger.debug(f"File crs: {self.crs}")
        if not self.file_connection or not isinstance(self.file_connection, dict):
            logger.error(f"The given database connection is broken: {self.file_connection}")
            return False

        connection: dict = self.file_connection.get("connection")
        if connection is None or not len(connection):
            logger.error(f"The given database connection is broken: {self.file_connection}")
            return False

        uri = QgsDataSourceUri()
        uri.setConnection(
            connection.get("host"),
            connection.get("port"),
            connection.get("database"),
            connection.get("user_name"),
            connection.get("password")
        )

        schema = self.file_connection.get("schema")
        table = self.file_connection.get("table")
        geometry_column = self.file_connection.get("geometry_column")

        if schema is not None and table is not None and geometry_column is not None:
            logger.debug("Use additional uri information to connect to database")
            uri.setDataSource(
                schema,
                table,
                geometry_column
            )
            layer = QgsVectorLayer(uri.uri(False), f"{schema}-{table}", "postgres")

            if not layer.isValid():
                logger.error(f"Can not create layer with given information: {self.file_connection}")
                return False
            if self.crs:
                layer.setCrs(QgsCoordinateReferenceSystem(self.crs))
            self.created_layer = layer
            return True
        logger.error("There are no additional url information stop process!")
        return False


LayerFactory.register("WfkShapeLayer", "Shape Layer", WfkShapeLayer)
LayerFactory.register("WfkEegCsvLayer", "EEG Layer", WfkEegCsvLayer)
LayerFactory.register("WfkPostgreSqlLayer", "PostGre SQL Layer", WfkPostgreSqlLayer)
