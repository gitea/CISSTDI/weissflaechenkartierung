from typing import List

try:
    from qgis.core import QgsVectorLayer
except ImportError as e:
    pass


class WfkLegendGroup:
    """
   Class which represents a wfk layer Legend group
    """

    def __init__(self, p_group_name: str):
        self.name: str = p_group_name
        self.layers: List['QgsVectorLayer'] = []
