import random
import time
import traceback
from decimal import *
from abc import abstractmethod

from typing import List, Tuple

from PyQt5.QtCore import pyqtSignal, QVariant

from app.common.config import config
from app.common.exception import RemoteFileDownloadError, InternetConnectionError, ExtractZipError, DifferenceError, \
    ModuleLayerError
from app.common.logger import logger
from app.common.wfk.tasks.task_result import TaskResult, WfkTaskResult
from app.common.wfk.tasks.task_info import TaskInfo
from app.common.wfk.wfk_config import WfkConfig
from app.common.wfk.wfk_difference import WfkBlacklistingDifference
from app.common.wfk.wfk_legend_group import WfkLegendGroup
from app.common.wfk.wfk_module import WfkModule

try:
    from qgis.core import QgsTask, QgsVectorLayer, QgsField, QgsFeatureRequest, QgsCoordinateReferenceSystem
    from app.common.qgis import QGisApiWrapper
except (ImportError, NameError):
    pass

# https://docs.python.org/3/tutorial/floatingpoint.html
getcontext().prec = 3


class BaseTask(QgsTask):
    result = pyqtSignal(TaskResult)

    def __init__(self, p_task_info: TaskInfo):
        super(BaseTask, self).__init__(p_task_info.desc, QgsTask.CanCancel)
        # task run information
        self._task_info: TaskInfo = p_task_info
        # qgis interface
        self._interface = config.qgis_interface
        # task result
        self._task_result: TaskResult = None
        # statistics
        self._start: float = 0.0
        self._elapsed: float = 0.0
        self._cur_step: int = 0  # current step of the process
        self._max_steps: int = 0  # maximum process steps

    def run(self) -> bool:
        """
        the action happens here
        """
        self._start = time.perf_counter()
        self._cur_step = 0  # reset
        logger.info(f"Start {self._task_info.desc}")

        if self._interface is None:
            logger.error("The given interface is None!")
            return False

        # check if we want to run this config
        if not self._task_info.wfk_config.run:
            logger.debug("The config is disabled")
            self._task_result.result = True
            self._task_result.txt = "Die Konfiguration ist deaktiviert!"
            return True

        logger.info(f"Run config: {self._task_info.wfk_config.name}")
        logger.info(f"Process: {str(len(self._task_info.wfk_config.modules))} module/s")
        logger.info(f"Run with errors: {config['run_tasks_with_errors']}")
        logger.info(f"Protocol layers: {config['protocol_layer']}")

        try:
            self._run(p_wfk_config=self._task_info.wfk_config)
        except Exception as e:
            logger.error(traceback.format_exc())
            logger.error("The configuration failed! " + str(e))
            self._task_result.txt = str(e)
            return False
        return True

    # # ######################################################
    # help functions

    def _set_percentage_params(self, p_process_list: list) -> None:
        """
        sets the percentage parameter
        :param p_process_list: list with tasks
        :return:
        """
        self._cur_step = 0
        self._max_steps = len(p_process_list)

    def _create_legend_layer_group(self, p_group_name: str, p_modules: list) -> WfkLegendGroup:
        """add given module layers to protocol list"""
        legend_group: WfkLegendGroup = WfkLegendGroup(p_group_name=p_group_name)
        for module in p_modules:
            if isinstance(module, WfkModule):
                if module.run and config.set_default("protocol_layer", False):
                    legend_group.layers.extend(self._prepare_module_layers(module))
        return legend_group

    def _prepare_module_layers(self, p_module: WfkModule) -> List['QgsVectorLayer']:
        """
        manipulate the given layers and return them
        :param p_module: module to process
        :return: manipulated layers
        """
        # set the layer names and add them to protocol
        if p_module.connector.layer.created_layer is not None:
            logger.debug("Try to set layer name of base layer...")
            p_module.connector.layer.created_layer.setName(p_module.name)
            logger.debug("Setting layer name of base layer finished")
            return [p_module.connector.layer.created_layer]
        return []

    def _create_layer(self, p_module: WfkModule, p_wfk_config: WfkConfig) -> bool:
        """
        Create layer with given data
        :param p_module:
        :param p_wfk_config:
        :return: true if successful, otherwise false
        """
        # set the root path to the connector
        p_module.connector.root_path = self._task_info.wfk_config.root_dir
        # run connector
        logger.debug("Run connection...")
        logger.info("Create file connection")
        logger.debug(f"Class: {p_module.connector.__class__.__name__}")
        p_module.connector.retrieve_data()
        self.setProgress(5 + (self._cur_step * (75 / self._max_steps)) + (10 / self._max_steps))
        if self.has_to_stop():
            logger.debug("The program has to stop after running connector")
            return True

        logger.debug("Run layer...")

        if p_module.connector.layer is None:
            logger.debug("There is no layer to run")
            return False

        # set connection of layer
        p_module.connector.layer.file_connection = p_module.connector.data_connection
        p_module.connector.layer.clip = p_module.connector.clip
        p_module.connector.layer.crs = p_module.connector.crs
        p_module.connector.layer.qgis_layer = p_wfk_config.qgis_layer
        p_module.connector.layer.create_layer()

        layer_crs = p_module.connector.layer.created_layer.crs()
        needed_crs = QgsCoordinateReferenceSystem(p_module.connector.crs)

        if layer_crs != needed_crs:
            self._task_result.txt = "Eingabe Layer besitzen abweichendes CRS!"

        self.setProgress(5 + (self._cur_step * (75 / self._max_steps)) + (20 / self._max_steps))
        if self.has_to_stop():
            logger.debug("The program has to stop after running layer")
            return True

        p_module.connector.layer.created_layer.setName(p_module.name)
        p_module.difference.base_layer = p_module.connector.layer.created_layer
        return True

    def _run_modules(self, p_wfk_config: WfkConfig, p_input_layer: 'QgsVectorLayer', p_modules: list,
                     p_run_diff: bool = True) -> tuple:
        """
        calcs the modules and produces resulting layer
        :param p_input_layer: input layer
        :param p_modules: modules to run
        :return: result, layer
        """
        modules_to_rem = []
        processed_layer = p_input_layer
        # iterate over the configuration modules
        for module in p_modules:
            if isinstance(module, WfkModule):
                try:
                    # add a new module
                    # p_wfk_config.statistic.add_module()

                    # check if the module should run
                    if not module.run:
                        logger.info(f"Skipp disabled module: {module.name}")
                        continue

                    # has the module a name
                    if len(module.name) == 0:
                        logger.warning(f"Module has no name!")
                        continue

                    logger.info(f"--------Run module: {module.name} crs: {module.default_crs}")

                    if self.has_to_stop():
                        logger.info(f"The program has to stop {module.name}")
                        return True, None

                    # check if the created layer is none, continue
                    if not self._create_layer(p_module=module, p_wfk_config=p_wfk_config):
                        logger.debug("Can not create the layer!")
                        continue

                    if p_run_diff:
                        logger.info("Create buffer")
                        logger.debug(f"Layer: {module.connector.layer.created_layer.name()}")
                        logger.debug(f"Class: {module.difference.__class__.__name__}")

                        # run difference between the wfk layer and the input layer from module layer
                        processed_layer = module.difference.run(p_layer_to_diff=processed_layer)

                    self.setProgress(5 + (self._cur_step * (75 / self._max_steps)) + (30 / self._max_steps))
                    if self.has_to_stop():
                        logger.debug("The program has to stop after running difference")
                        return True, None

                    self.setProgress(5 + (self._cur_step * (75 / self._max_steps)) + (75 / self._max_steps))
                    self._cur_step += 1

                    if processed_layer is not None:
                        logger.debug(f"Module {module.name} ran successful --------")
                    else:
                        raise DifferenceError(f"Fehler: {module.difference.__class__.__name__} Modul: {module.name}")
                except FileNotFoundError as e:
                    if config["run_tasks_with_errors"]:
                        self._task_result.txt = str(e)
                        modules_to_rem.append(module)
                        logger.warning("Skipped module: " + module.name)
                        logger.warning(str(e))
                        continue
                    else:
                        raise FileNotFoundError(f"Module failed: {module.name} {str(e)}")
                except RemoteFileDownloadError as e:
                    if config["run_tasks_with_errors"]:
                        self._task_result.txt = str(e)
                        modules_to_rem.append(module)
                        logger.warning("Skipped module: " + module.name)
                        logger.warning(str(e))
                        continue
                    else:
                        raise RemoteFileDownloadError(f"Module failed: {module.name} {str(e)}")
                except InternetConnectionError as e:
                    if config["run_tasks_with_errors"]:
                        self._task_result.txt = str(e)
                        modules_to_rem.append(module)
                        logger.warning("Skipped module: " + module.name)
                        logger.warning(str(e))
                        continue
                    else:
                        raise InternetConnectionError(f"Module failed: {module.name} {str(e)}")
                except ExtractZipError as e:
                    if config["run_tasks_with_errors"]:
                        self._task_result.txt = str(e)
                        modules_to_rem.append(module)
                        logger.warning("Skipped module: " + module.name)
                        logger.warning(str(e))
                        continue
                    else:
                        raise ExtractZipError(f"Module failed: {module.name} {str(e)}")
                except DifferenceError as e:
                    if config["run_tasks_with_errors"]:
                        self._task_result.txt = str(e)
                        modules_to_rem.append(module)
                        logger.warning("Skipped module: " + module.name)
                        logger.warning(str(e))
                        continue
                    else:
                        raise DifferenceError(f"Module failed: {module.name} {str(e)}")
                except ModuleLayerError as e:
                    if config["run_tasks_with_errors"]:
                        self._task_result.txt = str(e)
                        modules_to_rem.append(module)
                        logger.warning("Skipped module: " + module.name)
                        logger.warning(str(e))
                        continue
                    else:
                        raise ModuleLayerError(f"Module failed: {module.name} {str(e)}")

        # check if the wfk layer is none
        if processed_layer is None:
            logger.critical("The processed wfk layer is none after processing modules")
            return False, None

        # check if there are modules to remove because they fail
        if len(modules_to_rem) > 0:
            logger.debug("There where " + str(len(modules_to_rem)) + " failed modules.")
            for m in modules_to_rem:
                logger.debug(f"{m.name} failed")
                p_modules.remove(m)
                self._task_info.wfk_config.modules.remove(m)

        # check if there are modules left to process
        if len(p_modules) == 0:
            logger.warning("There were no layers to process left")
            self._task_result.txt = "Alle Module sind fehlgeschlagen!"
            return False, None

        if self.has_to_stop():
            logger.debug("The program has to stop")
            return True, None
        return True, processed_layer

    @abstractmethod
    def _run(self, p_wfk_config: WfkConfig) -> None:
        """
        run a wfk configuration
        :param p_wfk_config: config
        :return: None
        """
        raise Exception("This funktion needs to be overwritten")

    # ######################################################
    # post process functions

    def finished(self, result) -> None:
        """
        task finished
        :param result:
        :return:
        """
        """task finished"""
        logger.debug(f"{self._task_info.desc} finished")
        self._elapsed = time.perf_counter() - self._start
        self._finished(result)

    def _finished(self, result) -> None:
        """
        funktion which needs to be overwritten
        :param result: task result
        :return: None
        """
        raise Exception("This funktion needs to be overwritten")

    def _run_consideration(self, p_wfk_config: WfkConfig, p_wfk_layer: 'QgsVectorLayer',
                           p_modules: List[WfkModule]) -> Tuple[bool, List]:
        """
        run consideration criteria modules
        :param p_wfk_layer: input layer
        :param p_modules: modules
        :return: result, layer
        """
        wfk_layer_copy = p_wfk_layer.materialize(QgsFeatureRequest().setFilterFids(p_wfk_layer.allFeatureIds()))
        result, good_layer = self._run_modules(
            p_wfk_config=p_wfk_config, p_input_layer=wfk_layer_copy, p_modules=p_modules
        )
        self.good_layer = good_layer
        if not result or good_layer is None:
            logger.error("The processed consideration criteria layer is None!")
            if not len(self._task_result.txt):
                self._task_result.txt = "Bei den Abwägungskriterien ist ein Fehler aufgetreten!"
            return False, []
        if good_layer.featureCount() == 0:
            logger.error("The ok suited layer has no geometries!")
            if not len(self._task_result.txt):
                self._task_result.txt = "Bei den Abwägungskriterien ist ein Fehler aufgetreten! Gut " \
                                        "geeigneter Layer besitzt keine Geometrien!"
            return False, []
        good_layer.setName(f"Gut geeignete Flächen")

        # #########################################################
        # diff consideration layer with wfk input layer
        logger.debug("Create good area layer...")
        ok_layer = QGisApiWrapper.diff_from_layer(p_base_layer=wfk_layer_copy, p_input_layer=good_layer)
        if ok_layer is None:
            logger.error("Can not create ok area layer!")
            self._task_result.txt = "Bedingt geeigneter Layer konnte nicht erstellt werden!"
            # one of two layer failed, return the good one at least
            good_layer_copy = good_layer.materialize(QgsFeatureRequest().setFilterFids(good_layer.allFeatureIds()))
            self._add_area_attribute_to_layer(good_layer_copy)
            return False, [good_layer_copy, None]
        ok_layer.setName("Bedingt geeignete Flächen")

        # #########################################################
        # create new copy layer
        ok_layer_copy = ok_layer.materialize(QgsFeatureRequest().setFilterFids(ok_layer.allFeatureIds()))
        good_layer_copy = good_layer.materialize(QgsFeatureRequest().setFilterFids(good_layer.allFeatureIds()))

        # #########################################################
        # add area information to layer
        logger.debug("Set attributes to layer...")
        for layer in [good_layer_copy, ok_layer_copy]:
            self._add_area_attribute_to_layer(layer)
        return True, [good_layer_copy, ok_layer_copy]

    def _add_area_attribute_to_layer(self, p_layer) -> None:
        """
        add area len as attribute to layer field
        :param layer:
        :return:
        """
        p_layer.startEditing()
        pr = p_layer.dataProvider()
        pr.addAttributes([QgsField("flaeche", QVariant.Double)])
        p_layer.updateFields()
        p_layer.commitChanges()
        # add new attribute
        p_layer.startEditing()
        for feature in p_layer.getFeatures():
            # set the attribute value
            feature.setAttribute(
                "flaeche",
                QVariant(
                    float(f'{float(self._square_meters_to_ha(feature.geometry().area())):.3f}')
                )
            )
            p_layer.updateFeature(feature)
        p_layer.commitChanges()
        p_layer.updateExtents()

    def _square_meters_to_ha(self, p_m: Decimal) -> Decimal:
        """
        converts square meters to ha
        :param p_m: value to convert
        :return: converted value
        """
        return Decimal(p_m) / Decimal(10000.0)

    def _m2_to_km2(self, p_m: Decimal) -> Decimal:
        """
        converts square meters to ha
        :param p_m: value to convert
        :return: converted value
        """
        return Decimal(p_m) / Decimal(1000000.0)

    # ######################################################

    def has_to_stop(self) -> bool:
        """
        checks if the task needs to stopped
        """
        return True if self.isCanceled() else False


class WfkBaseTask(BaseTask):

    def __init__(self, p_task_info: TaskInfo):
        super().__init__(p_task_info)
        self._task_result = WfkTaskResult()

    def _run(self, p_wfk_config: WfkConfig) -> None:
        raise Exception("This funktion needs to be overwritten")

    def _prepare_module_layers(self, p_module: WfkModule) -> List['QgsVectorLayer']:
        """
        manipulates the given layers and return them
        :param p_module: module to process
        :return: manipulated layers
        """
        result: list = super()._prepare_module_layers(p_module)
        logger.debug(f"Is BlackListingLayer: {isinstance(p_module.difference, WfkBlacklistingDifference)}")
        if isinstance(p_module.difference, WfkBlacklistingDifference):
            if p_module.difference.diffed_layer is not None:
                # check if the user wants to add the blacklisted layer to qgis legend
                logger.debug(f"Show blacklist layer: {p_module.difference.save_blacklisted_layer}")
                if p_module.difference.save_blacklisted_layer:
                    p_module.difference.diffed_layer.setName("Blacklisting")
                    result.append(p_module.difference.diffed_layer)
                # todo 12.12.2023: wir muessen hier noch schauen wie das mit dem Analysieren funktionieren soll
                # prepare layer for analysis
                # self._task_result.layer_to_analyse.append(
                #     [p_module.difference.diffed_layer, True, self._config])
        else:
            if p_module.difference.diffed_layer is not None:
                logger.debug("Try to set layer name of difference diffed layer")
                name_extension: str = config.get("diff_name_extension", "")
                if len(name_extension):
                    diff_layer_name = f"{p_module.name} ({name_extension})"
                else:
                    diff_layer_name = f"{p_module.name} (Diff)"
                p_module.difference.diffed_layer.setName(diff_layer_name)
                result.append(p_module.difference.diffed_layer)
            elif p_module.difference.buffed_layer is not None:
                logger.debug("Try to set layer name of difference buffed layer")
                name_extension: str = config.get("buff_name_extension", "")
                if len(name_extension):
                    diff_layer_name = f"{p_module.name} ({name_extension})"
                else:
                    diff_layer_name = f"{p_module.name} (Buff)"
                p_module.difference.buffed_layer.setName(diff_layer_name)
        return result
