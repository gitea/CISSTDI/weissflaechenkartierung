from decimal import *
from typing import List

from PyQt5.QtCore import QVariant

from app.common import os_utils
from app.common.config import config
from app.common.logger import logger
from app.common.signal_bus import signalBus

from app.common.wfk.tasks.task_result import AnalysisTaskResult
from app.common.wfk.tasks.task_info import AnalysisTaskInfo
from app.common.wfk.wfk_config import WfkConfig
from app.common.wfk.wfk_module import WfkModule

try:
    from app.common.qgis import QGisApiWrapper, QGisProjectWrapper
    from app.common.wfk.tasks.base_task import BaseTask
    from qgis.core import QgsStyle, QgsGraduatedSymbolRenderer, QgsField, QgsProject, QgsVectorLayer, \
        QgsGraduatedSymbolRenderer, QgsClassificationEqualInterval, QgsRendererRangeLabelFormat
except (NameError, ImportError):
    pass

getcontext().prec = 2


class AnalysisTask(BaseTask):
    def __init__(self, p_task_info: AnalysisTaskInfo):
        super().__init__(p_task_info)
        self._task_result = AnalysisTaskResult()

    def _finished(self, result) -> None:
        """the wfk task finished"""
        self._task_result.result = result
        self._task_info.wfk_config.statistic.time_elapsed = self._elapsed

        self._task_result.wfk_config = self._task_info.wfk_config

        logger.info(f"The task finished in: { self._task_result.wfk_config.statistic.time_elapsed} seconds")

        self.result.emit(self._task_result)
        signalBus.analysisFinishedSig.emit()

    def has_to_stop(self) -> bool:
        """
        checks if the task needs to stopped
        """
        return True if self.isCanceled() else False

    # ################################################################
    # logic

    def _run(self, p_wfk_config: WfkConfig) -> None:
        """
        separate modules and run the right ones
        :param p_wfk_config:
        :return:
        """
        if self.has_to_stop():
            logger.debug("Task has to stop")
            return

        if p_wfk_config.processed_wfk_layer is None:
            logger.error("The given WFK-Layer is not valid!")
            self._task_result.txt = "The given WFK-Layer is not valid!"
            return

        if p_wfk_config.qgis_layer is None:
            logger.error("The given QGIS-Layer is not valid!")
            self._task_result.txt = "The given QGIS-Layer is not valid!"
            return

        consideration_modules: List[WfkModule] = []
        normal_modules: List[WfkModule] = []
        financial_sup_modules: List[WfkModule] = []

        # extract the different module types
        for module in p_wfk_config.modules:
            if isinstance(module, WfkModule):
                if module.consideration_criteria:
                    consideration_modules.append(module)
                elif module.financial_supported:
                    financial_sup_modules.append(module)
                else:
                    normal_modules.append(module)

        if not len(consideration_modules):
            logger.warning("There are no consideration criteria modules given!")
            self._task_result.txt = "Es wurden keine bedingt geeigneten Module erstellt!"
            return

        # check if we got a valid layer
        cloned_wfk_layer = QGisApiWrapper.clone_all_features(
            p_input_layer=p_wfk_config.processed_wfk_layer, p_layer_name="cloned_wfkLayer"
        )
        if cloned_wfk_layer is None:
            logger.error("The cloned wfk layer is none after cloning")
            self._task_result.txt = "Der WFK-Layer konnte nicht geklont werden!"
            return
        if cloned_wfk_layer.featureCount() == 0:
            logger.error("The cloned wfk layer has no features to process!")
            self._task_result.txt = "Der WFK-Layer besitzt keine Geometrien!"
            return

        # #######################
        # set percentage parameters
        self._set_percentage_params(p_process_list=p_wfk_config.modules)

        # #######################
        # run categorization
        if hasattr(self._task_info, "category_module"):
            if self._task_info.category_module is not None:
                self._task_result.categorised_layer = self._run_categorisation(
                    p_wfk_config=p_wfk_config, p_categorisation_module=self._task_info.category_module,
                    p_wfk_layer=cloned_wfk_layer
                )

        # #######################
        # create good and ok layer
        result, layer_list = self._run_consideration(
            p_wfk_config=p_wfk_config, p_wfk_layer=cloned_wfk_layer, p_modules=consideration_modules
        )
        if not result:
            logger.warning("There was a problem while processing consideration criteria!")
            self._task_result.result = False
        if len(layer_list) == 2:
            good_layer, ok_layer = layer_list
            if ok_layer is None or ok_layer.featureCount() == 0:
                logger.error("Ok layer is none or has no features!")
                return
            if good_layer is None or good_layer.featureCount() == 0:
                logger.error("Good layer is none or has no features!")
                return

            # #######################
            # create processed list
            processed_layer: List['QgsVectorLayer'] = [
                p_wfk_config.processed_wfk_layer,
                ok_layer,
                good_layer
            ]

            # #######################
            # calculate difference between good layer and ok layer
            if len(financial_sup_modules):
                logger.debug("Calculate financial supported differences...")
                processed_financial_supported_layer: List['QgsVectorLayer'] = self._calculate_difference_financial_supported(
                    p_wfk_config=p_wfk_config, p_input_layer=[ok_layer, good_layer], p_modules=financial_sup_modules
                )
                processed_layer.extend(processed_financial_supported_layer)

            # #######################
            # calculate relative area
            logger.debug("Calculate relative differences...")
            self._task_result.bar_chart_result = self._run_relative_area(
               p_wfk_config=p_wfk_config, p_ok_layer=ok_layer, p_modules=consideration_modules
            )

            # logger.warning(f"OUTPUT-Layer-Len: {len(processed_layer)}")
            # #######################
            # calculate table data
            self._task_result.calculations = self._calculate_table_data(
               p_processed_layers=processed_layer, p_base_layer=p_wfk_config.qgis_layer,
               p_power_density=p_wfk_config.power_density, p_full_load_hours=p_wfk_config.full_load_hours
               )

            # #######################
            # get pie relative sum result data
            self._task_result.pie_chart_sum_relative_result = self._calc_relative_result_layer(
                p_good_layer=good_layer, p_ok_layer=ok_layer, p_wfk_layer=cloned_wfk_layer,
                p_base_layer=p_wfk_config.qgis_layer
            )

            # add results
            self._task_result.consideration_ok_layer = ok_layer
            self._task_result.consideration_good_layer = good_layer

    # ######################################################################
    # business logic

    def _calc_relative_result_layer(self, p_good_layer: 'QgsVectorLayer', p_ok_layer: 'QgsVectorLayer',
                                    p_base_layer: 'QgsVectorLayer', p_wfk_layer: 'QgsVectorLayer') -> List[dict]:
        """
        Calculate the relative result area to base layer
        :param p_good_layer:
        :param p_ok_layer:
        :param p_base_layer:
        :return: results
        """
        base_layer_size: float = QGisProjectWrapper.calc_layer_area(p_base_layer)
        wfk_layer_size: float = QGisProjectWrapper.calc_layer_area(p_wfk_layer)
        good_layer_size: float = QGisProjectWrapper.calc_layer_area(p_good_layer)
        ok_layer_size: float = QGisProjectWrapper.calc_layer_area(p_ok_layer)

        logger.debug(f"input_layer_size: {base_layer_size}")
        logger.debug(f"wfk_layer_size: {wfk_layer_size}")
        logger.debug(f"good_layer_size: {good_layer_size}")
        logger.debug(f"ok_layer_size: {ok_layer_size}")
        logger.debug(f"input_layer_size - wfk_layer_size: {base_layer_size - wfk_layer_size}")

        if base_layer_size == 0:
            self._task_result.txt = "Das Untersuchungsgebiet besitzt keine Flächen!"
            return []
        if wfk_layer_size == 0:
            self._task_result.txt = "Der WFK-Layer besitzt keine Flächen!"
            return []
        if good_layer_size == 0:
            self._task_result.txt = "Der gut geeignete Layer besitzt keine Flächen!"
            return []
        if ok_layer_size == 0:
            self._task_result.txt = "Der bedingt geeignete Layer besitzt keine Flächen!"
            return []

        rel_data = [
            {
                "name": "Gut geeignet",
                "size": float(Decimal(good_layer_size) / Decimal(base_layer_size) * 100),
                "color": "#559F24"
            },
            {
                "name": "Bedingt geeignet",
                "size": float(Decimal(ok_layer_size) / Decimal(base_layer_size) * 100),
                "color": "#ECB61E"
            },
            {
                "name": "Ausschlusskriterien",
                "size": float((Decimal(base_layer_size) - Decimal(wfk_layer_size)) / Decimal(base_layer_size) * 100),
                "color": "#9B9B9B"
            }
        ]
        return rel_data

    def _run_categorisation(self, p_wfk_config: WfkConfig, p_categorisation_module: WfkModule,
                            p_wfk_layer: 'QgsVectorLayer') -> 'QgsVectorLayer':
        """
        run categorisation

        :param p_categorisation_module:
        :param p_wfk_layer:
        :return: categorised layer
        """
        if not p_categorisation_module.run:
            logger.info(f"The given module should not be ran: {p_categorisation_module.name}")
            return None
        try:
            logger.info("Run categorization")
            # create layer
            p_categorisation_module.connector.root_path = self._task_info.wfk_config.root_dir
            p_categorisation_module.connector.retrieve_data()
            p_categorisation_module.connector.layer.file_connection = p_categorisation_module.connector.data_connection
            p_categorisation_module.connector.layer.clip = p_categorisation_module.connector.clip
            p_categorisation_module.connector.layer.crs = p_categorisation_module.connector.crs
            p_categorisation_module.connector.layer.qgis_layer = p_wfk_config.qgis_layer
            p_categorisation_module.connector.layer.create_layer()
            p_categorisation_module.connector.layer.created_layer.setName(p_categorisation_module.name)

            layer = p_categorisation_module.connector.layer.created_layer
            if layer is None:
                logger.error("Categorisation failed, connector layer is None!")
                return None
            p_categorisation_module.categorization.ref_layer = layer

            # run categorisation
            categorised_layer = p_categorisation_module.categorization.run(p_wfk_layer)
            if categorised_layer is None:
                logger.error("Categorisation failed, layer is None!")
                return None
            categorised_layer.setName(f"{layer.name()} kategorisiert")

            field: str = self._task_info.field_name
            if categorised_layer.fields().indexFromName(field) == -1:
                raise NameError("Der Kategorisierungs-Layer besitz das Feld 'distance' nicht!")

            logger.debug("create classification...")
            classification_method = QgsClassificationEqualInterval()
            # You can use any of these classification method classes:
            # QgsClassificationQuantile()
            # QgsClassificationEqualInterval()
            # QgsClassificationJenks()
            # QgsClassificationPrettyBreaks()
            # QgsClassificationLogarithmic()
            # QgsClassificationStandardDeviation()

            # change format settings as necessary
            form = QgsRendererRangeLabelFormat()
            form.setFormat("%1 - %2")
            form.setPrecision(2)
            form.setTrimTrailingZeroes(True)
            # style
            default_style = QgsStyle().defaultStyle()
            color_ramp = default_style.colorRamp('Spectral')  # Spectral color ramp
            color_ramp.invert()
            # create renderer
            renderer = QgsGraduatedSymbolRenderer()
            renderer.setClassAttribute(field)
            renderer.setClassificationMethod(classification_method)
            renderer.setLabelFormat(form)
            renderer.updateClasses(categorised_layer, self._task_info.cat_classes)
            renderer.updateColorRamp(color_ramp)

            categorised_layer.setRenderer(renderer)
            categorised_layer.triggerRepaint()
            logger.debug("finished")
            return categorised_layer
        except Exception as e:
            if config["run_tasks_with_errors"]:
                self._task_result.txt = str(e)
                logger.warning(str(e))
                return None
            else:
                raise Exception(f"Categorisation failed! {e}")

    def _run_relative_area(self, p_wfk_config: WfkConfig, p_ok_layer: 'QgsVectorLayer',
                           p_modules: List[WfkModule]) -> list:
        """
        Calculate relative area of consideration criteria to OK-Layer
        :param p_wfk_config:
        :param p_ok_layer:
        :param p_modules:
        :return: relative values
        """
        if p_ok_layer is None or p_ok_layer.featureCount() == 0:
            logger.error("OK-Layer has no features!")
            return []
        total_ok_layer_area: Decimal = self._m2_to_km2(Decimal(QGisProjectWrapper.calc_layer_area(p_ok_layer)))
        bar_data = []
        colors = os_utils.generate_light_colors(len(p_modules))
        # iterate over module differences and calculate area to total area
        for module, color in zip(p_modules, colors):
            try:
                if not module.run:
                    logger.info(f"Ignore Layer: {module.name}")
                    continue
                # create layer
                self._create_layer(p_module=module, p_wfk_config=p_wfk_config)
                # run because we need the buffer
                module.difference.run(p_layer_to_diff=p_ok_layer)
                if module.difference.buffed_layer is None:
                    logger.error("Can not calculate area. Layer is none!")
                    return []
                # intersection with ok layer
                intersected = QGisApiWrapper.intersect(
                    p_input_layer=p_ok_layer, p_overlay_layer=module.difference.buffed_layer,
                    p_layer_name=f"{module.name}_rel_inter"
                )
                # diff module buffed layer
                size: Decimal = self._m2_to_km2(Decimal(QGisProjectWrapper.calc_layer_area(intersected)))
                #TODO: logger.error(f"before: {QGisProjectWrapper.calc_layer_area(intersected)}m2")
                #TODO: logger.error(f"after: {size}km2")
                #TODO: logger.error(f"total_ok: {total_ok_layer_area}km2")
                bar_data.append(
                    {
                        "name": module.name,
                        "size": round(float(size), 3),
                        "color": color,
                        "percentage":  round(float(size / total_ok_layer_area * 100), 3)
                    }
                )
            except (ZeroDivisionError, NameError) as e:
                logger.error("Bad zero division: " + str(e))
                continue
        return bar_data

    def _calculate_table_data(self, p_processed_layers: List['QgsVectorLayer'], p_base_layer: 'QgsVectorLayer',
            p_power_density: float, p_full_load_hours: float)-> List[list]:
        """
        calculate relative data of layers

        :param p_processed_layers: all processed layers
        :param p_base_layer: input/base layer
        :param p_power_density:
        :param p_full_load_hours:
        :return: List with results
        """
        if p_base_layer is None:
            logger.error("Can not calculate, input layer is None!")
            return []
        def calc_relative_area(a, b) -> float:
            return float(Decimal(a) / Decimal(b) * 100)
        def calc_power_density(a) -> Decimal:
            return  Decimal(a) * Decimal(p_power_density)
        def calc_full_load(a) -> Decimal:
            return Decimal(a) * Decimal(p_full_load_hours)

        result: List[list] = []
        if p_base_layer.featureCount() == 0:
            logger.warning("The input layer has no features!")
            return []
        layer_base_area = self._m2_to_km2(Decimal(QGisProjectWrapper.calc_layer_area(p_base_layer)))
        #TODO: logger.error("----------------------start------------------------------")
        for layer in p_processed_layers:
            area: Decimal = self._m2_to_km2(Decimal(QGisProjectWrapper.calc_layer_area(layer)))
            if area == .0:
                logger.warning(f"Layer {layer.name()} has no features!")
                continue
            relative_area_to_base: float = calc_relative_area(area, layer_base_area)
            power_density: Decimal = calc_power_density(area)
            yearly_result: Decimal = calc_full_load(power_density)
            #TODO: logger.error("----------------------------------------------------")
            #TODO: logger.warning(f"layer_base_area: {layer_base_area} name: {p_base_layer.name()}")
            #TODO: logger.warning(f"p_power_density: {p_power_density}")
            #TODO: logger.warning(f"p_full_load_hours: {p_full_load_hours}")
            #TODO: logger.error("-------------------------")
            #TODO: logger.warning(f"Layer: {layer.name()}")
            #TODO: logger.warning(f"area: {area}")
            #TODO: logger.warning(f"relArea: {area} / {layer_base_area} * 100 = {Decimal(area) / Decimal(layer_base_area) * 100}")
            #TODO: logger.warning(f"power_density: {area} * {p_power_density} = {Decimal(area) * Decimal(p_power_density)}")
            #TODO: logger.warning(f"yearly_result: {power_density} * {p_full_load_hours} = {Decimal(power_density) * Decimal(p_full_load_hours)}")
            result.append(
                [layer.name(), area, relative_area_to_base, power_density, yearly_result]
            )
        #TODO: logger.error("------------------------end-------------------------------")
        return result

    def _calculate_difference_financial_supported(self, p_wfk_config: WfkConfig, p_input_layer: List['QgsVectorLayer'],
                                                  p_modules: List[WfkModule]
                                                  ) -> List['QgsVectorLayer']:
        """
        calculate the difference between input wfk layer and given module layers

        :param p_wfk_config: wfk configuration
        :param p_input_layer: wfk layer
        :param p_modules: list with modules
        :return: list with processed layers
        """
        result: List['QgsVectorLayer'] = []
        for module in p_modules:
            for i, layer in enumerate(p_input_layer):
                ret, result_layer = self._run_modules(p_wfk_config=p_wfk_config, p_input_layer=layer, p_modules=[module])
                if not ret or result_layer is None:
                    logger.critical(f"Can not process module: {module.name}")
                    continue
                result_layer.setName(f"{module.name}_{i + 1}")
                result.append(result_layer)
        return result

    # ######################################################################
    # help functions

    def _add_consideration_names_to_layer(self, p_layer, p_modules: List[WfkModule]) -> bool:
        """
        add module name and layer path to result layer
        :param p_layer:
        :param p_modules:
        :return:
        """
        if p_layer is None:
            return False
        # add every module name and shape layer path to the layer
        for module in p_modules:
            p_layer.startEditing()
            pr = p_layer.dataProvider()
            pr.addAttributes([QgsField(module.name, QVariant.String)])
            p_layer.updateFields()
            p_layer.commitChanges()

        # add value
        p_layer.startEditing()
        for module in p_modules:
            for feature in p_layer.getFeatures():
                # set the attribute value
                feature.setAttribute(module.name, QVariant(module.connector.layer.file_connection))
                p_layer.updateFeature(feature)
        p_layer.commitChanges()
        return True
