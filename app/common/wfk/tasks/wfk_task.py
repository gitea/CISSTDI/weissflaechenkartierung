import traceback
from typing import Tuple, List

from ....common.logger import logger
from ....common.qgis import AreaUnit
from ....common.signal_bus import signalBus
from ....common.wfk.tasks.base_task import WfkBaseTask
from ....common.wfk.wfk_config import WfkConfig
from ....common.wfk.wfk_legend_group import WfkLegendGroup
from ....common.wfk.wfk_module import WfkModule

try:
    from qgis.core import QgsTask, QgsVectorDataProvider, QgsVectorLayer, QgsFeatureRequest
    from ....common.qgis import QGisApiWrapper
except (NameError, ImportError) as e:
    logger.debug("Can not import:", str(e))


class WfkTask(WfkBaseTask):
    """
    Task which can process wfk config and creates resulting layers
    """

    def _run_wfk(self, p_wfk_config: WfkConfig, p_layer, p_modules: list) -> tuple:
        """run the wfk"""

        result, wfk_layer = self._run_modules(p_wfk_config=p_wfk_config, p_input_layer=p_layer, p_modules=p_modules)

        if not result:
            return False, None

        if wfk_layer is None:
            return False, None

        # the wfk layer includes many features, combine them all into one
        logger.debug("Combine all wfk layer geometries to one")
        combined_wfk_layer = QGisApiWrapper.multi_2_single(p_base_layer=wfk_layer)

        if p_wfk_config.default_min_area > 0.0:
            # calculate the right default min are with are unit
            default_min_area = p_wfk_config.default_min_area
            if p_wfk_config.default_min_area_unit == AreaUnit.KM.value:
                default_min_area *= 1000000

            caps = combined_wfk_layer.dataProvider().capabilities()
            features = combined_wfk_layer.getFeatures()
            defeats: list = []

            logger.debug(f"Filter all areas which are smaller than: {str(p_wfk_config.default_min_area)} "
                         f"{p_wfk_config.default_min_area_unit}")

            try:
                if caps & QgsVectorDataProvider.DeleteFeatures:
                    for feature in features:
                        if feature.geometry().area() <= default_min_area:
                            defeats.append(feature.id())

                    # delete features if there are some to delete
                    if len(defeats) > 0:
                        # delete the features
                        combined_wfk_layer.startEditing()
                        logger.debug(f"Try to delete {str(len(defeats))} features")
                        res = combined_wfk_layer.dataProvider().deleteFeatures(defeats)
                        combined_wfk_layer.commitChanges()

                        if res:
                            logger.debug(f"Deleted {str(len(defeats))} features from layer")
                        else:
                            logger.warning("Can not delete the given features from layer!")
                    else:
                        logger.debug("No features to delete")
                else:
                    logger.warning("The given layer has not the capabilities to delete features!")
            except Exception as e:
                logger.warning(traceback.format_exc())
                logger.warning("Can not delete the given feature: " + str(e))

        logger.debug("Set name of combined wfk layer")
        # set the wfk layer name
        combined_wfk_layer.setName(p_wfk_config.wfk_layer_name)

        return True, combined_wfk_layer

    def _run_on_wfk_layer(self, p_wfk_config: WfkConfig,  p_layer, p_modules: list) \
            -> Tuple[bool, List['QgsVectorLayer']]:
        """run every financial supported module with wfk layer"""
        cloned_layer = p_layer.materialize(QgsFeatureRequest().setFilterFids(p_layer.allFeatureIds()))
        # check cloned layer
        if cloned_layer is None or cloned_layer.featureCount() == 0:
            logger.error("Can not clone the wfk layer")
            return False, []

        layers = []
        # run every module with wfk layer
        for module in p_modules:
            if not module.run:
                logger.warning(f"Skipp disabled module: {module.name}")
                continue

            result, layer = self._run_modules(p_wfk_config=p_wfk_config, p_input_layer=cloned_layer, p_modules=[module])

            logger.debug(f"result: {str(result)}")
            if not result and layer is None:
                return False, []
            layers.append(layer)
        return True, layers

    def _run(self, p_wfk_config: WfkConfig) -> None:
        """run the wfk task"""
        if self.has_to_stop():
            logger.debug("Task has to stop")
            return
        self.setProgress(5)

        # extract the different module types
        financial_supported_modules: list = []
        consideration_modules: list = []
        wfk_modules: list = []

        for module in p_wfk_config.modules:
            if isinstance(module, WfkModule):
                if module.financial_supported:
                    financial_supported_modules.append(module)
                elif module.consideration_criteria:
                    consideration_modules.append(module)
                else:
                    wfk_modules.append(module)

        # #######################

        # clone the selected working layer and save it temporary
        if p_wfk_config.qgis_layer.selectedFeatureCount() == 0:
            # clone the complete layer which we want to process and save it temporary
            wfk_cloned_layer = QGisApiWrapper.clone_all_features(p_input_layer=p_wfk_config.qgis_layer)
            logger.debug("Copy all features on layer")
        else:
            # clone the selected features
            logger.debug("Copy selected features on layer")
            wfk_cloned_layer = QGisApiWrapper.clone_selected_features(p_input_layer=p_wfk_config.qgis_layer)

        # check if we got a valid layer
        if wfk_cloned_layer is None:
            logger.error("The cloned wfk layer is none after cloning")
            self._task_result.txt = "Der Wfk-Layer konnte nicht geklont werden!"
            return

        if wfk_cloned_layer.featureCount() == 0:
            self._task_result.txt = "Der Wfk-Layer besitzt keine Geometrien!"
            return

        # #######################
        # set percentage parameters
        self._set_percentage_params(p_process_list=p_wfk_config.modules)
        # #######################

        wfk_result_group: WfkLegendGroup = WfkLegendGroup(p_group_name="WFK-Ergebnis")

        # save pointer
        wfk_layer = wfk_cloned_layer
        if len(wfk_modules) > 0:
            logger.debug("***Run wfk process***")
            result, wfk_layer = self._run_wfk(p_wfk_config=p_wfk_config, p_layer=wfk_cloned_layer, p_modules=wfk_modules
                                              )
            if not result or wfk_layer is None or wfk_layer.featureCount() == 0:
                self._task_result.txt = "Der Wfk-Layer konnte nicht erstellt werden!"
                return
            # safe a pointer to the wfk layer in a specific field
            p_wfk_config.processed_wfk_layer = wfk_layer

            wfk_layer_group = self._create_legend_layer_group(p_group_name="Ausschlusskriterien", p_modules=wfk_modules)
            self._task_result.layer_groups_to_protocol.append(wfk_layer_group)  # add layer group to result list
        else:
            logger.warning("No WFK modules to process")
            if wfk_layer is not None:
                wfk_layer.setName(p_wfk_config.wfk_layer_name)
                # safe a pointer to the wfk layer in a specific field
                p_wfk_config.processed_wfk_layer = wfk_layer

        logger.debug("Create wfk layer group...")
        wfk_result_group.layers.append(wfk_layer)
        logger.debug("Wfk-Layer group created")

        # #######################
        if self.has_to_stop():
            logger.debug("Task has to stop")
            return

        # #######################
        # run financial supported modules
        if len(financial_supported_modules) > 0:
            logger.debug("-------------------------------")
            logger.info("***Run financial supported modules***")
            logger.debug(f"Modules: {str(len(financial_supported_modules))}")
            # run financial supported modules
            result, supported_layers = self._run_on_wfk_layer(
                p_wfk_config=p_wfk_config, p_layer=wfk_layer, p_modules=financial_supported_modules
            )

            # #######################
            if not result:
                logger.error("There was an error while processing financial processing modules!")
                self._task_result.result = False
                return
            logger.info("After process financial supported layers")
            logger.debug(f"Modules left: {len(supported_layers)}")
            if len(supported_layers):
                logger.debug("Create layer group...")
                financial_layer_group = self._create_legend_layer_group(p_group_name="Gunstkriterien",
                                                                        p_modules=financial_supported_modules)
                financial_layer_group.layers.extend(supported_layers)
                self._task_result.layer_groups_to_protocol.append(financial_layer_group)
                logger.debug("Layer group created")
            else:
                logger.info("All modules are disabled or failed")
            logger.info("***Finished financial supported modules***")
        else:
            logger.warning("No financial supported modules to process")

        if self.has_to_stop():
            logger.debug("Task has to stop")
            return

        # #######################
        # run consideration modules
        if len(consideration_modules) > 0:
            logger.debug("-------------------------------")
            logger.info("***Run consideration modules***")
            logger.debug(f"Modules: {len(consideration_modules)}")

            result, layer_list = self._run_consideration(
                p_wfk_config=p_wfk_config, p_wfk_layer=wfk_layer, p_modules=consideration_modules
            )
            if not result:
                logger.warning("There was an error while processing consideration criteria modules!")
                self._task_result.result = False

            # add all results to layer group object
            for layer in layer_list:
                if layer is not None:
                    layer.setName(f"WFK_{layer.name()}")
                    wfk_result_group.layers.append(layer)

            # #######################
            logger.debug("Create layer group...")
            consideration_layer_group = self._create_legend_layer_group(p_group_name="Abwägungskriterien",
                                                                        p_modules=consideration_modules)
            self._task_result.layer_groups_to_protocol.append(consideration_layer_group)

            logger.debug("Layer group created")
            logger.info("***Finished consideration criteria modules***")

        # #######################
        # add wfk layer to group
        self._task_result.layer_groups_to_protocol.append(wfk_result_group)

    def _finished(self, result) -> None:
        """the wfk task finished"""
        self._task_result.result = result
        self._task_info.wfk_config.statistic.time_elapsed = self._elapsed

        self._task_result.wfk_config = self._task_info.wfk_config

        logger.info(f"The task finished in: { self._task_result.wfk_config.statistic.time_elapsed} seconds")

        self.result.emit(self._task_result)
        signalBus.wfkFinishedSig.emit()

    def has_to_stop(self) -> bool:
        """
        checks if the task needs to stopped
        """
        return True if self.isCanceled() else False
