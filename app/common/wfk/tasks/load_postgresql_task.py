import traceback

from PyQt5.QtCore import pyqtSignal

from app.common.logger import logger
from app.common.wfk.tasks.task_info import LoadPostgreConTaskInfo
from app.common.wfk.tasks.task_result import LoadPostGreDataResult

try:
    from qgis.core import QgsTask, QgsVectorLayer, QgsField, QgsProviderRegistry, QgsDataSourceUri
    from app.common.qgis import QGisApiWrapper
except (ImportError, NameError):
    pass

class LoadPostGreDataTask(QgsTask):
    result = pyqtSignal(LoadPostGreDataResult)

    def __init__(self, p_task_info: LoadPostgreConTaskInfo):
        super(LoadPostGreDataTask, self).__init__(p_task_info.desc, QgsTask.CanCancel)
        self._task_result = LoadPostGreDataResult()
        self._task_info: LoadPostgreConTaskInfo = p_task_info

    def run(self) -> bool:
        """
        the action happens here
        """
        self.setProgress(15)
        try:
            md = QgsProviderRegistry.instance().providerMetadata('postgres')
            uri = QgsDataSourceUri()
            connection: dict = self._task_info.connection
            uri.setConnection(
                connection.get("host"),
                connection.get("port"),
                connection.get("database"),
                connection.get("user_name"),
                connection.get("password")
            )
            self.setProgress(35)
            conn1 = md.createConnection(uri.uri(), {})
            self.setProgress(75)
            self._task_result.found_data = conn1.tables()
            self.setProgress(100)
        except Exception as e:
            logger.error(traceback.format_exc())
            logger.error("The process failed! " + str(e))
            self._task_result.txt = str(e)
            return False
        return True

    def finished(self, result) -> None:
        """the wfk task finished"""
        self._task_result.result = result
        self.result.emit(self._task_result)

    def has_to_stop(self) -> bool:
        """
        checks if the task needs to stopped
        """
        return True if self.isCanceled() else False