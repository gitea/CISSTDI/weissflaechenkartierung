class BaseTaskInfo:
    def __init__(self, p_desc: str):
        self.desc: str = p_desc


class LoadPostgreConTaskInfo(BaseTaskInfo):
    def __init__(self, p_connection: dict, p_desc: str):
        super().__init__(p_desc)
        self.connection: dict = p_connection


class TaskInfo(BaseTaskInfo):
    def __init__(self, p_wfk_config, p_desc: str):
        super().__init__(p_desc)
        self.wfk_config  = p_wfk_config


class AnalysisTaskInfo(TaskInfo):
    def __init__(self, p_wfk_config, p_desc: str, p_category_module, p_cat_classes: int, p_field_name: str):
        super().__init__(p_wfk_config, p_desc)
        self.category_module = p_category_module
        self.cat_classes = p_cat_classes
        self.field_name = p_field_name
