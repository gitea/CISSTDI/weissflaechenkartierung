from typing import List


class BaseTaskResult:
    def __init__(self):
        self.result: bool = False
        self.txt: str = ""

class TaskResult(BaseTaskResult):
    def __init__(self):
        super().__init__()
        self.layer_groups_to_protocol: list = []
        self.layer_to_protocol: list = []
        self.wfk_config = None

class LoadPostGreDataResult(BaseTaskResult):
    def __init__(self):
        super().__init__()
        self.found_data: List = []

class CalcModulesTaskResult(TaskResult):
    def __init__(self):
        super().__init__()
        self.calc_results: List[tuple] = []


class WfkTaskResult(TaskResult):
    def __init__(self):
        super().__init__()
        self.layer_to_analyse: list = []


class AnalysisTaskResult(TaskResult):
    def __init__(self):
        super().__init__()
        self.pie_chart_sum_relative_result: List = []
        self.consideration_good_layer = None
        self.consideration_ok_layer = None
        self.categorised_layer = None
        self.calculations: List[List] = []
        self.bar_chart_result: List = []
        self.pie_chart_result: List = []
