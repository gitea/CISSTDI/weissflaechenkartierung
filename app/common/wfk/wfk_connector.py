import os
from abc import abstractmethod
from typing import List

from PyQt5.QtCore import QSize, QDir, Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QApplication, QPushButton, QLabel

from ...common import os_utils
from ...common.config import config
from ...common.exception import ExtractZipError, RemoteFileDownloadError, InternetConnectionError
from ...common.logger import logger
from ...common.os_utils import unzip_file
from ...common.signal_bus import signalBus
from ...common.wfk.interface import JsonHandle, Construction
from ...common.wfk.wfk_factories import ConnectorFactory, LayerFactory
from ...components.buttons.tooltip_button import TooltipPushButton
from ...components.dialog_box import MessageDialog
from ...components.dialog_box.database_dialog import DatabaseConnectionDialog
from ...components.table_view.table_view import FilterTableView
from ...components.widgets.check_box import TextCheckBox
from ...components.widgets.combo_box import ComboBox
from ...components.widgets.line_edit import LineEdit
from ...components.widgets.tooltip import QGisTaskStateToolTip

try:
    import xlrd
    from qgis.core import QgsVectorLayer, QgsField, QgsGeometry, QgsPointXY, QgsFeature, QgsProviderRegistry, \
        QgsDataSourceUri, QgsApplication, QgsProject
    from ...common.wfk.tasks.task_info import LoadPostgreConTaskInfo
    from ...common.wfk.tasks.load_postgresql_task import LoadPostGreDataTask
    from ...common.wfk.tasks.task_result import LoadPostGreDataResult
    from ...common.qgis import QGisApiWrapper
    from ...common.qgis import QGisCrawler
except (NameError, ImportError):
    pass


class WfkConnectorBase(JsonHandle, Construction):
    def from_json(self, p_data: dict) -> None:
        pass

    def to_json(self) -> dict:
        pass

    def _init_widgets(self, p_parent=None) -> None:
        super()._init_widgets(p_parent)

    def _init_layout(self) -> None:
        super()._init_layout()

    def create_view(self, p_parent=None) -> None:
        super().create_view(p_parent)

    def from_view(self) -> None:
        pass

    @abstractmethod
    def retrieve_data(self) -> None:
        """
        abstract method for inheritance
        """
        pass

class WfkConnector(WfkConnectorBase):
    def __init__(self):
        super().__init__()

        # from the config
        self.root_path: str = config.plugin_dir

        # sub path
        self.sub_path: str = ""

        # is the local or web file zipped
        self.zipped: bool = False

        # connection to a path (web or local)
        self.data_connection: str = ""

        # name of the downloaded file
        self.unzipped_file_name: str = ""

        # 17.12.2022
        # moved from layer to connector, visual purpose; data model changed
        self.clip: bool = True  # from view / json
        self.crs: str = ""  # module set that

        self.layer = None  # set while wfk process

    @abstractmethod
    def translated_name(self) -> str:
        """return name for the ui"""
        return "Basis Verbindung"

    def from_json(self, p_data: dict) -> None:
        """ load from json """
        if p_data is None:
            return
        if isinstance(p_data.get("data_connection", None), str):
            self.data_connection = p_data.get("data_connection", "")

        if isinstance(p_data.get("sub_path", None), str):
            self.sub_path = p_data.get("sub_path", "")

        if isinstance(p_data.get("zipped", False), bool):
            self.zipped = p_data.get("zipped", False)

        if isinstance(p_data.get("download_file_name", None), str):
            self.unzipped_file_name = p_data.get("download_file_name", "")

        # if isinstance(p_data.get("clip", ""), str):
        #     self.clip = p_data.get("clip", True)
        self.clip = True

        if isinstance(p_data.get("crs", None), str):
            self.crs = p_data.get("crs", "")

    def to_json(self) -> dict:
        """Convert to json"""
        data = {"type": self.__class__.__name__,
                "zipped": self.zipped,
                "sub_path": self.sub_path,
                "download_file_name": self.unzipped_file_name,
                "clip": True,
                "crs": self.crs,
                "data_connection": self.data_connection}
        return data

    def _init_widgets(self, p_parent=None) -> None:
        """init widgets"""
        super(WfkConnector, self)._init_widgets(p_parent)

        # check box for possible
        self.cb_zipped = TextCheckBox(self.widget)
        self.cb_zipped.setText("Datei gezipped")
        self.cb_zipped.setChecked(False)

        self.le_crs = LineEdit(self.widget)
        self.le_crs.setTitle("Koordinatensystem")

        self.cb_clip = TextCheckBox(self.widget)
        self.cb_clip.setText("Mit Zielgebiet verschneiden")
        self.cb_clip.setToolTip("Dieser Algorithmus extrahiert Features aus dem Input-Layer,<br>die innerhalb oder "
                                "teilweise innerhalb der Grenzen von<br>Features im Clip-Layer liegen.")

        self.cb_clip.setDisabled(True)

    def create_view(self, p_parent=None):
        """create view"""
        super(WfkConnector, self).create_view(p_parent)
        if isinstance(self.zipped, bool):
                self.cb_zipped.setChecked(self.zipped)

        if isinstance(self.clip, bool):
            self.cb_clip.setChecked(self.clip)

        if isinstance(self.crs, str):
            self.le_crs.setText(self.crs)
        self.le_crs.hide()  # not used currently

    def from_view(self):
        """save data from view"""
        self.zipped = self.cb_zipped.check_box.isChecked()
        self.clip = True
        self.crs = self.le_crs.text()

    @abstractmethod
    def retrieve_data(self) -> None:
        """
        abstract method for inheritance
        """
        pass

    def _extract_zip(self, p_file_path: str, p_root_dir: str) -> bool:
        """extract the given zip file"""
        # if the file is not zipped
        self.data_connection = p_file_path
        logger.debug("File zipped: " + str(self.zipped))
        if self.zipped:
            logger.debug(f"File to unzip: {p_file_path}")
            logger.debug(f"Unzip to: {p_root_dir}")
            try:
                logger.debug("Try to extract the zip file...")
                if not unzip_file(p_file_path, p_root_dir):
                    raise ExtractZipError("The given zip is not a zip file")
            except Exception:
                raise ExtractZipError(f"The given zip: {p_file_path} can not be extracted")
            logger.debug("Zip file extracted!!")
            logger.debug(f"Last file connection: {self.data_connection}")
            self.data_connection = os.path.join(p_root_dir, self.unzipped_file_name)
            logger.debug(f"New file connection: {self.data_connection}")
            return True
        return True

class WfkDataBaseConnector(WfkConnectorBase):
    """
    cass class for database connectors
    """

    def translated_name(self) -> str:
        pass

    def __init__(self):
        super().__init__()
        self.data_connection: dict = {}
        self.db_connections: List[dict] = []

        super().__init__()

        # from the config
        self.root_path: str = config.plugin_dir

        # sub path
        self.sub_path: str = ""

        # is the local or web file zipped
        self.zipped: bool = False

        # connection to a path (web or local)
        self.data_connection: str = ""

        # name of the downloaded file
        self.unzipped_file_name: str = ""

        # 17.12.2022
        # moved from layer to connector, visual purpose; data model changed
        self.clip: bool = True  # from view / json
        self.crs: str = ""  # module set that

        self.layer = None  # set while wfk process

    def to_json(self) -> dict:
        """ convert to json """
        return {
            "type": self.__class__.__name__,
            "database_connections": self.db_connections,
            "data_connection": self.data_connection
        }

    def from_json(self, p_data: dict) -> None:
        """load json for postgre connector"""
        if p_data is None:
            return
        if isinstance(p_data.get("database_connections", None), list):
            self.db_connections = p_data.get("database_connections")
        if isinstance(p_data.get("data_connection", None), dict):
            self.data_connection = p_data.get("data_connection")


    def _init_widgets(self, p_parent=None) -> None:
        super()._init_widgets(p_parent)

    def _init_layout(self) -> None:
        super()._init_layout()

    def create_view(self, p_parent=None) -> None:
        super().create_view(p_parent)

    def from_view(self) -> None:
        pass

    def retrieve_data(self) -> None:
        """
        abstract method for inheritance
        """
        pass

class WfkLocalFileConnector(WfkConnector):
    def __init__(self):
        super().__init__()

    def translated_name(self) -> str:
        """return name for the ui"""
        return "Lokale Datei *.shp"

    def to_json(self) -> dict:
        """ convert to json """
        data = super(WfkLocalFileConnector, self).to_json()

        # at the moment we can not handle sub folder and other files than .shp
        data.pop("zipped")
        data.pop("sub_path")
        data.pop("download_file_name")
        return data

    def from_json(self, p_data: dict) -> None:
        """load json for local file connector"""
        super(WfkLocalFileConnector, self).from_json(p_data)

        # at the moment we can not handle sub folder and other files than .shp
        if not self.data_connection.endswith('.shp'):
            self.data_connection = ""
        self.sub_path = ""
        self.zipped = False

    def create_view(self, p_parent=None):
        """create view"""
        super(WfkLocalFileConnector, self).create_view(p_parent)
        # compute the connection
        if len(self.data_connection) > 0:
            self.le_path.setText(self.data_connection)

    def from_view(self):
        """save data from view"""
        super(WfkLocalFileConnector, self).from_view()
        self.data_connection = self.le_path.text()

    def _init_layout(self) -> None:
        """init layout"""
        self.le_path.move(0, 0)
        self.le_path.setFixedWidth(self.widget.width() - self.btn_file_dir.width() - 6)
        self.btn_file_dir.move(self.le_path.geometry().bottomRight().x() + 6, 23)
        # self.le_crs.move(0, self.le_path.geometry().bottomLeft().y() + 6)
        # self.le_crs.setFixedWidth(self.widget.width())
        self.widget.setFixedHeight(self.le_path.geometry().bottomLeft().y() + 6)

    def _init_widgets(self, p_parent=None) -> None:
        """init widgets"""
        super(WfkLocalFileConnector, self)._init_widgets(p_parent)

        def get_file():
            result: str = os_utils.get_open_file_name(p_caption="Shape-Datei auswählen",
                                                 p_filter="Shape Dateien (*.shp)",
                                                 p_file=self.le_path.text())
            if result:
                self.le_path.setText(result)

        self.btn_file_dir = TooltipPushButton("", self.widget)
        self.btn_file_dir.setIcon(QIcon(":/images/utils/add_file.svg"))
        self.btn_file_dir.setIconSize(QSize(20, 20))
        self.btn_file_dir.setFixedSize(32, 32)
        self.btn_file_dir.setToolTip("Relativen oder absoulten Pfad zu einer Shapedatei wählen")
        self.btn_file_dir.clicked.connect(get_file)

        self.le_path = LineEdit(parent=self.widget)
        self.le_path.setTitle("Dateipfad")

        # at the moment wen can not handle zipped local files
        self.cb_zipped.hide()

        # at the moment there is no need to do not clip
        self.cb_clip.hide()

    def retrieve_data(self):
        """retrieve data"""
        self.data_connection = QDir.toNativeSeparators(self.data_connection)
        logger.debug(f"Path: {self.data_connection}")

        # if the given root path is empty take the plugin folder
        if not len(self.root_path):
            self.root_path = config.plugin_dir

        # check and join if the root path is not absolut
        if not os.path.isabs(self.root_path):
            project_path = QgsProject.instance().absolutePath()
            if not os.path.exists(project_path):
                logger.debug("There is no project path! get the plugin path")
                project_path = config.plugin_dir

            self.root_path = os.path.join(project_path, self.root_path)
            if not os.path.isdir(self.root_path):
                raise FileNotFoundError("The given root directory does not exist: " + self.root_path)
        elif not os.path.exists(self.root_path):
            raise FileNotFoundError("The given root directory does not exist: " + self.root_path)

        self.root_path = QDir.toNativeSeparators(self.root_path)
        logger.debug(f"Root: {self.root_path}")

        file_path = self.data_connection
        if not len(file_path):
            raise FileNotFoundError("There is no local file given!")

        if not os.path.isabs(file_path):
            logger.debug("Path is relative")
            file_path = os.path.join(self.root_path, file_path)
        else:
            logger.debug("Path is absolut")

        if not os.path.isfile(file_path):
            raise FileNotFoundError("The given file does not exist: " + file_path)

        # check if the user wants to handle a local given zip file without a given sub-path
        if file_path.endswith(".zip"):
            logger.debug("The given local connection is a zip file")
            if len(self.sub_path):
                logger.debug("Zip file is given to extract, but no sub path!")

        # check if the sub path is absolut
        if self.sub_path != "":
            logger.debug(f"Sub path: {self.sub_path}")
            if os.path.isabs(self.sub_path):
                self.root_path = self.sub_path
            else:
                os.path.join(self.root_path, self.sub_path)
        self._extract_zip(p_file_path=file_path, p_root_dir=self.root_path)

class WfkRemoteFileConnector(WfkConnector):
    def __init__(self):
        super().__init__()
        # name of the zip file which is in the file system or get downloaded
        self.zip_file_name: str = "temp.zip"

    def translated_name(self) -> str:
        """return name for the ui"""
        return "Remote Datei"

    def _init_widgets(self, p_parent=None) -> None:
        """init widget"""
        super(WfkRemoteFileConnector, self)._init_widgets(p_parent)

        def test_connection():
            if len(self.le_url_path.text()) == 0:
                self.le_url_path.setProperty("hasError", True)
            elif not QGisCrawler.validate_url(url=self.le_url_path.text()):
                self.le_url_path.setProperty("hasError", True)
            else:
                self.le_url_path.setProperty("hasError", False)
            self.widget.setStyle(QApplication.style())

        self.le_url_path = LineEdit(self.widget)
        self.le_url_path.setReadOnly(False)
        self.le_url_path.textChanged.connect(test_connection)
        self.le_url_path.setTitle("Datenquelladresse im Web")

        self.le_sub_path = LineEdit(self.widget)
        self.le_sub_path.setTitle("Unterverzeichnis")

        self.le_download_file_name = LineEdit(self.widget)
        self.le_download_file_name.setTitle("Dateiname")

        self.le_zip_file_name = LineEdit(self.widget)
        self.le_zip_file_name.setReadOnly(True)
        self.le_zip_file_name.setTitle("Zipdateiname")

        self.cb_zipped.show()
        self.cb_clip.hide()

    def _init_layout(self) -> None:
        """init the layout of remote connector"""
        self.le_url_path.move(0, 0)
        self.le_url_path.setFixedSize(self.widget.width(), 50)

        # sub path
        self.le_sub_path.move(0, self.le_url_path.geometry().bottomLeft().y() + 6)
        self.le_sub_path.setFixedSize(self.widget.width(), 50)

        # download file name
        self.le_download_file_name.move(0, self.le_sub_path.geometry().bottomLeft().y() + 6)
        self.le_download_file_name.setFixedSize(self.widget.width(), 50)

        # zip file name
        self.le_zip_file_name.move(0, self.le_download_file_name.geometry().bottomLeft().y() + 6)
        self.le_zip_file_name.setFixedSize(self.widget.width(), 50)

        self.le_crs.move(0, self.le_zip_file_name.geometry().bottomLeft().y() + 6)
        self.le_crs.setFixedSize(self.widget.width(), 50)

        # checkbox
        self.cb_zipped.move(0, self.le_crs.geometry().bottomLeft().y() + 7)

        # resize
        self.widget.setFixedHeight(self.cb_zipped.geometry().bottomLeft().y() + 6)

    def create_view(self, p_parent=None) -> None:
        """crate and init the view"""
        super(WfkRemoteFileConnector, self).create_view(p_parent)
        # reset the data connection from local file -> remote file
        self.le_url_path.setText(self.data_connection)
        # sub path
        self.le_sub_path.setText(self.sub_path)
        # download file name
        self.le_download_file_name.setText(self.unzipped_file_name)
        # zip file name
        self.le_zip_file_name.setText(self.zip_file_name)

        self.le_url_path.setFocus()

    def from_view(self) -> None:
        """from view"""
        super(WfkRemoteFileConnector, self).from_view()

        self.data_connection = self.le_url_path.text()
        self.sub_path = self.le_sub_path.text()
        self.unzipped_file_name = self.le_download_file_name.text()

    def retrieve_data(self):
        """process remote file connector"""
        logger.debug("Process the remote file connector")

        logger.debug(f"Given connection: {self.data_connection}")
        logger.debug(f"Given root path: {self.root_path}")

        logger.debug(f"Given zipped: {self.zipped}")
        logger.debug(f"Given zip file name: {self.zip_file_name}")
        logger.debug(f"Given download file name: {self.unzipped_file_name}")
        logger.debug(f"Given sub_path: {self.sub_path}")

        # if the given root path is empty take the plugin folder
        logger.debug("Check if the root path is empty...")
        if len(self.root_path) == 0:
            self.root_path = config.plugin_dir
            logger.debug("The given root path is the plugin directory")

        logger.debug("Check if the given root path is absolut...")
        if not os.path.isabs(self.root_path):
            logger.debug("The given root path not absolut, take the project path")
            project_path = QgsProject.instance().absolutePath()
            if len(project_path) == 0 or not os.path.exists(project_path):
                logger.debug("There is no project path! get the plugin path")
                project_path = config.plugin_dir

            self.root_path = os.path.join(project_path, self.root_path)
            if not os.path.isdir(self.root_path):
                raise FileNotFoundError("The given root directory does not exist: " + self.root_path)
        elif not os.path.exists(self.root_path):
            raise FileNotFoundError("The given root directory does not exist: " + self.root_path)

        # check the sub path, if exists add it
        if self.sub_path != "":
            if os.path.isabs(self.sub_path):
                self.root_path = self.sub_path
            else:
                # build the root path with additional sub path
                self.root_path = os.path.join(self.root_path, self.sub_path)
            logger.debug("Root dir:" + self.root_path)

        # check if the file zip is on the plate
        local_path = os.path.join(self.root_path, self.zip_file_name)
        logger.debug(f"Joined local file path: {local_path} to check")
        if os.path.isfile(local_path):
            logger.debug(local_path + " inside cache")
        else:
            # build the directory structure with root and sub dir
            logger.debug("Create directory: " + str(self.root_path))
            os.makedirs(self.root_path, exist_ok=True)
            try:
                logger.debug("Check internet connection...")
                if not QGisCrawler.check_internet_qgis():
                    raise InternetConnectionError("There is no internet connection!")
                logger.info("There is an internet connection")
                logger.debug(f"Download file: {self.data_connection}")
                size, attr = QGisCrawler.download(p_url=self.data_connection, p_file_name=local_path)
                if size is not None and size > 0:
                    logger.debug("File downloaded successful.\nSize: " + str(size))
                else:
                    logger.debug(f"Can not download the file: {self.data_connection}")
                    raise RemoteFileDownloadError(f"Can not download the file: {self.data_connection}")
            except Exception as e:
                if not QGisCrawler.check_internet_qgis():
                    raise InternetConnectionError("There is no internet connection!")
                raise Exception(e)
        # extract the date and set the file connection
        self._extract_zip(p_file_path=local_path, p_root_dir=self.root_path)


# ##################################################################################
# kind of layer
# ##################################################################################

class WfkLocalShapeFileConnector(WfkLocalFileConnector):
    def __init__(self):
        super().__init__()
        self.layer = LayerFactory.build("WfkShapeLayer")

    def translated_name(self) -> str:
        """return name for the ui"""
        return "Lokale Datei *.shp"


class WfkLocalEegFileConnector(WfkLocalFileConnector):
    def __init__(self):
        super().__init__()
        self.data: list = []
        self.layer = LayerFactory.build("WfkEegCsvLayer")

    def translated_name(self) -> str:
        """return name for the ui"""
        return "Lokale Excel EEG .xlsx"

    def _init_widgets(self, p_parent=None) -> None:
        """init widgets"""
        super(WfkLocalEegFileConnector, self)._init_widgets(p_parent)

        def get_file():
            result: str = os_utils.get_open_file_name(p_caption="Excel-Sheet auswählen",
                                                 p_filter="Excel-Sheet (*.xlsx)",
                                                 p_file=self.le_path.text())
            if result:
                self.le_path.setText(result)

        # disconnect signals and add xlsx listener
        self.btn_file_dir.disconnect()
        self.btn_file_dir.clicked.connect(get_file)


# ##################################################################################
# remote
# ##################################################################################


class WfkRemoteShapeFileConnector(WfkRemoteFileConnector):
    def __init__(self):
        super().__init__()
        # name of the zip file which is in the file system or get downloaded
        self.zip_file_name: str = "temp.zip"
        self.layer = LayerFactory.build("WfkShapeLayer")

    def translated_name(self) -> str:
        """return name for the ui"""
        return "Remote Datei *.shp"


class WfkRemoteEegFileConnector(WfkRemoteFileConnector):
    def __init__(self):
        super().__init__()
        # name of the zip file which is in the file system or get downloaded
        self.zip_file_name: str = "temp.zip"
        self.layer = LayerFactory.build("WfkEegCsvLayer")

    def translated_name(self) -> str:
        """return name for the ui"""
        return "Remote Datei *.xlsx"


# ##################################################################################
# PostgreSQL
# ##################################################################################


class WfkPostgreSqlConnector(WfkDataBaseConnector):
    """
    PostGreSql connection class
    """
    def __init__(self):
        super().__init__()
        self.layer = LayerFactory.build("WfkPostgreSqlLayer")
        self.connection_dialog = None
        self._loading: bool = False

    def translated_name(self) -> str:
        """return name for the ui"""
        return "PostGreSQL Verbindung"

    def _filter_text_changed(self, p_new_filter: str) -> None:
        """
        filter text changed
        :param p_new_filter: new filter as string
        :return: None
        """
        if not p_new_filter:
            return
        p_new_filter = f"^{p_new_filter}"
        narrowed: bool = p_new_filter.startswith(self.table_view.proxy_model.filter_reg_exp.pattern())
        # narrowed = False
        # match from the beginning of the name p_new_filter
        self.table_view.proxy_model.set_filter_regex_string(p_new_filter, narrowed)

    def _on_add_new_connection_clicked(self) -> None:
        """
        create a new connection
        :return: None
        """
        self.connection_dialog = DatabaseConnectionDialog(self.main_window)
        self.connection_dialog.adjust_widget_geometry()
        self.masks.append(self.connection_dialog)
        self.connection_dialog.exec()
        connection: dict = self.connection_dialog.get_connection()
        name: str = connection.get("name")
        if name is not None and name:
            self.cb_connections.addItem(
                connection.setdefault("name", str(self.cb_connections.count() + 1)),
                connection
            )

    def _on_edit_connection(self) -> None:
        """
        on edit the current selected
        :return: None
        """
        if self.cb_connections.count() == 0:
            signalBus.showInfoToastTip.emit("Keine Verbindungen vorhanden!")
            return
        self.connection_dialog = DatabaseConnectionDialog(self.main_window)
        self.connection_dialog.set_connection(p_con=self.cb_connections.currentData(Qt.UserRole))
        self.connection_dialog.adjust_widget_geometry()
        self.masks.append(self.connection_dialog)
        self.connection_dialog.exec()
        new_connection: dict = self.connection_dialog.get_connection()
        name: str = new_connection.get("name")
        if name is not None and name:
            # set new data
            self.cb_connections.setCurrentText(name)
            self.cb_connections.setItemData(self.cb_connections.currentIndex(), new_connection)

    def _on_delete_connection(self) -> None:
        """
        test given connection
        :return: None
        """
        if self.cb_connections.count() == 0:
            signalBus.showInfoToastTip.emit("Keine Verbindungen vorhanden!")
            return
        def delete():
            self.cb_connections.removeItem(self.cb_connections.currentIndex())

        current_connection: dict = self.cb_connections.currentData(Qt.UserRole)
        self.message_box = MessageDialog(title="Abfrage",
                                         content=f"Soll {current_connection.get('name')} gelöscht werden?",
                                         parent=self.main_window)
        self.message_box.yesSignal.connect(delete)
        self.masks.append(self.message_box)
        self.message_box.show()

    def _on_connect_clicked(self) -> None:
        """
        test given connection
        :return: None
        """
        if self.cb_connections.count() == 0:
            signalBus.showInfoToastTip.emit("Keine Verbindung erstellt!")
            return
        if self._loading:
            signalBus.showInfoToastTip.emit("Nicht möglich!")
            return

        def process_changed(p_change):
            for widget in self.masks:
                if isinstance(widget, QGisTaskStateToolTip):
                    widget.show()
                    widget.setContent(content=f"Prozess zu {int(p_change)}% ausgeführt")
                    if p_change == 100:
                        widget.complete_task()

        def result(p_r: LoadPostGreDataResult):
            if not p_r:
                self.message_box = MessageDialog(title="Fehler",
                                                 content="Der Prozess meldet folgende Fehler:\n" + p_r.txt,
                                                 parent=self.main_window)
                self.masks.append(self.message_box)
                self.message_box.show()
                return
            # add results
            self.table_view.proxy_model.resetInternalData()
            self.table_view.source_model().resetInternalData()
            self.table_view.source_model().set_header(["Schema", "Tabelle", "Geometrie"])
            for i in p_r.found_data:
                self.table_view.append_row([i.schema(), i.tableName(), i.geometryColumn()])
            self._loading = False

        # start task
        self._loading = True
        self.data_connection.clear()    # clear dict
        connection: dict = self.cb_connections.currentData(Qt.UserRole)
        self._task = LoadPostGreDataTask(
            p_task_info=LoadPostgreConTaskInfo(p_connection=connection, p_desc="Load tables")
        )
        self._task.progressChanged.connect(process_changed)
        self._task.result.connect(result)
        loader = QGisTaskStateToolTip(title="Verbindung laden...",
                                      content="Prozess zu 0% ausgeführt", parent=self.main_window)
        loader.show()
        self.masks.append(loader)
        QgsApplication.taskManager().addTask(self._task)

    def _on_select_table(self) -> None:
        """
        save the current selected table
        :return: None
        """
        idx = self.table_view.currentIndex()
        if idx is not None:
            connection: dict  = self.cb_connections.currentData(Qt.UserRole)
            if connection is None or not len(connection):
                signalBus.showInfoToastTip.emit("Es gibt keine Verbindung!")
                return

            if self.table_view.model().rowCount() == 0:
                signalBus.showInfoToastTip.emit("Es wurden keine Daten gefunden!")
                return

            schema = self.table_view.model().index(idx.row(), 0).data()
            table = self.table_view.model().index(idx.row(), 1).data()
            geom_column = self.table_view.model().index(idx.row(), 2).data()

            self.data_connection = {
                "schema": schema,
                "table": table,
                "connection": connection,
                "geometry_column": geom_column
            }
            if schema is not None and table is not None:
                self.label_cur_table.setText(f"Aktuell ausgewählt: {schema}-{table}")
            else:
                self.label_cur_table.setText("Aktuell ausgewählt: Keine Auswahl")
            self.label_cur_table.adjustSize()

            signalBus.showInfoToastTip.emit(f"Tabelle '{table}' ausgewählt!")

    def _init_widgets(self, p_parent=None) -> None:
        super()._init_widgets(p_parent)

        # buttons
        self.btn_connect = TooltipPushButton("Verbinden", self.widget)
        self.btn_connect.setToolTip("Eine Verbindung aufbauen")
        self.btn_add_new = TooltipPushButton("Neu", self.widget)
        self.btn_add_new.setToolTip("Eine neue Verbindung erstellen")
        self.btn_edit = TooltipPushButton("Bearbeiten", self.widget)
        self.btn_edit.setToolTip("Eine Verbindung bearbeiten")
        self.btn_delete = TooltipPushButton("Entfernen", self.widget)
        self.btn_delete.setToolTip("Eine Verbindung entfernen")
        self.btn_select = TooltipPushButton("Auswählen", self.widget)
        self.btn_select.setToolTip("Ausgewählte Verbindung verwenden")

        self.btn_connect.clicked.connect(self._on_connect_clicked)
        self.btn_add_new.clicked.connect(self._on_add_new_connection_clicked)
        self.btn_edit.clicked.connect(self._on_edit_connection)
        self.btn_delete.clicked.connect(self._on_delete_connection)
        self.btn_select.clicked.connect(self._on_select_table)

        self.btn_connect.setFixedSize(100, 30)
        self.btn_add_new.setFixedSize(100, 30)
        self.btn_edit.setFixedSize(100, 30)
        self.btn_delete.setFixedSize(100, 30)
        self.btn_select.setFixedSize(100, 30)

        self.le_filter = LineEdit(self.widget)
        self.le_filter.setTitle("Tabelle filtern")
        self.le_filter.textChanged.connect(self._filter_text_changed)

        self.table_view = FilterTableView(self.widget)
        self.table_view.setVerticalScrollBarPolicy(Qt.ScrollBarPolicy.ScrollBarAsNeeded)
        self.table_view.menu = None

        self.cb_connections = ComboBox(self.widget)
        self.cb_connections.setPlaceHolderText("Verbindungen")
        self.cb_connections.setTitle("Verbindungen")
        self.cb_connections.setCursor(Qt.PointingHandCursor)

        self.label_cur_table = QLabel(self.widget)
        self.label_cur_table.setStyleSheet("font-family: Segoe UI;font-size:12px;")

    def _init_layout(self) -> None:
        super()._init_layout()

        # connections
        self.cb_connections.move(0, 0)
        self.cb_connections.setFixedWidth(self.widget.width())

        # buttons
        self.btn_connect.move(0, self.cb_connections.geometry().bottomLeft().y() + 12)
        self.btn_add_new.move(self.btn_connect.geometry().topRight().x() + 6,
                              self.btn_connect.y())
        self.btn_edit.move(self.btn_add_new.geometry().topRight().x() + 6,
                              self.btn_connect.y())
        self.btn_delete.move(self.btn_edit.geometry().topRight().x() + 6,
                              self.btn_connect.y())
        self.le_filter.move(0, self.btn_delete.geometry().bottomLeft().y() + 6)
        self.le_filter.setFixedWidth(self.widget.width() - self.btn_select.width() - 6)
        self.btn_select.move(self.le_filter.geometry().bottomRight().x() + 6,
                             self.le_filter.geometry().bottomRight().y() - self.btn_select.height() - 4)

        # table view
        self.table_view.move(0, self.le_filter.geometry().bottomLeft().y() + 6)
        self.table_view.setFixedSize(self.widget.width(), 200)

        self.label_cur_table.move(0, self.table_view.geometry().bottomRight().y() + 12)

        # resize
        self.widget.setFixedHeight(self.label_cur_table.geometry().bottomLeft().y())

    def create_view(self, p_parent=None) -> None:
        """
        create view
        :param p_parent: given parent
        :return: None
        """
        super().create_view(p_parent)

        # ad connections to combo box
        self.cb_connections.clear()
        for connection in self.db_connections:
            name = connection.get("name")
            if name is not None and name:
                self.cb_connections.addItem(
                    name,
                    connection
                )
        if not isinstance(self.data_connection, dict):
            return
        # select the given tabel
        saved_connection = self.data_connection.get("connection")
        if saved_connection is not None:
            if saved_connection.get("name") is not None:
                self.cb_connections.setCurrentText(saved_connection.get("name"))
        schema = self.data_connection.get("schema")
        table = self.data_connection.get("table")
        if schema is not None and table is not None:
            self.label_cur_table.setText(f"Aktuell ausgewählt: {schema}-{table}")
        else:
            self.label_cur_table.setText("Aktuell ausgewählt: Keine Auswahl")
        self.label_cur_table.adjustSize()

    def from_view(self) -> None:
        """
        get data from view
        :return: None
        """
        # add all financial supported modules
        self.db_connections.clear()
        model = self.cb_connections.model()
        for row in range(model.rowCount()):
            data = model.data(model.index(row, 0), Qt.UserRole)
            self.db_connections.append(data)
        # save current selected
        idx = self.table_view.currentIndex()
        if idx is not None:
            connection: dict = self.cb_connections.currentData(Qt.UserRole)
            if connection is None or not len(connection) and self.table_view.model().rowCount() == 0:
                return
            schema = self.table_view.model().index(idx.row(), 0).data()
            table = self.table_view.model().index(idx.row(), 1).data()
            geom_column = self.table_view.model().index(idx.row(), 2).data()
            self.data_connection = {
                "schema": schema,
                "table": table,
                "connection": connection,
                "geometry_column": geom_column
            }



ConnectorFactory.register("WfkLocalShapeFileConnector", "Lokale Datei *.shp", WfkLocalShapeFileConnector)
ConnectorFactory.register("WfkLocalEegFileConnector", "Lokale EEG-Datei *.xlsx", WfkLocalEegFileConnector)

ConnectorFactory.register("WfkRemoteShapeFileConnector", "Remote Datei *.shp", WfkRemoteShapeFileConnector)
ConnectorFactory.register("WfkRemoteEegFileConnector", "Remote Datei *.xlsx", WfkRemoteEegFileConnector)

ConnectorFactory.register("WfkPostgreSqlConnector", "PostGreSQL Verbindung", WfkPostgreSqlConnector)
