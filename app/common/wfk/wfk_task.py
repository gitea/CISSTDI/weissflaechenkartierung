import os.path
import time
import traceback
from typing import List, Tuple

from PyQt5.QtCore import *

from app.common.config import config
from app.common.exception import RemoteFileDownloadError, InternetConnectionError, ExtractZipError, DifferenceError
from app.common.logger import logger
from app.common.qgis import AreaUnit
from app.common.signal_bus import signalBus
from app.common.task_result import WfkTaskResult
from app.common.wfk.wfk_config import WfkConfig
from app.common.wfk.wfk_difference import WfkBlacklistingDifference
from app.common.wfk.wfk_module import WfkModule
from app.common.qgis.QGisApiWrapper import QGisApiWrapper

try:
    from qgis.core import QgsTask, QgsDistanceArea, QgsVectorLayer, QgsVectorDataProvider
except Exception as e:
    logger.debug("Can not import:", str(e))


class WfkTask(QgsTask):
    result = pyqtSignal(WfkTaskResult)

    def __init__(self, p_desc: str, p_config: WfkConfig):
        super(WfkTask, self).__init__(p_desc, QgsTask.CanCancel)
        # configuration to run
        self._config: WfkConfig = p_config

        # interface to qgis
        self._interface = config.qgis_interface

        # result
        self._task_result: WfkTaskResult = WfkTaskResult()

        # statistics
        self._start: float = 0.0

    def run(self) -> bool:
        """process task"""
        self._start = time.perf_counter()
        logger.info("Start WfkTask")
        if self._interface is None:
            return False

        # check if we want to run this config
        if not self._config.run:
            logger.warning("The config is disabled")
            self._task_result.result = True
            self._task_result.txt = "Die Konfiguration ist deaktiviert!"
            return True

        if not os.path.exists(self._config.root_dir):
            self._task_result.txt = "Das Root-Verzeichnis exitiert nicht mehr!"
            return False

        logger.info(f"Run config: {self._config.name}")
        logger.info(f"Process: {str(len(self._config.modules))} module/s")
        logger.info(f"Run with errors: {config['run_wfk_with_errors']}")
        logger.info(f"Protocol layers: {config['protocol_layer']}")
        logger.info(f"Root path: {self._config.root_dir}")

        try:
            self._run()
        except Exception as e:
            logger.error(traceback.format_exc())
            logger.critical("The configuration failed! " + str(e))
            self._task_result.txt = str(e)
            return False
        return True

    def _run_modules(self, p_layer, p_modules: list, p_step: int, p_max_steps: int) -> tuple:
        """calcs the modules and produces resulting layer"""
        modules_to_rem = []

        # layer to work with
        wfk_cloned_layer = p_layer

        # iterate over the configuration modules
        for module in p_modules:
            if isinstance(module, WfkModule):
                try:
                    # add a new module
                    self._config.statistic.add_module()

                    # check if the module should run
                    if not module.run:
                        logger.warning(f"Skipp disabled module: {module.name}")
                        continue

                    # has the module a name
                    if len(module.name) == 0:
                        logger.warning(f"Module has not a name!")
                        continue

                    logger.debug(f"--------Run module: {module.name} crs: {module.default_crs}")

                    if self.has_to_stop():
                        logger.info(f"The program has to stop {module.name}")
                        return True, None

                    # set the root path to the connector
                    module.connector.root_path = self._config.root_dir
                    # run connector
                    logger.debug(f"Run connector: {module.connector.__class__.__name__}")
                    module.connector.retrieve_data()
                    self.setProgress(5 + (p_step * (75 / p_max_steps)) + (10 / p_max_steps))
                    if self.has_to_stop():
                        logger.debug("The program has to stop after running connector")
                        return True, None

                    logger.debug("Run layer...")

                    if module.connector.layer is None:
                        logger.debug("There is no layer to run")
                        return True, None

                    # set connection of layer
                    module.connector.layer.file_connection = module.connector.data_connection
                    module.connector.layer.clip = module.connector.clip
                    module.connector.layer.crs = module.connector.crs
                    module.connector.layer.qgis_layer = self._config.qgis_layer
                    module.connector.layer.create_layer()
                    self.setProgress(5 + (p_step * (75 / p_max_steps)) + (20 / p_max_steps))
                    if self.has_to_stop():
                        logger.debug("The program has to stop after running layer")
                        return True, None

                    # check if the created layer is none, continue
                    if module.connector.layer.created_layer is None:
                        logger.debug("Can not create the layer!")
                        continue
                    logger.debug(f"Run difference on layer: {module.connector.layer.created_layer.name()}")

                    logger.debug(f"Run difference: {module.difference.__class__.__name__}")
                    # wfk layer -> diffed layer
                    # set the base layer to process, as module layer
                    module.difference.base_layer = module.connector.layer.created_layer

                    # run difference between the wfk layer and the input layer from module layer
                    wfk_cloned_layer = module.difference.run(p_layer_to_diff=wfk_cloned_layer)

                    self.setProgress(5 + (p_step * (75 / p_max_steps)) + (30 / p_max_steps))
                    if self.has_to_stop():
                        logger.debug("The program has to stop after running difference")
                        return True, None

                    self.setProgress(5 + (p_step * (75 / p_max_steps)) + (75 / p_max_steps))
                    p_step += 1

                    logger.debug(f"Module {module.name} ran successful --------")

                    # add a new module which is processed
                    self._config.statistic.module_processed()

                except FileNotFoundError as e:
                    if config["run_wfk_with_errors"]:
                        self._task_result.txt = str(e)
                        modules_to_rem.append(module)
                        logger.warning("Skipped module: " + module.name)
                        logger.warning(str(e))
                        continue
                    else:
                        raise FileNotFoundError(f"Module failed: {module.name} {str(e)}")
                except RemoteFileDownloadError as e:
                    if config["run_wfk_with_errors"]:
                        self._task_result.txt = str(e)
                        modules_to_rem.append(module)
                        logger.warning("Skipped module: " + module.name)
                        logger.warning(str(e))
                        continue
                    else:
                        raise RemoteFileDownloadError(f"Module failed: {module.name} {str(e)}")
                except InternetConnectionError as e:
                    if config["run_wfk_with_errors"]:
                        self._task_result.txt = str(e)
                        modules_to_rem.append(module)
                        logger.warning("Skipped module: " + module.name)
                        logger.warning(str(e))
                        continue
                    else:
                        raise InternetConnectionError(f"Module failed: {module.name} {str(e)}")
                except ExtractZipError as e:
                    if config["run_wfk_with_errors"]:
                        self._task_result.txt = str(e)
                        modules_to_rem.append(module)
                        logger.warning("Skipped module: " + module.name)
                        logger.warning(str(e))
                        continue
                    else:
                        raise ExtractZipError(f"Module failed: {module.name} {str(e)}")
                except DifferenceError as e:
                    if config["run_wfk_with_errors"]:
                        self._task_result.txt = str(e)
                        modules_to_rem.append(module)
                        logger.warning("Skipped module: " + module.name)
                        logger.warning(str(e))
                        continue
                    else:
                        raise DifferenceError(f"Module failed: {module.name} {str(e)}")

        # check if the wfk layer is none
        if wfk_cloned_layer is None:
            logger.critical("The cloned wfk layer is none after processing modules")
            self._task_result.txt = "Der Wfk layer ist ungültig!"
            return False, None

        # check if there are modules to remove because they fail
        if len(modules_to_rem) > 0:
            logger.debug("There where " + str(len(modules_to_rem)) + " failed modules.")
            for m in modules_to_rem:
                logger.debug(f"{m.name} failed")
                self._config.modules.remove(m)

        # set statistics
        self._config.statistic.modules_failed += len(modules_to_rem)

        # check if there are modules left to process
        if len(self._config.modules) == 0:
            logger.warning("There were no layers to process left")
            self._task_result.txt = "Alle Module sind fehlgeschlagen!"
            return False, None

        if self.has_to_stop():
            logger.debug("The program has to stop")
            return True, None

        return True, wfk_cloned_layer

    def _run_wfk(self, p_layer, p_modules: list, p_step: int, p_max_steps: int) -> tuple:
        """run the wfk"""

        result, wfk_layer = self._run_modules(p_layer=p_layer, p_modules=p_modules, p_step=p_step,
                                              p_max_steps=p_max_steps)

        if not result:
            return False, None

        if wfk_layer is None:
            return False, None

        # the wfk layer includes many features, combine them all into one
        logger.debug("Combine all wfk layer geometries to one")
        combined_wfk_layer = QGisApiWrapper.multi_2_single(p_base_layer=wfk_layer,
                                                           p_layer_name="tmp")

        if self._config.default_min_area > 0.0:
            # calculate the right default min are with are unit
            default_min_area = self._config.default_min_area
            if self._config.default_min_area_unit == AreaUnit.KM.value:
                default_min_area *= 1000000

            caps = combined_wfk_layer.dataProvider().capabilities()
            features = combined_wfk_layer.getFeatures()
            defeats: list = []

            logger.debug(f"Filter all areas which are smaller than: {str(self._config.default_min_area)} "
                         f"{self._config.default_min_area_unit}")

            try:
                if caps & QgsVectorDataProvider.DeleteFeatures:
                    for feature in features:
                        if feature.geometry().area() <= default_min_area:
                            defeats.append(feature.id())

                    # delete the features
                    combined_wfk_layer.startEditing()
                    logger.debug(f"Try to delete {str(len(defeats))} features")
                    res = combined_wfk_layer.dataProvider().deleteFeatures(defeats)
                    combined_wfk_layer.commitChanges()

                    if res:
                        logger.debug(f"Deleted {str(len(defeats))} features from layer")
                    else:
                        logger.warning("Can not delete the given features from layer!")
                else:
                    logger.warning("The given layer has not the capabilities to delete features!")

            except Exception as e:
                logger.warning(traceback.format_exc())
                logger.warning("Can not delete the given feature: " + str(e))

        logger.debug("Set name of combined wfk layer")
        # set the wfk layer name
        combined_wfk_layer.setName(self._config.wfk_layer_name)

        return True, combined_wfk_layer

    def _run_financial_supported(self, p_layer, p_modules: list, p_step: int, p_max_steps: int) -> Tuple[bool, List]:
        """run every financial supported module with wfk layer"""
        cloned_layer = QGisApiWrapper.clone_all_features(p_input_layer=p_layer)

        # check resulting
        if cloned_layer is None:
            logger.error("Can not clone the wfk layer")
            return False, []

        layers = []
        # run every module with wfk layer
        for module in p_modules:
            if not module.run:
                logger.warning(f"Skipp disabled module: {module.name}")
                continue

            result, layer = self._run_modules(p_layer=cloned_layer, p_modules=[module], p_step=p_step,
                                              p_max_steps=p_max_steps)
            logger.debug(f"result: {str(result)}")
            # check resulting
            if not result and layer is None:
                return False, []

            # if we want to run this module try to set the names
            logger.debug(f"Set layer name of module {module.name} to {module.legend_layer_name}")
            # check if there is no non layer
            if module.difference.diffed_layer is not None:
                module.difference.diffed_layer.setName(f"{module.legend_layer_name} (Differenz)")
                layers.append(module.difference.diffed_layer)

            # set layer name and append
            layer.setName(module.legend_layer_name)
            layers.append(layer)
        return True, layers

    def _add_layer_to_result(self, p_modules: list) -> None:
        """add given module layers to protocol list"""
        for module in p_modules:
            if isinstance(module, WfkModule):
                if module.run:
                    # set the layer names and add them to protocol
                    if module.connector.layer.created_layer is not None:
                        logger.debug("Try to set layer name of base layer")
                        module.connector.layer.created_layer.setName(module.legend_layer_name)
                        if config.set_default("protocol_layer", True):
                            self._task_result.layer_to_protocol.append(module.connector.layer.created_layer)

                    logger.debug("Check if there is a blacklisted layer...")
                    if isinstance(module.difference, WfkBlacklistingDifference):
                        if module.difference.diffed_layer is not None:
                            # check if the user wants to add the blacklisted layer to qgis legend
                            if module.difference.save_blacklisted_layer:
                                self._task_result.layer_to_protocol.extend(module.difference.diffed_layer)
                                logger.debug("Blacklisted layer will be shown in qgis")
                            # prepare layer for analysis
                            self._task_result.layer_to_analyse.append(
                                [module.difference.diffed_layer, True, self._config])
                    else:
                        if module.difference.diffed_layer is not None:
                            logger.debug("Try to set layer name of difference buffer layer")
                            module.difference.diffed_layer.setName(f"{module.legend_layer_name} (Differenz)")
                            if config.set_default("protocol_layer", True):
                                self._task_result.layer_to_protocol.append(module.difference.diffed_layer)

    def _run(self) -> None:
        """run the wfk task"""
        if self.has_to_stop():
            logger.debug("Task has to stop")
            return
        self.setProgress(5)

        # extract the different module types
        financial_supported_modules: list = []
        wfk_modules: list = []

        for module in self._config.modules:
            if isinstance(module, WfkModule):
                if module.financial_support:
                    financial_supported_modules.append(module)
                else:
                    wfk_modules.append(module)

        # clone the selected working layer and save it temporary
        if self._config.qgis_layer.selectedFeatureCount() == 0:
            # clone the complete layer which we want to process and save it temporary
            wfk_cloned_layer = QGisApiWrapper.clone_all_features(p_input_layer=self._config.qgis_layer)
            logger.debug("Copy all features on layer")
        else:
            # clone the selected features
            logger.debug("Copy selected features on layer")
            wfk_cloned_layer = QGisApiWrapper.clone_selected_features(p_input_layer=self._config.qgis_layer)

        # check if we got a valid layer
        if wfk_cloned_layer is None:
            logger.error("The cloned wfk layer is none after cloning")
            self._task_result.txt = "Der Wfk-Layer konnte nicht geklont werden!"
            return

        step = 0
        module_list_len = len(self._config.modules)

        logger.debug("***Run wfk***")
        # run wfk modules
        result, wfk_layer = self._run_wfk(p_layer=wfk_cloned_layer, p_modules=wfk_modules, p_step=step,
                                          p_max_steps=module_list_len)
        if not result:
            self._task_result.txt = "Der Wfk-Layer konnte nicht erstellt werden!"
            return
        if wfk_layer is None:
            self._task_result.txt = "Der Wfk-Layer konnte nicht erstellt werden!"
            return

        logger.debug("Add module layer to protocol...")
        # check if the user wants to add the layers to qgis interface
        # rename the given layers
        self._add_layer_to_result(wfk_modules)

        if len(financial_supported_modules) > 0:
            logger.debug("***Run financial supported modules***")
            logger.debug(f"There are {str(len(financial_supported_modules))} modules to process")
            # run financial supported modules
            result, supported_layers = self._run_financial_supported(p_layer=wfk_layer, p_step=step,
                                                                     p_modules=financial_supported_modules,
                                                                     p_max_steps=module_list_len)
            logger.debug(f"There are {len(supported_layers)} financial supported layers")
            if not result and len(supported_layers) == 0:
                logger.error("There was an error while processing financial processing modules!")
                self._task_result.txt = "Die Förderungskriterien konnten nicht angewandt werden!"
                return
            self._task_result.layer_to_protocol.extend(supported_layers)
            logger.info("***Finished financial supported modules***")

        # add wfk layer to analysis
        self._task_result.layer_to_analyse.append([wfk_layer, False, self._config])
        # add wfk layer to protocol list
        logger.debug("Add wfk layer to protocol...")
        self._task_result.layer_to_protocol.extend([wfk_layer])

    def finished(self, result) -> None:
        """the wfk task finished"""
        logger.info("The wfk config finished")

        self._task_result.result = result
        self._config.statistic.time_elapsed = time.perf_counter() - self._start
        self._task_result.config = self._config

        logger.info(f"The task finished in: {self._config.statistic.time_elapsed} seconds")

        self.result.emit(self._task_result)
        signalBus.wfkFinishedSig.emit()

    def has_to_stop(self) -> bool:
        """
        checks if the task needs to stopped
        """
        return True if self.isCanceled() else False
