from ...common.exception import ConfigNameError
from ...common.wfk.interface import JsonHandle
from ...common.wfk.wfk_factories import ConnectorFactory, DifferenceFactory, CategorizationFactory
from ...common.wfk_statistic import WfkModuleStatisticData


class WfkModule(JsonHandle):
    def __init__(self):
        # parent set that
        self.qgis_layer = None
        self.default_crs: str = ""
        self.default_buffer: float = 0.0
        self.default_min_area: float = 0.0

        # set from the ui or from json
        self.name: str = ""
        self.run: bool = True  # button to control the run of the module
        self.show_layer: bool = True  # the user can decide if the module should be shown inside qgis
        self.financial_supported: bool = False  # marks the module as a financial supported area
        self.consideration_criteria: bool = False    # marks the model as consideration module

        # created from json and ui
        self.connector = None
        self.difference = None
        self.categorization = None
        self.legend = None

        # created and filled inside the wfk process
        self.statistic = WfkModuleStatisticData()

    def __eq__(self, other):
        """check if the given other class is equal to me"""
        if not isinstance(other, WfkModule):
            return False
        if other.name == self.name and other.default_crs == self.default_crs and other.default_buffer == \
                self.default_buffer and other.default_min_area == self.default_min_area and other.run == self.run \
                and other.financial_supported == self.financial_supported and \
                other.consideration_criteria == self.consideration_criteria:
            return True
        return False

    def __str__(self):
        return self.__class__.__name__ + "<" + str(hex(id(self))) + ">"

    def from_json(self, p_data: dict) -> None:
        if not len(p_data):
            return
        if "name" not in p_data or len(p_data["name"]) == 0:
            raise ConfigNameError("There is no configuration name given!")
        self.name = p_data["name"]

        # should run the module, default true
        self.run = p_data.get("run", True)

        # is the module financial supported
        self.financial_supported = p_data.get("financial_supported", False)

        self.consideration_criteria = p_data.get("consideration_criteria", False)

        # first load the connector
        if "connector" in p_data and p_data["connector"] is not None:
            if "type" in p_data["connector"]:
                if p_data["connector"]["type"] in ConnectorFactory.registry:
                    # get the connector type and build the class, afterward fill it
                    self.connector = ConnectorFactory.build(class_name=p_data["connector"]["type"])
                    self.connector.from_json(p_data=p_data["connector"])

        # load and create difference class
        if "difference" in p_data and p_data["difference"] is not None:
            if "type" in p_data["difference"]:
                if p_data["difference"]["type"] in DifferenceFactory.registry:
                    # get the difference type and build the class, afterward fill it
                    self.difference = DifferenceFactory.build(class_name=p_data["difference"]["type"])
                    self.difference.from_json(p_data=p_data["difference"])
                    self.difference.crs = self.default_crs

        # load and create the categorization class
        if "categorization" in p_data and p_data["categorization"] is not None:
            if "type" in p_data["categorization"]:
                if p_data["categorization"]["type"] in CategorizationFactory.registry:
                    # get the difference type and build the class, afterward fill it
                    self.categorization = CategorizationFactory.build(
                        class_name=p_data["categorization"]["type"])
                    self.categorization.from_json(p_data=p_data["categorization"])

    def to_json(self) -> dict:
        data = {
            "name": self.name if self.name else "unexpected name",
            "run": self.run,
            "financial_supported": self.financial_supported,
            "consideration_criteria": self.consideration_criteria
        }
        if self.connector:
            data["connector"] = self.connector.to_json()
        if self.difference:
            data["difference"] = self.difference.to_json()
        if self.categorization:
            data["categorization"] = self.categorization.to_json()
        if self.legend:
            data["legend"] = self.legend.to_json()
        return data
