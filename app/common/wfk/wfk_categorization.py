import json
import os
from abc import abstractmethod

import requests as requests
from PyQt5.QtCore import Qt, QSize
from PyQt5.QtGui import QIntValidator, QIcon, QDoubleValidator
from PyQt5.QtWidgets import QLabel

from ...common import os_utils
from ...common.auto_wrap import auto_wrap
from ...common.config import config
from ...common.exception import ByCentroidCategorizationError, USUDistanceCategorizationError, \
    SPARQLCategorizationError, AdditionalInfoCategorizationError
from ...common.logger import logger
from ...common.qgis import QGisCrawler
from ...common.wfk.csv_owner import Owner
from ...common.wfk.interface import JsonHandle, Construction
from ...common.wfk.wfk_factories import CategorizationFactory
from ...components.buttons.tooltip_button import TooltipPushButton

from ...components.widgets.line_edit import DoubleVLineEditor, LineEdit, ValidateButtonLineEdit, BrowseLineEdit

from ...view.creation_interface.editable_listview_widget import EditCatDisListView, EditCatPosListView, \
    EditAddCatListView

try:
    from ...common.wfk.SPARQLWrapper import SPARQLWrapper, JSON, POST
    from ...common.qgis import QGisApiWrapper
    from qgis.PyQt.QtCore import QVariant
    from qgis.core import QgsField, QgsProject
except ImportError as e:
    pass


class WfkCategorization(JsonHandle, Construction):
    def __init__(self):
        self.ref_layer = None

    @abstractmethod
    def translated_name(self) -> str:
        """return name for the ui"""
        return "Basis Puffer"

    def from_json(self, p_data: dict) -> None:
        pass

    def to_json(self) -> dict:
        return {"type": self.__class__.__name__}

    @abstractmethod
    def run(self, cat_layer):
        return cat_layer

    def _init_widgets(self, p_parent=None) -> None:
        super(WfkCategorization, self)._init_widgets(p_parent)

    def _init_layout(self) -> None:
        super(WfkCategorization, self)._init_layout()

    def create_view(self, p_parent=None):
        super(WfkCategorization, self).create_view(p_parent)

    def from_view(self) -> None:
        super(WfkCategorization, self).from_view()


class WfkByDistanceCategorization(WfkCategorization):
    def __init__(self):
        super().__init__()
        self.inherited_fields: list = []

    def translated_name(self) -> str:
        """return name for the ui"""
        return "Kategorisierung nach Distanz"

    def from_json(self, p_data: dict) -> None:
        """load from json"""
        if isinstance(p_data.get("inherited_fields"), list):
            self.inherited_fields = p_data.get("inherited_fields")

    def to_json(self) -> dict:
        """to json"""
        data = super(WfkByDistanceCategorization, self).to_json()
        data["inherited_fields"] = self.inherited_fields
        return data

    def run(self, cat_layer):
        """run categorization"""
        logger.debug("Run WfkByDistanceCategorization")
        logger.debug(f"Inherited fields: {str(len(self.inherited_fields))}")

        if self.ref_layer is None:
            logger.warning("The base ref layer is none")
            return cat_layer

        return QGisApiWrapper.att_from_nearest(p_input_layer=cat_layer,
                                               p_ref_layer=self.ref_layer,
                                               p_fields=self.inherited_fields)

    def _init_widgets(self, p_parent=None) -> None:
        super(WfkByDistanceCategorization, self)._init_widgets(p_parent)

        self.lw_inherited_fields = EditCatDisListView(self.widget, p_parent)
        self.lw_inherited_fields.setFixedSize(self.widget.width(), 200)
        self.masks.append(self.lw_inherited_fields.editor_dialog)

    def _init_layout(self) -> None:
        super(WfkByDistanceCategorization, self)._init_layout()

        # list view object
        self.lw_inherited_fields.move(0, 9)

        # resize
        self.widget.setFixedSize(self.widget.width(),
                                 self.lw_inherited_fields.y() + self.lw_inherited_fields.height() + 6)
        self.widget.adjustSize()

    def create_view(self, p_parent=None):
        super(WfkByDistanceCategorization, self).create_view(p_parent)
        # add the current data to the list
        for item in self.inherited_fields:
            self.lw_inherited_fields.on_create_new_row(p_data=[item, ""])

    def from_view(self):
        super(WfkByDistanceCategorization, self).from_view()
        # get the data the user created
        model = self.lw_inherited_fields.list_view.model()
        self.inherited_fields.clear()
        for row in range(model.rowCount()):
            data = model.data(model.index(row, 0), Qt.DisplayRole)
            self.inherited_fields.append(data)


class WfkByPositionCategorization(WfkCategorization):
    def __init__(self):
        super().__init__()
        self.position_selectors: list = []

    def translated_name(self) -> str:
        """return name for the ui"""
        return "Kategorisierung nach Position"

    def from_json(self, p_data: dict) -> None:
        if "position_selectors" in p_data and isinstance(p_data["position_selectors"], list):
            self.position_selectors = p_data["position_selectors"]

    def to_json(self) -> dict:
        data = super(WfkByPositionCategorization, self).to_json()
        data["position_selectors"] = self.position_selectors
        return data

    def run(self, cat_layer):
        logger.debug("Run WfkByPositionCategorization")
        logger.debug(f"position selectors: {str(len(self.position_selectors))}")

        if len(self.position_selectors) == 0:
            logger.debug("Position selector list is empty")
            return cat_layer

        if self.ref_layer is None:
            logger.warning("The base ref layer is none")
            return cat_layer

        return QGisApiWrapper.att_from_position(p_input_layer=cat_layer,
                                                p_ref_layer=self.ref_layer,
                                                p_fields=self.position_selectors)

    def _init_widgets(self, p_parent=None) -> None:
        super(WfkByPositionCategorization, self)._init_widgets(p_parent)

        self.lw_position_selector = EditCatPosListView(self.widget, p_parent)
        self.lw_position_selector.setFixedSize(self.widget.width(), 200)
        self.masks.append(self.lw_position_selector.editor_dialog)

    def _init_layout(self) -> None:
        super(WfkByPositionCategorization, self)._init_layout()
        # list view object
        self.lw_position_selector.move(0, 9)

        # resize
        self.widget.setFixedSize(self.widget.width(),
                                 self.lw_position_selector.y() + self.lw_position_selector.height() + 6)
        self.widget.adjustSize()

    def create_view(self, p_parent=None):
        super(WfkByPositionCategorization, self).create_view(p_parent)
        # add the current data to the list
        for item in self.position_selectors:
            self.lw_position_selector.on_create_new_row(p_data=[item, ""])

    def from_view(self):
        super(WfkByPositionCategorization, self).from_view()
        # get the data the user created
        model = self.lw_position_selector.list_view.model()
        self.position_selectors.clear()
        for row in range(model.rowCount()):
            data = model.data(model.index(row, 0), Qt.DisplayRole)
            self.position_selectors.append(data)


class WfkByCentroidCategorization(WfkCategorization):
    """
    19.12.2022 wird aktuell nicht mehr gebraucht
    """
    def __init__(self):
        super().__init__()

    def translated_name(self) -> str:
        """return name for the ui"""
        return "Kategorisierung nach Mittelpunkt"

    def _init_widgets(self, p_parent=None) -> None:
        super(WfkByCentroidCategorization, self)._init_widgets(p_parent)
        self.label = QLabel(
            auto_wrap("Bei dieser Kategorisierung wird der Mittelpunkt jedes Features berechnet und gespeichert."
                      "", 70)[0], self.widget)

    def _init_layout(self) -> None:
        super(WfkByCentroidCategorization, self)._init_layout()
        self.label.move(0, 9)
        # set fixed size
        self.widget.setFixedSize(self.widget.width(), self.label.y() + self.label.height() + 6)
        self.widget.adjustSize()

    def create_view(self, p_parent=None):
        super(WfkByCentroidCategorization, self).create_view(p_parent)

    def run(self, cat_layer):
        """run usu distance"""
        logger.debug("Run ByCentroidCategorization")
        out_layer = QGisApiWrapper.clone_all_features(p_input_layer=cat_layer)
        try:
            out_layer.startEditing()
            pr = out_layer.dataProvider()
            pr.addAttributes([QgsField("CentroidCoordX", QVariant.Double),
                              QgsField("CentroidCoordY", QVariant.Double)
                              ])
            out_layer.updateFields()
            out_layer.commitChanges()
            out_layer.startEditing()

            for f in out_layer.getFeatures():
                if f.hasGeometry():
                    center = f.geometry().centroid().asPoint()
                else:
                    continue
                f = QGisApiWrapper.try_set_attribute_value(f, 'CentroidCoordX', center.y())
                f = QGisApiWrapper.try_set_attribute_value(f, 'CentroidCoordY', center.x())
                out_layer.updateFeature(f)

            out_layer.commitChanges()
        except Exception as e:
            raise ByCentroidCategorizationError(e)
        return out_layer


class WfkUSUDistanceCategorization(WfkByCentroidCategorization):
    """
    19.12.2022 usu war ein partner beim limbo project
    """
    def __init__(self):
        super().__init__()
        self.time_out: int = 15
        self.start_x: float = 54.144703
        self.start_y: float = 12.099037
        self.feature_limit: int = 100
        self.service_url: str = "http://limbo.usu-research.ml:8080/limbo/routing/"

    def translated_name(self) -> str:
        """return name for the ui"""
        return "Kategorisierung nach USU"

    def from_json(self, p_data: dict) -> None:
        super(WfkUSUDistanceCategorization, self).from_json(p_data)
        if isinstance(p_data.get("time_out"), int):
            self.time_out = p_data.get("time_out")
        if isinstance(p_data.get("start_x"), float):
            self.start_x = p_data.get("start_x")
        if isinstance(p_data.get("start_y"), float):
            self.start_y = p_data.get("start_y")
        if isinstance(p_data.get("feature_limit"), int):
            self.feature_limit = p_data.get("feature_limit")
        if isinstance(p_data.get("service_url"), str):
            self.service_url = p_data.get("service_url")

    def to_json(self) -> dict:
        data = super(WfkUSUDistanceCategorization, self).to_json()
        data["time_out"] = self.time_out
        data["start_x"] = self.start_x
        data["start_y"] = self.start_y
        data["feature_limit"] = self.feature_limit
        data["service_url"] = self.service_url
        return data

    def _init_widgets(self, p_parent=None) -> None:
        super(WfkUSUDistanceCategorization, self)._init_widgets(p_parent)

        def test_connection():
            if len(self.le_path.text()) == 0:
                self.le_path.setProperty("error", True)
            elif not QGisCrawler.validate_url(url=self.le_path.text()):
                self.le_path.setProperty("error", True)
            else:
                self.le_path.setProperty("error", False)

        self.le_path = ValidateButtonLineEdit(self.widget)
        self.le_path.setPlaceholderText("URL")
        self.le_path.resize(150, 30)
        self.le_path.btn_browse.clicked.connect(test_connection)
        self.le_path.setReadOnly(True)
        self.le_path.setPlaceholderText("URL")
        self.le_path.setObjectName("contentEdit")

        self.l_search = QLabel("URL:", self.widget)
        self.l_search.adjustSize()

        self.le_time_out = LineEdit(self.widget)
        self.le_time_out.edit.setValidator(QIntValidator(1, 120, self.widget))
        self.le_time_out.setTitle("Timeout")
        self.le_time_out.edit.setPlaceholderText("15")

        # QDoubleValidator
        self.le_start_x = DoubleVLineEditor(self.widget, values=(1, 1000, 10))
        self.le_start_x.resize(150, 30)
        self.le_start_x.setPlaceholderText("54.144703")

        self.le_start_x = LineEdit(self.widget)
        self.le_start_x.edit.setValidator(QDoubleValidator(1, 1000, 10, self.widget))
        self.le_start_x.setTitle("Start X")
        self.le_start_x.edit.setPlaceholderText("54.144703")

        self.le_start_y = DoubleVLineEditor(self.widget, values=(1, 1000, 10))
        self.le_start_y.resize(150, 30)
        self.le_start_y.setPlaceholderText("12.099037")
        self.l_start_y = QLabel("Start Y:", self.widget)
        self.l_start_y.adjustSize()

        self.le_feature_limit = LineEdit(self.widget)
        self.le_feature_limit.edit.setValidator(QIntValidator(1, 10000, self.widget))
        self.le_feature_limit.resize(150, 30)
        self.le_feature_limit.setPlaceholderText("100")
        self.l_feature_limit = QLabel("Max Features:", self.widget)
        self.l_feature_limit.adjustSize()

    def _init_layout(self) -> None:
        super(WfkUSUDistanceCategorization, self)._init_layout()
        # calculate font width
        labels: list = [self.l_start_x, self.l_start_y, self.l_feature_limit, self.l_timeout, self.l_search]
        x = max(self.widget.fontMetrics().width(i.text()) for i in labels) + 15
        height = self.le_path.height()

        # WfkByCentroidCategorization hide label
        self.label.hide()
        # url
        self.le_path.move(x, 9)
        self.l_search.move(0, 9 + int(self.le_path.height() / 2) - int(self.l_search.height() / 2))
        self.le_path.setFixedSize(self.widget.width() - x, height)

        # timeout
        self.le_time_out.move(x, self.le_path.geometry().bottomLeft().y() + 6)
        self.l_timeout.move(0, int(self.le_time_out.height() / 2) - int(self.l_timeout.height() / 2) +
                            self.le_time_out.y())
        self.le_time_out.setFixedSize(self.widget.width() - x, height)

        # start x
        self.le_start_x.move(x, self.le_time_out.geometry().bottomLeft().y() + 6)
        self.l_start_x.move(0, int(self.le_start_x.height() / 2) -
                            int(self.l_start_x.height() / 2) + self.le_start_x.y())
        self.le_start_x.setFixedSize(self.widget.width() - x, height)

        # start y
        self.le_start_y.move(x, self.le_start_x.geometry().bottomLeft().y() + 6)
        self.l_start_y.move(0, int(self.le_start_y.height() / 2) - int(self.l_start_y.height() / 2) +
                            self.le_start_y.y())
        self.le_start_y.setFixedSize(self.widget.width() - x, height)

        # le_feature_limit
        self.le_feature_limit.move(x, self.le_start_y.geometry().bottomLeft().y() + 6)
        self.l_feature_limit.move(0, int(self.le_feature_limit.height() / 2) - int(self.l_feature_limit.height() / 2) +
                                  self.le_feature_limit.y())
        self.le_feature_limit.setFixedSize(self.widget.width() - x, height)

        # resize
        self.widget.setFixedSize(self.widget.width(), self.le_feature_limit.y() + self.le_feature_limit.height() + 6)
        self.widget.adjustSize()

    def create_view(self, p_parent=None):
        super(WfkUSUDistanceCategorization, self).create_view(p_parent)
        # service url
        self.le_path.setText(self.service_url)
        # timeout
        if self.time_out > 0:
            self.le_time_out.setText(str(self.time_out))
        # start x
        if self.start_x >= 0.0:
            self.le_start_x.setText(str(self.start_x))
        # start y
        if self.start_y >= 0.0:
            self.le_start_y.setText(str(self.start_y))
        # feature limit
        if self.feature_limit > 0:
            self.le_feature_limit.setText(str(self.feature_limit))

    def from_view(self):
        super(WfkUSUDistanceCategorization, self).from_view()
        self.service_url = self.le_path.text()
        # timeout
        self.time_out = int(self.le_time_out.text())
        # start x
        self.start_x = float(self.le_start_x.text())
        # start y
        self.start_y = float(self.le_start_y.text())
        # feature limit
        self.feature_limit = int(self.le_feature_limit.text())

    def run(self, cat_layer):
        out_layer = super().run(cat_layer)
        logger.debug("Run USUDistanceCategorization")
        count = 0

        if out_layer is None:
            logger.warning(f"The input layer is None!")
            return cat_layer

        if self.time_out == 0 or self.feature_limit == 0:
            return cat_layer

        try:
            out_layer.startEditing()
            pr = out_layer.dataProvider()
            pr.addAttributes([QgsField("USUCost", QVariant.String)])
            out_layer.updateFields()
            out_layer.commitChanges()
            out_layer.startEditing()

            for f in out_layer.getFeatures():
                if count >= self.feature_limit:
                    logger.debug("USUDistanceCategorization: Feature-Limit von: " + str(
                        self.feature_limit) + " wurde erreicht. Es werden keine weiteren Kosten berechnet.", )
                    break
                x = f['CentroidCoordX']
                y = f['CentroidCoordY']

                myobj = "{\n\t\"start\": {\n\t\t\"x\": " + str(self.start_x) + ",\n\t\t\"y\": " + str(
                    self.start_y) + "\n\t},\n\t\"end\": {\n\t\t\"x\": " + str(x) + ",\n\t\t\"y\": " + str(
                    y) + "\n\t},\n\t\"height\": 2,\n\t\"width\": 3,\n\t\"cost\": \"cost\"\n}"

                headers = {'Content-type': 'application/json'}

                req = requests.post(self.service_url, data=myobj, timeout=self.time_out, headers=headers)
                # print(req.status_code)
                jval = json.loads(req.text)

                f = QGisApiWrapper.try_set_attribute_value(f, 'USUCost',
                                                           jval["features"][0]["properties"]["duration"])
                out_layer.updateFeature(f)
                count += 1
            out_layer.commitChanges()
        except Exception as e:
            raise USUDistanceCategorizationError(e)
        return out_layer


class WfkSPARQLCategorization(WfkByPositionCategorization):
    """
    19.12.2022 wird aktuell nicht mehr beneotigt
    hierbei muss gschaut werden ob die Daten aktuell sind
    """
    def __init__(self):
        super().__init__()
        self.attribute_selector: str = "id"

    def translated_name(self) -> str:
        """return name for the ui"""
        return "Kategorisierung nach Gemeinde"

    def from_json(self, p_data: dict) -> None:
        super(WfkSPARQLCategorization, self).from_json(p_data)
        if "attribute_selector" in p_data and isinstance(p_data["attribute_selector"], str):
            self.attribute_selector = p_data["attribute_selector"]

    def to_json(self) -> dict:
        data = super(WfkSPARQLCategorization, self).to_json()
        data["attribute_selector"] = self.attribute_selector
        return data

    def _init_widgets(self, p_parent=None) -> None:
        super(WfkSPARQLCategorization, self)._init_widgets(p_parent)

        self.le_attribute_selector = LineEdit(self.widget)
        self.le_attribute_selector.setPlaceholderText("Attribut Selektor")
        self.le_attribute_selector.resize(100, 30)
        self.l_attribute_selector = QLabel("Attribut Selektor:", self.widget)
        self.l_attribute_selector.adjustSize()

    def _init_layout(self) -> None:
        super(WfkSPARQLCategorization, self)._init_layout()
        # calc text metrics
        x = max(self.widget.fontMetrics().width(i.text()) for i in [self.l_attribute_selector]) + 15
        height = self.le_attribute_selector.height()

        # selector
        self.le_attribute_selector.move(x, 9)
        self.l_attribute_selector.move(0, 9 + int(self.le_attribute_selector.height() / 2) -
                                       int(self.l_attribute_selector.height() / 2))
        self.le_attribute_selector.setFixedSize(self.widget.width() - x, height)

        # list view object
        self.lw_position_selector.move(0, self.le_attribute_selector.y() + self.le_attribute_selector.height() + 6)

        # resize
        self.widget.setFixedSize(self.widget.width(),
                                 self.lw_position_selector.y() + self.lw_position_selector.height() + 6)

    def create_view(self, p_parent=None):
        super(WfkSPARQLCategorization, self).create_view(p_parent)

        self.le_attribute_selector.setText(self.attribute_selector)

    def from_view(self):
        super(WfkSPARQLCategorization, self).from_view()
        self.attribute_selector = self.le_attribute_selector.text()

    def run(self, cat_layer):
        """run sql analysis"""
        out_layer = super().run(cat_layer)

        if out_layer is None:
            return cat_layer

        if len(self.attribute_selector) == 0:
            logger.debug("The attribute selector is empty")
            return cat_layer

        fields_to_add = ["Name", "dbpedia", "Homepage", "Adresse", "Buergermeister", "Partei", "Einwohnerzahl"]
        try:
            try:
                out_layer.startEditing()

                pr = out_layer.dataProvider()
                for field in fields_to_add:
                    pr.addAttributes([QgsField(field, QVariant.String)])

                out_layer.updateFields()
                out_layer.commitChanges()
            except Exception as e:
                raise SPARQLCategorizationError(e)

            logger.debug("SPARQLCategorization attributes added")
            q_dict = {}

            out_layer.startEditing()
            for f in out_layer.getFeatures():
                key = f.attribute(self.attribute_selector)
                sparql = SPARQLWrapper("http://dbpedia.org/sparql")
                try:
                    x = int(key)
                except:
                    continue
                if int(key) in q_dict:
                    res = q_dict[int(key)]
                else:
                    sparql.setQuery("""
                    PREFIX foaf: <http://xmlns.com/foaf/0.1/>
                    PREFIX dbp: <http://dbpedia.org/property/>
                    select ?url ?partei ?name ?buergermeister ?homepage (GROUP_CONCAT(DISTINCT ?add; SEPARATOR=" , ") AS ?adresse) ?pop
                    where {
                        ?url dbp:gemeindeschlüssel """ + str(key) + """ .
                        optional{
                            ?url rdfs:label ?title .
                            BIND (STR(?title)  AS ?name) 
                            FILTER ( LANG(?title) = "de" )
                        }
                        optional{
                            ?url dbp:party ?partei .
                        }
                        optional{
                            ?url dbp:mayor ?buergermeister .
                        }
                        optional{
                            ?url foaf:homepage ?homepage .
                        }
                        optional{
                            ?url dbp:adresse ?add .
                        }optional
                        {
                        ?url dbo:populationTotal ?pop .
                        }
                    } 
                    LIMIT 1
                    """)
                    sparql.setReturnFormat(JSON)
                    sparql.setMethod(POST)
                    results = sparql.query().convert()
                    if len(results["results"]["bindings"]) == 0:
                        continue
                    res = results["results"]["bindings"][0]
                    q_dict[int(key)] = res

                f = QGisApiWrapper.try_set_attribute(f, res, 'Name', 'name')
                f = QGisApiWrapper.try_set_attribute(f, res, 'dbpedia', 'url')
                f = QGisApiWrapper.try_set_attribute(f, res, 'Homepage', 'homepage')
                f = QGisApiWrapper.try_set_attribute(f, res, 'Adresse', 'adresse')
                f = QGisApiWrapper.try_set_attribute(f, res, 'Buergermeister', 'buergermeister')
                f = QGisApiWrapper.try_set_attribute(f, res, 'Partei', 'partei')
                f = QGisApiWrapper.try_set_attribute(f, res, 'Einwohnerzahl', 'pop')

                out_layer.updateFeature(f)

            out_layer.commitChanges()
        except Exception as e:
            logger.warning("Delete field names which can not be added")
            logger.warning(str(e))
            del_attributes_pos = []
            count = 0
            for field_united in out_layer.dataProvider().fields():
                if field_united.name() != self.attribute_selector and field_united.name() in fields_to_add:
                    del_attributes_pos.append(count)
                count += 1

            try:
                logger.debug("try to delete:  " + str(len(del_attributes_pos)) + " items",
                             )
                out_layer.startEditing()
                out_layer.dataProvider().deleteAttributes(del_attributes_pos)
                out_layer.updateFields()
                out_layer.commitChanges()
            except Exception as e:
                raise SPARQLCategorizationError(e)

            logger.debug("SPARQLCategorization deleted " + str(len(del_attributes_pos)) + " items", )
        return out_layer


class WfkByPositionCategorizationWithClipping(WfkCategorization):
    """
    19.12.2022 wieso brauchen wir das
    """
    def __init__(self):
        super().__init__()
        self.position_selector: str = "id"
        self.clipped_layer_name: str = "Clipped"
        self.tailored_layer_name: str = "Tailored"

    def translated_name(self) -> str:
        """return name for the ui"""
        return "Kategorisierung nach Überlappung"

    def from_json(self, p_data: dict) -> None:
        super(WfkByPositionCategorizationWithClipping, self).from_json(p_data)

        if isinstance(p_data.get("position_selector", None), str):
            self.position_selector = p_data["position_selector"]

        if isinstance(p_data.get("clipped_layer_name", None), str):
            self.clipped_layer_name = p_data["clipped_layer_name"]

        if isinstance(p_data.get("tailored_layer_name", None), str):
            self.tailored_layer_name = p_data["tailored_layer_name"]

    def to_json(self) -> dict:
        data = super(WfkByPositionCategorizationWithClipping, self).to_json()
        data["position_selector"] = self.position_selector
        data["clipped_layer_name"] = self.clipped_layer_name
        data["tailored_layer_name"] = self.tailored_layer_name
        return data

    def _init_widgets(self, p_parent=None) -> None:
        super(WfkByPositionCategorizationWithClipping, self)._init_widgets(p_parent)
        # pos selector
        self.le_pos_selector = LineEdit(self.widget)
        self.le_pos_selector.setTitle("Id")

        # clipped layer name
        self.le_clipped_layer_name = LineEdit(self.widget)
        self.le_clipped_layer_name.setTitle("Layer Name")

        # clipped layer name
        self.le_tailored_layer_name = LineEdit(self.widget)
        self.le_tailored_layer_name.setTitle("Layer Name")

    def _init_layout(self) -> None:
        super(WfkByPositionCategorizationWithClipping, self)._init_layout()
        self.le_pos_selector.move(0, 0)
        self.le_pos_selector.setFixedWidth(self.widget.width())

        self.le_clipped_layer_name.move(0, self.le_pos_selector.geometry().bottomLeft().y() + 6)
        self.le_clipped_layer_name.setFixedWidth(self.widget.width())

        self.le_tailored_layer_name.move(0, self.le_clipped_layer_name.geometry().bottomLeft().y() + 6)
        self.le_tailored_layer_name.setFixedWidth(self.widget.width())

        self.widget.setFixedHeight(self.le_tailored_layer_name.geometry().bottomLeft().y() + 6)

    def create_view(self, p_parent=None):
        super(WfkByPositionCategorizationWithClipping, self).create_view(p_parent)

        self.le_pos_selector.setText(self.position_selector)
        self.le_clipped_layer_name.setText(self.clipped_layer_name)
        self.le_tailored_layer_name.setText(self.tailored_layer_name)

    def from_view(self):
        super(WfkByPositionCategorizationWithClipping, self).from_view()
        self.position_selector = self.le_pos_selector.text()
        self.clipped_layer_name = self.le_clipped_layer_name.text()
        self.tailored_layer_name = self.le_tailored_layer_name.text()

    def run(self, cat_layer):
        """run clipping analysis"""
        if cat_layer is None:
            return None

        if self.ref_layer is None:
            return cat_layer

        # 1) clip layer
        logger.debug("clip layer")
        clipped_layer = QGisApiWrapper.clip_by_layer(p_input_layer=self.ref_layer,
                                                     p_overlay_layer=cat_layer,
                                                     p_layer_name=self.clipped_layer_name)

        # 2) unite attributes from cat_layer and clipped layer
        logger.debug("unite layer")
        united_layer = QGisApiWrapper.union_by_layer(p_input_layer=clipped_layer,
                                                     p_overlay_layer=cat_layer,
                                                     p_layer_name=self.tailored_layer_name)

        # get names which will not be deleted
        not_del_attributes = []
        for field_cat in cat_layer.dataProvider().fields():
            not_del_attributes.append(field_cat.name())

        # 3) delete attributes except selector
        del_attributes_pos = []
        count = 0

        for field_united in united_layer.dataProvider().fields():
            if field_united.name() != self.position_selector and field_united.name() not in not_del_attributes:
                del_attributes_pos.append(count)
            count += 1

        logger.debug("delete attributes")
        united_layer.dataProvider().deleteAttributes(del_attributes_pos)
        united_layer.updateFields()

        return united_layer


class WfkAdditionalInformationCategorization(WfkByPositionCategorizationWithClipping):
    def __init__(self):
        super().__init__()
        self.additional_info_csv_file: str = ""
        self.additional_information_items: list = []
        self.csv_column_selector: str = ""

        self.root_path = config.plugin_dir      # base directory for relative scv file

    def translated_name(self) -> str:
        """return name for the ui"""
        return "Zusatzinformationen"

    def from_json(self, p_data: dict) -> None:
        """load from json"""
        super(WfkAdditionalInformationCategorization, self).from_json(p_data)
        if p_data.get("additional_info_csv_file", None) is not None and \
                p_data.get("additional_info_csv_file", "").endswith('.csv'):
            self.additional_info_csv_file = p_data.get("additional_info_csv_file")
        if isinstance(p_data.get("additional_information_items", None), list):
            self.additional_information_items = p_data["additional_information_items"]
        if isinstance(p_data.get("csv_column_selector", None), str):
            self.csv_column_selector = p_data["csv_column_selector"]

    def to_json(self) -> dict:
        data = super(WfkAdditionalInformationCategorization, self).to_json()
        data["additional_info_csv_file"] = self.additional_info_csv_file
        data["additional_information_items"] = self.additional_information_items
        data["csv_column_selector"] = self.csv_column_selector
        return data

    def _init_widgets(self, p_parent=None) -> None:
        super(WfkAdditionalInformationCategorization, self)._init_widgets(p_parent)
        def get_file():
            result: str = os_utils.get_open_file_name(p_caption="CSV-Datei auswählen...",
                                                 p_filter="CSV Dateien (*.csv)",
                                                 p_file=self.le_csv_path.text())
            if result:
                self.le_csv_path.setText(result)

        # csv path
        self.le_csv_path = LineEdit(parent=self.widget)
        self.le_csv_path.setTitle("CSV-Dateipfad")

        self.btn_file_dir = TooltipPushButton("", self.widget)
        self.btn_file_dir.setIcon(QIcon(":/images/utils/add_file.svg"))
        self.btn_file_dir.setIconSize(QSize(20, 20))
        self.btn_file_dir.setFixedSize(32, 32)
        self.btn_file_dir.setToolTip("Relativen oder absoulten Pfad zu einer CSV wählen")
        self.btn_file_dir.clicked.connect(get_file)

        # shape selector
        self.le_pos_selector.edit.setPlaceholderText("Shape Selector: KNZ..")

        # csv column name
        self.le_csv_column_selector = LineEdit(self.widget)
        self.le_csv_column_selector.setTitle("CSV Spalten-Name")

        # add info list
        self.lw_add_info = EditAddCatListView(self.widget, p_parent)
        self.lw_add_info.setFixedSize(self.widget.width(), 200)
        self.masks.append(self.lw_add_info.editor_dialog)

    def _init_layout(self) -> None:
        super(WfkAdditionalInformationCategorization, self)._init_layout()
        self.le_pos_selector.move(0, 9)
        self.le_pos_selector.setFixedWidth(self.widget.width())

        self.le_clipped_layer_name.move(0, self.le_pos_selector.geometry().bottomLeft().y() + 6)
        self.le_clipped_layer_name.setFixedWidth(self.widget.width())

        self.le_tailored_layer_name.move(0, self.le_clipped_layer_name.geometry().bottomLeft().y() + 6)
        self.le_tailored_layer_name.setFixedWidth(self.widget.width())

        self.le_csv_path.move(0, self.le_tailored_layer_name.geometry().bottomLeft().y() + 6)
        self.le_csv_path.setFixedWidth(self.widget.width() - self.btn_file_dir.width() - 6)
        self.btn_file_dir.move(self.le_csv_path.geometry().bottomRight().x() + 6,
                               self.le_csv_path.geometry().topRight().y() + 23)

        # zip file name
        self.le_csv_column_selector.move(0, self.le_csv_path.geometry().bottomLeft().y() + 6)
        self.le_csv_column_selector.setFixedWidth(self.widget.width())

        # additional data list
        self.lw_add_info.move(0, self.le_csv_column_selector.y() + self.le_csv_column_selector.height() + 6)

        # resize
        self.widget.setFixedSize(self.widget.width(), self.lw_add_info.y() +
                                 self.lw_add_info.height() + 6)
        self.widget.adjustSize()

    def create_view(self, p_parent=None):
        super(WfkAdditionalInformationCategorization, self).create_view(p_parent)
        # csv path
        if len(self.additional_info_csv_file) > 0 and self.additional_info_csv_file.endswith('.csv'):
            self.le_csv_path.setText(self.additional_info_csv_file)
            self.le_csv_path.setToolTip(self.additional_info_csv_file)

        # csv column selector
        self.le_csv_column_selector.setText(self.csv_column_selector)

        # add the current data to the list
        for item in self.additional_information_items:
            self.lw_add_info.on_create_new_row(p_data=[item, ""])

    def from_view(self):
        super(WfkAdditionalInformationCategorization, self).from_view()
        # csv path selector
        if self.le_csv_path.text().endswith('.csv'):
            self.additional_info_csv_file = self.le_csv_path.text()
        # column selector
        self.csv_column_selector = self.le_csv_column_selector.text()
        # get the data the user created
        self.additional_information_items.clear()
        model = self.lw_add_info.list_view.model()
        for row in range(model.rowCount()):
            data = model.data(model.index(row, 0), Qt.DisplayRole)
            self.additional_information_items.append(data)

    def run(self, cat_layer):
        # run parent run()
        out_layer = super().run(cat_layer)

        if out_layer is None:
            raise AdditionalInfoCategorizationError("Clipping of layer failed! Bad input parameters!")

        if not self.additional_info_csv_file.endswith('.csv'):
            logger.error("the file is not a *.csv file")
            return out_layer

        # check if path is not absolut
        if not os.path.isabs(self.additional_info_csv_file):
            # if the given root path is empty, take plugin folder
            if len(self.root_path) == 0:
                self.root_path = config.plugin_dir

            # check and join if the root path is not absolut
            if not os.path.isabs(self.root_path):
                project_path = QgsProject.instance().absolutePath()
                if not os.path.exists(project_path):
                    logger.debug("There is no project path! get the plugin path")
                    project_path = config.plugin_dir

                self.root_path = os.path.join(project_path, self.root_path)
                if not os.path.isdir(self.root_path):
                    raise FileNotFoundError("The given root directory does not exist: " + self.root_path)
            elif not os.path.exists(self.root_path):
                raise FileNotFoundError("The given root directory does not exist: " + self.root_path)

            logger.debug(f"Given root path: {self.root_path}")
            logger.debug(f"Given csv path: {self.additional_info_csv_file}")
            # join root and file
            self.additional_info_csv_file = os.path.join(self.root_path, self.additional_info_csv_file)

        if self.additional_info_csv_file == "" or not os.path.exists(self.additional_info_csv_file):
            logger.error("path for 'file' is not given or does not exist")
            raise FileNotFoundError("The given csv does not exist: " + self.additional_info_csv_file)

        if len(self.additional_information_items) == 0:
            logger.warning("There is no additional data to process")
            return out_layer

        logger.debug("Parse the excel file")
        obj_cts = Owner.from_csv_to_dict(file_name=self.additional_info_csv_file,
                                         column_name=self.csv_column_selector)
        logger.debug("loaded " + str(len(obj_cts)) + " additional information objects")
        logger.debug("Add attributes from xml")
        for attr in self.additional_information_items:
            if type(attr) is str:
                logger.debug("Attribute: " + attr)
                try:
                    out_layer.startEditing()
                    pr = out_layer.dataProvider()
                    pr.addAttributes([QgsField(attr, QVariant.String)])
                    out_layer.updateFields()
                    out_layer.commitChanges()
                except Exception as e:
                    logger.warning("Can not add the following attr: " + attr)
                    raise AdditionalInfoCategorizationError(e)

        def check_feature(feat, o, a):
            if hasattr(o, a):
                try:
                    feat.attribute(a)
                    return True
                except KeyError:
                    logger.warning("The feature does not have the following key: " + str(a))
            return False

        try:
            out_layer.startEditing()
            logger.debug("Iterate over features")
            for f in out_layer.getFeatures():
                atr_key = str(f.attribute(self.position_selector))  # KNZ example: '07256600000007______'

                # iterate over the owner dict
                for key, owner_list in obj_cts.items():
                    # check the selector
                    if key == atr_key:
                        # iterate over the additional info list
                        for atr_name in self.additional_information_items:
                            # iterate over the owner list

                            #
                            if len(owner_list) == 1:
                                for owner in owner_list:
                                    # check if the owner object has the member from the AdditionalInformationItems list
                                    if check_feature(feat=f, o=owner, a=atr_name):

                                        try:
                                            attr_value: str = str(getattr(owner, atr_name))
                                        except AttributeError:
                                            logger.warning("Attribute does not exists: " + atr_name)
                                            return out_layer

                                        # if there is nothing in the string
                                        if attr_value == "":
                                            attr_value = "No information"

                                        # set the attribute value
                                        QGisApiWrapper.try_set_attribute_value(p_feature=f,
                                                                               p_attr_name=atr_name,
                                                                               p_attr_new_value=attr_value)
                            elif len(owner_list) > 1:
                                add_information = ""
                                count = 0
                                while count < len(owner_list):
                                    owner = owner_list[count]

                                    if check_feature(feat=f, o=owner, a=atr_name):
                                        try:
                                            attr_value: str = str(getattr(owner, atr_name))
                                        except AttributeError:
                                            logger.warning("Attribute does not exists: " + atr_name)
                                            return out_layer
                                        if attr_value != "":
                                            add_information += attr_value
                                        else:
                                            add_information += "No information"

                                        if count != len(owner_list) - 1:
                                            add_information += ", "

                                    count += 1
                                QGisApiWrapper.try_set_attribute_value(p_feature=f,
                                                                       p_attr_name=atr_name,
                                                                       p_attr_new_value=add_information)
                out_layer.updateFeature(f)
            out_layer.commitChanges()
            logger.warning("Added additional information")
        except Exception as e:
            raise AdditionalInfoCategorizationError(e)
        return out_layer


CategorizationFactory.register("WfkByDistanceCategorization",
                               "Kategorisierung nach Distanz", WfkByDistanceCategorization)
#CategorizationFactory.register("WfkByPositionCategorization",
#                               "Kategorisierung nach Position", WfkByPositionCategorization)
#CategorizationFactory.register("WfkByCentroidCategorization",
#                               "Kategorisierung nach Mittelpunkt", WfkByCentroidCategorization)
#CategorizationFactory.register("WfkUSUDistanceCategorization",
#                               "Kategorisierung nach USU", WfkUSUDistanceCategorization)
#CategorizationFactory.register("WfkSPARQLCategorization",
#                               "Kategorisierung nach Gemeinde", WfkSPARQLCategorization)
#CategorizationFactory.register("WfkByPositionCategorizationWithClipping",
#                               "Kategorisierung nach Überlappung", WfkByPositionCategorizationWithClipping)
CategorizationFactory.register("WfkAdditionalInformationCategorization",
                               "Zusatzinformationen", WfkAdditionalInformationCategorization)
