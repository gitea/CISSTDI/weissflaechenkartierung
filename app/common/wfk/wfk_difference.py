import os.path
import time
from abc import abstractmethod

from PyQt5.QtCore import QSize, Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QLabel

from ...common import str_utils, os_utils
from ...common.auto_wrap import auto_wrap
from ...common.exception import BlackListError, DifferenceError
from ...common.logger import logger
from ...common.qgis import DistanceUnit
from ...common.wfk.csv_owner import Owner
from ...common.wfk.interface import JsonHandle, Construction
from ...common.wfk.wfk_factories import DifferenceFactory
from ...components.buttons.tooltip_button import TooltipPushButton
from ...components.widgets.check_box import TextCheckBox
from ...components.widgets.combo_box import ComboBox
from ...components.widgets.line_edit import LineEdit
from ...components.widgets.spin_box import DoubleSpinBox
from ...components.table_view.table_view_model import USER_ROLE_WHOLE_DATA
from ...view.creation_interface.editable_table_widget import EditBufferTableView

try:
    from ...common.qgis import QGisApiWrapper
    from qgis.core import QgsVectorLayer, QgsFeatureRequest
except (NameError, ImportError):
    pass


class WfkDifference(JsonHandle, Construction):

    @staticmethod
    @abstractmethod
    def translated_name(self) -> str:
        """return name for the ui"""
        return "Basis Puffer"

    def __init__(self):
        # layer to diff
        self.base_layer: 'QgsVectorLayer' = None  # will be set during the wfk

        # diffed layer
        self.diffed_layer: 'QgsVectorLayer' = None

        # buffed layer if
        self.buffed_layer: 'QgsVectorLayer' = None

        # crs
        self.crs: str = ""  # module set that or inside module_dialog.py while instantiating object

    def from_json(self, p_data: dict) -> None:
        """base from json"""
        if isinstance(p_data.get("crs"), str) and len(p_data.get("crs")) > 0:
            self.crs = p_data.get("crs")

    def to_json(self) -> dict:
        """base to json"""
        data = {"type": self.__class__.__name__}
        return data

    @abstractmethod
    def run(self, p_layer_to_diff):
        """standard run or difference"""
        if self.diffed_layer is not None and p_layer_to_diff is not None:
            self.diffed_layer = QGisApiWrapper.diff_from_layer(p_base_layer=p_layer_to_diff,
                                                               p_input_layer=self.diffed_layer,
                                                               p_layer_name=f"{p_layer_to_diff.name()}_diff")
            return self.diffed_layer
        else:
            return p_layer_to_diff


class WfkBufferDifference(WfkDifference):
    def __init__(self):
        super().__init__()
        self.buffer: float = 0.0  # from json / view
        self.distance_unit: str = "m"  # from json / view

    def translated_name(self) -> str:
        """return name for the ui"""
        return "Umring Puffer"

    def from_json(self, p_data: dict) -> None:
        """fill class members from json attributes"""
        super(WfkBufferDifference, self).from_json(p_data)

        if "buffer" in p_data and isinstance(p_data["buffer"], float):
            self.buffer = float(p_data["buffer"])

        # if it's a string and bigger than 0
        if isinstance(p_data.get("distance_unit"), str) and len(p_data.get("distance_unit")) > 0:
            self.distance_unit = p_data.get("distance_unit")

    def to_json(self) -> dict:
        """convert object to json"""
        data = super(WfkBufferDifference, self).to_json()

        if self.buffer >= 0.0:
            data["buffer"] = self.buffer

        if len(self.distance_unit) > 0:
            data["distance_unit"] = self.distance_unit
        return data

    def _init_widgets(self, p_parent=None) -> None:
        """init widgets"""
        super(WfkBufferDifference, self)._init_widgets(p_parent)
        self.dsb_buffer = DoubleSpinBox(self.widget)
        self.dsb_buffer.setValue(3.0)
        self.dsb_buffer.setFixedSize(100, 35)

        self.cb_default_buffer_unit = ComboBox(self.widget)
        self.cb_default_buffer_unit.addItem(DistanceUnit.M.value, DistanceUnit.M)
        self.cb_default_buffer_unit.addItem(DistanceUnit.KM.value, DistanceUnit.KM)
        self.cb_default_buffer_unit.setPlaceHolderText("Einheit auswählen")
        self.cb_default_buffer_unit.setCursor(Qt.PointingHandCursor)

    def _init_layout(self) -> None:
        """init layout"""
        self.dsb_buffer.move(0, int(self.cb_default_buffer_unit.get_contents_rect().height() // 2 + 1))
        self.dsb_buffer.setFixedSize(self.widget.width() - self.cb_default_buffer_unit.width() - 6,
                                     self.dsb_buffer.height())

        # combo box right from
        self.cb_default_buffer_unit.move(self.dsb_buffer.geometry().bottomRight().x() + 6, 0)

        # resize
        self.widget.setFixedSize(self.widget.width(), self.dsb_buffer.y() + self.dsb_buffer.height() + 6)
        self.widget.adjustSize()

    def create_view(self, p_parent=None) -> None:
        """create view"""
        # recreate the widgets and their layout
        super(WfkBufferDifference, self).create_view(p_parent)
        # set the ui
        self.dsb_buffer.setValue(float(self.buffer))
        self.cb_default_buffer_unit.setCurrentText(self.distance_unit)
        # focus line edit
        self.dsb_buffer.setFocus()

    def from_view(self):
        """save the user input from ui"""
        self.buffer = self.dsb_buffer.value()
        self.distance_unit = self.cb_default_buffer_unit.currentText()

    def run(self, p_layer_to_diff):
        """ run the buffer difference """
        logger.debug(f"distance unit: {str(self.distance_unit)}")
        logger.debug(f"given buffer: {str(self.buffer)}")

        if p_layer_to_diff is None:
            raise DifferenceError("Can not process given layer! Check console!")

        if self.buffer > .0:
            # compute the buffer and calculate the right unit
            # the api uses meters
            buffer: float = self.buffer
            if self.distance_unit == DistanceUnit.KM.value:
                buffer *= 1000
                logger.debug(f"buffer from km in m: {str(buffer)}")

            self.diffed_layer = QGisApiWrapper.buffer_layer(
                p_input_layer=self.base_layer, p_buffer=buffer,
                p_layer_name=f"{self.base_layer.name()}_{time.time()}"
            )
            # copy layer
            self.buffed_layer = self.diffed_layer.materialize(
                QgsFeatureRequest().setFilterFids(self.diffed_layer.allFeatureIds())
            )
        else:
            self.diffed_layer = self.base_layer
            self.buffed_layer = self.base_layer

        return super(WfkBufferDifference, self).run(p_layer_to_diff)


class WfkSelectiveBufferDifference(WfkDifference):
    def __init__(self):
        super().__init__()
        self.selector: str = "id"  # from json / view
        self.buffer_list: list = []  # from json / view
        self.type_list: list = []  # from json / view
        self.distance_unit: str = "m"  # from json / view

    def translated_name(self) -> str:
        """return name for the ui"""
        return "Selektiver Puffer"

    def from_json(self, p_data: dict) -> None:
        """from json"""
        super(WfkSelectiveBufferDifference, self).from_json(p_data)
        if "selector" in p_data and isinstance(p_data["selector"], str):
            self.selector = p_data["selector"]
        if "buffer_list" in p_data and isinstance(p_data["buffer_list"], list):
            self.buffer_list = p_data["buffer_list"]
        if "type_list" in p_data and isinstance(p_data["type_list"], list):
            self.type_list = p_data["type_list"]

        # if it's a string and bigger than 0
        if isinstance(p_data.get("distance_unit"), str) and len(p_data.get("distance_unit")) > 0:
            self.distance_unit = p_data.get("distance_unit")

    def to_json(self) -> dict:
        """to json"""
        data = super(WfkSelectiveBufferDifference, self).to_json()
        data["selector"] = self.selector
        data["buffer_list"] = self.buffer_list
        data["type_list"] = self.type_list

        if len(self.distance_unit) > 0:
            data["distance_unit"] = self.distance_unit
        return data

    def _init_widgets(self, p_parent=None) -> None:
        """init widgets"""
        super(WfkSelectiveBufferDifference, self)._init_widgets(p_parent)

        self.le_selector = LineEdit(self.widget)
        self.le_selector.setTitle("Selektion Spalte")
        self.le_selector.resize(100, 50)
        self.le_selector.setToolTip("Das Feld muss in der Eingabequelle vorhanden sein!")

        # direction unit
        self.cb_direction_unit = ComboBox(self.widget)
        self.cb_direction_unit.setTitle("Einheit")
        self.cb_direction_unit.resize(100, 50)
        self.cb_direction_unit.addItem(DistanceUnit.M.value, DistanceUnit.M)
        self.cb_direction_unit.addItem(DistanceUnit.KM.value, DistanceUnit.KM)

        self.tw_buffer_editable = EditBufferTableView(self.widget, p_parent)
        self.tw_buffer_editable.setFixedSize(self.widget.width(), 236)
        self.masks.append(self.tw_buffer_editable.editor_dialog)

    def _init_layout(self) -> None:
        """init layout"""
        super(WfkSelectiveBufferDifference, self)._init_layout()
        self.le_selector.move(0, 0)
        self.le_selector.setFixedSize(self.widget.width(), self.le_selector.height())

        # cb units
        self.cb_direction_unit.move(0, self.le_selector.geometry().bottomLeft().y() + 6)
        self.cb_direction_unit.setFixedSize(self.widget.width(), self.cb_direction_unit.height())

        # table view object
        self.tw_buffer_editable.move(0, self.cb_direction_unit.geometry().bottomLeft().y() + 6)

        # resize
        self.widget.setFixedSize(self.widget.width(),
                                 self.tw_buffer_editable.y() + self.tw_buffer_editable.height() + 6)

    def create_view(self, p_parent=None):
        """Create view"""
        super(WfkSelectiveBufferDifference, self).create_view(p_parent)
        # init the ui with local members
        self.le_selector.setText(self.selector)
        # direction unit
        self.cb_direction_unit.setCurrentText(self.distance_unit)
        # table view
        if len(self.type_list) > 0 and len(self.buffer_list) > 0:
            for _type, buffer in zip(self.type_list, self.buffer_list):
                self.tw_buffer_editable.table_view.append_row(p_row=[_type, buffer])
        self.tw_buffer_editable.editor_dialog.adjust_widget_geometry()

    def from_view(self):
        """save data from view"""
        # get the data from the view
        self.selector = self.le_selector.text()
        # direction unit
        self.distance_unit = self.cb_direction_unit.currentText()
        # get the table view data
        # clear the lists even if there is no data
        self.type_list.clear()
        self.buffer_list.clear()
        model = self.tw_buffer_editable.table_view.model()
        if model.rowCount() > 0:
            data = model.data(None, USER_ROLE_WHOLE_DATA)
            for row in data:
                self.type_list.append(row[0])
                self.buffer_list.append(row[1])

    def run(self, p_layer_to_diff):
        """ run the selective buffer """
        if len(self.selector) == 0:
            raise DifferenceError("There is no selector given!")

        if len(self.type_list) == 0:
            raise DifferenceError("There are no types given!")

        if len(self.buffer_list) == 0:
            raise DifferenceError("There are no buffer given!")

        if len(self.type_list) > len(self.buffer_list):
            raise DifferenceError("There are more types than buffer given!")

        if len(self.buffer_list) > len(self.type_list):
            raise DifferenceError("There are more buffer than types given!")

        # example
        # buffer list:
        # 1000.0
        # 5.0
        #
        # type_list:
        # AX_FlaecheBesondererFunktionalerPraegung
        # AX_FlaecheGemischterNutzung
        buffer_list: list = []
        for i, buffer in enumerate(self.buffer_list):
            if self.distance_unit == DistanceUnit.KM.value:
                buffer_list.append(buffer * 1000)
                logger.debug(f"buffer {str(buffer)} to {str(buffer * 1000)}")
            else:
                buffer_list.append(buffer)

        # check if the buffer and type list have the same length
        if len(self.buffer_list) == len(self.type_list):
            self.diffed_layer = QGisApiWrapper.selective_buffer(
                p_input_layer=self.base_layer, p_crs=self.crs, p_type_list=self.type_list,
                p_buffer_list=buffer_list, p_attribute=self.selector,
                p_layer_name=f"sel_{self.base_layer.name()}_{time.time()}"
            )
            # copy layer
            self.buffed_layer = self.diffed_layer.materialize(
                QgsFeatureRequest().setFilterFids(self.diffed_layer.allFeatureIds())
            )
            logger.debug("Created SelectiveBufferDifference")
        else:
            logger.error("The type or buffer list has not the same length as the other!")

        return super(WfkSelectiveBufferDifference, self).run(p_layer_to_diff)


class WfkBlacklistingDifference(WfkDifference):

    def __init__(self):
        super().__init__()
        self.save_blacklisted_layer: bool = False
        self.csv_file_path: str = ""
        self.shape_selector: str = ""
        self.csv_selector: str = ""
        self.csv_black_list_selector: str = ""

    def translated_name(self) -> str:
        """return name for the ui"""
        return "Ausschlussbildung"

    def from_json(self, p_data: dict) -> None:
        super(WfkBlacklistingDifference, self).from_json(p_data)
        if "save_blacklisted_layer" in p_data and isinstance(p_data["save_blacklisted_layer"], bool):
            self.save_blacklisted_layer = p_data["save_blacklisted_layer"]
        if "csv_file_path" in p_data and isinstance(p_data["csv_file_path"], str):
            self.csv_file_path = p_data["csv_file_path"]
        if "shape_selector" in p_data and isinstance(p_data["shape_selector"], str):
            self.shape_selector = p_data["shape_selector"]
        if "csv_selector" in p_data and isinstance(p_data["csv_selector"], str):
            self.csv_selector = p_data["csv_selector"]
        if "csv_black_list_selector" in p_data and isinstance(p_data["csv_black_list_selector"], str):
            self.csv_black_list_selector = p_data["csv_black_list_selector"]

    def to_json(self) -> dict:
        data = super(WfkBlacklistingDifference, self).to_json()
        data["save_blacklisted_layer"] = self.save_blacklisted_layer
        data["csv_file_path"] = self.csv_file_path
        data["shape_selector"] = self.shape_selector
        data["csv_selector"] = self.csv_selector
        data["csv_black_list_selector"] = self.csv_black_list_selector
        return data

    def run(self, p_layer_to_diff):
        """ run the blacklist algorithm """
        if not os.path.exists(self.csv_file_path):
            raise BlackListError("The csv file path does not exist!")

        if len(self.shape_selector) == 0:
            raise BlackListError("There is no shape_selector!")

        if len(self.csv_selector) == 0:
            raise BlackListError("There is no csv_selector!")

        if len(self.csv_black_list_selector) == 0:
            raise BlackListError("There is no csv_black_list_selector!")

        # load the csv data
        owner_dict: dict = Owner.from_csv_to_dict(file_name=self.csv_file_path,
                                                  column_name=self.csv_selector)

        # get attribute
        def get_attribute(o: Owner, a_n: str):
            try:
                a = getattr(o, a_n)
                return a
            except AttributeError:
                logger.error("AttributeError while blacklisting")
                return None

        # iterate over owner
        count = 0
        expression = ""
        for key, owner_list in owner_dict.items():
            # iterate over owner
            for owner in owner_list:
                attr_value = get_attribute(o=owner, a_n=self.csv_black_list_selector)
                # check if blacklist attribute is one or zero
                if attr_value == "1":
                    if count == 0:
                        expression += """\"""" + self.shape_selector + """\" = '""" + key + """' """
                        count += 1
                    else:
                        expression += """or \"""" + self.shape_selector + """\" = '""" + key + """' """

        logger.debug("Blacklisting expression: " + expression)

        if expression != "":

            # run qgis function
            logger.debug("Start extract_by_expression")
            self.diffed_layer = QGisApiWrapper.extract_by_expression(p_input_layer=self.base_layer,
                                                                     p_expression=expression)
            self.diffed_layer.setName("Blacklisted")

            # get names which will not be deleted
            not_del_attributes = []
            for field_cat in p_layer_to_diff.dataProvider().fields():
                not_del_attributes.append(field_cat.name())

            # delete attributes except selector
            del_attributes_pos = []
            count = 0
            for field in self.diffed_layer.dataProvider().fields():
                f_n = field.name()
                if f_n not in not_del_attributes and \
                        f_n != self.shape_selector and \
                        f_n != self.csv_selector and \
                        f_n != self.csv_black_list_selector:
                    del_attributes_pos.append(count)
                count += 1
            self.diffed_layer.dataProvider().deleteAttributes(del_attributes_pos)
            self.diffed_layer.updateFields()

            # copy layer
            self.buffed_layer = self.diffed_layer.materialize(
                QgsFeatureRequest().setFilterFids(self.diffed_layer.allFeatureIds())
            )

            logger.debug("finished extract_by_expression ")
        else:
            self.save_blacklisted_layer = False

        return super().run(p_layer_to_diff)

    def _init_widgets(self, p_parent=None) -> None:
        super(WfkBlacklistingDifference, self)._init_widgets(p_parent)

        def get_file():
            result: str = os_utils.get_open_file_name(p_caption="CSV-Datei auswählen",
                                                 p_filter="CSV Dateien (*.csv)",
                                                 p_file=self.le_csv_file_path.text())
            if result:
                self.le_csv_file_path.setText(result)

        self.btn_file_dir = TooltipPushButton("", self.widget)
        self.btn_file_dir.setIcon(QIcon(":/images/utils/add_file.svg"))
        self.btn_file_dir.setIconSize(QSize(20, 20))
        self.btn_file_dir.setFixedSize(32, 32)
        self.btn_file_dir.setToolTip("Relativen oder absoulten Pfad zu einer Shapedatei wählen")
        self.btn_file_dir.clicked.connect(get_file)

        self.le_csv_file_path = LineEdit(parent=self.widget)
        self.le_csv_file_path.setFixedHeight(50)
        self.le_csv_file_path.setTitle("Eigentuemer CSV-Datei")

        self.le_csv_selector = LineEdit(self.widget)
        self.le_csv_selector.setFixedHeight(50)
        self.le_csv_selector.setTitle("CSV-Spalte")

        self.le_csv_bl_selector = LineEdit(self.widget)
        self.le_csv_bl_selector.setFixedHeight(50)
        self.le_csv_bl_selector.setTitle("BL CSV-Spalte")
        self.le_csv_bl_selector.setToolTip("CSV-Spalten name für die Blacklistung")

        self.le_shape_selector = LineEdit(self.widget)
        self.le_shape_selector.setFixedHeight(50)
        self.le_shape_selector.setTitle("Shape Selector")

        self.cb_save_bl = TextCheckBox(self.widget)
        self.cb_save_bl.setText("Blacklist-Layer speichern und in QGIS anzeigen")

    def _init_layout(self) -> None:
        super(WfkBlacklistingDifference, self)._init_layout()
        self.le_csv_file_path.move(0, 0)
        self.le_csv_file_path.setFixedSize(self.widget.width() - self.btn_file_dir.width() - 6, 50)
        self.btn_file_dir.move(self.le_csv_file_path.geometry().bottomRight().x() + 6, 15)

        # csv selector
        self.le_csv_selector.move(0, self.le_csv_file_path.geometry().bottomLeft().y() + 6)
        self.le_csv_selector.setFixedSize(self.widget.width(), self.le_csv_selector.height())

        # csv blacklist column selector
        self.le_csv_bl_selector.move(0, self.le_csv_selector.y() + self.le_csv_selector.height() + 6)
        self.le_csv_bl_selector.setFixedSize(self.widget.width(), self.le_csv_bl_selector.height())

        # zip file name
        self.le_shape_selector.move(0, self.le_csv_bl_selector.y() + self.le_csv_bl_selector.height() + 6)
        self.le_shape_selector.setFixedSize(self.widget.width(), self.le_shape_selector.height())

        # checkbox
        self.cb_save_bl.move(0, self.le_shape_selector.y() + self.le_shape_selector.height() + 6)

        # resize
        self.widget.setFixedSize(self.widget.width(), self.cb_save_bl.y() + self.cb_save_bl.height() + 6)
        self.widget.adjustSize()

    def create_view(self, p_parent=None):
        super(WfkBlacklistingDifference, self).create_view(p_parent)
        # file path csv
        if len(self.csv_file_path) > 0:
            self.le_csv_file_path.setText(self.csv_file_path)

        # sub path
        self.le_csv_selector.setText(self.csv_selector)
        # download file name
        self.le_csv_bl_selector.setText(self.csv_black_list_selector)
        # zip file name
        self.le_shape_selector.setText(self.shape_selector)
        # save blacklist
        self.cb_save_bl.setChecked(self.save_blacklisted_layer)
        self.le_csv_selector.setFocus()

    def from_view(self):
        super(WfkBlacklistingDifference, self).from_view()
        # join tooltip and text -> path and file name
        self.csv_file_path = os.path.join(self.le_csv_file_path.toolTip(), self.le_csv_file_path.text())
        self.csv_selector = self.le_csv_selector.text()
        self.csv_black_list_selector = self.le_csv_bl_selector.text()
        self.shape_selector = self.le_shape_selector.text()
        self.save_blacklisted_layer = self.cb_save_bl.isChecked()


class WfkTranslatedDifference(WfkDifference):
    def __init__(self):
        super().__init__()

        # buffer directions
        self.west_buffer: float = 0.0  # from json / view
        self.north_buffer: float = 0.0  # from json / view
        self.east_buffer: float = 0.0  # from json / view
        self.south_buffer: float = 0.0  # from json / view

        self.distance_unit: str = "m"  # from json / view

    def translated_name(self) -> str:
        """return name for the ui"""
        return "Richtungsabhängiger Puffer"

    def from_json(self, p_data: dict) -> None:
        """fill class members from json attributes"""
        super(WfkTranslatedDifference, self).from_json(p_data)

        # if it's a string and bigger than 0
        if isinstance(p_data.get("distance_unit"), str) and len(p_data.get("distance_unit")) > 0:
            self.distance_unit = p_data.get("distance_unit")
        # directions
        if isinstance(p_data.get("west_buffer"), float):
            self.west_buffer = p_data.get("west_buffer")

        if isinstance(p_data.get("north_buffer"), float):
            self.north_buffer = p_data.get("north_buffer")

        if isinstance(p_data.get("east_buffer"), float):
            self.east_buffer = p_data.get("east_buffer")

        if isinstance(p_data.get("south_buffer"), float):
            self.south_buffer = p_data.get("south_buffer")

    def to_json(self) -> dict:
        """convert object to json"""
        data = super(WfkTranslatedDifference, self).to_json()
        # direction unit
        if len(self.distance_unit) > 0:
            data["distance_unit"] = self.distance_unit
        # directions
        data["west_buffer"] = self.west_buffer
        data["north_buffer"] = self.north_buffer
        data["east_buffer"] = self.east_buffer
        data["south_buffer"] = self.south_buffer
        return data

    def _init_widgets(self, p_parent=None) -> None:
        """init widgets"""
        super(WfkTranslatedDifference, self)._init_widgets(p_parent)
        # direction unit
        self.cb_direction_unit = ComboBox(self.widget)
        self.cb_direction_unit.setFixedHeight(50)
        self.cb_direction_unit.setTitle("Einheit")
        self.cb_direction_unit.addItem(DistanceUnit.M.value, DistanceUnit.M)
        self.cb_direction_unit.addItem(DistanceUnit.KM.value, DistanceUnit.KM)

        # direction buffer
        self.l_n = QLabel("Norden:", self.widget)
        self.l_n.adjustSize()
        self.dsb_north_buffer = DoubleSpinBox(self.widget)
        self.dsb_north_buffer.setToolTip("Buffer für Norden")
        self.dsb_north_buffer.setValue(3.0)
        self.dsb_north_buffer.setFixedHeight(45)

        self.l_e = QLabel("Osten:", self.widget)
        self.l_e.adjustSize()
        self.dsb_east_buffer = DoubleSpinBox(self.widget)
        self.dsb_east_buffer.setToolTip("Buffer für Osten")
        self.dsb_east_buffer.setValue(3.0)
        self.dsb_east_buffer.setFixedHeight(45)

        self.l_s = QLabel("Süden:", self.widget)
        self.l_s.adjustSize()
        self.dsb_south_buffer = DoubleSpinBox(self.widget)
        self.dsb_south_buffer.setToolTip("Buffer für Süden")
        self.dsb_south_buffer.setValue(3.0)
        self.dsb_south_buffer.setFixedHeight(45)

        self.l_w = QLabel("Westen:", self.widget)
        self.l_w.adjustSize()
        self.dsb_west_buffer = DoubleSpinBox(self.widget)
        self.dsb_west_buffer.setToolTip("Buffer für Westen")
        self.dsb_west_buffer.setValue(3.0)
        self.dsb_west_buffer.setFixedHeight(45)

    def _init_layout(self) -> None:
        """init layout"""
        # metrics
        labels: list = [self.l_n, self.l_e, self.l_s, self.l_w]
        x = max(self.widget.fontMetrics().width(i.text()) for i in labels) + 15
        # direction unit
        self.cb_direction_unit.move(0, 9)
        self.cb_direction_unit.setFixedSize(self.widget.width(), self.cb_direction_unit.height())

        # north
        self.dsb_north_buffer.move(x, self.cb_direction_unit.geometry().bottomLeft().y() + 6)
        self.l_n.move(0, int(self.dsb_north_buffer.height() / 2) -
                      int(self.l_n.height() / 2) + self.dsb_north_buffer.y())
        self.dsb_north_buffer.setFixedSize(self.widget.width() - x, self.dsb_north_buffer.height())

        # east
        self.dsb_east_buffer.move(x, self.dsb_north_buffer.geometry().bottomLeft().y() + 6)
        self.l_e.move(0, int(self.dsb_east_buffer.height() / 2) -
                      int(self.l_e.height() / 2) + self.dsb_east_buffer.y())
        self.dsb_east_buffer.setFixedSize(self.widget.width() - x, self.dsb_east_buffer.height())

        # south
        self.dsb_south_buffer.move(x, self.dsb_east_buffer.geometry().bottomLeft().y() + 6)
        self.l_s.move(0, int(self.dsb_south_buffer.height() / 2) -
                      int(self.l_s.height() / 2) + self.dsb_south_buffer.y())
        self.dsb_south_buffer.setFixedSize(self.widget.width() - x, self.dsb_south_buffer.height())

        # west
        self.dsb_west_buffer.move(x, self.dsb_south_buffer.geometry().bottomLeft().y() + 6)
        self.l_w.move(0, int(self.dsb_west_buffer.height() / 2) -
                      int(self.l_w.height() / 2) + self.dsb_west_buffer.y())
        self.dsb_west_buffer.setFixedSize(self.widget.width() - x, self.dsb_west_buffer.height())

        # resize
        self.widget.setFixedSize(self.widget.width(), self.dsb_west_buffer.geometry().bottomLeft().y() + 6)
        self.widget.adjustSize()

    def create_view(self, p_parent=None) -> None:
        """create view"""
        # recreate the widgets and their layout
        super(WfkTranslatedDifference, self).create_view(p_parent)
        # direction unit
        self.cb_direction_unit.setCurrentText(self.distance_unit)
        # direction buffer
        self.dsb_north_buffer.setValue(float(self.north_buffer))
        self.dsb_east_buffer.setValue(float(self.east_buffer))
        self.dsb_south_buffer.setValue(float(self.south_buffer))
        self.dsb_west_buffer.setValue(float(self.west_buffer))
        # focus into first spin box
        self.dsb_north_buffer.setFocus()

    def from_view(self):
        """save the user input from ui"""
        # direction unit
        self.distance_unit = self.cb_direction_unit.currentText()
        # direction buffer
        self.north_buffer = self.dsb_north_buffer.value()
        self.east_buffer = self.dsb_east_buffer.value()
        self.south_buffer = self.dsb_south_buffer.value()
        self.west_buffer = self.dsb_west_buffer.value()

    def run(self, p_layer_to_diff):
        """ run the translated buffer difference """
        if p_layer_to_diff is None:
            logger.warning("The given buffer is none!")
            return p_layer_to_diff

        if self.base_layer is None:
            logger.warning("The given base layer is none!")
            return p_layer_to_diff

        logger.debug(f"distance unit: {str(self.distance_unit)}")
        logger.debug(f"given north buffer: {str(self.north_buffer)}")
        logger.debug(f"given east buffer: {str(self.east_buffer)}")
        logger.debug(f"given south buffer: {str(self.south_buffer)}")
        logger.debug(f"given west buffer: {str(self.west_buffer)}")

        north_buffer: float = self.north_buffer
        east_buffer: float = self.east_buffer
        south_buffer: float = self.south_buffer
        west_buffer: float = self.west_buffer
        # compute the buffer and calculate the right unit
        # the api uses meters
        if self.distance_unit == DistanceUnit.KM.value:
            north_buffer *= 1000
            east_buffer *= 1000
            south_buffer *= 1000
            west_buffer *= 1000
            logger.debug(f"buffer north from km in m: {str(north_buffer)}")
            logger.debug(f"buffer east from km in m: {str(east_buffer)}")
            logger.debug(f"buffer south from km in m: {str(south_buffer)}")
            logger.debug(f"buffer west from km in m: {str(west_buffer)}")

        # compute buffer
        # 1 translate the given layer to north x coordinates are 0 y is north buffer
        north_translated = self.base_layer
        east_translated = self.base_layer
        south_translated = self.base_layer
        west_translated = self.base_layer
        try:
            logger.debug("Translate...")
            if north_buffer > 0.0:
                logger.debug(f"Buff north")
                north_translated = QGisApiWrapper.translate_geometry(p_input_layer=self.base_layer, p_delta_x=0.0,
                                                                     p_delta_y=north_buffer, p_delta_m=0, p_delta_z=0,
                                                                     p_layer_name="north_translated")
                if north_translated is None:
                    raise DifferenceError("Translate to north failed!")

            if east_buffer > 0.0:
                # 2 translate to east plus in x-axis
                logger.debug(f"Buff east")
                east_translated = QGisApiWrapper.translate_geometry(p_input_layer=self.base_layer,
                                                                    p_delta_x=east_buffer,
                                                                    p_delta_y=0, p_delta_m=0, p_delta_z=0,
                                                                    p_layer_name="east_translated")
                if east_translated is None:
                    raise DifferenceError("Translate to east failed!")

            if south_buffer > 0.0:
                # 3 south is minus on y-axis
                logger.debug(f"Buff south")
                south_translated = QGisApiWrapper.translate_geometry(p_input_layer=self.base_layer, p_delta_x=0.0,
                                                                     p_delta_y=-south_buffer, p_delta_m=0, p_delta_z=0,
                                                                     p_layer_name="south_translated")
                if south_translated is None:
                    raise DifferenceError("Translate to south failed!")

            if west_buffer > 0.0:
                # 4 south is minus on x-axis
                logger.debug(f"Buff west")
                west_translated = QGisApiWrapper.translate_geometry(p_input_layer=self.base_layer,
                                                                    p_delta_x=-west_buffer,
                                                                    p_delta_y=0, p_delta_m=0, p_delta_z=0,
                                                                    p_layer_name="west_translated")
                if west_translated is None:
                    raise DifferenceError("Translate to west failed!")
        except Exception as e:
            raise DifferenceError(e)
            # ###########################################
            # unite all layers to one layer
        logger.debug("Unite all translated layers...")

        north_united = self.base_layer
        east_united = self.base_layer
        south_united = self.base_layer
        west_united = self.base_layer

        if north_translated is not None:
            north_united = QGisApiWrapper.union_by_layer(p_input_layer=self.base_layer,
                                                         p_overlay_layer=north_translated,
                                                         p_layer_name="n")

            if north_united is None:
                raise DifferenceError("Can not unite north translated layer!")

        if east_translated is not None:
            east_united = QGisApiWrapper.union_by_layer(p_input_layer=self.base_layer,
                                                        p_overlay_layer=east_translated,
                                                        p_layer_name="e")

            if east_united is None:
                raise DifferenceError("Can not unite east translated layer!")

        if south_translated is not None:
            south_united = QGisApiWrapper.union_by_layer(p_input_layer=self.base_layer,
                                                         p_overlay_layer=south_translated,
                                                         p_layer_name="s")

            if south_united is None:
                raise DifferenceError("Can not unite south translated layer!")

        if west_translated is not None:
            west_united = QGisApiWrapper.union_by_layer(p_input_layer=self.base_layer,
                                                        p_overlay_layer=west_translated,
                                                        p_layer_name="w")

            if west_united is None:
                raise DifferenceError("Can not unite south translated layer!")

        # ###################################################
        # unite all pieces
        north_east_united = self.base_layer
        west_south_united = self.base_layer

        logger.debug("Unite north and east layers...")
        if north_united is not None and east_united is not None:
            north_east_united = QGisApiWrapper.union_by_layer(p_input_layer=north_united,
                                                              p_overlay_layer=east_united,
                                                              p_layer_name="ne")

            if north_east_united is None:
                raise DifferenceError("Can not unite east translated layer!")

        logger.debug("unite south and west layers...")
        if north_united is not None and south_united is not None:
            west_south_united = QGisApiWrapper.union_by_layer(p_input_layer=south_united,
                                                              p_overlay_layer=west_united,
                                                              p_layer_name="sw")

            if west_south_united is None:
                raise DifferenceError("Can not unite east translated layer!")

        # ###################################################
        # unite all pieces
        logger.debug("Unite north, east, south and west layers...")
        diffed_layer = QGisApiWrapper.union_by_layer(p_input_layer=north_east_united,
                                                     p_overlay_layer=west_south_united,
                                                     p_layer_name="nesw")
        if diffed_layer is None:
            raise DifferenceError("Can not unite translated layers!")

        self.diffed_layer = diffed_layer
        # copy layer
        self.buffed_layer = self.diffed_layer.materialize(
            QgsFeatureRequest().setFilterFids(self.diffed_layer.allFeatureIds())
        )
        # get the difference between the base and input layer, input layer is often the wfk layer
        result_layer = super(WfkTranslatedDifference, self).run(p_layer_to_diff)
        if result_layer is None:
            raise DifferenceError("Richtungsabhängiger Puffer konnte nicht erzeugt werden! (Geometriefehler)")
        return result_layer


class WfkFinancialSupportedDifference(WfkDifference):

    def __init__(self):
        super().__init__()
        self.min_buffer: float = 0.0  # from json / view
        self.max_buffer: float = 0.0  # from json / view
        self.min_distance_unit: str = "m"  # from json / view
        self.max_distance_unit: str = "m"  # from json / view

    def translated_name(self) -> str:
        """return name for the ui"""
        return "Min/Max Puffer"

    def from_json(self, p_data: dict) -> None:
        """fill class members from json attributes"""
        super(WfkFinancialSupportedDifference, self).from_json(p_data)

        if isinstance(p_data.get('min_buffer', None), float):
            self.min_buffer = p_data.get('min_buffer')

        if isinstance(p_data.get('max_buffer', None), float):
            self.max_buffer = p_data.get('max_buffer')

        # if it's a string and bigger than 0
        if isinstance(p_data.get("min_distance_unit", None), str) and len(p_data.get("min_distance_unit")) > 0:
            self.min_distance_unit = p_data.get("min_distance_unit")

        if isinstance(p_data.get("max_distance_unit", None), str) and len(p_data.get("max_distance_unit")) > 0:
            self.max_distance_unit = p_data.get("max_distance_unit")

    def to_json(self) -> dict:
        """convert object to json"""
        data = super(WfkFinancialSupportedDifference, self).to_json()

        if self.min_buffer >= 0.0:
            data["min_buffer"] = self.min_buffer

        if self.max_buffer >= 0.0:
            data["max_buffer"] = self.max_buffer

        if len(self.min_distance_unit) > 0:
            data["min_distance_unit"] = self.min_distance_unit

        if len(self.max_distance_unit) > 0:
            data["max_distance_unit"] = self.max_distance_unit
        return data

    def _init_widgets(self, p_parent=None) -> None:
        """init widgets"""
        super(WfkFinancialSupportedDifference, self)._init_widgets(p_parent)
        self.label = QLabel(
            str_utils.tooltip(auto_wrap("Mit dieser Funktion werden die erzeugten Weißflächen mit dem erzeugtem  "
                                        "Puffer  verschnitten, um so die Potentialflächen innerhalb\n der "
                                        "gesuchten EEG-Flächenkulisse zu identifizieren.\n  ", 70)[0],
                              "EEG-Verschneidung innerhalb eines Puffers (Min/Max)"),
            self.widget)

        self.cb_min_buffer_unit = ComboBox(self.widget)
        self.cb_min_buffer_unit.setTitle("Min Buffer")
        self.cb_min_buffer_unit.addItem(DistanceUnit.M.value, DistanceUnit.M)
        self.cb_min_buffer_unit.addItem(DistanceUnit.KM.value, DistanceUnit.KM)

        self.cb_max_buffer_unit = ComboBox(self.widget)
        self.cb_max_buffer_unit.setTitle("Max Buffer")
        self.cb_max_buffer_unit.addItem(DistanceUnit.M.value, DistanceUnit.M)
        self.cb_max_buffer_unit.addItem(DistanceUnit.KM.value, DistanceUnit.KM)

        self.dsb_min_buffer = DoubleSpinBox(self.widget)
        self.dsb_min_buffer.setFixedHeight(self.cb_min_buffer_unit.get_contents_rect().toRect().height())

        self.dsb_max_buffer = DoubleSpinBox(self.widget)
        self.dsb_max_buffer.setFixedHeight(self.cb_min_buffer_unit.get_contents_rect().toRect().height())


    def _init_layout(self) -> None:
        """init layout"""
        # info label
        self.label.move(0, 9)
        self.label.adjustSize()

        # min buffer
        self.dsb_min_buffer.move(0, self.label.geometry().bottomLeft().y())
        self.dsb_min_buffer.setFixedSize(self.widget.width() - self.cb_min_buffer_unit.width() - 6,
                                         self.dsb_min_buffer.height())


        self.cb_min_buffer_unit.move(
            self.dsb_min_buffer.geometry().bottomRight().x() + 6,
            self.dsb_min_buffer.geometry().topRight().y() - (
                self.cb_min_buffer_unit.height() - self.cb_min_buffer_unit.get_contents_rect().toRect().height()
            )
        )

        # max buffer
        self.dsb_max_buffer.move(0, self.dsb_min_buffer.geometry().bottomLeft().y() + 10)
        self.dsb_max_buffer.setFixedSize(self.widget.width() - self.cb_max_buffer_unit.width() - 6,
                                         self.dsb_max_buffer.height())

        self.cb_max_buffer_unit.move(
            self.dsb_max_buffer.geometry().bottomRight().x() + 6,
            self.dsb_max_buffer.geometry().topRight().y() - (
                    self.cb_max_buffer_unit.height() - self.cb_max_buffer_unit.get_contents_rect().toRect().height()
            )
        )

        # resize
        self.widget.setFixedSize(self.widget.width(), self.dsb_max_buffer.y() + self.dsb_max_buffer.height() + 6)
        self.widget.adjustSize()

    def create_view(self, p_parent=None) -> None:
        """create view"""
        # recreate the widgets and their layout
        super(WfkFinancialSupportedDifference, self).create_view(p_parent)
        # set the ui
        self.dsb_max_buffer.setValue(float(self.max_buffer))
        self.dsb_min_buffer.setValue(float(self.min_buffer))
        self.cb_min_buffer_unit.setCurrentText(self.min_distance_unit)
        self.cb_max_buffer_unit.setCurrentText(self.max_distance_unit)

        # focus line edit
        self.dsb_min_buffer.setFocus()

    def from_view(self):
        """save the user input from ui"""
        self.min_buffer = self.dsb_min_buffer.value()
        self.max_buffer = self.dsb_max_buffer.value()
        self.min_distance_unit = self.cb_min_buffer_unit.currentText()
        self.max_distance_unit = self.cb_max_buffer_unit.currentText()

    def run(self, p_layer_to_diff):
        """run the buffer difference"""
        logger.debug(f"min distance unit: {str(self.min_distance_unit)}")
        logger.debug(f"max distance unit: {str(self.max_distance_unit)}")
        logger.debug(f"given min buffer: {str(self.min_buffer)}")
        logger.debug(f"given max buffer: {str(self.max_buffer)}")
        # compute the buffer and calculate the right unit
        # the api uses meters
        min_buffer: float = self.min_buffer
        if self.min_distance_unit == DistanceUnit.KM.value:
            min_buffer *= 1000
            logger.debug(f"buffer min from km in m: {str(min_buffer)}")

        max_buffer: float = self.max_buffer
        if self.min_distance_unit == DistanceUnit.KM.value:
            max_buffer *= 1000
            logger.debug(f"buffer max from km in m: {str(max_buffer)}")

        # buffer the given base layer with the min and max buffer
        min_diffed_layer = QGisApiWrapper.buffer_layer(p_input_layer=self.base_layer,
                                                       p_buffer=min_buffer,
                                                       p_layer_name=f"min{self.base_layer.name()}_{time.time()}"
                                                       )
        # buffer with max buffer
        max_diffed_layer = QGisApiWrapper.buffer_layer(p_input_layer=self.base_layer,
                                                       p_buffer=max_buffer,
                                                       p_layer_name=f"max{self.base_layer.name()}_{time.time()}"
                                                       )

        if min_diffed_layer is None:
            raise DifferenceError("The given min buffed layer is None!")

        if max_diffed_layer is None:
            raise DifferenceError("The given max buffed layer is None!")

        # get difference between max and min layer
        diffed_layer = QGisApiWrapper.diff_from_layer(p_base_layer=max_diffed_layer,
                                                           p_input_layer=min_diffed_layer,
                                                      p_layer_name=f"max_diff{self.base_layer.name()}_{time.time()}"
                                                      )

        dissolved_layer = QGisApiWrapper.dissolve_layer(p_input_layer=diffed_layer,
                                                          p_layer_name=f"diss{self.base_layer.name()}_{time.time()}")

        # run intersection between buffed layer and given layer to diff
        intersected_layer = QGisApiWrapper.intersect(p_input_layer=p_layer_to_diff,
                                        p_overlay_layer=dissolved_layer,
                                        p_layer_name=f"inter{self.base_layer.name()}_{time.time()}"
                                                     )

        self.diffed_layer = QGisApiWrapper.dissolve_layer(p_input_layer=intersected_layer,
                                                          p_layer_name=f"diff_{self.base_layer.name()}_{time.time()}"
                                                          )
        # copy layer
        self.buffed_layer = self.diffed_layer.materialize(
            QgsFeatureRequest().setFilterFids(self.diffed_layer.allFeatureIds())
        )
        return self.diffed_layer


class WfkFinancialSupportedIntersection(WfkDifference):
    def translated_name(self) -> str:
        """return name for the ui"""
        return "Verschneidung mit Weißflächen"

    def _init_widgets(self, p_parent=None) -> None:
        """create"""
        super(WfkFinancialSupportedIntersection, self)._init_widgets(p_parent)
        self.label = QLabel(
            str_utils.tooltip(auto_wrap("Mit dieser Funktion werden die erzeugten Weißflächen mit dem eingegebenem "
                                        "Layer verschnitten, um so die Potentialflächen innerhalb\n der "
                                        "gesuchten EEG-Flächenkulisse zu identifizieren.\n  ", 70)[0],
                              "EEG-Verschneidung innerhalb eines Gebiets"),
            self.widget)

    def _init_layout(self) -> None:
        """size the ui"""
        self.label.move(0, 9)
        self.label.adjustSize()
        # set fixed size
        self.widget.setFixedSize(self.widget.width(), self.label.y() + self.label.height() + 6)
        self.widget.adjustSize()

    def create_view(self, p_parent=None) -> None:
        """there are no ui elements"""
        super(WfkFinancialSupportedIntersection, self).create_view(p_parent)

    def from_view(self) -> None:
        """there are no ui elements"""
        pass

    def run(self, p_layer_to_diff):
        """intersect the input layer with self layer"""
        if p_layer_to_diff is None:
            raise DifferenceError("The wfk layer to intersect is None!")
        logger.debug(f"input layer: {p_layer_to_diff.name()}")

        if self.base_layer is None:
            raise DifferenceError("The base layer to intersect is None!")
        logger.debug(f"overlay layer: {self.base_layer.name()}")

        logger.debug("run intersection")
        # run intersection between layer and given layer to intersect
        self.diffed_layer = QGisApiWrapper.intersect(p_input_layer=p_layer_to_diff,
                                                     p_overlay_layer=self.base_layer,
                                                     p_layer_name=f"inter_{self.base_layer.name()}_{time.time()}")
        # copy layer
        self.buffed_layer = self.diffed_layer.materialize(
            QgsFeatureRequest().setFilterFids(self.diffed_layer.allFeatureIds())
        )
        logger.debug("intersection finished")
        if self.diffed_layer is None:
            raise DifferenceError("Layer is none after intersection!")
        return self.diffed_layer


DifferenceFactory.register("WfkBufferDifference", "Umring Puffer", WfkBufferDifference)
DifferenceFactory.register("WfkSelectiveBufferDifference", "Selektiver Puffer", WfkSelectiveBufferDifference)
# DifferenceFactory.register("WfkBlacklistingDifference", "Ausschlussbildung", WfkBlacklistingDifference)
DifferenceFactory.register("WfkTranslatedDifference", "Richtungsabhängiger Puffer", WfkTranslatedDifference)
DifferenceFactory.register("WfkFinancialSupportedIntersection", "Verschneidung mit Weißflächen",
                           WfkFinancialSupportedIntersection)
DifferenceFactory.register("WfkFinancialSupportedDifference", "Min/Max Puffer", WfkFinancialSupportedDifference)
