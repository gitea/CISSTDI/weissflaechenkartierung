from typing import List
from ...common.config import config
from ...common.exception import ConfigNameError
from ...common.qgis import AreaUnit
from ...common.wfk.interface import JsonHandle
from ...common.wfk.wfk_module import WfkModule
from ...common.wfk_statistic import WfkConfigStatisticsData, AnalysisStatisticData


class WfkConfig(JsonHandle):
    """
    wfk configuration
    """

    def __init__(self, p_qgis_layer=None):
        self.name: str = ""
        self.run: bool = True  # run the config

        self.root_dir: str = ""
        self.default_crs: str = ""
        self.wfk_layer_name: str = ""
        self.analysis_layer_name: str = ""
        self.default_min_area: float = 0.0
        self.default_min_area_unit: str = ""

        self.qgis_layer = p_qgis_layer  # will be set if the user starts the config
        self.processed_wfk_layer = None
        self.wfk_layer_path: str = ""
        
        # analysis
        self.average_land_comp: float = 0.0
        self.power_density: float = .0
        self.full_load_hours: float = .0
        self.area_use: float = 0.0
        self.class_count: int = 5
        self.field_name: str = "distance"
        self.analysis_module: WfkModule = None

        # analysis of runtime
        self.statistic = WfkConfigStatisticsData()
        self.statistic_analysis = AnalysisStatisticData()

        self.modules: List[WfkModule] = []

    def __str__(self):
        return type(self).__name__ + ":" + self.name + " M:" + str(len(self.modules))

    def processed(self) -> bool:
        """
        am I processed
        :return: true otherwise false
        """
        return True if self.name and (self.processed_wfk_layer is not None or self.wfk_layer_path) else False

    def is_valid(self) -> bool:
        """
        am I valid
        :return: true otherwise false
        """
        return True if self.name and self.root_dir and self.modules else False

    def to_json(self) -> dict:
        data = {
            "name": self.name,
            "root_dir": self.root_dir,
            "run": self.run,
            # "default_crs": self.default_crs,
            "default_min_area": float(self.default_min_area),
            "default_min_area_unit": self.default_min_area_unit,
            "analysis_layer_name": self.analysis_layer_name,
            "wfk_layer_name": self.wfk_layer_name,
            "wfk_layer_path": self.wfk_layer_path,
            "modules": [],
            "analysis": {
                "average_land_comp": self.average_land_comp,
                "area_use": self.area_use,
                "class_count": self.class_count,
                "field_name": self.field_name,
                "power_density": self.power_density,
                "full_load_hours": self.full_load_hours,
                "class": self.analysis_module.to_json() if self.analysis_module is not None else None
            }
        }
        # iterate over modules and convert them to json and add them
        for module in self.modules:
            data["modules"].append(module.to_json())
        return data

    def from_json(self, p_data: dict) -> None:
        if "name" not in p_data or len(p_data["name"]) == 0:
            raise ConfigNameError("There is no configuration name given!")
        self.name = p_data["name"]
        self.run = p_data.get("run", True)
        self.wfk_layer_name = p_data.get("wfk_layer_name", "")
        self.wfk_layer_path = p_data.get("wfk_layer_path", "")
        self.analysis_layer_name = p_data.get("analysis_layer_name", "")
        # self.default_crs = p_data.get("default_crs", "")
        self.default_min_area_unit = p_data.setdefault("default_min_area_unit", str(AreaUnit.KM.value))
        self.default_min_area = p_data.setdefault("default_min_area", float(config["default_min_area"]))
        
        # analysis
        if p_data.get('analysis') is not None:
            analysis: dict = p_data.get('analysis')
            self.average_land_comp = analysis.setdefault("average_land_comp", 0.0)
            self.area_use = analysis.setdefault("area_use", 0.0)
            self.class_count = analysis.setdefault("class_count", 0)
            self.field_name = analysis.setdefault("field_name", "")
            self.power_density = analysis.setdefault("power_density", .0)
            self.full_load_hours = analysis.setdefault("full_load_hours", 0)

            if analysis.get("class") is not None:
                wfk_module: WfkModule = WfkModule()
                wfk_module.qgis_layer = self.qgis_layer
                wfk_module.default_crs = self.default_crs
                wfk_module.default_min_area = float(self.default_min_area)
                wfk_module.from_json(p_data=analysis.get("class"))
                # after building class check categorization field
                if wfk_module.categorization is not None:
                    self.analysis_module = wfk_module

        for module in p_data.get("modules", []):
            wfk_module: WfkModule = WfkModule()
            wfk_module.qgis_layer = self.qgis_layer
            wfk_module.default_crs = self.default_crs
            wfk_module.default_min_area = float(self.default_min_area)
            wfk_module.from_json(p_data=module)
            self.modules.append(wfk_module)

        self.root_dir = p_data.get("root_dir", "")

    # ################################################################
