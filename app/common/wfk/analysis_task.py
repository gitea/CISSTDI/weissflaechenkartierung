import time
import traceback

from PyQt5.QtCore import *

from app.common.config import config
from app.common.logger import logger
from app.common.signal_bus import signalBus
from app.common.task_result import AnalysisTaskResult, WfkTaskResult
from app.common.wfk.wfk_config import WfkConfig
from app.common.wfk.wfk_module import WfkModule

try:
    from qgis.core import QgsTask
    from app.common.qgis.QGisApiWrapper import QGisApiWrapper
except:
    pass


class AnalysisTask(QgsTask):
    result = pyqtSignal(AnalysisTaskResult)

    def __init__(self, p_desc: str, p_wfk_result: WfkTaskResult):
        super(AnalysisTask, self).__init__(p_desc, QgsTask.CanCancel)

        # interface to qgis
        self._interface = config.qgis_interface

        # result
        self._task_result: AnalysisTaskResult = AnalysisTaskResult()

        # wfk result
        self._wfk_result = p_wfk_result

        # statistics
        self._start: float = 0.0

    def run(self) -> bool:
        """
        the action happens here
        """
        self._start = time.perf_counter()
        logger.info("Start AnalysisTask")

        if self._interface is None:
            logger.error("The given interface is None!")
            return False

        # check if we want to run this config
        if not self._wfk_result.config.run:
            logger.debug("The config is disabled")
            self._task_result.result = True
            self._task_result.txt = "Die Konfiguration ist deaktiviert!"
            return True

        logger.info(f"Run config: {self._wfk_result.config.name}")
        logger.info(f"Process: {str(len(self._wfk_result.config.modules))} module/s")
        logger.info(f"Run with errors: {config['run_wfk_with_errors']}")
        logger.info(f"Protocol layers: {config['protocol_layer']}")

        try:
            for conf in self._wfk_result.layer_to_analyse:
                if len(conf) != 3:
                    raise Exception("The configuration is not valid!")
                layer = conf[0]
                blacklist: bool = conf[1]
                wfk_config: WfkConfig = conf[2]
                analysed_layer = self._run(p_layer=layer, p_blacklist=blacklist, p_wfk_config=wfk_config)

                # check if the computed layer is not the base layer
                # if an error occurs inside self._run the base layer will be returned
                if analysed_layer is not layer and analysed_layer is not None:
                    self._task_result.layer_to_protocol.append(analysed_layer)
                else:
                    self._task_result.txt = "Die Analyse konnte nicht abgeschlossen werden!\nDie Kategorisierung ist" \
                                            "fehlgeschlagen!"
                    logger.error("The analysis was not successful")
                    return False
        except Exception as e:
            logger.error(traceback.format_exc())
            logger.error("The configuration failed! " + str(e))
            self._task_result.txt = str(e)
            return False
        return True

    def _run(self, p_layer, p_blacklist: bool, p_wfk_config: WfkConfig):
        """
        run
        """
        if self.has_to_stop():
            logger.debug("The program has to stop")
            return p_layer
        self.setProgress(5)

        logger.debug(f"--------Start analysis for {p_wfk_config.name}")

        if p_layer.selectedFeatureCount() == 0:
            # clone the complete layer which we want to analyze and save it temporary
            layer_to_analyze = QGisApiWrapper.clone_all_features(p_input_layer=p_layer)
        else:
            # clone the selected features
            layer_to_analyze = QGisApiWrapper.clone_selected_features(p_input_layer=p_layer)

        # check if there are modules to compute
        if len(p_wfk_config.modules) == 0:
            logger.info("There are no modules to analyze!")
            return layer_to_analyze

        self.setProgress(5)

        logger.info(f"There are {len(p_wfk_config.modules)} modules to run")

        step_process = 1
        modules_to_rem = []
        for module in p_wfk_config.modules:
            # if there is a wrong module inside continue
            if not isinstance(module, WfkModule):
                continue
            try:
                logger.debug(f"-------- Run {module.name}")

                # check if the user wants to skip the module
                if not module.run:
                    logger.debug(f"Skipp module: {module.name}")
                    continue

                # set ref layer for processing
                module.categorization.ref_layer = module.connector.layer.created_layer

                logger.debug("Run categorization")
                # run the categorization class
                analyzed_layer = module.categorization.run(cat_layer=layer_to_analyze)

                # the user defines an object of categorization type on the ui
                # if the module don't need categorization accepts it if the resulting layer is not none
                if analyzed_layer is not None:
                    layer_to_analyze = analyzed_layer

                if self.has_to_stop():
                    logger.debug(f"The program has to stop after {module.name} module")
                    return layer_to_analyze

                logger.debug(f"Module {module.name} ran successful --------")

                # add a new module which is processed
                self._wfk_result.config.statistic.module_processed()
            except Exception as e:
                logger.error(traceback.format_exc())
                if config["run_wfk_with_errors"]:
                    modules_to_rem.append(module)
                    self._task_result.txt = str(e)
                    logger.warning("Skipped module: " + module.name)
                    logger.warning(str(e))
                    continue
                else:
                    raise Exception(f"Module failed: {module.name} {str(e)}")

            # add progress
            self.setProgress(20 + (step_process * (20 / len(p_wfk_config.modules))))
            step_process += 1

        # set statistics
        self._wfk_result.config.statistic_analysis.modules_failed = modules_to_rem

        # check if there are modules to remove because they fail
        if len(modules_to_rem) > 0:
            logger.warning("There where " + str(len(modules_to_rem)) + " failed modules.")
            for m in modules_to_rem:
                self._wfk_result.config.modules.remove(m)

        logger.debug("Set analysis layer name")
        # set the layer name
        if layer_to_analyze is not None:
            layer_to_analyze.setName(p_wfk_config.analysis_layer_name)
        else:
            logger.error("Can not rename the analysis layer! The layer is None")
            raise TypeError("The layer is none!")

        return layer_to_analyze

    def finished(self, result) -> None:
        """the wfk task finished"""
        logger.debug("The wfk analysis finished")

        self._task_result.result = result
        self._wfk_result.config.statistic_analysis.time_elapsed = time.perf_counter() - self._start
        self._task_result.config = self._wfk_result.config

        logger.debug("Emit process finished")
        logger.info(f"The task finished in: {self._wfk_result.config.statistic_analysis.time_elapsed} seconds")
        self.result.emit(self._task_result)
        signalBus.analysisFinishedSig.emit()

    def has_to_stop(self) -> bool:
        """
        checks if the task needs to stopped
        """
        return True if self.isCanceled() else False
