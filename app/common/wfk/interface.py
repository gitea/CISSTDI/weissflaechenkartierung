from abc import ABC, abstractmethod

from PyQt5.QtWidgets import QWidget, QSizePolicy, QMainWindow

from ...components.dialog_box import MaskDialogBase, MessageDialog
from ...components.widgets.tooltip import QGisTaskStateToolTip


class CreateInterface(ABC):
    @staticmethod
    @abstractmethod
    def create(p_data: dict):
        pass


class Construction(ABC):
    @abstractmethod
    def _init_widgets(self, p_parent=None) -> None:
        """ init the widgets """
        self.widget = QWidget()
        self.widget.setSizePolicy(QSizePolicy(QSizePolicy.Expanding,
                                              QSizePolicy.Expanding))
        if p_parent is not None:
            self.main_window = self.find_main_window(p_parent)
            self.widget.setParent(p_parent)
            self.widget.setFixedWidth(p_parent.width())

        self.masks: list = []

    def find_main_window(self, widget):
        while widget is not None:
            if isinstance(widget, QMainWindow):
                return widget
            widget = widget.parent()
        return None

    def adjust_layout_of_masks(self) -> None:
        """
        adjust layouts
        :return: None
        """
        if not hasattr(self, 'masks'):
            return
        for mask in self.masks:
            try:
                if isinstance(mask, MaskDialogBase):
                    if mask.isVisible():
                        mask.adjust_widget_geometry()
                if isinstance(mask, QGisTaskStateToolTip):
                    if mask.isVisible():
                        mask.move(self.main_window.width() - mask.width() - 30, mask.y())
                if isinstance(mask, MessageDialog):
                    if mask.isVisible():
                        mask.resize(self.main_window.size())
                        mask.windowMask.resize(self.main_window.size())
            except:
                continue

    @abstractmethod
    def _init_layout(self) -> None:
        """ init layout """
        pass

    @abstractmethod
    def create_view(self, p_parent=None) -> None:
        """ refresh the view with new data -> from_json"""
        # init the widgets
        self._init_widgets(p_parent)
        # init the layout
        self._init_layout()

    @abstractmethod
    def from_view(self) -> None:
        """ save the data from the view into the local members """
        pass


class JsonHandle(ABC):
    @abstractmethod
    def from_json(self, p_data: dict) -> None:
        pass

    @abstractmethod
    def to_json(self) -> dict:
        return {}
