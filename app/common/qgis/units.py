from enum import Enum


class AreaUnit(Enum):
    KM = 'km²'


class DistanceUnit(Enum):
    M = 'm'
    KM = 'km'
