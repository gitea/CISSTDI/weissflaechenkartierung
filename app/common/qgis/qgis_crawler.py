import os

from PyQt5.QtCore import QUrl, QEventLoop
from PyQt5.QtNetwork import QNetworkRequest, QNetworkReply

try:
    from qgis.core import QgsNetworkAccessManager
except ImportError as e:
    pass


class RawHeader:
    def __init__(self):
        self.key: str = ""
        self.value: str = ""


class QGisCrawler:

    @staticmethod
    def check_internet_qgis() -> bool:
        """
        check internet connection with qgis
        """
        return QGisCrawler.validate_url(url="https://www.google.de")

    @staticmethod
    def validate_url(url: str) -> bool:
        """
        check if the url is valid
        """
        try:
            def on_finish():
                loop.quit()

            request = QNetworkRequest()

            if not url.startswith("https://"):
                url = "https://" + url

            request.setUrl(QUrl(url))
            manager = QgsNetworkAccessManager.instance()
            reply = manager.get(request)
            reply.setParent(None)
            loop = QEventLoop()
            reply.finished.connect(on_finish)
            loop.exec_()

            status = reply.attribute(QNetworkRequest.HttpStatusCodeAttribute)
            if status is None:
                return False
            if status == 301:
                redirectUrl = reply.attribute(request.RedirectionTargetAttribute)
                request = QNetworkRequest()
                request.setUrl(redirectUrl)
                manager = QgsNetworkAccessManager.instance()

                reply = manager.get(request)
                reply.setParent(None)

                loop = QEventLoop()
                reply.finished.connect(on_finish)
                loop.exec_()
            return True
        except Exception as e:
            return False

    @staticmethod
    def get_url(p_url: str, p_header_info: list = None) -> any:
        """
        runs a get request

        :param p_url: url of the source
        :param p_header_info:
        :return: any data we get or None if the there is no data to return
        """

        if len(p_url) == 0:
            return None

        if p_header_info is None:
            p_header_info = []

        request = QNetworkRequest()

        for header in p_header_info:
            if isinstance(header, RawHeader):
                # convert str to byte array. QByteArray is needed
                request.setRawHeader(str.encode(header.key), str.encode(header.value))

        request.setUrl(QUrl(p_url))
        manager = QgsNetworkAccessManager.instance()

        reply = manager.get(request)
        reply.setParent(None)

        loop = QEventLoop()
        reply.finished.connect(lambda: loop.quit())
        loop.exec_()

        # resolve redirections
        while reply.attribute(QNetworkRequest.HttpStatusCodeAttribute) == 301:
            reply = QGisCrawler.resolve_redirection(p_reply=reply)

        # get the status code attribute
        status = reply.attribute(QNetworkRequest.HttpStatusCodeAttribute)
        if status in [409, 400, 500, 200]:
            return reply.readAll()
        return None

    @staticmethod
    def download(p_url: str, p_file_name: str):
        """
        runs a get request

        :param p_url: url of the source
        :param p_file_name: url of the source
        :return: any data we get or None if the there is no data to return
        """

        # QGIS3:  funktioniert unter QGIS 3.x recht zuverlässig auch auf "eingeschränktem Rechner"
        # - nutzt die Proxyeinstellungen von QGIS
        # - funktioniert in QGIS selbst auch über HTTPS (es wird durch QGIS eiun Abfragefenster geöffnet)
        # - bei extrem großen Dateien (z.B. 500MBYte) crasht es bei ReadAll()

        # QGIS2:  funktioniert unter QGIS 2.x innerhalb von QGIS aktuell recht zuverlässig auch auf "eingeschränktem Rechner"
        #         außerhalb hängt sich der Code auf "eingeschräktem Rechner" auf und bringt dann auch kein Ergebnis
        #         Normalrechner funktioniert es
        def WriteFile(LokFileName, content):
            # 1. Ziel löschen, wenn existent
            if os.path.exists(LokFileName):
                os.remove(LokFileName)
            out = open(LokFileName, 'wb')
            out.write(content)
            out.close()

        def onfinish():
            WriteFile(p_file_name, reply.readAll())
            loop.quit()

        # 2. Download
        request = QNetworkRequest()
        request.setUrl(QUrl(p_url))
        manager = QgsNetworkAccessManager.instance()

        reply = manager.get(request)
        reply.setParent(None)

        loop = QEventLoop()
        reply.finished.connect(onfinish)
        loop.exec_()

        # Wiederholung bei redirekt (13.08.18 Thüringen leitet an HTTPS weiter)
        status = reply.attribute(QNetworkRequest.HttpStatusCodeAttribute)
        if status == 301:
            redirectUrl = reply.attribute(request.RedirectionTargetAttribute)
            request = QNetworkRequest()
            request.setUrl(redirectUrl)
            manager = QgsNetworkAccessManager.instance()

            reply = manager.get(request)
            reply.setParent(None)

            loop = QEventLoop()
            reply.finished.connect(onfinish)
            loop.exec_()

        if os.path.exists(p_file_name):
            return os.path.getsize(p_file_name), reply.attribute(QNetworkRequest.HttpStatusCodeAttribute)
        else:
            return None, reply.attribute(QNetworkRequest.HttpStatusCodeAttribute)

    @staticmethod
    def resolve_redirection(p_reply: QNetworkReply) -> QNetworkReply:
        """
        resolves redirections

        :param p_reply: network reply object
        :return: reply
        """

        redirect_url = p_reply.attribute(QNetworkRequest.RedirectionTargetAttribute)
        request = QNetworkRequest()
        request.setUrl(redirect_url)
        manager = QgsNetworkAccessManager.instance()

        reply = manager.get(request)
        reply.setParent(None)

        loop = QEventLoop()
        reply.finished.connect(lambda: loop.quit())
        loop.exec_()

        return reply
