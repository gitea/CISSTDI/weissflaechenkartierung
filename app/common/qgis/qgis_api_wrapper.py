import os
from typing import List

from PyQt5.QtGui import QColor
from PyQt5.QtCore import QVariant
from .qgis_project_wrapper import QGisProjectWrapper
from .units import DistanceUnit

try:
    from osgeo import ogr
    from qgis import processing
    from qgis.core import QgsMessageLog, Qgis, QgsVectorLayer, QgsCoordinateReferenceSystem, QgsFeature
    from qgis.gui import QgsHighlight
except ImportError as e:
    pass


class QGisApiWrapper:
    """
    interface with qgis functionality

    helpful: https://docs.qgis.org/3.22/en/docs/user_manual/processing/console.html
    https://docs.qgis.org/3.28/en/docs/user_manual/processing/console.html

    to know the input parameters:
    processing.algorithmHelp("native:translategeometry")


    color = random.choice(list(mcolors.CSS4_COLORS.values()))
    """

    @staticmethod
    def colorize_features(p_border_colors: List[QColor], p_background_colors: List[QColor],
                         p_layer=None, p_interface=None, p_border_width: int = 3) -> List['QgsHighlight']:
        """
        draw highlight outline all given features
        :param p_layer:  which should be colorized
        :param p_interface: interface
        :param p_border_colors: border colors
        :param p_background_colors: background colors
        :param p_border_width: border width
        :return: List with QgsHighlight
        """
        if p_interface is None or p_layer is None:
            return []
        h_list: List[QgsHighlight] = []
        map_canvas = p_interface.mapCanvas()
        count = 0
        for f in layer.getFeatures():
            h_list.append(QGisApiWrapper.colorize_feature(
                p_border_color=p_border_colors[count], p_background_color=p_background_colors[count],
                p_border_width=p_border_width, p_feature=f,p_map_canvas=map_canvas
            ))
            count += 1
        return h_list

    @staticmethod
    def colorize_feature(p_border_color: QColor = QColor("red"), p_background_color: QColor = QColor("grey"),
                        p_border_width: int = 3, p_feature=None,
                        p_map_canvas=None) -> 'QgsHighlight':
        """
        draw highlight outline feature geometry
        :param p_feature: feature which should be colorized
        :param p_map_canvas: interface map canvas
        :param p_border_color: border color
        :param p_background_color: background color
        :param p_border_width: border width
        :return: QgsHighlight
        """
        if p_map_canvas is None or p_feature is None:
            return
        h = QgsHighlight(p_map_canvas, p_feature.geometry(), layer)
        h.setColor(p_border_color)
        h.setWidth(p_border_width)
        h.setFillColor(p_background_color)
        return h

    @staticmethod
    def intersect(p_input_layer: any, p_overlay_layer: any, p_input_fields=None, p_overlay_fields=None,
                  p_overlay_fields_prefix: str = "", p_layer_name: str = "", p_out_put_type: str = "TEMPORARY_OUTPUT")\
            -> 'QgsVectorLayer':
        """
        processing.algorithmHelp("native:intersection")

        This algorithm extracts overlapping parts of objects in the input and overlay layers. The objects in the output
         cut layer are assigned the attributes of the overlapping objects from both the input and overlay layers.

        :param p_input_layer: input layer
        :param p_overlay_layer: overlay layer
        :param p_input_fields: input fields to be retained (leave blank if all fields are to be retained)
        :param p_overlay_fields: input fields to be retained (leave blank if all fields are to be retained)
        :param p_overlay_fields_prefix: Prefix for overlaid fields
        :param p_layer_name: name of the new layer
        :param p_out_put_type: file name 'd:/test.shp' or standard 'memory' or 'TEMPORARY_OUTPUT', vector layer
        """
        if p_input_fields is None:
            p_input_fields = []
        if p_overlay_fields is None:
            p_overlay_fields = []

        if p_input_layer is None or p_overlay_layer is None:
            return None

        result = processing.run("native:intersection",
                                {'INPUT': p_input_layer,
                                 'OVERLAY': p_overlay_layer,
                                 'INPUT_FIELDS': p_input_fields,
                                 'OVERLAY_FIELDS': p_overlay_fields,
                                 'OVERLAY_FIELDS_PREFIX': p_overlay_fields_prefix,
                                 'OUTPUT': p_out_put_type},
                                context=QGisProjectWrapper.get_project_context())
        vl = result['OUTPUT']
        if len(p_layer_name) > 0:
            vl.setName(p_layer_name)
        return vl

    @staticmethod
    def translate_geometry(p_input_layer: any, p_delta_x: float, p_delta_y: float, p_delta_z: float, p_delta_m: float,
                           p_out_put_type: str = "TEMPORARY_OUTPUT", p_layer_name: str = ""):
        """
        This algorithm moves geometries within a layer with a given x and y offset.

        :param p_input_layer: layer to translate
        :param p_delta_x: x translation
        :param p_delta_y: y translation
        :param p_delta_z: z translation
        :param p_delta_m: middle value translation
        :param p_layer_name: name of the output layer
        :param p_out_put_type: file name 'd:/test.shp' or standard 'memory', vector layer
        """
        if p_input_layer is None:
            return None
        result = processing.run("native:translategeometry",
                                {'INPUT': p_input_layer,
                                 'DELTA_X': p_delta_x,
                                 'DELTA_Y': p_delta_y,
                                 'DELTA_Z': p_delta_z,
                                 'DELTA_M': p_delta_m,
                                 'OUTPUT': p_out_put_type},
                                context=QGisProjectWrapper.get_project_context())
        vl = result['OUTPUT']
        if len(p_layer_name) > 0:
            vl.setName(p_layer_name)
        return vl

    @staticmethod
    def diff_from_layer(p_base_layer: any, p_input_layer: any, p_layer_name: str = "",
                        p_out_put_type: str = "TEMPORARY_OUTPUT"):
        """
        This algorithm extracts objects from the input layer that are outside of or partially overlay the overlay
        layer. Input layer objects that partially overlap with overlay layer objects are split along the boundary of
        the overlay layer objects and only the areas outside the overlay layer objects are retained. The attributes
        of the objects are not changed although the properties such as area or length of the objects are changed by
        the difference operation. If such properties are kept as attributes, they must be updated manually.

        :param p_base_layer: base layer
        :param p_input_layer: input layer
        :param p_layer_name: optional layer name
        :param p_out_put_type: output format of the result layer, standard 'TEMPORARY_OUTPUT'

        :return: layer
        """
        if p_base_layer is None:
            return None
        if p_input_layer is None:
            return None
        if len(p_out_put_type) == 0:
            return None
        try:
            result = processing.run("native:difference",
                                    {'INPUT': p_base_layer,
                                     'OVERLAY': p_input_layer,
                                     'OUTPUT': p_out_put_type},
                                    context=QGisProjectWrapper.get_project_context())
            vl = result['OUTPUT']
            if len(p_layer_name) > 0:
                vl.setName(p_layer_name)
            return vl
        except Exception as e:
            QgsMessageLog.logMessage(message="QGisApiWrapper:diff_from_layer:error: unexpected " + str(e),
                                     level=Qgis.Critical)
            return None

    @staticmethod
    def union_by_layer(p_input_layer: any, p_overlay_layer: any, p_layer_name: str = "united",
                       p_out_put_type: str = "TEMPORARY_OUTPUT"):
        """
        This algorithm checks overlaps of objects within the input layer and creates separate objects for overlapping
        and non-overlapping parts. The overlapped area creates as many identical objects as there are objects
        involved in the overlap. An overlay layer can also be used. Then the objects of each layer are separated when
        they overlap with objects of the other, creating a layer with all surfaces of both layers. The attribute
        table of the union layer is filled with the attributes of the original layers for non-overlapping objects and
        with the attribute values of both layers for overlapping objects.

        :param p_input_layer: base layer
        :param p_overlay_layer: overlay layer
        :param p_layer_name: optional layer name
        :param p_out_put_type: output format of the result layer, standard 'TEMPORARY_OUTPUT'
        :return: layer
        """
        if p_input_layer is None:
            return None
        if p_overlay_layer is None:
            return None
        try:
            result = processing.run("native:union",
                                    {'INPUT': p_input_layer,
                                     'OVERLAY': p_overlay_layer,
                                     'OUTPUT': p_out_put_type},
                                    context=QGisProjectWrapper.get_project_context())
            vl = result['OUTPUT']

            if len(p_layer_name) > 0:
                vl.setName(p_layer_name)
            return vl
        except Exception as e:
            QgsMessageLog.logMessage(message="QGisApiWrapper:union_by_layer:error: name " + p_layer_name,
                                     level=Qgis.Critical)
            return None

    @staticmethod
    def clip_by_layer(p_input_layer: any, p_overlay_layer: any, p_layer_name: str = "",
                      p_out_put_type: str = "TEMPORARY_OUTPUT"):
        """
        This algorithm prunes a vector layer by the objects on an additional polygon layer. Only the parts of the
        objects of the input layer that fall into the polygons of the overlay layer are added to the result layer.
        The attributes of the objects are not changed although the properties like areas or length of the objects are
        changed by the cut operation. If such properties are kept as attributes, they must be updated manually.

        :param p_input_layer: base layer
        :param p_overlay_layer: overlay layer
        :param p_layer_name: optional layer name
        :param p_out_put_type: output format of the result layer, standard 'TEMPORARY_OUTPUT'

        :return: layer
        """
        if p_input_layer is None:
            return None
        if p_overlay_layer is None:
            return None
        try:
            result = processing.run("native:clip",
                                    {'INPUT': p_input_layer,
                                     'OVERLAY': p_overlay_layer,
                                     'OUTPUT': p_out_put_type},
                                    context=QGisProjectWrapper.get_project_context())
            vl = result['OUTPUT']
            if len(p_layer_name) > 0:
                vl.setName(p_layer_name)
            return vl
        except Exception as e:
            QgsMessageLog.logMessage(message=f"QGisApiWrapper:clip_by_layer:error: name {p_layer_name} code: {e}",
                                     level=Qgis.Critical)
            return None

    @staticmethod
    def dissolve_layer(p_input_layer: any, p_layer_name: str = "", p_out_put_type: str = "TEMPORARY_OUTPUT",
                       p_separate_joints: bool = True):
        """
        Dissolve geometries

        :param p_input_layer: base layer
        :param p_layer_name: optional layer name
        :param p_out_put_type: output format of the result layer, standard 'TEMPORARY_OUTPUT'
        :param p_separate_joints: true or false
        :return: layer
        """
        if p_input_layer is None:
            return None

        try:
            # dissolve
            result = processing.run("native:dissolve",
                                    {'INPUT': p_input_layer,
                                     'SEPARATE_DISJOINT': p_separate_joints,
                                     'FIELD': [],
                                     'OUTPUT': p_out_put_type},
                                    context=QGisProjectWrapper.get_project_context())
            dissolved_layer = result['OUTPUT']

            if len(p_layer_name) > 0:
                dissolved_layer.setName(f"{p_layer_name}_buffered")
            return dissolved_layer
        except Exception as e:
            QgsMessageLog.logMessage(message=f"QGisApiWrapper:buffer_layer:error: cloning layer failed {p_layer_name}",
                                     level=Qgis.Critical)
            return None

    @staticmethod
    def buffer_layer(p_input_layer: any, p_buffer: float, p_layer_name: str = "", p_unit: DistanceUnit = DistanceUnit.M,
                     p_out_put_type: str = "TEMPORARY_OUTPUT"):
        """
        The algorithm calculates a buffer with fixed or dynamic spacing for all objects of an input layer. The
        segments parameter controls the number of line segments to approximate a quarter circle when creating rounded
        offsets. The end style parameter controls how line ends are handled in the buffer. The connection style
        parameter specifies whether to use round, square or chamfered transitions when offsetting corners. The Corner
        Limit parameter applies only to square joints and controls the maximum distance of the offset line when
        creating the corner transition.
        After buffering dissolve geometries to prevent bad calculations: processing.algorithmHelp("native:dissolve")

        :param p_input_layer: base layer
        :param p_buffer: buffer
        :param p_layer_name: optional layer name
        :param p_unit: buffer unit normal in meters
        :param p_out_put_type: output format of the result layer, standard 'TEMPORARY_OUTPUT'

        :return: layer
        """
        if p_input_layer is None:
            return None
        if p_buffer == 0.0:
            return p_input_layer

        # calculate the buffer
        if p_unit == DistanceUnit.KM:
            p_buffer = p_buffer * 1000

        try:
            result = processing.run("native:buffer",
                                    {'INPUT': p_input_layer,
                                     'DISTANCE': p_buffer,
                                     'OUTPUT': p_out_put_type},
                                    context=QGisProjectWrapper.get_project_context())
            buffed_layer = result['OUTPUT']
            # buffed_layer.setName(f"{p_layer_name}_dissolve")

            # dissolve
            result = processing.run("native:dissolve",
                                    {'INPUT': buffed_layer,
                                     'SEPARATE_DISJOINT': True,
                                     'FIELD': [],
                                     'OUTPUT': p_out_put_type},
                                    context=QGisProjectWrapper.get_project_context())
            dissolved_layer = result['OUTPUT']

            if len(p_layer_name) > 0:
                dissolved_layer.setName(f"{p_layer_name}_buffered")
            return dissolved_layer
        except Exception as e:
            QgsMessageLog.logMessage(message=f"QGisApiWrapper:buffer_layer:error: cloning layer failed {p_layer_name}",
                                     level=Qgis.Critical)
            return None

    @staticmethod
    def clone_selected_features(p_input_layer: any, p_layer_name: str = "", p_out_put_type: str = "TEMPORARY_OUTPUT"):
        """
        This algorithm creates a new layer with all objects selected in the given vector layer.
        If no objects are selected in the layer, the newly created layer is empty.

        :param p_input_layer: base layer
        :param p_layer_name: optional layer name
        :param p_out_put_type: output format of the result layer, standard 'TEMPORARY_OUTPUT'

        :return: cloned layer
        """
        if p_input_layer is None:
            return None
        try:
            result = processing.run("native:saveselectedfeatures",
                                    {'INPUT': p_input_layer,
                                     'OUTPUT': p_out_put_type},
                                    context=QGisProjectWrapper.get_project_context())
            p_input_layer.removeSelection()
            output_layer = result['OUTPUT']

            if len(p_layer_name) > 0:
                output_layer.setName(p_layer_name)
            return output_layer
        except Exception as e:
            QgsMessageLog.logMessage(
                message="QGisApiWrapper:clone_selected_layer:error: cloning selected layer failed " + str(e),
                level=Qgis.Critical)
            return None

    @staticmethod
    def clone_all_features(p_input_layer: any, p_layer_name: str = "", p_out_put_type: str = "TEMPORARY_OUTPUT"):
        """
        Selects all features of the given input layer
        This algorithm creates a new layer with all objects selected in the given vector layer.
        If no objects are selected in the layer, the newly created layer is empty.

        :param p_input_layer: base layer
        :param p_layer_name: optional layer name
        :param p_out_put_type: output format of the result layer, standard 'TEMPORARY_OUTPUT'

        :return: cloned layer
        """
        if p_input_layer is None:
            return None
        try:
            p_input_layer.selectAll()
            result = processing.run("native:saveselectedfeatures",
                                    {'INPUT': p_input_layer,
                                     'OUTPUT': p_out_put_type},
                                    context=QGisProjectWrapper.get_project_context())
            p_input_layer.removeSelection()
            output_layer = result['OUTPUT']

            if len(p_layer_name) == 0:
                output_layer.setName(p_input_layer.name())
            else:
                output_layer.setName(p_layer_name)
            return output_layer
        except Exception as e:
            QgsMessageLog.logMessage(
                message="QGisApiWrapper:clone_selected_layer:error: cloning selected layer failed " + str(e),
                level=Qgis.Critical)
            return None

    @staticmethod
    def multi_2_single(p_base_layer: any, p_layer_name: str = "multi2single", p_out_put_type: str = "TEMPORARY_OUTPUT"):
        """
        This algorithm takes a vector layer with multi-part geometries and creates a new one in which all geometries
        are only one part. Objects with multi-part geometries are split into as many different parts as there are
        parts in the geometry, all with the same attributes.

        :param p_base_layer: base layer
        :param p_layer_name: optional layer name
        :param p_out_put_type: output format of the result layer, standard 'TEMPORARY_OUTPUT'

        :return: cloned layer
        """
        if p_base_layer is None:
            return None
        try:
            result = processing.run("native:multiparttosingleparts",
                                    {'INPUT': p_base_layer,
                                     'OUTPUT': p_out_put_type},
                                    context=QGisProjectWrapper.get_project_context())
            vl = result['OUTPUT']
            if len(p_layer_name) > 0:
                vl.setName(p_layer_name)
            return vl
        except Exception as e:
            print(e)
            QgsMessageLog.logMessage(message="QGisApiWrapper:multi_2_single:error: multi to single failed " + str(e),
                                     level=Qgis.Critical)
            return None

    @staticmethod
    def att_from_nearest(p_input_layer: any, p_ref_layer: any, p_fields: list, p_layer_name: str = "",
                         p_out_put_type: str = "TEMPORARY_OUTPUT"):
        """
        This algorithm takes an input vector layer and creates a new vector layer extended by additional attributes
        in its attribute table. The additional attributes and their values are taken from a second vector layer by
        searching for the nearest objects. By default, the only nearest object is linked, but optionally the n
        nearest objects can be used. If a maximum distance is specified, only objects closer than this are
        considered. These output objects have the selected attributes of the nearest object and attributes with its
        distance, its index and the nearest point to it on the input object (feature_x, feature_y) and the nearest
        point of the nearest object (nearest_x, nearest_y). The algorithm uses only Cartesian distance calculations
        and does not consider geodesic or ellipsoidal features.

        :param p_input_layer: input layer
        :param p_ref_layer: reference layer
        :param p_fields: fields to copy to
        :param p_layer_name: name of the layer
        :param p_out_put_type: output format of the result layer, standard 'TEMPORARY_OUTPUT'
        :return: layer with nearest
        """
        if p_input_layer is None:
            return None
        if p_ref_layer is None:
            return None
        try:
            result = processing.run("qgis:joinbynearest",
                                    {'MAX_DISTANCE': None, 'NEIGHBORS': 1, 'DISCARD_NONMATCHING': False,
                                     'INPUT': p_input_layer, 'FIELDS_TO_COPY': p_fields, 'INPUT_2': p_ref_layer,
                                     'OUTPUT': p_out_put_type}, context=QGisProjectWrapper.get_project_context())
            vl = result['OUTPUT']
            if len(p_layer_name) > 0:
                vl.setName(p_layer_name)
            else:
                vl.setName(p_input_layer.name())
            return vl
        except Exception as e:
            QgsMessageLog.logMessage(message="QGisApiWrapper:att_from_nearest:error: " + str(e),
                                     level=Qgis.Critical)
            return None

    @staticmethod
    def att_from_position(p_input_layer: any, p_ref_layer: any, p_fields: list, p_predicate=None, p_method: int = 1,
                          p_discard_non_matching: bool = False, p_layer_name: str = "",
                          p_out_put_type: str = "TEMPORARY_OUTPUT"):
        """
        This algorithm expects a vector layer and creates a new vector layer that is an extended version of the input
        with additional attributes in the attribute table. The additional attributes and their values are taken from
        a second layer. A spatial criterion is used to select values from the second layer, which are added to each
        object of the first layer in the result layer. The algorithm calculates a statistical summary for the values
        from matching objects of the second layer (e.g. maximum value, mean value etc.).

        :param p_input_layer: input layer
        :param p_ref_layer: reference layer
        :param p_fields: fields to copy
        :param p_predicate: predicate [0]
        :param p_method: method as int
        :param p_discard_non_matching: discard non matching false
        :param p_layer_name: name of the layer
        :param p_out_put_type: output format of the result layer, standard 'TEMPORARY_OUTPUT'
        :return: new layer
        """
        if p_input_layer is None:
            return None

        if p_ref_layer is None:
            return None

        if p_predicate is None:
            p_predicate = [0]
        try:
            result = processing.run("qgis:joinattributesbylocation",
                                    {'PREDICATE': p_predicate,
                                     'DISCARD_NONMATCHING': p_discard_non_matching,
                                     'INPUT': p_input_layer,
                                     'JOIN_FIELDS': p_fields,
                                     'JOIN': p_ref_layer,
                                     'METHOD': p_method,
                                     'OUTPUT': p_out_put_type},
                                    context=QGisProjectWrapper.get_project_context())
            vl = result['OUTPUT']

            if len(p_layer_name) > 0:
                vl.setName(p_layer_name)
            else:
                vl.setName(p_input_layer.name())
            return vl
        except Exception as e:
            QgsMessageLog.logMessage(message="QGisApiWrapper:att_from_position:error: " + str(e),
                                     level=Qgis.Critical)
            return None

    @staticmethod
    def gml_xml_file_to_layer(p_input_file: str, p_layer_name: str) -> 'QgsVectorLayer':
        """

        :param p_layer_name:
        :param p_input_file:
        :return: vector layer
        """
        if not os.path.exists(p_input_file):
            return None
        try:
            # get the ogr driver
            driver = ogr.GetDriverByName('GML')

            # open data source, which will create a gfs file in case of GML
            data_source = driver.Open(p_input_file, 0)
            layer = data_source.GetLayer()
            # dereference and close connection
            data_source = None
            layer = None
            return QgsVectorLayer(p_input_file,
                                  p_layer_name,
                                  "ogr")
        except Exception as e:
            QgsMessageLog.logMessage(message="QGisApiWrapper:gml_xml_file_to_layer: unexpected " + str(e),
                                     level=Qgis.Critical)
            return None

    @staticmethod
    def load_shape(p_url: str, p_crs: str = "", p_layer_name: str = "", set_crs: bool = True) -> 'QgsVectorLayer':
        """
        load a shape file and set crs and name

        :param set_crs: should set crs automatically
        :param p_url: path to the shape or web url
        :param p_layer_name: name of the layer
        :param p_crs: coordinate reference system
        :return: layer with loaded data from url, otherwise None
        """
        if not os.path.exists(p_url):
            return None
        try:
            vl1 = QgsVectorLayer(p_url, p_layer_name, "ogr")
            if set_crs and p_crs:
                vl1.setCrs(QgsCoordinateReferenceSystem(p_crs))

            if len(p_layer_name) > 0:
                vl1.setName(p_layer_name)
            return vl1
        except Exception as e:
            return None

    @staticmethod
    def selective_buffer(p_input_layer: any, p_crs, p_type_list: list, p_buffer_list: list, p_attribute,
                         p_layer_name: str = "", p_out_put_type: str = "TEMPORARY_OUTPUT"):
        """
        see buffer

        :param p_input_layer:  input layer
        :param p_crs: coordinate reference system
        :param p_type_list: type list
        :param p_buffer_list:  buffer list
        :param p_attribute: attribute field
        :param p_layer_name: name of the layer
        :param p_out_put_type: output format of the result layer, standard 'TEMPORARY_OUTPUT'
        :return: new layer
        """

        if p_input_layer is None:
            return None

        layer_list = []
        for i in range(len(p_type_list)):
            result = processing.run("native:extractbyattribute",
                                    {'INPUT': p_input_layer, 'VALUE': p_type_list[i], 'OPERATOR': 0,
                                     'FIELD': p_attribute,
                                     'OUTPUT': p_out_put_type}, context=QGisProjectWrapper.get_project_context())

            layer_list.append(QGisApiWrapper.buffer_layer(p_input_layer=result['OUTPUT'], p_buffer=p_buffer_list[i]))
            if layer_list[i] is None:
                QgsMessageLog.logMessage(message="QGisApiWrapper:selective_buffer:error: buffer is non ",
                                         level=Qgis.Info)
                return None
        result = processing.run("native:mergevectorlayers",
                                {'CRS': p_crs, 'LAYERS': layer_list,
                                 'OUTPUT': 'TEMPORARY_OUTPUT'},
                                context=QGisProjectWrapper.get_project_context())
        vl2 = result['OUTPUT']
        # vl2.setName(p_layer_name + " (buffered)")
        return vl2

    @staticmethod
    def try_set_attribute(p_feature: 'QgsFeature', p_res, p_key1, p_key2):
        """
        sets an attribute and its value of the given feature

        @param p_feature: given feature
        @param p_res: resource dict
        @param p_key1: name of the feature field
        @param p_key2: name of the field inside the dict
        @return: feature
        """
        try:
            p_feature.setAttribute(p_key1, QVariant(p_res[p_key2]["value"]))
        except Exception as e:
            QgsMessageLog.logMessage(message="QGisApiWrapper:try_set_attribute:error: unexpected " + str(e),
                                     level=Qgis.Critical)
            return None
        return p_feature

    @staticmethod
    def try_set_attribute_value(p_feature: 'QgsFeature', p_attr_name, p_attr_new_value) -> bool:
        """
        @param p_feature: given feature
        @param p_attr_name: name of the feature field
        @param p_attr_new_value: new value
        @return: feature with set attribute value
        """
        try:
            p_feature.setAttribute(p_attr_name, QVariant(p_attr_new_value))
        except Exception as e:
            QgsMessageLog.logMessage(message="QGisApiWrapper:try_set_attribute_value:error: unexpected " + str(e),
                                     level=Qgis.Critical)
            print(e)
            return False
        return True

    @staticmethod
    def extract_by_expression(p_input_layer: any, p_expression: str, p_layer_name: str = "",
                              p_out_put_type: str = "memory"):
        """
        extract layer from input layer with given expression filtering

        @param p_input_layer: layer to extract
        @param p_expression: expression
        @param p_layer_name: name of the new layer
        @param p_out_put_type: output format of the result layer, standard 'memory'
        @return: extracted features
        """
        if p_input_layer is None:
            return None
        try:
            new_layer = processing.run("qgis:extractbyexpression",
                                       {'INPUT': p_input_layer,
                                        'EXPRESSION': p_expression,
                                        'OUTPUT': p_out_put_type})['OUTPUT']
            if len(p_layer_name) > 0:
                new_layer.setName(p_layer_name)
        except Exception as e:
            QgsMessageLog.logMessage(message="QGisApiWrapper:extract_by_expression:error: " + str(e),
                                     level=Qgis.Critical)
            print(e)
            return None
        return new_layer
