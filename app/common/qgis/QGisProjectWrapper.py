from qgis.core import QgsProcessingContext, QgsProject, QgsFeatureRequest, Qgis, QgsMessageLog


class QGisProjectWrapper:
    @staticmethod
    def get_project_context():
        def_context = QgsProcessingContext()
        def_context.setProject(QgsProject.instance())
        def_context.setInvalidGeometryCheck(QgsFeatureRequest.GeometrySkipInvalid)
        return def_context

    @staticmethod
    def get_project_crs(p_interface=None) -> str:
        """returns the project crs in epsg format like EPSG:3045"""
        if p_interface is None:
            return ""
        try:
            return QgsProject.instance().crs().authid()
        except Exception as e:
            print(str(e))
        return ""

    @staticmethod
    def get_layer_by_id(p_id: int, p_interface=None):
        """
        returns the current layer

        :param p_interface: qgis interface
        :param p_id: id of the layer

        :return: layer if exists, otherwise None
        """
        if p_interface is None:
            return None

        if p_id == -1:
            return None
        try:
            layer = p_interface.mapCanvas().layers()[p_id]
        except Exception as e:
            return None
        return layer

    @staticmethod
    def get_current_available_layer_names(p_interface=None) -> list:
        """
        :return: a list with all available layer names
        """
        if p_interface is None:
            return []
        return [layer.name() for layer in p_interface.mapCanvas().layers()]

    @staticmethod
    def get_current_available_layer(p_interface=None) -> list:
        """
        :return: a list with all available layers
        """
        if p_interface is None:
            return []
        return [layer for layer in p_interface.mapCanvas().layers()]

    @staticmethod
    def delete_layers_by_name(p_name: str) -> bool:
        """
        deletes all layers which have the given name

        @param p_name: name of the layer/s to delete
        @return: true if the layers are deleted otherwise false
        """
        found_layers = QgsProject.instance().mapLayersByName(p_name)
        if found_layers is not None and isinstance(found_layers, list):
            for layer in found_layers:
                try:
                    QgsProject.instance().removeMapLayer(layer)
                except Exception as e:
                    print("Can not delete layer with name: ", layer)
                    return False
            return True
        return False

    @staticmethod
    def show_message(caption: str = "caption", txt: str = "text", lvl: int = Qgis.Info, timer: bool = False,
                     duration: int = 3, qgis_interface=None):
        """
        shows a message in the 'horizontalLayout_message' - layout
        :param caption: message caption
        :param txt: message text
        :param lvl: message level
        :param timer: timer activate or not
        :param duration: how long the message stays
        :param qgis_interface: interface to qgis
        :return: none
        """
        msg_lvl_list = [Qgis.Critical, Qgis.Info, Qgis.Warning, Qgis.Success]
        if lvl not in msg_lvl_list:
            QgsMessageLog.logMessage(message="show_message:The given log level does not exist",
                                     level=Qgis.Critical
                                     )
        else:
            if timer:
                qgis_interface.messageBar().pushMessage(caption, txt, lvl, duration)
            else:
                qgis_interface.messageBar().pushMessage(caption, txt, lvl)

    @staticmethod
    def add_layer_to_map(p_layer: any) -> bool:
        """
        adds the layer to map
        deletes all layer where the name is the same as 'layer'

        :param p_layer: layer to add
        :return: true if the layer was added otherwise false
        """
        if p_layer is None:
            return False
        try:
            QGisProjectWrapper.delete_layers_by_name(p_layer.name())
            QgsProject.instance().addMapLayer(p_layer)
            return True
        except Exception as e:
            QgsMessageLog.logMessage(message="QGisInterface:add_layer_to_map:error: " + p_layer.name() + str(e),
                                     level=Qgis.Critical)
        return False
