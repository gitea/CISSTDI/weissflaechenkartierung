from typing import List

from PyQt5.QtGui import QColor

try:
    from qgis.core import QgsProcessingContext, QgsProject, QgsFeatureRequest, Qgis, QgsMessageLog, \
        QgsLayerTreeGroup, QgsVectorLayer, QgsFeature, QgsSymbol, QgsFillSymbol, \
        QgsSimpleFillSymbolLayer, QgsPointXY, QgsSingleSymbolRenderer, QgsWkbTypes
except ImportError as e:
    pass


class QGisProjectWrapper:
    @staticmethod
    def get_project_context():
        def_context = QgsProcessingContext()
        def_context.setProject(QgsProject.instance())
        def_context.setInvalidGeometryCheck(QgsFeatureRequest.GeometrySkipInvalid)
        return def_context

    @staticmethod
    def get_project_crs(p_interface=None) -> str:
        """returns the project crs in epsg format like EPSG:3045"""
        if p_interface is None:
            return ""
        try:
            return QgsProject.instance().crs().authid()
        except Exception as e:
            print(str(e))
        return ""

    @staticmethod
    def get_layer_by_id(p_id: int, p_interface=None):
        """
        returns the current layer

        :param p_interface: qgis interface
        :param p_id: id of the layer

        :return: layer if exists, otherwise None
        """
        if p_interface is None:
            return None

        if p_id == -1:
            return None
        try:
            layer = p_interface.mapCanvas().layers()[p_id]
        except Exception as e:
            return None
        return layer

    @staticmethod
    def get_layer_by_name(p_name: str):
        """
        returns the layer with the given name
        :param p_name: the name of the wanted layer
        :return: found layer or none if not found
        """
        found_layers = QgsProject.instance().mapLayersByName(p_name)
        if found_layers:
            return found_layers[0]

    @staticmethod
    def get_current_available_layer_names(p_interface=None) -> list:
        """
        :return: a list with all available layer names
        """
        if p_interface is None:
            return []
        return [layer.name() for layer in p_interface.mapCanvas().layers()]

    @staticmethod
    def get_current_available_layer(p_interface=None) -> list:
        """
        :return: a list with all available layers
        """
        if p_interface is None:
            return []
        return [layer for layer in p_interface.mapCanvas().layers()]

    @staticmethod
    def delete_layers_by_name(p_name: str) -> bool:
        """
        deletes all layers which have the given name

        @param p_name: name of the layer/s to delete
        @return: true if the layers are deleted otherwise false
        """
        found_layers = QgsProject.instance().mapLayersByName(p_name)
        if found_layers is not None and isinstance(found_layers, list):
            for layer in found_layers:
                try:
                    QgsProject.instance().removeMapLayer(layer)
                except Exception as e:
                    print("Can not delete layer with name: ", layer)
                    return False
            return True
        return False

    @staticmethod
    def show_message(caption: str = "caption", txt: str = "text", lvl: int = 1, timer: bool = False,
                     duration: int = 3, qgis_interface=None):
        """
        shows a message in the 'horizontalLayout_message' - layout
        :param caption: message caption
        :param txt: message text
        :param lvl: message level
        :param timer: timer activate or not
        :param duration: how long the message stays
        :param qgis_interface: interface to qgis
        :return: none
        """
        msg_lvl_list = [Qgis.Critical, Qgis.Info, Qgis.Warning, Qgis.Success]
        if lvl not in msg_lvl_list:
            QgsMessageLog.logMessage(message="show_message:The given log level does not exist",
                                     level=Qgis.Critical
                                     )
        else:
            if timer:
                qgis_interface.messageBar().pushMessage(caption, txt, lvl, duration)
            else:
                qgis_interface.messageBar().pushMessage(caption, txt, lvl)

    # ################################################################################################
    # layer legend manipulation

    @staticmethod
    def get_group_by_name(p_group_name: str) -> 'QgsLayerTreeGroup':
        """
        Search the layer tree for group with given name

        :param p_group_name:
        :return:
        """
        root = QgsProject.instance().layerTreeRoot()
        group = None
        for child in root.children():
            if isinstance(child, QgsLayerTreeGroup) and child.name() == p_group_name:
                group = child
                break
        return group

    @staticmethod
    def remove_group(p_group_name: str) -> bool:
        """
        Remove the group matching the given name

        :param p_group_name: Name of the group we want remove
        :return: if successful true, otherwise false
        """
        if not len(p_group_name):
            return False
        group: 'QgsLayerTreeGroup' = QGisProjectWrapper.get_group_by_name(p_group_name=p_group_name)
        if group is None:
            return True
        QgsProject.instance().layerTreeRoot().removeChildNode(group)
        group: 'QgsLayerTreeGroup' = QGisProjectWrapper.get_group_by_name(p_group_name=p_group_name)
        return group is None

    @staticmethod
    def add_layers_to_group(p_group_name: str, p_layer_list: List['QgsVectorLayer']) -> bool:
        """
        Adds a layer group to the QGIS project and then adds layers to this group.

        :param p_group_name: name of the new or existing group
        :param p_layer_list: list with layers to add
        :return: None
        """
        if not len(p_group_name) or not len(p_layer_list):
            return False
        root = QgsProject.instance().layerTreeRoot()
        group = root.findGroup(p_group_name)
        # if group does not exist, create one
        if group is None:
            group = root.addGroup(p_group_name)
        # add layer to group
        for layer in p_layer_list:
            if layer is None:
                continue
            QGisProjectWrapper.add_layer_to_map(p_layer=layer)
            tree_layer = root.findLayer(layer.id())
            tree_layer_clone = tree_layer.clone()
            parent = tree_layer.parent()
            group.insertChildNode(0, tree_layer_clone)
            parent.removeChildNode(tree_layer)
        return True

    @staticmethod
    def add_layers_to_map(p_layers: List['QgsVectorLayer']) -> bool:
        """
        adds the layer to map
        deletes all layer where the name is the same as 'layer'

        :param p_layers: layers to add
        :return: true if the layer was added otherwise false
        """
        for layer in p_layers:
            if not QGisProjectWrapper.add_layer_to_map(layer):
                return False
        return True

    @staticmethod
    def add_layer_to_map(p_layer: 'QgsVectorLayer') -> bool:
        """
        adds the layer to map
        deletes all layer where the name is the same as 'layer'

        :param p_layer: layer to add
        :return: true if the layer was added otherwise false
        """
        if p_layer is None:
            return False
        if not QGisProjectWrapper.delete_layers_by_name(p_layer.name()):
            return False
        QgsProject.instance().addMapLayer(p_layer)
        return True

    # ################################################################################################
    #

    @staticmethod
    def calc_layer_area(p_layer: 'QgsVectorLayer') -> float:
        """
        Calculate the total area of a QgsVectorLayer.

        :param p_layer: QgsVectorLayer for which the total area needs to be calculated.
        :return: Total area of the layer in square meters.
        """
        total_area = 0.0
        # Check if the layer is valid
        if p_layer is not None and p_layer.isValid():
            # Iterate over all features in the layer
            for feature in p_layer.getFeatures():
                # Check if the feature has a valid geometry
                if feature.geometry() is not None and feature.geometry().isGeosValid():
                    # Calculate the area of the feature and add it to the total area
                    total_area += feature.geometry().area()
        return total_area

    @staticmethod
    def change_feature_polygon_outline_color(p_feature, p_new_color: QColor) -> bool:
        """
        Changes the outline colour of a polygon symbol of a QgsPolygon feature

        :param p_feature: the feature to be manipulated
        :param p_new_color: the new colour of the symbol outline
        :return: None
        """
        # Check whether it is a QgsPolygon feature
        if p_feature.geometry().wkbType() in [QgsWkbTypes.Polygon, QgsWkbTypes.MultiPolygon]:
            # Create a new fill symbol with the desired contour colour
            fill_symbol = QgsFillSymbol.createSimple({'color': p_new_color})

            renderer = QgsSingleSymbolRenderer(fill_symbol)

            # Wenden Sie den Renderer auf das Feature an
            p_feature.setRenderer(renderer)

            # Change the symbol of the feature
            p_feature.setSymbol(fill_symbol)
            return True
        return False
