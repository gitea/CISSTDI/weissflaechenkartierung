# coding:utf-8
import base64
import hashlib
import os
import random
import re
import sys
import zipfile
from platform import platform

from PyQt5.QtGui import QDesktopServices
from PyQt5.QtCore import QProcess, QFileInfo, QDir, QUrl
from PyQt5.QtWidgets import QFileDialog


def adjust_name(name: str):
    """ adjust file name

    Returns
    -------
    name: str
        file name after adjusting
    """
    name = re.sub(r'[\\/:*?"<>|\r\n]+', "_", name).strip()
    return name

def generate_static_key(p_key: str):
    return hashlib.sha256(p_key.encode('utf-8')).digest()

def encrypt_password(password, key):
    hmac_key = hashlib.sha256(key.encode('utf-8')).digest()
    encrypted_password = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), hmac_key, 100000)
    return base64.b64encode(encrypted_password)

def get_open_file_name(p_caption: str, p_filter: str, p_directory: str = "", p_file: str = "", p_parent=None) -> str:
    """
    Get open file name with given parameters

    :param p_caption: Window caption
    :param p_filter: File filter
    :param p_directory: Start directory
    :param p_file: File which contains the Start directory
    :param p_parent: Window parent
    :return: To native seperator converted selected path
    """
    if len(p_file) and not len(p_directory):
        d, f = split_file_path(p_file=QDir.toNativeSeparators(p_file))
    else:
        d = p_directory

    return QDir.toNativeSeparators(QFileDialog.getOpenFileName(p_parent, p_caption, d, p_filter)[0])


def split_directory(p_directory: str) -> tuple:
    """splits the directory"""
    m = p_directory.split(os.sep)
    if not len(m) > 1:
        m = p_directory.split(QDir.separator())
    return len(m), m


def split_file_path(p_file: str, p_check_exist: bool = True) -> tuple:
    """
    :param p_file: file including path
    :param p_check_exist: check if the file does exist
    :return: file dir, file name
    """
    if p_check_exist:
        if not os.path.exists(p_file):
            return "", ""
    drive, path_and_file = os.path.splitdrive(p_file)
    f_dir, f_name = os.path.split(path_and_file)
    return drive + f_dir, f_name


def getWindowsVersion():
    if "Windows-7" in platform():
        return 7

    build = sys.getwindowsversion().build
    version = 10 if build < 22000 else 11
    return version


def unzip_file(file_path: str, destination: str) -> bool:
    """
    unzips a zip file
    returns true if successful otherwise false
    """
    if len(file_path) > 3 and not file_path.endswith('zip'):
        return False
    with zipfile.ZipFile(file_path, 'r') as zip_ref:
        zip_ref.extractall(destination)
    return True


def show_in_folder(path: str) -> None:
    """
    Show file in file explorer
    :param path: given path
    :return: None
    """
    if not path or path.lower() == 'http':
        return

    if path.startswith('http'):
        QDesktopServices.openUrl(QUrl(path))
        return

    info = QFileInfo(path)  # type:QFileInfo
    if sys.platform == "win32":
        args = [QDir.toNativeSeparators(path)]
        if not info.isDir():
            args.insert(0, '/select,')

        QProcess.startDetached('explorer', args)
    elif sys.platform == "darwin":
        args = [
            "-e", 'tell application "Finder"', "-e", "activate",
            "-e", f'select POSIX file "{path}"', "-e", "end tell",
            "-e", "return"
        ]
        QProcess.execute("/usr/bin/osascript", args)
    else:
        url = QUrl.fromLocalFile(path if info.isDir() else info.path())
        QDesktopServices.openUrl(url)

def open_email_launcher() -> None:
    """
    open std email program
    :return: None
    """
    if sys.platform == "win32":
        QDesktopServices.openUrl(QUrl("mailto:team@ciss.de"))
    elif sys.platform == "darwin":
        args = [
            "--utf-8"
            "team@ciss.de"
        ]
        QProcess.execute("xdg-email", args)

def generate_light_colors(num_colors, min_brightness=384):
    colors = []
    for _ in range(num_colors):
        while True:
            color = "#{:06x}".format(random.randint(0, 0xFFFFFF))
            brightness = sum(int(color[i:i+2], 16) for i in (1, 3, 5))
            if brightness >= min_brightness:
                colors.append(color)
                break
    return colors

