# coding: utf-8
import re
from typing import List

from PyQt5.QtWidgets import QWidget
from math import isclose, sqrt

def error_gen(actual, rounded):
    divisor = sqrt(1.0 if actual < 1.0 else actual)
    return abs(rounded - actual) ** 2 / divisor

def round_to_100(percents: List[float]) -> List[int]:
    """
    Largest Remainder Method
    Round given floats
    https://revs.runtime-revolution.com/getting-100-with-rounded-percentages-273ffa70252b
    :param percents:
    :return:
    """
    if not isclose(sum(percents), 100):
        raise ValueError("Sum of input is over 100!")
    n = len(percents)
    rounded = [int(x) for x in percents]
    up_count = 100 - sum(rounded)
    errors = [(error_gen(percents[i], rounded[i] + 1) - error_gen(percents[i], rounded[i]), i) for i in range(n)]
    rank = sorted(errors)
    for i in range(up_count):
        rounded[rank[i][1]] += 1
    return rounded

def text_width(p_obj: QWidget, p_txt: str) -> int:
    """ calculate the width of the given text """
    return p_obj.fontMetrics().width(p_txt)


def auto_wrap(text: str, maxCharactersNum: int) -> tuple:
    """ auto word wrap according to the length of the string

    Parameters
    ----------
    maxCharactersNum: int
        the length of text (convert to letter size, e.g. `.length = 2`)

    Returns
    -------
    newText: str
        text after auto word wrap process

    isWordWrap: bool
        whether a line break occurs in the text
    """
    isWordWrap = True
    text_list = list(text)
    alpha_num = 0
    not_alpha_num = 0
    blank_index = 0
    for index, i in enumerate(text):
        Match = re.match(r'[0-9A-Za-z:\+\-\{\}\d\(\)\*\.\s]', i)
        if Match:
            alpha_num += 1
            if Match.group() == ' ':
                # record previous blank position
                blank_index = index

            if alpha_num + 2 * not_alpha_num == maxCharactersNum:
                try:
                    if text[index + 1] == ' ':
                        # insert \n
                        text_list.insert(index + 1, '\n')
                        # pop blank
                        text_list.pop(index + 2)
                    else:
                        text_list.insert(blank_index, '\n')
                        text_list.pop(blank_index + 1)
                    break
                except IndexError:
                    pass

        else:
            not_alpha_num += 1
            if alpha_num + 2 * not_alpha_num == maxCharactersNum:
                text_list.insert(index + 1, '\n')
                try:
                    if text_list[index + 2] == ' ':
                        text_list.pop(index + 2)
                    break
                except Exception as e:
                    pass
            elif alpha_num + 2 * not_alpha_num > maxCharactersNum:
                if text_list[index - 1] == ' ':
                    text_list.insert(index - 1, '\n')
                    text_list.pop(index)
                else:
                    text_list.insert(index, '\n')
                break
    else:
        isWordWrap = False

    newText = ''.join(text_list)

    return newText, isWordWrap
