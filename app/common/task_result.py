from app.common.wfk.wfk_config import WfkConfig


class TaskResult:
    def __init__(self):
        self.result: bool = False
        self.txt: str = ""


class WfkTaskResult(TaskResult):
    def __init__(self):
        super().__init__()
        self.layer_to_analyse: list = []
        self.layer_to_protocol: list = []
        self.config: WfkConfig = None


class AnalysisTaskResult(TaskResult):
    def __init__(self):
        super().__init__()
        self.layer_to_protocol: list = []
