import locale
import re

from PyQt5.QtCore import QRectF, QRect
from PyQt5.QtGui import QPainter, QFont
from PyQt5.QtWidgets import QWidget


def is_url(p_str: str) -> bool:
    """checks if the given string contains an url"""
    regex = re.compile(
        r'^(?:http|ftp)s?://'  # http:// or https://
        r'(?:(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)|'  # domain...
        r'localhost|'  # localhost...
        r'\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})'  # ...or ip
        r'(?::\d+)?'  # optional port
        r'(?:/?|[/?]\S+)$', re.IGNORECASE)
    return re.match(regex, p_str) is not None


def tooltip(p_content: str, p_capital: str = "") -> str:
    """translate python string to html"""
    if len(p_capital) > 0:
        return f"<b>{p_capital}</b> <br>{p_content}".replace('\n', '<br>')
    return p_content.replace('\n', '<br>')


def adapt_font_size(painter: QPainter, flags: int, rect: QRectF, text: str, font: QFont) -> None:
    """
    calc font size to fit inside the given rect
    :param painter: painter object
    :param flags: flags for metrics
    :param rect: given widget rect
    :param text: text to draw
    :param font:
    :return: None
    """
    font_bound_rect: QRect = painter.fontMetrics().boundingRect(rect.toRect(), flags, text)
    while rect.width() < font_bound_rect.width() or rect.height() < font_bound_rect.height():
        font.setPointSizeF(font.pointSizeF() * 0.95)
        painter.setFont(font)
        font_bound_rect = painter.fontMetrics().boundingRect(rect.toRect(), flags, text)

def adapt_widget_font_size(widget: QWidget, flags: int, rect: QRect, text: str, font: QFont) -> float:
    font_bound_rect: QRect = widget.fontMetrics().boundingRect(rect, flags, text)
    size = .0
    while rect.width() < font_bound_rect.width() or rect.height() < font_bound_rect.height():
        font.setPointSizeF(font.pointSizeF() * 0.95)
        size = font.pointSizeF()
        widget.setFont(font)
        font_bound_rect = widget.fontMetrics().boundingRect(rect, flags, text)
    return size


def format_float(number: float, local_name: str ="german") -> float:
    """
    format number with local standard
    :param local_name: local name
    :param number:
    :return: formatted number
    """
    locale.setlocale(locale.LC_NUMERIC, local_name)
    return float(locale.format_string("%.2f", number, grouping=True))

def replace_comma_or_split(p_str: str) -> str:
    """replace the last found comma and delete"""
    if len(p_str) > 0:
        new: str = p_str
        if p_str.count('.') > 1 and p_str.count(',') == 0:
            pp: int = p_str.find('.')
            new = p_str.replace('.', '')
            new = new[: pp] + '.' + new[pp:]
        elif p_str.count('.') >= 1 and p_str.count(',') >= 1:
            pp: int = p_str.find('.')
            fc: int = p_str.find(',')
            # if the first found comma is after point replace comma with nothing
            if pp < fc:
                new = p_str.replace(',', '').replace('.', '')
                new = new[: pp] + '.' + new[pp:]
        # replace comma with point
        return new.replace(',', '.')
    return ""
