from PyQt5.QtCore import QObject, pyqtSignal
from .singleton import Singleton
from .wfk.tasks.task_result import WfkTaskResult


class SignalBus(Singleton, QObject):
    """Signal bus of the app"""
    # main window
    switchToWfkInterfaceSig = pyqtSignal()
    switchToCissInterfaceSig = pyqtSignal()
    switchToSettingInterfaceSig = pyqtSignal()
    switchToEditConfigInterfaceSig = pyqtSignal()
    switchToAnalysisInterfaceSig = pyqtSignal()

    showInfoToastTip = pyqtSignal(str)
    showCompleteToastTip = pyqtSignal(str)
    showErrorToastTip = pyqtSignal(str)

    # restore main window
    showMainWindowSig = pyqtSignal()
    showMainWindowMaxSig = pyqtSignal()
    showMainWindowMinSig = pyqtSignal()

    closeMainWindowSig = pyqtSignal()

    # qgis layer
    refreshLayerSelection = pyqtSignal()
    layerSectionRefreshed = pyqtSignal(list)    # emit after the layer list got refreshed
    newLayerSelected = pyqtSignal(int)          # idx user selected a new layer inside wfk interface

    # wfk
    stopWfkSig = pyqtSignal()
    wfkResultSig = pyqtSignal(WfkTaskResult)
    wfkFinishedSig = pyqtSignal()

    # analysis
    stopAnalysisSig = pyqtSignal()
    analysisFinishedSig = pyqtSignal()

    # wfk config
    wfkConfigChangedSig = pyqtSignal(dict, object)
    newWfkConfigSelectedSig = pyqtSignal()          # new  wfk configuration
    editWfkConfigSig = pyqtSignal(dict)             # edit wfk configuration
    # wfk layer as dict, QVectorLayer
    updateWfkConfigSig = pyqtSignal(dict, object)       # new or edited config

    # module wizard
    newModuleSig = pyqtSignal(tuple)   # emit with a new object of type module and its index inside wfk_interface list


signalBus = SignalBus()
