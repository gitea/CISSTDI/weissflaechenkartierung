from enum import Enum


class DataType(Enum):
    SHAPE_FILE = 0
    REMOTE_FILE = 1


class WfkModuleStatisticData:
    def __init__(self):
        self.data_type: DataType = DataType.SHAPE_FILE
        self.time_elapsed: float = 0.0
        self.error_occurred: bool = False


class AnalysisStatisticData(WfkModuleStatisticData):
    def __init__(self):
        super().__init__()


class WfkConfigStatisticsData:
    """ class which holds information about wfk run """

    def __init__(self):
        self.modules: int = 0
        self.modules_processed: int = 0
        self.modules_failed: int = 0
        self.module_succeeded: int = 0
        self.time_elapsed: float = 0.0

    def error_occurred(self) -> bool:
        return True if self.modules_failed == 0 else False

    def add_module(self) -> None:
        """ increments the modules member """
        self.modules += 1

    def module_processed(self) -> None:
        """ increments the modules processed member """
        self.modules_processed += 1
