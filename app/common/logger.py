from abc import ABC, abstractmethod

from app.common.singleton import Singleton

try:
    from app.common.qgis.QGisProjectWrapper import QGisProjectWrapper
    from qgis.core import Qgis, QgsMessageLog
except ImportError:
    pass


class LogClass(ABC):
    @staticmethod
    @abstractmethod
    def log(message: str, level=None):
        """log to any console"""
        print(message)


class PythonConsole(LogClass):
    @staticmethod
    def log(message: str, level=None):
        """log to python console"""
        print(message)


class QGisConsole(LogClass):
    @staticmethod
    def log(message: str, level=None):
        """log to python console"""
        if level is None:
            level = Qgis.Info
        QgsMessageLog.logMessage(message=message, level=level)


class Logger(Singleton):
    """custom Qgis logger"""

    def __init__(self):
        self._interface = None  # qgis interface
        self._timeout: int = 3

    @property
    def interface(self):
        return self._interface

    @interface.setter
    def interface(self, value):
        if value is not None:
            self._interface = value

    def info(self, p_body: str):
        """logs to qgis interface"""
        p_body = f"INFO - {p_body}"
        try:
            self._log(p_body=p_body, p_level=Qgis.Info)
        except Exception as e:
            PythonConsole.log(message=p_body)

    def debug(self, p_body: str):
        """logs to qgis interface"""
        p_body = f"DEBUG - {p_body}"
        try:
            self._log(p_body=p_body, p_level=Qgis.Info)
        except Exception as e:
            PythonConsole.log(message=p_body)

    def warning(self, p_body: str):
        """logs to qgis interface"""
        p_body = f"WARNING - {p_body}"
        try:
            self._log(p_body=p_body, p_level=Qgis.Warning)
        except Exception as e:
            PythonConsole.log(message=p_body)

    def critical(self, p_body: str):
        """logs to qgis interface"""
        p_body = f"CRITICAL - {p_body}"
        try:
            self._log(p_body=p_body, p_level=Qgis.Critical)
        except Exception as e:
            PythonConsole.log(message=p_body)

    def error(self, p_body: str):
        """"""
        p_body = f"ERROR - {p_body}"
        try:
            self.critical(p_body=p_body)
        except Exception as e:
            PythonConsole.log(message=p_body)

    def success(self, p_body: str):
        """logs to qgis interface"""
        try:
            self._log(p_body=p_body, p_level=Qgis.Success)
        except Exception as e:
            PythonConsole.log(message=p_body)

    def _log(self, p_body: str, p_level=None):
        """log to a console"""
        try:
            QGisConsole.log(message=p_body, level=p_level)
        except Exception as e:
            PythonConsole.log(message=p_body, level=p_level)


logger = Logger()
