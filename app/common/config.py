import json

from app.common.singleton import Singleton


class Config(Singleton):
    """Config of app"""

    def __init__(self):
        self.__config = {
            "mode": "Light",
            "minimize_to_tray": False,
            "search_config_in_project_dir": False,
            "search_config_in_plugin_dir": False,
            "open_last_config": True,
            "open_cur_config_dir": True,
            "protocol_layer": True,
            "wfk_one_run": False,
            "run_tasks_with_errors": False,
            "get_crs_from_project": False,
            "diff_name_extension": "Differenz",
            "buff_name_extension": "Puffer",
            "reprocess_layer": False,
            "default_min_area": 1,
            "default_crs": "EPSG:32632",
            "configuration": [
                {
                    "name": "Konfiguration 1",
                    "root_dir": "",
                    "modules": [],
                    "analysis": {
                        "power_density": 0.0,
                        # "area_use": 0.0,
                        "class_count": 5,
                        "field_name": "distance",
                        "class": {}
                    }
                }
            ]
        }
        self.__active_border_color: str = "#CC3300"     # ciss color in web safe version
        self.__inactive_border_color: str = "#f2ccbf"
        self.__theme = "Light"
        self.__qgis_interface = None
        self.__path: str = ""
        self.__plugin_dir: str = ""
        self.__version: str = "3.1.4"

    def __setitem__(self, key, value):
        """set the value of the given key"""
        if key not in self.__config:
            raise KeyError(f'Config `{key}` is illegal')

        if self.__config[key] == value:
            return

        self.__config[key] = value

    def __getitem__(self, key):
        """return the value of the given key"""
        return self.__config[key]

    def set_default(self, key: str, default: object) -> object:
        """
        Insert key with a value of default if key is not in the dictionary.
        Return the value for key if key is in the dictionary, else default.
        """
        return self.__config.setdefault(key, default)

    def get(self, key, default: object = None) -> object:
        return self.__config.get(key, default)

    def read_config(self, p_path: str) -> bool:
        """read config"""
        try:
            with open(p_path, encoding="utf-8") as f:
                self.__config.update(json.load(f))
            config.path = p_path
            return True
        except Exception as e:
            print(e)
            return False

    def save(self, p_path: str = "") -> bool:
        """save config"""
        if len(p_path) == 0:
            p_path = "configuration.cfg"
        try:
            with open(p_path, "w", encoding="utf-8") as f:
                json.dump(self.__config, f, ensure_ascii=False, indent=4)
                return True
        except Exception as e:
            return False

    def reset_configuration(self) -> None:
        """deletes all configurations"""
        try:
            self["configuration"].clear()
        except KeyError:
            pass

    def update(self, p_config: dict):
        """update config"""
        for k, v in p_config.items():
            self[k] = v

    def delete_wfk_config(self, p_config_name: str) -> bool:
        """delete the given config name"""
        if len(p_config_name) == 0:
            return False

        for i in range(len(self.__config["configuration"])):
            if p_config_name == self.__config["configuration"][i]["name"]:
                try:
                    self.__config["configuration"].pop(i)
                    return True
                except KeyError:
                    pass
        return False

    def update_wfk_config(self, p_config: dict) -> None:
        """update wfk config"""
        if "name" not in p_config:
            return

        if not isinstance(self.__config["configuration"], list):
            return

        for i in range(len(self.__config["configuration"])):
            if p_config["name"] == self.__config["configuration"][i]["name"]:
                self.__config["configuration"][i] = p_config
                return
        # there is no wfk config with the given name, create a new one
        self.__config["configuration"].append(p_config)

    @property
    def theme(self) -> str:
        """get theme mode, can be `light` or `dark`"""
        return self.__theme.lower()

    @property
    def qgis_interface(self):
        """get the interface of qgis"""
        return self.__qgis_interface

    @qgis_interface.setter
    def qgis_interface(self, p_interface):
        """set the qgis interface"""
        self.__qgis_interface = p_interface

    @property
    def path(self) -> str:
        """return the config path"""
        return self.__path

    @path.setter
    def path(self, value: str):
        """set the config path"""
        self.__path = value

    @property
    def active_border_color(self) -> str:
        """return the active border color"""
        return self.__active_border_color

    @active_border_color.setter
    def active_border_color(self, value: str):
        """set the active border color"""
        if len(value) > 0:
            self.__active_border_color = value

    @property
    def inactive_border_color(self) -> str:
        """return the active border color"""
        return self.__inactive_border_color

    @inactive_border_color.setter
    def inactive_border_color(self, value: str):
        """set the active border color"""
        if len(value) > 0:
            self.__inactive_border_color = value

    @property
    def plugin_dir(self) -> str:
        """return the plugin dir path"""
        return self.__plugin_dir

    @plugin_dir.setter
    def plugin_dir(self, value: str):
        """set the plugin dir path"""
        self.__plugin_dir = value

    @property
    def version(self) -> str:
        """return version of program"""
        return self.__version


config = Config()
