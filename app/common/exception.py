
class ConfigRootDirError(Exception):
    pass


class ConfigModuleError(Exception):
    pass


class ModuleLayerError(Exception):
    pass


class ModuleConnectorError(Exception):
    pass


class ModuleDifferenceError(Exception):
    pass


class ModuleCategorizationError(Exception):
    pass


class ModuleLegendError(Exception):
    pass


class ConfigNameError(Exception):
    pass


class RemoteFileDownloadError(Exception):
    pass


class InternetConnectionError(Exception):
    pass


class ExtractZipError(Exception):
    pass


class BlackListError(Exception):
    pass


class DifferenceError(Exception):
    pass


class ByCentroidCategorizationError(Exception):
    pass


class USUDistanceCategorizationError(Exception):
    pass


class SPARQLCategorizationError(Exception):
    pass


class AdditionalInfoCategorizationError(Exception):
    pass
