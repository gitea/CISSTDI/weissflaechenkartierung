# -*- coding: utf-8 -*-
# noinspection PyPep8Naming
import os

plugin_dir = os.path.dirname(__file__)
plugin_name = u"Weissflaechenkartierung"
plugin_tooltip = u"Weissflaechenkartierung anzeigen"
plugin_icon_path = ':/images/main_window/ciss.png'


def classFactory(iface):  # pylint: disable=invalid-name
    """Load wfk class from file wfk.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    # import plugin creator class and returns it
    from .plugin import PluginInterface
    return PluginInterface(iface)
