from PyQt5 import QtWidgets
from PyQt5.QtCore import QSettings
from PyQt5.QtWidgets import QApplication, QMainWindow, QLabel

from app.common.resource_rc import *        # important
from app.common.config import config
from app.common.signal_bus import signalBus
from app.common.style_sheet import setStyleSheet
from app.common.logger import logger

from app.components.dialog_box.about_dialog import AboutDialog
from app.components.system_tray import SystemTrayIcon
from app.components.widgets.stacked_widget import PopUpAniStackedWidget
from app.components.widgets.tooltip import ToastTooltip

from app.view.creation_interface import CreationInterface
from app.view.analyse_interface import AnalyseInterface
from app.view.navigation_bar import NavigationInterface
from app.view.setting_interface import SettingInterface
from app.view.wfk_interface import WfkInterface

import matplotlib
# Ensure using PyQt5 backend
matplotlib.use('QT5Agg')


class MainWindow(QMainWindow):
    """Main window"""

    def __init__(self, parent=None, p_name: str = "", p_iface=None):
        super().__init__(parent=parent)

        """
        Does not work at the moment with qgis
        QFontDatabase.addApplicationFont('app/resource/font/FiraGO-Regular.ttf')
        QFontDatabase.addApplicationFont('app/resource/font/FiraGO-Medium.ttf')
        font = QFont("FiraGo")
        font.setPixelSize(13)
        font.setStyleHint(QFont.Monospace)
        QApplication.setFont(font)
        """

        if len(p_name) == 0:
            p_name = MainWindow.__class__.__name__
        self._settings = QSettings(p_name)
        config.path = self._settings.value("last_path", "")

        # set the qgis interface here
        config.qgis_interface = p_iface

        # set interface inside logger
        logger.interface = config.qgis_interface

        # list with tooltips
        self._resize_x_toasts: list = []

        self.create_widgets()
        self.init_widgets()

    def create_widgets(self) -> None:
        """
        creates widgets
        :return: None
        """
        self.main_widget = QtWidgets.QWidget(self)

        # stacked widget
        self.subStackWidget = PopUpAniStackedWidget(self.main_widget)
        self.subStackWidget.aniFinished.connect(self.adjust_widget_geometry)

        # navigation interface, left side of the window
        # create navigation interface
        self.navigationInterface = NavigationInterface(self.main_widget)

        self.label_logo = QLabel(parent=self.main_widget)
        self.label_logo.setStyleSheet("image: url(:images/main_window/wind_wheel_side_solar.svg);")

        self.label_title = QLabel(parent=self.main_widget)
        self.label_sub_title = QLabel(parent=self.main_widget)

        self.about_dialog = AboutDialog(self)

        self.init_window()

        # analysis interface
        self.analysisInterface = AnalyseInterface(self.subStackWidget)

        # wfk interface
        self.wfkInterface = WfkInterface(self.subStackWidget)

        # configuration creation interface
        self.creationInterface = CreationInterface(self.subStackWidget)

        # settings interface
        self.settingInterface = SettingInterface(self.subStackWidget)

        # create system tray icon
        self.systemTrayIcon = SystemTrayIcon(self)

    def init_widgets(self) -> None:
        """initialize widgets"""
        self.installEventFilter(self)

        # sub stack widget
        self.subStackWidget.addWidget(self.wfkInterface, 0, 70)
        self.subStackWidget.addWidget(self.analysisInterface, 0, 120)
        self.subStackWidget.addWidget(self.settingInterface, 0, 120)
        self.subStackWidget.addWidget(self.creationInterface, 0, 120)

        self.label_logo.setFixedSize(131, 131)
        self.label_title.setFixedSize(95, 48)
        self.label_title.setText(self.tr("WFK"))
        self.label_sub_title.setFixedSize(240, 40)
        self.label_sub_title.setWordWrap(True)
        self.label_sub_title.setText(self.tr(
            "<b>Weißflächenkartierung</b><br>Potentiale zukünftiger Energieprojekte"
        ))
        self.label_sub_title.setStyleSheet("color:#191C2C;font-size: 14px;font-family: 'Segoe UI';")


        # resize all widgets
        self.adjust_widget_geometry()
        self.set_qss()

        # connect to signal bus
        self.connect_signal_to_slot()

        # we are done here do everything we need after these now
        self.on_init_finished()

    def set_qss(self):
        """set style sheet"""
        self.setObjectName("mainWindow")
        self.label_title.setObjectName("titleLabel")
        self.setProperty('useAcrylic', False)
        self.subStackWidget.setObjectName("subStackWidget")
        setStyleSheet(self, 'main_window')

    def on_init_finished(self) -> None:
        """
        called if the window finished the initialisation

        :return: None
        """

        # show tray
        self.systemTrayIcon.show()

        # show main widget
        self.main_widget.show()

        # show navigation bar
        self.navigationInterface.show()

        # show sub stacked widget
        self.subStackWidget.show()

    def adjust_widget_geometry(self):
        """adjust the geometry of widgets"""
        self.main_widget.resize(self.width(), self.height())

        # resize stacked widget
        self.label_logo.move(21, 11)
        self.label_title.move(251, 49)
        self.label_sub_title.move(self.label_title.geometry().bottomRight().x() + 8, self.label_title.y() + 6)


        self.subStackWidget.move(
            self.navigationInterface.navigationBar.width(),
                                 self.label_logo.geometry().bottomLeft().y()
        )
        self.subStackWidget.resize(
            self.width() - self.navigationInterface.navigationBar.width(),
                                   self.height() - self.label_logo.geometry().bottomLeft().y()
        )

        # navigation interface
        self.navigationInterface.resize(
            self.navigationInterface.width(),
            self.height() - self.label_logo.geometry().bottomLeft().y()
        )
        self.navigationInterface.move(0,self.label_logo.geometry().bottomLeft().y())

        self.about_dialog.move(0, 0)
        self.about_dialog.resize(self.size())
        self.about_dialog.windowMask.resize(self.size())

        for widget in self._resize_x_toasts:
            try:
                if isinstance(widget, ToastTooltip):
                    if widget.isVisible():
                        widget.move(self.width() - widget.width() - 30, widget.y())
            except Exception as e:
                # widgets are deleted
                pass

    # ###################################################
    # window events

    def init_window(self) -> None:
        """initialize window"""
        # r = self.devicePixelRatioF()
        desktop = QApplication.desktop().availableGeometry()
        w, h = desktop.width(), desktop.height()
        # self.resize(int(w*0.2), 600)
        # self.resize(w*1240/1920, h*970/1080)

        self.setMinimumSize(790, 600)

        self.setWindowTitle(self.tr("Weißflächenkartierung"))

        self.move(w//2 - self.width()//2, h//2 - self.height()//2)
        self.show()
        QApplication.processEvents()

    def resizeEvent(self, e):
        """resize event"""
        super().resizeEvent(e)
        self.adjust_widget_geometry()

    def on_show_main_window(self):
        """show main window"""
        self.show()
        self.setFocus()
        if self.isMinimized():
            self.showNormal()

    def closeEvent(self, e) -> None:
        """
        called if the window should close
        """
        self.on_exit()
        super().closeEvent(e)

    def on_exit(self):
        """exit main window"""
        # set the last used config file
        if len(config.path) > 0:
            self._settings.setValue("last_path", config.path)
            config.save(config.path)
        else:
            config.save()

        if config["minimize_to_tray"]:
            logger.debug(self.tr("Minimize main window"))
            self.hide()
        else:
            logger.debug(self.tr("Close main window"))
            # save and hide or close connections
            self.systemTrayIcon.hide()
            self.close()

    def on_navigation_display_mode_changed(self) -> None:
        """navigation interface display mode changed slot"""
        # resize the whole interface
        self.adjust_widget_geometry()

    # ##################################################

    def _on_show_error_toast_tip(self, p_content: str) -> None:
        """shows an info toast tip"""
        self.show_information(p_title=self.tr("Fehler"), p_content=p_content, p_icon="close_normal", p_interval=4000)

    def _on_show_complete_toast_tip(self, p_content: str) -> None:
        """shows an info toast tip"""
        self.show_information(p_title=self.tr("Fertig"), p_content=p_content, p_icon="completed")

    def _on_show_info_toast_tip(self, p_content: str) -> None:
        """shows an info toast tip"""
        self.show_information(p_title=self.tr("Info"), p_content=p_content, p_icon="info", p_interval=3500)

    def show_information(self, p_title: str, p_content: str, p_icon: str = "", p_interval: int = 2500):
        """shows a tool tip"""
        toast = ToastTooltip(p_title, p_content, p_icon, self.window(), p_interval)
        toast.show()
        self._resize_x_toasts.append(toast)

    # ##################################################

    def on_show_about_info(self):
        """show the information dialog"""
        self.about_dialog.show()

    # ##################################################
    # switch to page

    def on_switch_to_config_creation_interface(self) -> None:
        """switch to config creation interface"""
        self.creationInterface.update_window()
        self.subStackWidget.setCurrentWidget(self.creationInterface)

    def on_switch_to_analysis_interface(self) -> None:
        """switch to config creation interface"""
        self.subStackWidget.setCurrentWidget(self.analysisInterface)

    def switch_to_settings_interface(self) -> None:
        """
        switch to the settings interface

        :return: None
        """
        self.subStackWidget.setCurrentWidget(self.settingInterface)

    def switch_to_wfk_interface(self) -> None:
        """
        switch tio the wfk interface

        :return: None
        """
        self.subStackWidget.setCurrentWidget(self.wfkInterface)

    # ##################################################
    # connect signal bus

    def connect_signal_to_slot(self) -> None:
        """
        connect every signal here

        :return: None
        """
        # main window
        signalBus.showMainWindowSig.connect(self.on_show_main_window)
        signalBus.showMainWindowMinSig.connect(self.showMinimized)
        signalBus.showMainWindowMaxSig.connect(self.showMaximized)
        signalBus.closeMainWindowSig.connect(self.on_exit)

        signalBus.showInfoToastTip.connect(self._on_show_info_toast_tip)
        signalBus.showCompleteToastTip.connect(self._on_show_complete_toast_tip)
        signalBus.showErrorToastTip.connect(self._on_show_error_toast_tip)

        # navigation interface
        signalBus.switchToSettingInterfaceSig.connect(self.switch_to_settings_interface)
        signalBus.switchToWfkInterfaceSig.connect(self.switch_to_wfk_interface)
        signalBus.switchToCissInterfaceSig.connect(self.on_show_about_info)
        signalBus.switchToEditConfigInterfaceSig.connect(self.on_switch_to_config_creation_interface)
        signalBus.switchToAnalysisInterfaceSig.connect(self.on_switch_to_analysis_interface)


        # tray
        # system tray icon signal
        # qApp.aboutToQuit.connect(self.on_exit)

        # closes the whole program and qgis
        self.systemTrayIcon.exitSignal.connect(self.close)
